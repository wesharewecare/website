<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Accommodation;
use App\Entity\Association;
use App\Entity\Location;
use App\Enum\AccommodationStatusEnum;
use App\Tabulator\TabulatorModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class AccommodationRepository extends ServiceEntityRepository
{
    use ColumnFiltersTrait;
    use GeoDistanceTrait;

    public const TABLE_FILTERS_LIST = [
        Accommodation::class => 'accommodation',
        Location::class => 'location',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Accommodation::class);
    }

    public function getListAccommodations(TabulatorModel $tabulatorParams, Association $association = null): array
    {
        $qb = $this->getFilteredAccommodations($tabulatorParams->getSearch(), $association)
            ->setFirstResult(($tabulatorParams->getPage() - 1) * $tabulatorParams->getMaxResults())
            ->setMaxResults($tabulatorParams->getMaxResults())
        ;

        $qb = $this->addColumnSorter(
            $qb,
            $tabulatorParams->getSort(),
            self::TABLE_FILTERS_LIST
        );

        return $qb->getQuery()->getResult();
    }

    public function getNbAccommodations(): int
    {
        $qb = $this->getFilteredAccommodations([], null, 'accommodation.id');

        return \count($qb->getQuery()->getResult());
    }

    public function getNbAccommodationToOpen(Association $association = null): int
    {
        $filters = ['status' => ['type' => '=', 'value' => AccommodationStatusEnum::STATUS_AWAITING_APPROVAL]];
        $qb = $this->getFilteredAccommodations($filters, $association, 'accommodation.id');

        return \count($qb->getQuery()->getResult());
    }

    public function getNbListAccommodations(TabulatorModel $tabulatorParams, Association $association = null): int
    {
        return \count($this->getFilteredAccommodations($tabulatorParams->getSearch(), $association, 'accommodation.id')->getQuery()->getResult());
    }

    public function getByDistance(
        Location $location,
        int $radiusInKm,
        array $status = [],
        Association $association = null,
        string $countryCode = null
    ): array {
        $qb = $this->addDistanceFilter(
            $this->getFilteredAccommodations(),
            $location,
            $radiusInKm
        );

        if (!empty($status)) {
            $qb
                ->andWhere('accommodation.status in (:status)')
                ->setParameter('status', $status)
            ;
        }

        if ($association) {
            $qb
                ->leftJoin('senior.association', 'association')
                ->andWhere('association is null or association.id = :association')
                ->setParameter('association', $association->getId())
            ;
        }

        if ($countryCode) {
            $qb
                ->andWhere('location.countryCode = :countryCode')
                ->setParameter('countryCode', $countryCode)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getByStatus(array $status = []): array
    {
        $qb = $this->getFilteredAccommodations();

        if (!empty($status)) {
            $qb
                ->andWhere('accommodation.status in (:status)')
                ->setParameter('status', $status)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    private function getFilteredAccommodations(
        array $filters = [],
        Association $association = null,
        string $select = null
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('accommodation', 'accommodation.id')
            ->distinct()
            ->innerJoin('accommodation.senior', 'senior')
            ->innerJoin('senior.location', 'location')
            ->innerJoin('senior.profile', 'profile')
            ->where('profile.showHomesharing = :showHomesharing')
            ->setParameter('showHomesharing', true)
        ;

        if ($select) {
            $qb->select($select);
        }

        if ($association) {
            $qb
                ->leftJoin('senior.association', 'association')
                ->andWhere('association is null or association.id = :association')
                ->setParameter('association', $association->getId())
            ;

            $qb = $this->addDistanceFilter($qb, $association->getLocation(), $association->getRadiusRangeInKms());
        }

        return $this->addColumnFilters(
            $qb,
            $filters,
            self::TABLE_FILTERS_LIST
        );
    }
}
