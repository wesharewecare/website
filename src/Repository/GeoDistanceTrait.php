<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Location;
use Doctrine\ORM\QueryBuilder;

trait GeoDistanceTrait
{
    public function addDistanceFilter(QueryBuilder $qb, Location $location, $radiusInKm): QueryBuilder
    {
        $qb
            ->addSelect('GEO_DISTANCE(:lat, :long, location.latitude, location.longitude) as HIDDEN distance')
            ->setParameter('lat', $location->getLatitude())
            ->setParameter('long', $location->getLongitude())
            ->having('distance <= '.$radiusInKm)
            ->orderBy('distance', 'ASC')
        ;

        if ($location->getCountryCode()) {
            $qb
                ->andWhere('location.countryCode = :countryCode')
                ->setParameter('countryCode', $location->getCountryCode())
            ;
        }

        return $qb;
    }
}
