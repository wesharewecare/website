<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Accommodation;
use App\Entity\AccommodationDemand;
use App\Entity\Association;
use App\Entity\Cohabitation;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use App\Tabulator\TabulatorModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class CohabitationRepository extends ServiceEntityRepository
{
    use ColumnFiltersTrait;

    public const TABLE_FILTERS_LIST = [
        Cohabitation::class => 'cohabitation',
        AccommodationDemand::class => 'accommodationDemand',
        Accommodation::class => 'accommodation',
    ];

    private $grantedService;

    public function __construct(ManagerRegistry $registry, GrantedService $grantedService)
    {
        parent::__construct($registry, Cohabitation::class);
        $this->grantedService = $grantedService;
    }

    public function getCurrentCohabitationByUser(User $user): ?Cohabitation
    {
        $qb = $this->createQueryBuilder('cohabitation')
            ->innerJoin('cohabitation.accommodationDemand', 'demand')
            ->where('demand.status = :status')
            ->setParameter('status', AccommodationDemandStatusEnum::STATUS_ACCEPTED)
        ;

        switch ($this->grantedService->getRole($user)) {
            case UserRoleEnum::ROLE_YOUNG:
                $qb->innerJoin('demand.young', 'user');

                break;

            case UserRoleEnum::ROLE_SENIOR:
                $qb
                    ->innerJoin('demand.accommodation', 'accommodation')
                    ->innerJoin('accommodation.senior', 'user')
                ;

                break;

            default:
                return null;
        }

        return $qb->andWhere('user.id = :user')
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getWithEndDateReached(): array
    {
        $currentDate = new \DateTime('now');

        return $this->createQueryBuilder('cohabitation')
            ->innerJoin('cohabitation.accommodationDemand', 'accommodationDemand')
            ->where('accommodationDemand.status = :accepted')
            ->andWhere('cohabitation.endDate < :date')
            ->setParameter('accepted', AccommodationDemandStatusEnum::STATUS_ACCEPTED)
            ->setParameter('date', $currentDate->format('Y-m-d'))
            ->getQuery()->getResult()
        ;
    }

    public function getListCohabitations(TabulatorModel $tabulatorParams, Association $association = null): array
    {
        $qb = $this->getFilteredCohabitations($tabulatorParams->getSearch(), $association)
            ->setFirstResult(($tabulatorParams->getPage() - 1) * $tabulatorParams->getMaxResults())
            ->setMaxResults($tabulatorParams->getMaxResults())
        ;

        $qb = $this->addColumnSorter(
            $qb,
            $tabulatorParams->getSort(),
            self::TABLE_FILTERS_LIST
        );

        return $qb->getQuery()->getResult();
    }

    public function getNbListCohabitations(TabulatorModel $tabulatorParams, Association $association = null): int
    {
        return \count($this->getFilteredCohabitations($tabulatorParams->getSearch(), $association, 'cohabitation.id')->getQuery()->getResult());
    }

    private function getFilteredCohabitations(
        array $filters,
        Association $association = null,
        string $select = null
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('cohabitation', 'cohabitation.id')
            ->distinct()
            ->innerJoin('cohabitation.accommodationDemand', 'accommodationDemand')
            ->innerJoin('accommodationDemand.young', 'young')
            ->innerJoin('accommodationDemand.accommodation', 'accommodation')
            ->innerJoin('accommodation.senior', 'senior')
        ;

        if ($select) {
            $qb->select($select);
        }

        if ($association) {
            if (AssociationTypeEnum::TYPE_YOUNG_ONLY === $association->getType()) {
                $qb->innerJoin('young.association', 'association');
            } else {
                $qb->innerJoin('senior.association', 'association');
            }

            $qb
                ->andWhere('association.id = :association')
                ->setParameter('association', $association->getId())
            ;
        }

        return $this->addColumnFilters(
            $qb,
            $filters,
            self::TABLE_FILTERS_LIST
        );
    }
}
