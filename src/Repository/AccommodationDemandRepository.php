<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Accommodation;
use App\Entity\AccommodationDemand;
use App\Entity\Association;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AccommodationDemandRepository extends ServiceEntityRepository
{
    use GeoDistanceTrait;

    private $grantedService;

    public function __construct(ManagerRegistry $registry, GrantedService $grantedService)
    {
        parent::__construct($registry, AccommodationDemand::class);
        $this->grantedService = $grantedService;
    }

    public function getStatus(User $user, array $status): ?string
    {
        $res = null;

        $accommodationDemand = $this->createQueryBuilder('ad')
            ->innerJoin('ad.young', 'young')
            ->where('ad.status in (:status)')
            ->andWhere('young.id = :user')
            ->setParameter('status', $status)
            ->setParameter('user', $user->getId())
            ->orderBy("FIELD(ad.status, '".implode("', '", $status)."')")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if ($accommodationDemand) {
            $res = $accommodationDemand->getStatus();
        }

        return $res;
    }

    public function getExistingDemand(User $young, Accommodation $accommodation): ?AccommodationDemand
    {
        return $this->createQueryBuilder('ad')
            ->innerJoin('ad.young', 'young')
            ->innerJoin('ad.accommodation', 'accommodation')
            ->where('young.id = :young')
            ->andWhere('accommodation.id = :accommodation')
            ->setParameter('young', $young->getId())
            ->setParameter('accommodation', $accommodation->getId())
            ->getQuery()->getOneOrNullResult()
        ;
    }

    public function getByAccommodationAndStatus(Accommodation $accommodation, array $status): array
    {
        return $this->createQueryBuilder('ad')
            ->innerJoin('ad.accommodation', 'accommodation')
            ->where('ad.status in (:status)')
            ->andWhere('accommodation.id = :accommodation')
            ->setParameter('status', $status)
            ->setParameter('accommodation', $accommodation->getId())
            ->getQuery()
            ->getResult()
        ;
    }

    public function getNbDemandToRespond(Association $association = null): int
    {
        $qb = $this->createQueryBuilder('accommodation_demand')
            ->select('accommodation_demand.id')
            ->distinct()
            ->innerJoin('accommodation_demand.accommodation', 'accommodation')
            ->innerJoin('accommodation.senior', 'senior')
            ->innerJoin('senior.location', 'location')
            ->where('accommodation_demand.status = :pending')
            ->setParameter('pending', AccommodationDemandStatusEnum::STATUS_PENDING)
        ;

        if ($association) {
            $qb = $this->addDistanceFilter($qb, $association->getLocation(), $association->getRadiusRangeInKms());
        }

        return \count($qb->getQuery()->getResult());
    }

    public function getDemandsAwaitingApproval(User $user): array
    {
        $qb = $this->createQueryBuilder('demand')
            ->where('demand.status = :status')
            ->setParameter('status', AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL)
        ;

        switch ($this->grantedService->getRole($user)) {
            case UserRoleEnum::ROLE_YOUNG:
                $qb->innerJoin('demand.young', 'user');

                break;

            case UserRoleEnum::ROLE_SENIOR:
                $qb
                    ->innerJoin('demand.accommodation', 'accommodation')
                    ->innerJoin('accommodation.senior', 'user')
                ;

                break;

            default:
                return [];
        }

        return $qb->andWhere('user.id = :user')
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getResult()
        ;
    }

    public function getViewable(User $user, Association $association = null): array
    {
        $qb = $this->createQueryBuilder('demand');

        switch ($this->grantedService->getRole($user)) {
            case UserRoleEnum::ROLE_YOUNG:
                $qb->innerJoin('demand.young', 'user');

                break;

            case UserRoleEnum::ROLE_SENIOR:
                $qb
                    ->innerJoin('demand.accommodation', 'accommodation')
                    ->innerJoin('accommodation.senior', 'user')
                ;

                break;

            default:
                return [];
        }

        $qb
            ->andWhere('user.id = :user')
            ->setParameter('user', $user->getId())
        ;

        if ($association) {
            $qb
                ->innerJoin('demand.associations', 'association')
                ->andWhere('association.id = :association')
                ->setParameter('association', $association->getId())
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
