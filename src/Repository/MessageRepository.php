<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function getByThreads(array $threads, int $offset = 0, int $limit = 10): array
    {
        $qb = $this->createQueryBuilder('m')
            ->innerJoin('m.thread', 't')
            ->where('t.id in (:threads)')
            ->setParameter('threads', $threads)
            ->orderBy('m.createdAt', 'desc')
            ->setFirstResult($offset * $limit)
            ->setMaxResults($limit)
        ;

        return array_reverse($qb->getQuery()->getResult());
    }
}
