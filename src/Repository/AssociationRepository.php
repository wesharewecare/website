<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Location;
use App\Entity\User;
use App\Tabulator\TabulatorModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class AssociationRepository extends ServiceEntityRepository
{
    use ColumnFiltersTrait;
    use GeoDistanceTrait;

    public const TABLE_FILTERS_LIST = [
        Association::class => 'association',
        Location::class => 'location',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Association::class);
    }

    public function getListAssociations(TabulatorModel $tabulatorParams): array
    {
        $qb = $this->getFilteredAssociations($tabulatorParams->getSearch())
            ->setFirstResult(($tabulatorParams->getPage() - 1) * $tabulatorParams->getMaxResults())
            ->setMaxResults($tabulatorParams->getMaxResults())
        ;

        $qb = $this->addColumnSorter(
            $qb,
            $tabulatorParams->getSort(),
            self::TABLE_FILTERS_LIST
        );

        return $qb->getQuery()->getResult();
    }

    public function getNbListAssociations(TabulatorModel $tabulatorParams): int
    {
        return \count($this->getFilteredAssociations($tabulatorParams->getSearch(), 'association.id')->getQuery()->getResult());
    }

    public function getNbAssociations(string $countryCode = null, bool $onlyDisabled = false): int
    {
        $filters = [];

        if ($countryCode) {
            $filters['countryCode'] = ['type' => '=', 'value' => $countryCode];
        }

        if ($onlyDisabled) {
            $filters['activated'] = ['type' => '=', 'value' => false];
        }

        return \count($this->getFilteredAssociations($filters, 'association.id')->getQuery()->getResult());
    }

    public function getByDistance(Location $location, int $radiusInKm, string $countryCode = null): array
    {
        $qb = $this->addDistanceFilter(
            $this->getFilteredAssociations(['activated' => ['type' => '=', 'value' => true]]),
            $location,
            $radiusInKm
        );

        if ($countryCode) {
            $qb
                ->andWhere('location.countryCode = :countryCode')
                ->setParameter('countryCode', $countryCode)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getAssociationsLinkedToUsers(): array
    {
        return $this->createQueryBuilder('association', 'association.id')
            ->distinct()
            ->innerJoin('association.users', 'users')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getAssociationsLinkedToEvents(): array
    {
        return $this->createQueryBuilder('association', 'association.id')
            ->distinct()
            ->innerJoin('association.events', 'events')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getAssociationsReachingLocation(Location $location): array
    {
        return $this->addDistanceFilter(
            $this->getFilteredAssociations(),
            $location,
            'association.radiusRangeInKms'
        )->getQuery()->getResult();
    }

    public function getAssociationsReachingUser(User $user): array
    {
        return $this->getAssociationsReachingLocation($user->getLocation());
    }

    private function getFilteredAssociations(array $filters = [], string $select = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('association', 'association.id')
            ->distinct()
            ->innerJoin('association.location', 'location')
        ;

        if ($select) {
            $qb->select($select);
        }

        return $this->addColumnFilters(
            $qb,
            $filters,
            self::TABLE_FILTERS_LIST
        );
    }
}
