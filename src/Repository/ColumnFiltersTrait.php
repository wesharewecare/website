<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\QueryBuilder;

trait ColumnFiltersTrait
{
    public function addColumnFilters(QueryBuilder $qb, array $filters, array $classes): QueryBuilder
    {
        foreach ($filters as $field => $params) {
            $table = null;

            foreach ($classes as $class => $name) {
                if (property_exists($class, $field)) {
                    $table = $name;

                    break;
                }
            }

            if (!$table) {
                foreach ($classes as $name) {
                    if ($name === $field) {
                        $table = $name;
                        $field = 'id';

                        break;
                    }
                }
            }

            if ($table) {
                $operator = $params['type'];
                $value = $params['value'];

                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('like' === $operator) {
                            $val = "%{$val}%";
                        }

                        if ('' !== $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= "{$table}.{$field} {$operator} :".$field.'_'.$key;
                        $qb->setParameter($field.'_'.$key, $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    if ('like' === $operator) {
                        $value = "%{$value}%";
                    }

                    $qb
                        ->andWhere("{$table}.{$field} {$operator} :{$field}")
                        ->setParameter($field, $value)
                    ;
                }
            }
        }

        return $qb;
    }

    public function addColumnSorter(QueryBuilder $qb, array $sorters, array $classes): QueryBuilder
    {
        foreach ($sorters as $field => $dir) {
            $table = null;

            foreach ($classes as $class => $name) {
                if (property_exists($class, $field)) {
                    $table = $name;

                    break;
                }
            }

            if ($table) {
                $qb->addOrderBy("{$table}.{$field}", $dir);
            }
        }

        return $qb;
    }
}
