<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Event;
use App\Entity\Location;
use App\Enum\EventStatusEnum;
use App\Tabulator\TabulatorModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class EventRepository extends ServiceEntityRepository
{
    use ColumnFiltersTrait;
    use GeoDistanceTrait;

    public const TABLE_FILTERS_LIST = [
        Event::class => 'event',
        Location::class => 'location',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function getListEvents(TabulatorModel $tabulatorParams, Association $association = null): array
    {
        $qb = $this->getFilteredEvents($tabulatorParams->getSearch(), $association)
            ->setFirstResult(($tabulatorParams->getPage() - 1) * $tabulatorParams->getMaxResults())
            ->setMaxResults($tabulatorParams->getMaxResults())
        ;

        foreach ($tabulatorParams->getSort() as $field => $dir) {
            $table = null;
            if (property_exists(Event::class, $field)) {
                $table = 'event';
            }

            if ($table) {
                $qb->addOrderBy("{$table}.{$field}", $dir);
            }
        }

        return $qb->getQuery()->getResult();
    }

    public function getNbListEvents(TabulatorModel $tabulatorParams, Association $association = null): int
    {
        return \count($this->getFilteredEvents($tabulatorParams->getSearch(), $association, 'event.id')->getQuery()->getResult());
    }

    public function getNbEvents(): int
    {
        return \count($this->getFilteredEvents([], null, 'event.id')->getQuery()->getResult());
    }

    public function getByDistance(
        Location $location,
        int $radiusInKm,
        array $status = [],
        Association $association = null,
        string $countryCode = null
    ): array {
        $qb = $this->addDistanceFilter(
            $this->getFilteredEvents([], $association),
            $location,
            $radiusInKm
        );

        if (!empty($status)) {
            $qb
                ->andWhere('event.status in (:status)')
                ->setParameter('status', $status)
            ;
        }

        if ($countryCode) {
            $qb
                ->andWhere('location.countryCode = :countryCode')
                ->setParameter('countryCode', $countryCode)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getByStatus(array $status = []): array
    {
        $qb = $this->getFilteredEvents();

        if (!empty($status)) {
            $qb
                ->andWhere('event.status in (:status)')
                ->setParameter('status', $status)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getWithDateReached(): array
    {
        $currentDate = new \DateTime('now');

        return $this->createQueryBuilder('event')
            ->where('event.status in (:status)')
            ->andWhere('event.date < :date')
            ->setParameter('status', [EventStatusEnum::STATUS_OPEN, EventStatusEnum::STATUS_CANCELED])
            ->setParameter('date', $currentDate->format('Y-m-d H:i:s'))
            ->getQuery()->getResult()
        ;
    }

    public function getUpcoming(int $daysBefore): array
    {
        $dateInterval = new \DateInterval('P'.$daysBefore.'D');
        $dateToReach = (new \DateTime('now'))->add($dateInterval);

        return $this->createQueryBuilder('event')
            ->where('event.status = :status')
            ->andWhere('event.date < :date')
            ->andWhere('event.reminderNotificationSent = :reminderNotificationSent')
            ->setParameter('status', EventStatusEnum::STATUS_OPEN)
            ->setParameter('date', $dateToReach->format('Y-m-d H:i:s'))
            ->setParameter('reminderNotificationSent', false)
            ->getQuery()->getResult()
        ;
    }

    public function getWithNewParticipants(): array
    {
        return $this->createQueryBuilder('event')
            ->where('event.newParticipantNotificationToSend = :newParticipant')
            ->setParameter('newParticipant', true)
            ->getQuery()->getResult()
        ;
    }

    public function getNbParticipantsByRole(Event $event, string $role): int
    {
        return (int) $this->createQueryBuilder('event')
            ->select('count(event.id)')
            ->innerJoin('event.users', 'user')
            ->where('user.roles like :role')
            ->andWhere('event.id = :event')
            ->setParameter('role', '%'.$role.'%')
            ->setParameter('event', $event->getId())
            ->getQuery()->getSingleScalarResult()
        ;
    }

    private function getFilteredEvents(
        array $filters = [],
        Association $association = null,
        string $select = null
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('event', 'event.id')
            ->distinct()
            ->innerJoin('event.location', 'location')
            ->innerJoin('event.association', 'association')
        ;

        if ($select) {
            $qb->select($select);
        }

        if ($association) {
            $qb
                ->andWhere('association.id = :association')
                ->setParameter('association', $association->getId())
            ;
        }

        return $this->addColumnFilters(
            $qb,
            $filters,
            self::TABLE_FILTERS_LIST
        );
    }
}
