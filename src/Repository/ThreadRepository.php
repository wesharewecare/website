<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Thread;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ThreadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Thread::class);
    }

    public function getByUserAndSubject(User $user, string $subject): array
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.users', 'u1')
            ->innerJoin('t.users', 'u2', 'with', 'u1.id <> u2.id')
            ->where('u1.id = :user')
            ->andWhere('t.subject = :subject')
            ->setParameter('user', $user->getId())
            ->setParameter('subject', $subject)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getBySubjectAndUsers(string $subject, array $users): ?Thread
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.subject = :subject')
            ->setParameter('subject', $subject)
        ;

        foreach ($users as $k => $user) {
            $qb
                ->innerJoin('t.users', 'u_'.$k)
                ->andWhere('u_'.$k.'.id = :user_'.$k)
                ->setParameter('user_'.$k, $user)
            ;
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function hasUnread(User $user, array $threads, int $hoursInterval = 0): bool
    {
        $threadMetadatasMissing = $this->createQueryBuilder('t1')
            ->innerJoin('t1.metadata', 'tm1')
            ->innerJoin('tm1.participant', 'u1')
            ->where('u1.id = :user')
            ->andWhere('t1.id = t.id')
            ->getQuery()->getDQL();

        $threadMetadatasNotEquals = $this->createQueryBuilder('t2')
            ->innerJoin('t2.metadata', 'tm2')
            ->innerJoin('tm2.participant', 'u2')
            ->where('u2.id = :user')
            ->andWhere('t2.id = t.id')
            ->andWhere('t2.lastMessageAt <> tm2.lastMessageReadAt')
            ->getQuery()->getDQL();

        $qb = $this->createQueryBuilder('t');

        $qb
            ->select('count(t.id)')
            ->where('t.id in (:threads)')
            ->andWhere('t.lastMessageAt is not null')
            ->andWhere($qb->expr()->not($qb->expr()->exists($threadMetadatasMissing)).' or '.$qb->expr()->exists($threadMetadatasNotEquals))
            ->setParameter('user', $user->getId())
            ->setParameter('threads', $threads)
        ;

        if ($hoursInterval > 0) {
            $qb
                ->andWhere("CURRENT_TIMESTAMP() >= DATE_ADD(t.lastMessageAt, :hoursInterval,'HOUR')")
                ->setParameter('hoursInterval', $hoursInterval)
            ;
        }

        return 0 < $qb->getQuery()->getSingleScalarResult();
    }
}
