<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Document;
use App\Service\LocaleManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DocumentRepository extends ServiceEntityRepository
{
    private $localeManager;

    public function __construct(ManagerRegistry $registry, LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
        parent::__construct($registry, Document::class);
    }

    public function getDocuments(
        Association $association = null,
        string $recipient = null,
        bool $applyLocaleFilter = true
    ): array {
        $qb = $this->createQueryBuilder('document');

        if ($applyLocaleFilter) {
            $qb
                ->where('document.locale = :locale or document.locale = :defaultLocale')
                ->setParameter('locale', $this->localeManager->getCurrentLocale())
                ->setParameter('defaultLocale', $this->localeManager->getDefaultLocale())
            ;
        }

        if ($association) {
            $qb
                ->innerJoin('document.association', 'association')
                ->where('association.id = :association')
                ->setParameter('association', $association->getId())
            ;
        } else {
            $qb->andWhere('document.association is null');
        }

        if ($recipient) {
            $qb
                ->andWhere('JSON_CONTAINS(document.recipients, :recipient) = 1')
                ->setParameter('recipient', '"'.$recipient.'"')
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
