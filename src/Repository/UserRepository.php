<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Event;
use App\Entity\Location;
use App\Entity\Profile;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\UserRoleEnum;
use App\Tabulator\TabulatorModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    use ColumnFiltersTrait;
    use GeoDistanceTrait;

    public const TABLE_FILTERS_LIST = [
        User::class => 'user',
        Profile::class => 'profile',
        Location::class => 'location',
        Event::class => 'event',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function getListUsers(TabulatorModel $tabulatorParams, Association $association = null): array
    {
        $qb = $this->getFilteredUsers($tabulatorParams->getSearch(), $association)
            ->setFirstResult(($tabulatorParams->getPage() - 1) * $tabulatorParams->getMaxResults())
            ->setMaxResults($tabulatorParams->getMaxResults())
        ;

        $qb = $this->addColumnSorter(
            $qb,
            $tabulatorParams->getSort(),
            self::TABLE_FILTERS_LIST
        );

        return $qb->getQuery()->getResult();
    }

    public function getNbListUsers(TabulatorModel $tabulatorParams, Association $association = null): int
    {
        return \count($this->getFilteredUsers($tabulatorParams->getSearch(), $association, 'user.id')->getQuery()->getResult());
    }

    public function getNbUsers(array $roles = [], Association $association = null, string $countryCode = null): int
    {
        $filters = $countryCode
            ? ['countryCode' => ['type' => '=', 'value' => $countryCode]]
            : []
        ;

        $qb = $this->getFilteredUsers($filters, $association, 'user.id');

        if (!empty($roles)) {
            $qb = $this->addRolesFilter($qb, $roles);
        }

        return \count($qb->getQuery()->getResult());
    }

    public function getNbManagersToApprove(string $countryCode = null): int
    {
        $roles = [UserRoleEnum::ROLE_ASSOCIATION];
        $filters = ['isApproved' => ['type' => '=', 'value' => false]];

        if ($countryCode) {
            $filters['countryCode'] = ['type' => '=', 'value' => $countryCode];
        } else {
            $roles = array_merge($roles, [
                UserRoleEnum::ROLE_ADMIN,
                UserRoleEnum::ROLE_SUPER_ADMIN,
            ]);
        }

        $qb = $this->getFilteredUsers($filters, null, 'user.id');
        $qb = $this->addRolesFilter($qb, $roles);

        return \count($qb->getQuery()->getResult());
    }

    public function getNbUsersToApprove(Association $association): int
    {
        $qb = $this->createQueryBuilder('user', 'user.id')
            ->select('count(distinct user.id)')
            ->innerJoin('user.location', 'location')
            ->innerJoin('user.profile', 'profile')
            ->innerJoin('user.association', 'association')
            ->andWhere('user.isVerified = :isVerified')
            ->andWhere('profile.isApproved = :approved')
            ->andWhere('association = :association')
            ->andWhere('profile.passportName is not null')
            ->andWhere('profile.profilePictureName is not null')
            ->setParameter('isVerified', true)
            ->setParameter('approved', false)
            ->setParameter('association', $association->getId())
        ;

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function isViewableByAssociation(Association $association, User $user): bool
    {
        return \array_key_exists(
            $user->getId(),
            $this->getFilteredUsers([], $association)->getQuery()->getResult()
        );
    }

    public function getUsersSearchAutocomplete(
        string $search,
        User $currentUser,
        int $limit = null,
        int $offset = null
    ): array {
        $association = $currentUser->getAssociation();

        $qb = $this->createQueryBuilder('user', 'user.id')
            ->distinct()
            ->innerJoin('user.location', 'location')
            ->innerJoin('user.profile', 'profile')
            ->andWhere('user.email is not null')
            ->andWhere('user.isVerified = :isVerified')
            ->setParameter('isVerified', true)
        ;

        if ($association) {
            $qb
                ->innerJoin('user.association', 'association')
                ->andWhere('association = :association')
                ->setParameter('association', $association->getId())
            ;
        }

        $qb
            ->andWhere("user.firstname like :search or user.lastname like :search or user.email like :search or concat(user.firstname, ' ', user.lastname) like :search")
            ->andWhere('user.id <> :user')
            ->setParameter('search', $search.'%')
            ->setParameter('user', $currentUser->getId())
        ;

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->getResult();
    }

    public function getAssociationManager(Association $association = null): array
    {
        $qb = $this->createQueryBuilder('user');

        $qb = $this->addRolesFilter($qb, [UserRoleEnum::ROLE_ASSOCIATION]);

        if ($association) {
            $qb
                ->innerJoin('user.association', 'association')
                ->andWhere('association.id = :association')
                ->setParameter('association', $association->getId())
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getByDistance(Location $location, int $radiusRangeInKms = 50): array
    {
        $qb = $this->createQueryBuilder('user', 'user.id')
            ->distinct()
            ->innerJoin('user.location', 'location')
            ->andWhere('user.isVerified = :isVerified')
            ->setParameter('isVerified', true)
        ;

        $qb = $this->addDistanceFilter($qb, $location, $radiusRangeInKms);

        return $qb->getQuery()->getResult();
    }

    public function getYoungLookingForAccommodation(Association $association = null): array
    {
        $qb = $this->createQueryBuilder('user', 'user.id')
            ->distinct()
            ->innerJoin('user.profile', 'profile')
            ->leftJoin('user.accommodationDemands', 'accommodationDemands', 'with', 'accommodationDemands.status = :status')
            ->where('user.isVerified = :isVerified')
            ->andWhere('profile.showHomesharing = :showHomesharing')
            ->andWhere('accommodationDemands.id is null')
            ->setParameter('isVerified', true)
            ->setParameter('showHomesharing', true)
            ->setParameter('status', AccommodationDemandStatusEnum::STATUS_ACCEPTED)
        ;

        $qb = $this->addRolesFilter($qb, [UserRoleEnum::ROLE_YOUNG]);

        if ($association) {
            $qb
                ->innerJoin('user.association', 'association')
                ->andWhere('association.id = :association')
                ->setParameter('association', $association->getId())
            ;
        }

        return $qb->getQuery()->getResult();
    }

    private function getFilteredUsers(
        array $filters = [],
        Association $association = null,
        string $select = null
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('user', 'user.id')
            ->distinct()
            ->innerJoin('user.location', 'location')
            ->innerJoin('user.profile', 'profile')
            ->leftJoin('user.events', 'event')
            ->andWhere('user.isVerified = :isVerified')
            ->setParameter('isVerified', true)
        ;

        if ($select) {
            $qb->select($select);
        }

        if ($association) {
            $qb
                ->leftJoin('user.association', 'association')
                ->leftJoin('user.accommodation', 'accommodation')
                ->leftJoin('accommodation.accommodationDemands', 'ad1', 'with', 'ad1.status in (:status)')
                ->leftJoin('user.accommodationDemands', 'ad2', 'with', 'ad2.status in (:status)')
                ->andWhere('association is null or association.id = :association or (ad1 is null and ad2 is null)')
                ->setParameter('association', $association->getId())
                ->setParameter('status', [
                    AccommodationDemandStatusEnum::STATUS_ACCEPTED,
                    AccommodationDemandStatusEnum::STATUS_PENDING,
                    AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL,
                ])
            ;

            if (!\array_key_exists('event', $filters)) {
                $qb = $this->addDistanceFilter($qb, $association->getLocation(), $association->getRadiusRangeInKms());
            }

            $rolesToExclude = UserRoleEnum::$admins;

            if (AssociationTypeEnum::TYPE_YOUNG_ONLY === $association->getType()) {
                $rolesToExclude[] = UserRoleEnum::ROLE_SENIOR;
            }

            $qb = $this->addRolesFilter($qb, $rolesToExclude, false);
        }

        return $this->addColumnFilters(
            $qb,
            $filters,
            self::TABLE_FILTERS_LIST
        );
    }

    private function addRolesFilter(QueryBuilder $qb, array $roles, bool $like = true): QueryBuilder
    {
        if (!empty($roles)) {
            if ($like) {
                $mode = 'LIKE';
                $method = ' or ';
            } else {
                $mode = 'NOT LIKE';
                $method = ' and ';
            }

            $where = '';
            foreach ($roles as $role) {
                if (!empty($where)) {
                    $where .= $method;
                }

                $where .= 'user.roles '.$mode.' :role_'.$role;

                $qb->setParameter('role_'.$role, '%'.$role.'%');
            }

            $qb->andWhere($where);
        }

        return $qb;
    }
}
