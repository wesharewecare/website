<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Hobby;
use App\Tabulator\TabulatorModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class HobbyRepository extends ServiceEntityRepository
{
    use ColumnFiltersTrait;

    public const TABLE_FILTERS_LIST = [
        Hobby::class => 'hobby',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hobby::class);
    }

    public function getListHobbies(TabulatorModel $tabulatorParams): array
    {
        $qb = $this->getFilteredHobbies($tabulatorParams->getSearch())
            ->setFirstResult(($tabulatorParams->getPage() - 1) * $tabulatorParams->getMaxResults())
            ->setMaxResults($tabulatorParams->getMaxResults())
        ;

        $qb = $this->addColumnSorter(
            $qb,
            $tabulatorParams->getSort(),
            self::TABLE_FILTERS_LIST
        );

        return $qb->getQuery()->getResult();
    }

    public function getNbListHobbies(TabulatorModel $tabulatorParams): int
    {
        return \count($this->getFilteredHobbies($tabulatorParams->getSearch(), 'hobby.id')->getQuery()->getResult());
    }

    private function getFilteredHobbies(array $filters = [], string $select = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('hobby', 'hobby.id')
            ->distinct()
        ;

        if ($select) {
            $qb->select($select);
        }

        return $this->addColumnFilters(
            $qb,
            $filters,
            self::TABLE_FILTERS_LIST
        );
    }
}
