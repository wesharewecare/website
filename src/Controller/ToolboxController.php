<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Document;
use App\Enum\DocumentRecipientEnum;
use App\Enum\DocumentVoterEnum;
use App\Enum\UserRoleEnum;
use App\Form\DocumentType;
use App\Repository\DocumentRepository;
use App\Service\GrantedService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/toolbox")
 */
class ToolboxController extends BaseController
{
    /**
     * @Route(name="toolbox")
     */
    public function index(
        Request $request,
        TranslatorInterface $translator,
        DocumentRepository $documentRepo,
        GrantedService $grantedService
    ): Response {
        $params = [];
        $recipient = null;
        $user = $this->getUser();
        $association = $user->getAssociation();

        if ($grantedService->isManager($user)) {
            $document = new Document();

            if ($association) {
                $recipient = DocumentRecipientEnum::RECIPIENT_ASSOCIATION;
                $document->setAssociation($association);
            }

            $params['addedDocuments'] = $documentRepo->getDocuments(
                $association,
                null,
                false
            );

            $form = $this->createForm(DocumentType::class, $document);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $om = $this->getDoctrine()->getManager();
                $om->persist($document);
                $om->flush();

                $this->addFlash('success', $translator->trans('toolbox.document.flash.created'));

                return new RedirectResponse($this->generateUrl('toolbox'));
            }

            $params['form'] = $form->createView();
        } else {
            $role = $grantedService->getRole($user);

            if (UserRoleEnum::ROLE_YOUNG === $role) {
                $recipient = DocumentRecipientEnum::RECIPIENT_YOUNG;
            } else {
                $recipient = DocumentRecipientEnum::RECIPIENT_SENIOR;
            }

            if ($association) {
                $params['associationDocuments'] = $documentRepo->getDocuments($association, $recipient);
            }
        }

        if ($recipient) {
            $params['generalDocuments'] = $documentRepo->getDocuments(null, $recipient);
        }

        return $this->render('toolbox/list.html.twig', $params);
    }

    /**
     * @Route("/document/{id}/edit", name="toolbox_document_edit", methods={"GET", "POST"}, options={ "expose" : true })
     */
    public function edit(TranslatorInterface $translator, Request $request, Document $document): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoterEnum::EDIT, $document);
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($document);
            $om->flush();

            $this->addFlash('success', $translator->trans('toolbox.document.flash.edited'));

            return $this->redirectToRoute('toolbox');
        }

        return $this->render('toolbox/edit.html.twig', [
            'form' => $form->createView(),
            'document' => $document,
        ]);
    }

    /**
     * @Route("/document/{id}/delete", name="toolbox_document_delete", methods={"GET", "POST"}, options={"expose" : true})
     */
    public function delete(TranslatorInterface $translator, Request $request, Document $document): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoterEnum::EDIT, $document);
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($document);
            $om->flush();

            $this->addFlash('success', $translator->trans('toolbox.document.flash.deleted'));

            return $this->redirectToRoute('toolbox');
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $document->getId(),
            'path' => 'toolbox_document_delete',
            'approvalText' => $translator->trans('popup.document.delete.content', ['%name%' => $document->getName()]),
            'submitText' => $translator->trans('actions.delete'),
            'submitClass' => 'btn-danger',
        ]);
    }
}
