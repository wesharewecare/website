<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Cohabitation;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationStatusEnum;
use App\Enum\CohabitationVoterEnum;
use App\Enum\CohabitationWorkflowStepStatusEnum;
use App\Form\CohabitationClosingFormType;
use App\Form\CohabitationFormType;
use App\Repository\CohabitationRepository;
use App\Service\CohabitationWorkflowStepManager;
use App\Tabulator\Response\CohabitationListResponse;
use App\Tabulator\TabulatorModel;
use App\Tabulator\TabulatorResponseFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Handler\DownloadHandler;

class CohabitationController extends BaseController
{
    /**
     * @Route(
     *     "/cohabitations/{id}",
     *     name="cohabitation_show",
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(CohabitationVoterEnum::SHOW, subject="cohabitation")
     */
    public function showCohabitation(
        Request $request,
        Cohabitation $cohabitation,
        CohabitationWorkflowStepManager $cohabitationWorkflowStepManager,
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator
    ): Response {
        $om = $this->getDoctrine()->getManager();
        $workflow = $cohabitationWorkflowStepManager->getWorkflow();

        $formPrevious = null;
        $formNext = null;

        if ($this->isGranted(CohabitationVoterEnum::EDIT, $cohabitation)) {
            $association = $this->getUser()->getAssociation();

            if ($workflow[$cohabitation->getCurrentStep()->getStatus()]['previous']) {
                $formPrevious = $formFactory->createNamedBuilder('form_previous')->getForm();
                $formPrevious->handleRequest($request);

                if ($formPrevious->isSubmitted() && $formPrevious->isValid()) {
                    $cohabitationWorkflowStepManager->reachPreviousStep($cohabitation);
                    $om->persist($cohabitation);
                    $om->flush();

                    return $this->redirectToRoute('cohabitation_show', ['id' => $cohabitation->getId()]);
                }

                $formPrevious = $formPrevious->createView();
            }

            if ($workflow[$cohabitation->getCurrentStep()->getStatus()]['next']) {
                $formNext = $this->createForm(
                    CohabitationFormType::class,
                    $cohabitation,
                    [
                        'current_step' => $workflow[$cohabitation->getCurrentStep()->getStatus()]['next'],
                        'association' => $association,
                    ]
                );
                $formNext->handleRequest($request);

                if ($formNext->isSubmitted() && $formNext->isValid()) {
                    $saveOnly = $request->query->has('save_only');

                    if (!$saveOnly) {
                        if (!$cohabitationWorkflowStepManager->reachNextStep($cohabitation, $association)) {
                            $this->addFlash('warning', $translator->trans('cohabitation.next_step.failed'));
                        }
                    }

                    $om->persist($cohabitation);
                    $om->flush();

                    return $this->redirectToRoute('cohabitation_show', ['id' => $cohabitation->getId()]);
                }

                $formNext = $formNext->createView();
            }
        }

        return $this->render('cohabitation/show.html.twig', [
            'cohabitation' => $cohabitation,
            'accommodationDemand' => $cohabitation->getAccommodationDemand(),
            'formPrevious' => $formPrevious,
            'formNext' => $formNext,
            'workflow' => $workflow,
            'stepsDone' => $cohabitation->getStepsDone(),
            'currentStep' => $cohabitation->getCurrentStep()->getStatus(),
            'stepsInfo' => $cohabitationWorkflowStepManager->getStepsInformation($cohabitation),
        ]);
    }

    /**
     * @Route(
     *     "/cohabitations/{id}/agreement",
     *     name="agreement_show",
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(CohabitationVoterEnum::SHOW, subject="cohabitation")
     */
    public function showAgreement(
        Request $request,
        Cohabitation $cohabitation,
        DownloadHandler $downloadHandler,
        CohabitationWorkflowStepManager $cohabitationWorkflowStepManager
    ): Response {
        $user = $this->getUser();

        if ($request->query->has('mode') && 'twig' === $request->query->get('mode')) {
            $accommodationDemand = $cohabitation->getAccommodationDemand();
            $senior = $accommodationDemand->getSenior();
            $young = $accommodationDemand->getYoung();
            $association = $senior->getAssociation();
            $accommodation = $accommodationDemand->getAccommodation();

            return $this->render(
                'cohabitation/agreement.html.twig',
                [
                    'manager' => $user,
                    'senior' => $senior,
                    'young' => $young,
                    'cohabitation' => $cohabitation,
                    'association' => $association,
                    'accommodation' => $accommodation,
                ]
            );
        }

        $forceDownload = $request->query->has('force_download') && $request->query->get('force_download');
        $forceRefresh = $request->query->has('force_refresh') && $request->query->get('force_refresh');

        if (!$cohabitation->getAgreementName() || $forceRefresh) {
            $cohabitationWorkflowStepManager->generateAgreement($cohabitation, $user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cohabitation);
            $em->flush();
        }

        return $downloadHandler->downloadObject(
            $cohabitation,
            'agreementFile',
            Cohabitation::class,
            null,
            $forceDownload
        );
    }

    /**
     * @Route(
     *     "/cohabitations/{id}/agreement_signed",
     *     name="agreement_signed_show",
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(CohabitationVoterEnum::SHOW, subject="cohabitation")
     */
    public function showAgreementSigned(
        Request $request,
        Cohabitation $cohabitation,
        DownloadHandler $downloadHandler
    ): Response {
        $forceDownload = $request->query->has('force_download') && $request->query->get('force_download');

        return $downloadHandler->downloadObject(
            $cohabitation,
            'agreementSignedFile',
            Cohabitation::class,
            null,
            $forceDownload
        );
    }

    /**
     * @Route(
     *     "/admin/cohabitations/{id}/close",
     *     name="cohabitation_closing",
     *     requirements={"id" : "\d+"},
     *     options={"expose" : true}
     * )
     * @IsGranted(CohabitationVoterEnum::EDIT, subject="cohabitation")
     */
    public function closeCohabitation(
        Request $request,
        TranslatorInterface $translator,
        Cohabitation $cohabitation
    ): Response {
        $form = $this->createForm(CohabitationClosingFormType::class, $cohabitation);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $accommodationDemand = $cohabitation->getAccommodationDemand();
            $accommodation = $accommodationDemand->getAccommodation();
            $accommodationDemand->setStatus(AccommodationDemandStatusEnum::STATUS_CLOSED);
            $accommodation->setStatus(AccommodationStatusEnum::STATUS_CLOSED);

            $om = $this->getDoctrine()->getManager();
            $om->persist($cohabitation);
            $om->flush();

            $this->addFlash('success', $translator->trans('cohabitation.closed.flash'));

            return $this->redirectToRoute('user_show', ['id' => $accommodation->getSenior()->getId()]);
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $cohabitation->getId(),
            'path' => 'cohabitation_closing',
            'approvalText' => $translator->trans('popup.cohabitation.closing.content'),
            'submitClass' => 'btn-danger',
        ]);
    }

    /**
     * @Route("/admin/cohabitations", name="cohabitation_list")
     */
    public function list(TranslatorInterface $translator): Response
    {
        $steps = [];

        foreach (CohabitationWorkflowStepStatusEnum::getReadableValues() as $key => $value) {
            $steps[$key] = $translator->trans($value);
        }

        return $this->render('cohabitation/list.html.twig', [
            'steps' => $steps,
        ]);
    }

    /**
     * @Route("/admin/cohabitations/data", name="cohabitation_list_data", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function listData(
        CohabitationRepository $cohabitationRepo,
        TabulatorResponseFactory $responseFactory,
        TabulatorModel $tabulatorParams
    ): JsonResponse {
        $association = $this->getUser()->getAssociation();

        return new JsonResponse([
            'data' => $responseFactory->getResponse(
                CohabitationListResponse::class,
                $cohabitationRepo->getListCohabitations($tabulatorParams, $association)
            ),
            'last_page' => ceil(
                $cohabitationRepo->getNbListCohabitations($tabulatorParams, $association) / $tabulatorParams->getMaxResults()
            ),
        ]);
    }
}
