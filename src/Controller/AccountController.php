<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Accommodation;
use App\Entity\Association;
use App\Entity\Location;
use App\Entity\UserNotification;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\NotificationTypeEnum;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Form\AccommodationFormType;
use App\Form\Model\EditUser;
use App\Form\UserFormType;
use App\Form\UserNotificationType;
use App\Notifier\Accommodation\AccommodationNewNotification;
use App\Repository\AccommodationDemandRepository;
use App\Repository\AccommodationRepository;
use App\Repository\AssociationRepository;
use App\Repository\UserRepository;
use App\Service\GrantedService;
use App\Service\NotifierHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/account")
 */
class AccountController extends BaseController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard(
        AssociationRepository $associationRepo,
        AccommodationRepository $accommodationRepo,
        AccommodationDemandRepository $accommodationDemandRepo,
        GrantedService $grantedService,
        UserRepository $userRepo
    ): Response {
        $user = $this->getUser();
        $profile = $user->getProfile();
        $mode = $grantedService->getProfileMode($user);

        $params = ['mode' => $mode];

        if (UserProfileTypeEnum::PROFILE_USER === $mode) {
            $params = array_merge($params, [
                'association' => $user->getAssociation(),
                'hasProfilePicture' => null !== $profile->getProfilePictureName(),
                'hasPassport' => null !== $profile->getPassportName(),
                'isProfileVerified' => $profile->isApproved(),
                'events' => $user->getEvents(),
            ]);

            if ($profile->isShowHomesharing()) {
                if ($this->isGranted(UserRoleEnum::ROLE_SENIOR)) {
                    if ($accommodation = $user->getAccommodation()) {
                        $params['accommodationFilled'] = true;
                        $params['accommodation'] = $accommodation;

                        if (!$accommodation->getAccommodationDemands()->isEmpty()) {
                            $params['accommodationDemands'] = $accommodation->getAccommodationDemands()->toArray();
                        }
                    } else {
                        $params['accommodationFilled'] = false;
                    }
                } else {
                    $params['accommodationDemandsStatus'] = $accommodationDemandRepo->getStatus(
                        $user,
                        [
                            AccommodationDemandStatusEnum::STATUS_ACCEPTED,
                            AccommodationDemandStatusEnum::STATUS_PENDING,
                            AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL,
                            AccommodationDemandStatusEnum::STATUS_REFUSED,
                        ]
                    );

                    if (!$user->getAccommodationDemands()->isEmpty()) {
                        $params['accommodationDemands'] = $user->getAccommodationDemands()->toArray();
                    }
                }
            }
        } else {
            $association = null;
            $countryCode = null;

            if (UserProfileTypeEnum::PROFILE_ADMIN === $mode) {
                if (!$this->isGranted(UserRoleEnum::ROLE_SUPER_ADMIN)) {
                    $countryCode = $user->getLocation()->getCountryCode();
                }

                $params['nbUsersToApprove'] = $userRepo->getNbManagersToApprove($countryCode);
                $params['nbAssociations'] = $associationRepo->getNbAssociations($countryCode);
                $params['nbAssociationsToActivate'] = $associationRepo->getNbAssociations($countryCode, true);
            } else {
                /** @var Association $association */
                $association = $user->getAssociation();

                $params = array_merge($params, [
                    'nbUsersToApprove' => $userRepo->getNbUsersToApprove($association),
                    'nbAccommodationDemandToRespond' => $accommodationDemandRepo->getNbDemandToRespond($association),
                    'events' => $association->getEvents(),
                ]);

                if (AssociationTypeEnum::TYPE_BOTH === $association->getType()) {
                    $params['nbAccommodationToOpen'] = $accommodationRepo->getNbAccommodationToOpen($association);
                }
            }

            $params = array_merge($params, [
                'nbYouth' => $userRepo->getNbUsers([UserRoleEnum::ROLE_YOUNG], $association, $countryCode),
                'nbSeniors' => $userRepo->getNbUsers([UserRoleEnum::ROLE_SENIOR], $association, $countryCode),
            ]);
        }

        return $this->render('account/dashboard.html.twig', $params);
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile(): Response
    {
        return $this->render('account/profile.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/profile/edit", name="profile_edit")
     */
    public function editProfile(
        Request $request,
        GrantedService $grantedService,
        TranslatorInterface $translator
    ): Response {
        $user = $this->getUser();
        $editUser = new EditUser($user);
        $form = $this->createForm(
            UserFormType::class,
            $editUser,
            ['mode' => $grantedService->getProfileMode($user)]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($editUser->getUser());
            $om->flush();

            $this->addFlash('success', $translator->trans('profile.edit.flash'));
        }

        return $this->render('account/profile.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/profile/accommodation/edit", name="profile_accommodation_edit")
     * @IsGranted(UserRoleEnum::ROLE_SENIOR)
     */
    public function editAccommodation(
        Request $request,
        TranslatorInterface $translator,
        NotifierHelper $notifierHelper,
        UserRepository $userRepo
    ): Response {
        $user = $this->getUser();
        $accommodation = $user->getAccommodation();

        if (!$accommodation) {
            $this->denyAccessUnlessGranted(UserVoterEnum::CREATE_ACCOMMODATION, $user);
            $accommodation = new Accommodation();
            $accommodation->setSenior($user);
            $isNewAccommodation = true;
        } else {
            $isNewAccommodation = false;
        }

        $form = $this->createForm(AccommodationFormType::class, $accommodation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodation);
            $em->flush();

            if ($isNewAccommodation && $association = $user->getAssociation()) {
                $notifierHelper->sendNotifications(
                    AccommodationNewNotification::class,
                    $userRepo->getAssociationManager($association),
                    NotificationTypeEnum::TYPE_ACCOMMODATION_NEW,
                    $accommodation
                );
            }

            $this->addFlash('success', $translator->trans('accommodation.edit.flash'));
            $this->redirectToRoute('profile');
        }

        return $this->render('account/profile.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/profile/notifications/edit", name="profile_notifications_edit", options={ "expose" : true })
     */
    public function editNotification(
        Request $request,
        GrantedService $grantedService,
        TranslatorInterface $translator,
        NotificationTypeEnum $notificationTypeEnum
    ): Response {
        $user = $this->getUser();
        $notificationTypes = $notificationTypeEnum->getNotificationsByRole($grantedService->getRole($user));
        $notification = $user->getUserNotification();

        if ($notification) {
            $notification->addMissingNotifications($notificationTypes);
        } else {
            $notification = new UserNotification($notificationTypes);
        }

        $form = $this->createForm(UserNotificationType::class, $notification);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $user->setUserNotification($notification);
            $om->persist($user);
            $om->flush();

            $this->addFlash('success', $translator->trans('profile.notifications.edit.flash'));

            return $this->redirectToRoute('profile');
        }

        $types = [];
        foreach ($notificationTypes as $type) {
            $typeName = strtok($type, '_');
            if (!\in_array($typeName, $types)) {
                $types[] = $typeName;
            }
        }

        return $this->render('user/notifications.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'types' => $types,
        ]);
    }

    /**
     * @Route("/map", name="account_map")
     */
    public function map(GrantedService $grantedService): Response
    {
        $user = $this->getUser();

        /** @var Location $location */
        $location = UserRoleEnum::ROLE_ASSOCIATION === $grantedService->getRole($user)
            ? $user->getAssociation()->getLocation()
            : $user->getLocation()
        ;

        return $this->render('account/map.html.twig', [
            'location' => ['lat' => $location->getLatitude(), 'lng' => $location->getLongitude()],
            'zoom' => MapController::ZOOM_AUTHENTICATE,
            'maxZoom' => MapController::ZOOM_MAX_AUTHENTICATE,
        ]);
    }

    /**
     * @Route("/not-approved", name="manager_not-approved")
     */
    public function notApproved(): Response
    {
        if ($this->isGranted(UserVoterEnum::ACCESS_ACCOUNT, $this->getUser())) {
            return $this->redirectToRoute('dashboard');
        }

        return $this->render('account/not_approved.html.twig');
    }
}
