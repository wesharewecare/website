<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\EmailFormType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;
use Symfony\Component\Security\Http\LoginLink\LoginLinkNotification;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends BaseController
{
    use TargetPathTrait;

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('dashboard');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/login/modal", name="login_modal", options={ "expose" : true })
     */
    public function loginModal(Request $request): Response
    {
        if ($request->query->has('referer')) {
            $this->saveTargetPath(
                $request->getSession(),
                'main',
                (string) $request->query->get('referer')
            );
        }

        return $this->render('security/_login_required_modal.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(): void
    {
        throw new \LogicException();
    }

    /**
     * @Route("/login-link", name="login_link_request")
     */
    public function requestLoginLink(
        Request $request,
        LoginLinkHandlerInterface $loginLinkHandler,
        UserRepository $userRepository,
        NotifierInterface $notifier,
        TranslatorInterface $translator
    ): Response {
        $form = $this->createForm(EmailFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $user = $userRepository->findOneBy(['email' => $email]);

            if ($user instanceof User) {
                $loginLinkDetails = $loginLinkHandler->createLoginLink($user);

                $notification = new LoginLinkNotification(
                    $loginLinkDetails,
                    $translator->trans('login_link.notification.title')
                );

                $recipient = new Recipient($user->getEmail());
                $notifier->send($notification, $recipient);
            }

            return $this->redirectToRoute('login_link_check_email');
        }

        return $this->render('security/login_link_request.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login-link/check-email", name="login_link_check_email")
     */
    public function checkEmail(): Response
    {
        return $this->render('security/check_email.html.twig');
    }
}
