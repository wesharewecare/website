<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Thread;
use App\Enum\ThreadVoterEnum;
use App\Enum\UserVoterEnum;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\Service\GrantedService;
use App\Service\JWTFactory;
use App\Service\MessagingHelper;
use App\Service\MessagingProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\WebLink\Link;

/**
 * @Route("/messaging")
 */
class MessagingController extends BaseController
{
    /**
     * @Route(name="messages")
     */
    public function messages(
        Request $request,
        UserRepository $userRepo,
        MessagingProvider $messagingProvider,
        GrantedService $grantedService
    ): Response {
        $user = $this->getUser();
        $newThread = [];

        if ($request->query->has('recipient')) {
            $recipient = $userRepo->findOneBy(['id' => $request->query->get('recipient')]);
            if ($recipient) {
                $newThread['recipient'] = [
                    'fullname' => $recipient->getFullName(),
                    'email' => $recipient->getEmail(),
                ];
            }
        }

        return $this->render('account/messages.html.twig', [
            'user' => [
                'id' => $user->getId(),
                'iri' => $messagingProvider->getUserIRI($user),
                'isManager' => $grantedService->isManager($user),
            ],
            'newThread' => $newThread,
        ]);
    }

    /**
     * @Route("/api/threads_groups", methods={"GET"}, name="messaging_api_get_threads_groups", options={ "expose" : true })
     */
    public function apiGetThreadsGroups(
        Request $request,
        MessagingProvider $messagingProvider,
        SerializerInterface $serializer,
        JWTFactory $JWTFactory,
        string $mercureSubscribeUrl
    ): Response {
        $user = $this->getUser();
        $this->addLink($request, new Link('mercure', $mercureSubscribeUrl));

        $response = new JsonResponse(
            $serializer->serialize($messagingProvider->getAllowedThreads($user), 'json'),
            Response::HTTP_OK,
            [],
            true
        );

        $response->headers->setCookie($JWTFactory->getCookie($messagingProvider->getAllTopicsIRI($user)));

        return $response;
    }

    /**
     * @Route(
     *     "/api/threads_groups/{id}/messages",
     *     methods={"GET"},
     *     name="messaging_api_get_threads_groups_messages",
     *     options={ "expose" : true }
     * )
     */
    public function apiGetThreadGroupMessages(
        Request $request,
        MessagingProvider $messagingProvider,
        MessageRepository $messageRepo,
        SerializerInterface $serializer,
        string $id
    ): Response {
        $threads = $messagingProvider->getThreadsByThreadGroupId($id);

        if (empty($threads)) {
            throw new NotFoundHttpException('ThreadGroup not found.');
        }

        foreach ($threads as $thread) {
            $this->denyAccessUnlessGranted(ThreadVoterEnum::SEE, $thread);
        }

        $offset = $request->query->getInt('page', 1);

        $data = [];
        $messages = $messageRepo->getByThreads($threads, $offset - 1);

        foreach ($messages as $message) {
            $data[] = $messagingProvider->getMessageDTO($message);
        }

        return new JsonResponse(
            $serializer->serialize($data, 'json'),
            Response::HTTP_OK,
            ['Link' => $id.'; rel="mercure"'],
            true
        );
    }

    /**
     * @Route(
     *     "/api/threads/{id}/messages",
     *     methods={"POST"},
     *     name="messaging_api_post_thread_messages",
     *     options={ "expose" : true }
     * )
     * @IsGranted(ThreadVoterEnum::REPLY, subject="thread")
     */
    public function apiPostThreadMessage(
        Request $request,
        SerializerInterface $serializer,
        MessagingProvider $messagingProvider,
        MessagingHelper $messagingHelper,
        Thread $thread
    ): Response {
        $data = json_decode((string) $request->getContent(), true);
        if (!$data || !isset($data['content'])) {
            throw new \InvalidArgumentException('Missing content.');
        }

        $message = $messagingHelper->createMessage($thread, $data['content'], $this->getUser());

        $om = $this->getDoctrine()->getManager();
        $om->flush();

        return new JsonResponse(
            $serializer->serialize($messagingProvider->getMessageDTO($message), 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/api/threads_groups/{id}/mark_as_read",
     *     methods={"POST"},
     *     name="messaging_api_put_threads_groups_mark_as_read",
     *     options={ "expose" : true }
     * )
     */
    public function apiThreadGroupMarkAsRead(
        MessagingProvider $messagingProvider,
        MessagingHelper $messagingHelper,
        string $id
    ): Response {
        $threads = $messagingProvider->getThreadsByThreadGroupId($id);

        if (empty($threads)) {
            throw new NotFoundHttpException('ThreadGroup not found.');
        }

        foreach ($threads as $thread) {
            $messagingHelper->markAsRead($thread, $this->getUser());
        }

        $om = $this->getDoctrine()->getManager();
        $om->flush();

        return new Response();
    }

    /**
     * @Route(
     *     "/api/threads_groups/{id}/toggle_favorite",
     *     methods={"POST"},
     *     name="messaging_api_put_threads_groups_toggle_favorite",
     *     options={ "expose" : true }
     * )
     */
    public function apiThreadGroupToggleFavorite(MessagingHelper $messagingHelper, string $id): Response
    {
        $messagingHelper->toggleFavorite($id, $this->getUser());

        return new Response();
    }

    /**
     * @Route(
     *     "/api/threads",
     *     methods={"POST"},
     *     name="messaging_api_post_thread",
     *     options={ "expose" : true }
     * )
     */
    public function apiNewThread(
        Request $request,
        SerializerInterface $serializer,
        MessagingProvider $messagingProvider,
        MessagingHelper $messagingHelper,
        UserRepository $userRepo
    ): Response {
        $data = json_decode((string) $request->getContent(), true);
        if (!$data || !isset($data['content']) || !isset($data['recipient'])) {
            throw new \InvalidArgumentException('Missing content.');
        }

        $user = $this->getUser();
        $recipient = $userRepo->findOneBy(['email' => $data['recipient']]);
        $this->denyAccessUnlessGranted(UserVoterEnum::SEND_MESSAGE, $recipient);

        $thread = $messagingHelper->getThreadBySubjectAndParticipants(
            MessagingHelper::DIRECT_MESSAGE,
            [
                $user,
                $recipient,
            ]
        );

        $message = $messagingHelper->createMessage($thread, $data['content'], $user);

        $om = $this->getDoctrine()->getManager();
        $om->flush();

        return new JsonResponse(
            $serializer->serialize($messagingProvider->getMessageDTO($message), 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }
}
