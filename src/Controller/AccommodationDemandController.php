<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\AccommodationDemand;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationDemandVoterEnum;
use App\Form\AccommodationDemandFormType;
use App\Form\AccommodationDemandRefusingFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/accommodations_demands")
 */
class AccommodationDemandController extends BaseController
{
    /**
     * @Route(
     *     "/{id}/edit",
     *     name="accommodation_demand_edit",
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AccommodationDemandVoterEnum::EDIT, subject="accommodationDemand")
     */
    public function editDemand(
        Request $request,
        TranslatorInterface $translator,
        AccommodationDemand $accommodationDemand
    ): Response {
        $senior = $accommodationDemand->getSenior();
        $form = $this->createForm(AccommodationDemandFormType::class, $accommodationDemand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodationDemand);
            $em->flush();

            $this->addFlash('success', $translator->trans('accommodation_demand.edit.flash'));

            return $this->redirectToRoute('user_show', ['id' => $senior->getId()]);
        }

        return $this->render('account/profile_other.html.twig', [
            'form' => $form->createView(),
            'user' => $senior,
        ]);
    }

    /**
     * @Route(
     *     "/{id}/refuse",
     *     name="accommodation_demand_status_refused",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AccommodationDemandVoterEnum::RESPOND, subject="accommodationDemand")
     */
    public function refuseDemand(
        Request $request,
        TranslatorInterface $translator,
        AccommodationDemand $accommodationDemand
    ): Response {
        return $this->setAccommodationDemandStatus(
            $request,
            $translator,
            $accommodationDemand,
            AccommodationDemandStatusEnum::STATUS_REFUSED,
            'btn-danger'
        );
    }

    /**
     * @Route(
     *     "/{id}/accept",
     *     name="accommodation_demand_status_accepted",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AccommodationDemandVoterEnum::RESPOND, subject="accommodationDemand")
     */
    public function acceptDemand(
        Request $request,
        TranslatorInterface $translator,
        AccommodationDemand $accommodationDemand
    ): Response {
        return $this->setAccommodationDemandStatus(
            $request,
            $translator,
            $accommodationDemand,
            AccommodationDemandStatusEnum::STATUS_ACCEPTED,
        );
    }

    /**
     * @Route(
     *     "/{id}/abandon",
     *     name="accommodation_demand_status_abandoned",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AccommodationDemandVoterEnum::EDIT, subject="accommodationDemand")
     */
    public function abandonDemand(
        Request $request,
        TranslatorInterface $translator,
        AccommodationDemand $accommodationDemand
    ): Response {
        return $this->setAccommodationDemandStatus(
            $request,
            $translator,
            $accommodationDemand,
            AccommodationDemandStatusEnum::STATUS_ABANDONED,
            'btn-danger'
        );
    }

    private function setAccommodationDemandStatus(
        Request $request,
        TranslatorInterface $translator,
        AccommodationDemand $accommodationDemand,
        string $status,
        string $submitClass = null
    ): Response {
        switch ($status) {
            case AccommodationDemandStatusEnum::STATUS_REFUSED:
                $form = $this->createForm(AccommodationDemandRefusingFormType::class, $accommodationDemand);

                break;

            default:
                $form = $this->createFormBuilder()->getForm();

                break;
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $accommodationDemand->setStatus($status);

            $om = $this->getDoctrine()->getManager();
            $om->persist($accommodationDemand);
            $om->flush();

            $this->addFlash('success', $translator->trans('accommodation_demand.'.$status.'.flash'));

            return $this->redirectToRoute(
                'user_show',
                [
                    'id' => $accommodationDemand->getSenior()->getId(),
                ]
            );
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $accommodationDemand->getId(),
            'path' => 'accommodation_demand_status_'.$status,
            'approvalText' => $translator->trans("popup.accommodation_demand.status_{$status}.content"),
            'submitClass' => $submitClass,
        ]);
    }
}
