<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Association;
use App\Enum\AssociationTypeEnum;
use App\Enum\AssociationVoterEnum;
use App\Enum\UserRoleEnum;
use App\Form\AssociationFormType;
use App\Repository\AssociationRepository;
use App\Repository\UserRepository;
use App\Service\GrantedService;
use App\Service\LocationHelper;
use App\Tabulator\Response\AssociationListResponse;
use App\Tabulator\TabulatorModel;
use App\Tabulator\TabulatorResponseFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AssociationController extends BaseController
{
    /**
     * @Route("/admin/associations", name="association_list")
     */
    public function list(TranslatorInterface $translator): Response
    {
        $types = [];

        foreach (AssociationTypeEnum::getReadableValues() as $key => $value) {
            $types[$key] = $translator->trans($value);
        }

        return $this->render('association/list.html.twig', [
            'types' => $types,
            'booleanFilter' => [1 => $translator->trans('yes'), 0 => $translator->trans('no')],
        ]);
    }

    /**
     * @Route("/admin/associations/data", name="association_list_data", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function listData(
        AssociationRepository $associationRepo,
        TabulatorResponseFactory $responseFactory,
        TabulatorModel $tabulatorParams
    ): JsonResponse {
        return new JsonResponse([
            'data' => $responseFactory->getResponse(
                AssociationListResponse::class,
                $associationRepo->getListAssociations($tabulatorParams)
            ),
            'last_page' => ceil(
                $associationRepo->getNbListAssociations($tabulatorParams) / $tabulatorParams->getMaxResults()
            ),
        ]);
    }

    /**
     * @Route(
     *     "/associations/{id}",
     *     name="association_show",
     *     requirements={"id" : "\d+"},
     *     options={ "expose" : true }
     * )
     * @IsGranted(AssociationVoterEnum::SHOW, subject="association")
     */
    public function showAssociation(
        Association $association,
        GrantedService $grantedService,
        UserRepository $userRepo
    ): Response {
        $user = $this->getUser();
        $params = ['association' => $association];

        if (!$grantedService->isManager($user)) {
            $params['canManageUser'] = $userRepo->isViewableByAssociation($association, $user);
        }

        return $this->render('account/association.html.twig', $params);
    }

    /**
     * @Route(
     *     "/admin/associations/{id}/edit",
     *     name="association_edit",
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AssociationVoterEnum::EDIT, subject="association")
     */
    public function editAssociation(
        Request $request,
        TranslatorInterface $translator,
        Association $association
    ): Response {
        $form = $this->createForm(AssociationFormType::class, $association);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($association);
            $em->flush();

            $this->addFlash('success', $translator->trans('association.edit.flash'));

            return $this->redirectToRoute('association_show', ['id' => $association->getId()]);
        }

        return $this->render('account/association.html.twig', [
            'form' => $form->createView(),
            'association' => $association,
        ]);
    }

    /**
     * @Route("/admin/associations/new", name="association_new")
     */
    public function newAssociation(
        Request $request,
        TranslatorInterface $translator,
        LocationHelper $locationHelper
    ): Response {
        $association = new Association();

        if (!$this->isGranted(UserRoleEnum::ROLE_SUPER_ADMIN)) {
            $associationLocation = $association->getLocation();
            $userLocation = $this->getUser()->getLocation();

            $locationHelper->copyLocationInformation($userLocation, $associationLocation);
        }

        $this->denyAccessUnlessGranted(AssociationVoterEnum::EDIT, $association);

        $form = $this->createForm(AssociationFormType::class, $association);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($association);
            $em->flush();

            $this->addFlash('success', $translator->trans('association.new.flash'));

            return $this->redirectToRoute('association_show', ['id' => $association->getId()]);
        }

        return $this->render('account/association.html.twig', [
            'form' => $form->createView(),
            'association' => $association,
        ]);
    }

    /**
     * @Route(
     *     "/admin/associations/{id}/delete",
     *     name="association_delete",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AssociationVoterEnum::DELETE, subject="association")
     */
    public function deleteAssociation(
        Request $request,
        TranslatorInterface $translator,
        Association $association
    ): Response {
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($association);
            $om->flush();

            $this->addFlash('success', $translator->trans('association.deleted.flash'));

            return $this->redirectToRoute('association_list');
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $association->getId(),
            'path' => 'association_delete',
            'approvalText' => $translator->trans('popup.association.delete.content', ['%name%' => $association->getName()]),
            'submitText' => $translator->trans('actions.delete'),
            'submitClass' => 'btn-danger',
        ]);
    }

    /**
     * @Route("/association_suggestion", name="association_suggestion", options={ "expose" : true })
     */
    public function suggestAssociation(Request $request, TranslatorInterface $translator): Response
    {
        $association = new Association();
        $association->setActivated(false);

        $form = $this->createForm(AssociationFormType::class, $association, ['isSuggestion' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($association);
            $em->flush();

            $this->addFlash('success', $translator->trans('association.suggestion.flash'));

            return $this->redirectToRoute('homepage');
        }

        return $this->render('association/_suggestion_modal.html.twig', [
            'form' => $form->createView(),
            'association' => $association,
        ]);
    }
}
