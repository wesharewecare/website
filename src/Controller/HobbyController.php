<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Hobby;
use App\Form\HobbyType;
use App\Repository\HobbyRepository;
use App\Tabulator\Response\HobbyListResponse;
use App\Tabulator\TabulatorModel;
use App\Tabulator\TabulatorResponseFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/hobbies")
 */
class HobbyController extends BaseController
{
    private $defaultLocale;

    public function __construct(string $defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @Route(name="hobby_list")
     */
    public function index(): Response
    {
        return $this->render('hobby/list.html.twig');
    }

    /**
     * @Route("/data", name="hobby_list_data", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function listData(
        HobbyRepository $hobbyRepo,
        TabulatorResponseFactory $responseFactory,
        TabulatorModel $tabulatorParams
    ): JsonResponse {
        return new JsonResponse([
            'data' => $responseFactory->getResponse(
                HobbyListResponse::class,
                $hobbyRepo->getListHobbies($tabulatorParams)
            ),
            'last_page' => ceil(
                $hobbyRepo->getNbListHobbies($tabulatorParams) / $tabulatorParams->getMaxResults()
            ),
        ]);
    }

    /**
     * @Route("/new", name="hobby_new")
     */
    public function new(Request $request): Response
    {
        $hobby = new Hobby();
        $hobby->setTranslatableLocale($this->defaultLocale);

        $form = $this->createForm(HobbyType::class, $hobby);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($hobby);
            $om->flush();

            $this->addFlash('success', 'Hobby created');

            return $this->redirectToRoute('hobby_list');
        }

        return $this->render('account/hobby.html.twig', [
            'hobby' => $hobby,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="hobby_edit")
     */
    public function edit(Request $request, Hobby $hobby): Response
    {
        $om = $this->getDoctrine()->getManager();

        $hobby->setTranslatableLocale($this->defaultLocale);
        $om->refresh($hobby);

        $form = $this->createForm(HobbyType::class, $hobby);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om->persist($hobby);
            $om->flush();

            $this->addFlash('success', 'Hobby edited');

            return $this->redirectToRoute('hobby_list');
        }

        return $this->render('account/hobby.html.twig', [
            'hobby' => $hobby,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="hobby_delete", options={"expose" : true})
     */
    public function delete(Request $request, Hobby $hobby): Response
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($hobby);
            $om->flush();

            $this->addFlash('success', 'Hobby deleted');

            return $this->redirectToRoute('hobby_list');
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $hobby->getId(),
            'path' => 'hobby_delete',
            'approvalText' => 'Do you really want to delete the hobby "'.$hobby->getName().'"',
            'submitClass' => 'btn-danger',
        ]);
    }
}
