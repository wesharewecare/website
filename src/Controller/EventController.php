<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\Enum\EventStatusEnum;
use App\Enum\EventVoterEnum;
use App\Enum\ProfileGenderEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Form\EventType;
use App\Repository\AssociationRepository;
use App\Repository\EventRepository;
use App\Tabulator\Response\EventListResponse;
use App\Tabulator\TabulatorModel;
use App\Tabulator\TabulatorResponseFactory;
use libphonenumber\PhoneNumberUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventController extends BaseController
{
    /**
     * @Route("/admin/events", name="event_list")
     */
    public function index(TranslatorInterface $translator, AssociationRepository $associationRepo): Response
    {
        $status = [];

        foreach (EventStatusEnum::getReadableValues() as $key => $value) {
            $status[$key] = $translator->trans($value);
        }

        $associations = $associationRepo->getAssociationsLinkedToEvents();
        array_walk(
            $associations,
            function (&$association): void {
                $association = $association->getName();
            }
        );

        return $this->render('event/list.html.twig', [
            'status' => $status,
            'associations' => $associations,
        ]);
    }

    /**
     * @Route(
     *     "/admin/events/data",
     *     name="event_list_data",
     *     options={ "expose" : true },
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function listData(
        EventRepository $eventRepo,
        TabulatorResponseFactory $responseFactory,
        TabulatorModel $tabulatorParams
    ): JsonResponse {
        $association = $this->getUser()->getAssociation();

        return new JsonResponse([
            'data' => $responseFactory->getResponse(
                EventListResponse::class,
                $eventRepo->getListEvents($tabulatorParams, $association)
            ),
            'last_page' => ceil(
                $eventRepo->getNbListEvents($tabulatorParams, $association) / $tabulatorParams->getMaxResults()
            ),
        ]);
    }

    /**
     * @Route(
     *     "/events/{id}",
     *     name="event_show",
     *     requirements={"id" : "\d+"},
     *     options={ "expose" : true }
     * )
     */
    public function showEvent(Event $event, EventRepository $eventRepo, TranslatorInterface $translator): Response
    {
        $options = [
            'event' => $event,
            'isParticipating' => false,
        ];

        /** @var User $user */
        if ($user = $this->getUser()) {
            $roles = [
                UserRoleEnum::ROLE_YOUNG => $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_YOUNG)),
                UserRoleEnum::ROLE_SENIOR => $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_SENIOR)),
                UserRoleEnum::ROLE_ASSOCIATION => $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_ASSOCIATION)),
            ];

            if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
                $roles[UserRoleEnum::ROLE_ADMIN] = $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_ADMIN));
            }

            $genders = ProfileGenderEnum::getReadableValues();
            array_walk(
                $genders,
                function (&$gender) use ($translator): void {
                    $gender = $translator->trans($gender);
                }
            );

            $options['isParticipating'] = $event->isParticipating($user);
            $options['nbParticipantsSenior'] = $eventRepo->getNbParticipantsByRole($event, UserRoleEnum::ROLE_SENIOR);
            $options['nbParticipantsYoung'] = $eventRepo->getNbParticipantsByRole($event, UserRoleEnum::ROLE_YOUNG);
            $options['roles'] = $roles;
            $options['booleanFilter'] = [1 => $translator->trans('yes'), 0 => $translator->trans('no')];
            $options['genders'] = $genders;

            return $this->render('account/event.html.twig', $options);
        }

        return $this->render('home/event.html.twig', $options);
    }

    /**
     * @Route(
     *     "/events/{id}/toggle_participation",
     *     name="event_toggle_participation",
     *     requirements={"id" : "\d+"},
     *     options={ "expose" : true }
     * )
     * @IsGranted(EventVoterEnum::PARTICIPATE, subject="event")
     */
    public function toggleParticipationEvent(Request $request, TranslatorInterface $translator, Event $event): Response
    {
        $user = $this->getUser();
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($event->isParticipating($user)) {
            $user->removeEvent($event);
            $approvalText = 'popup.event.remove_participation.content';
            $flashText = 'event.participation_removed.flash';
            $submitText = 'event.remove_participation.button';
            $submitClass = 'btn-danger';
        } else {
            $user->addEvent($event);
            $approvalText = 'popup.event.add_participation.content';
            $flashText = 'event.participation_added.flash';
            $submitText = 'event.add_participation.button';
            $submitClass = 'btn-success';
            $event->setNewParticipantNotificationToSend(true);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($user);
            $om->flush();

            $this->addFlash('success', $translator->trans($flashText));

            return $this->redirectToRoute('event_show', ['id' => $event->getId()]);
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $event->getId(),
            'path' => 'event_toggle_participation',
            'approvalText' => $translator->trans($approvalText, ['%name%' => $event->getName()]),
            'submitText' => $translator->trans($submitText),
            'submitClass' => $submitClass,
        ]);
    }

    /**
     * @Route(
     *     "/admin/events/new",
     *     name="event_new"
     * )
     */
    public function new(Request $request, TranslatorInterface $translator): Response
    {
        $user = $this->getUser();

        $this->denyAccessUnlessGranted(UserVoterEnum::CREATE_EVENT, $user);

        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        $phoneNumber = $user->getProfile()->getPhoneNumber();

        $event = new Event();
        $event->setAssociation($user->getAssociation());
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($event);
            $om->flush();

            $this->addFlash('success', $translator->trans('event.created.flash'));

            return $this->redirectToRoute('event_show', ['id' => $event->getId()]);
        }

        return $this->render('account/event.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
            'email' => $user->getEmail(),
            'firstname' => $user->getFirstName(),
            'lastname' => $user->getLastName(),
            'phoneNumber_country' => $phoneNumber
                ? $phoneNumberUtil->getRegionCodeForNumber($phoneNumber)
                : null,
            'phoneNumber_number' => $phoneNumber
                ? $phoneNumber->getNationalNumber()
                : null,
        ]);
    }

    /**
     * @Route(
     *     "/admin/events/{id}/edit",
     *     name="event_edit",
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(EventVoterEnum::EDIT, subject="event")
     */
    public function edit(Request $request, TranslatorInterface $translator, Event $event): Response
    {
        $user = $this->getUser();
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        $phoneNumber = $user->getProfile()->getPhoneNumber();

        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($event);
            $om->flush();

            $this->addFlash('success', $translator->trans('event.edited.flash'));

            return $this->redirectToRoute('event_show', ['id' => $event->getId()]);
        }

        return $this->render('account/event.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
            'email' => $user->getEmail(),
            'firstname' => $user->getFirstName(),
            'lastname' => $user->getLastName(),
            'phoneNumber_country' => $phoneNumber
                ? $phoneNumberUtil->getRegionCodeForNumber($phoneNumber)
                : null,
            'phoneNumber_number' => $phoneNumber
                ? $phoneNumber->getNationalNumber()
                : null,
        ]);
    }

    /**
     * @Route(
     *     "/admin/events/{id}/delete",
     *     name="event_delete",
     *     requirements={"id" : "\d+"},
     *     options={"expose" : true}
     * )
     * @IsGranted(EventVoterEnum::DELETE, subject="event")
     */
    public function delete(Request $request, TranslatorInterface $translator, Event $event): Response
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($event);
            $om->flush();

            $this->addFlash('success', $translator->trans('event.deleted.flash'));

            return $this->redirectToRoute('event_list');
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $event->getId(),
            'path' => 'event_delete',
            'approvalText' => $translator->trans('popup.event.delete.content', ['%name%' => $event->getName()]),
            'submitText' => $translator->trans('actions.delete'),
            'submitClass' => 'btn-danger',
        ]);
    }

    /**
     * @Route(
     *     "/admin/events/{id}/open",
     *     name="event_status_open",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(EventVoterEnum::EDIT, subject="event")
     */
    public function openEvent(Request $request, TranslatorInterface $translator, Event $event): Response
    {
        return $this->setEventStatus(
            $request,
            $translator,
            $event,
            EventStatusEnum::STATUS_OPEN,
        );
    }

    /**
     * @Route(
     *     "/admin/events/{id}/draft",
     *     name="event_status_draft",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(EventVoterEnum::EDIT, subject="event")
     */
    public function draftEvent(Request $request, TranslatorInterface $translator, Event $event): Response
    {
        return $this->setEventStatus(
            $request,
            $translator,
            $event,
            EventStatusEnum::STATUS_DRAFT,
            'btn-warning'
        );
    }

    /**
     * @Route(
     *     "/admin/events/{id}/canceled",
     *     name="event_status_canceled",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(EventVoterEnum::EDIT, subject="event")
     */
    public function canceledEvent(Request $request, TranslatorInterface $translator, Event $event): Response
    {
        return $this->setEventStatus(
            $request,
            $translator,
            $event,
            EventStatusEnum::STATUS_CANCELED,
            'btn-danger'
        );
    }

    private function setEventStatus(
        Request $request,
        TranslatorInterface $translator,
        Event $event,
        string $status,
        string $submitClass = null
    ): Response {
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $event->setStatus($status);

            $om = $this->getDoctrine()->getManager();
            $om->persist($event);
            $om->flush();

            $this->addFlash('success', $translator->trans('event.'.$status.'.flash'));

            return $this->redirectToRoute('event_show', ['id' => $event->getId()]);
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $event->getId(),
            'path' => 'event_status_'.$status,
            'approvalText' => $translator->trans("popup.event.status_{$status}.content"),
            'submitClass' => $submitClass,
        ]);
    }
}
