<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\UserProfileTypeEnum;
use App\Form\Model\CreateUser;
use App\Form\RegistrationFlow;
use App\Repository\UserRepository;
use App\Security\Authentication\EmailVerifier;
use App\Service\MailerService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

/**
 * @Route("/register")
 */
class RegistrationController extends BaseController
{
    private $emailVerifier;
    private $registrationFlow;
    private $passwordHasher;
    private $mailerService;
    private $translator;

    public function __construct(
        EmailVerifier $emailVerifier,
        RegistrationFlow $registrationFlow,
        UserPasswordHasherInterface $passwordHasher,
        MailerService $mailerService,
        TranslatorInterface $translator
    ) {
        $this->emailVerifier = $emailVerifier;
        $this->registrationFlow = $registrationFlow;
        $this->passwordHasher = $passwordHasher;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
    }

    /**
     * @Route(name="register")
     */
    public function register(Request $request): Response
    {
        return $this->showRegistration($request, UserProfileTypeEnum::PROFILE_USER);
    }

    /**
     * @Route("/association-manager", name="register_manager")
     */
    public function registerAssociationManager(Request $request): Response
    {
        return $this->showRegistration($request, UserProfileTypeEnum::PROFILE_ASSOCIATION);
    }

    /**
     * @Route("/admin", name="register_admin")
     */
    public function registerAdmin(Request $request): Response
    {
        return $this->showRegistration($request, UserProfileTypeEnum::PROFILE_ADMIN);
    }

    /**
     * @Route("/verify/email", name="verify_email")
     */
    public function verifyUserEmail(
        Request $request,
        UserRepository $userRepo,
        TokenStorageInterface $tokenStorage,
        Session $session
    ): Response {
        try {
            if ($user = $userRepo->findOneBy(['email' => $request->query->get('email')])) {
                $this->emailVerifier->handleEmailConfirmation($request, $user);
                $token = new UsernamePasswordToken($user, 'main', $user->getRoles());
                $tokenStorage->setToken($token);
                $session->set('_security_main', serialize($token));
            } else {
                return $this->redirectToRoute('homepage');
            }
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('register');
        }

        return $this->redirectToRoute('dashboard');
    }

    private function showRegistration(Request $request, string $mode): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('dashboard');
        }

        $createUser = new CreateUser();
        $createUser->getProfile()->setLocaleLanguage($request->getLocale());

        $this->registrationFlow->setGenericFormOptions(['mode' => $mode]);
        $this->registrationFlow->bind($createUser);
        $form = $this->registrationFlow->createForm();

        if ($this->registrationFlow->isValid($form)) {
            $this->registrationFlow->saveCurrentStepData($form);

            if ($this->registrationFlow->nextStep()) {
                $form = $this->registrationFlow->createForm();
            } else {
                $user = $createUser->getUser();

                $user->setPassword(
                    $this->passwordHasher->hashPassword(
                        $user,
                        $user->getPlainPassword()
                    )
                );

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->registrationFlow->reset();

                $this->emailVerifier->sendEmailConfirmation(
                    'verify_email',
                    $user,
                    $this->mailerService->getEmailConfirmation($user->getEmail())
                );

                $this->addFlash('success', $this->translator->trans('registration.check_email.flash'));

                return $this->redirectToRoute('homepage');
            }
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'flow' => $this->registrationFlow,
            'mode' => $mode,
        ]);
    }
}
