<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Enum\EventStatusEnum;
use App\Enum\UserRoleEnum;
use App\Form\ContactFormType;
use App\Form\LocationFormType;
use App\Repository\AccommodationRepository;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    /**
     * @Route(name="homepage")
     */
    public function index(
        UserRepository $userRepo,
        AccommodationRepository $accommodationRepo,
        EventRepository $eventRepo
    ): Response {
        $nbUsers = $userRepo->getNbUsers(UserRoleEnum::$users);
        $nbEvents = $eventRepo->getNbEvents();
        $nbAccommodations = $accommodationRepo->getNbAccommodations();

        return $this->render('home/index.html.twig', [
            'nbUsers' => $nbUsers,
            'nbEvents' => $nbEvents,
            'nbAccommodations' => $nbAccommodations,
        ]);
    }

    /**
     * @Route("/the-project", name="project")
     * @Route("/context", name="context")
     */
    public function context(): Response
    {
        return $this->render('home/context.html.twig');
    }

    /**
     * @Route("/our-initiative", name="initiative")
     * @Route("/our-solutions", name="solutions")
     */
    public function solutions(): Response
    {
        return $this->render('home/solutions.html.twig');
    }

    /**
     * @Route("/map", name="map")
     */
    public function map(): Response
    {
        $params = [
            'location' => ['lat' => MapController::LAT_NOT_AUTHENTICATE, 'lng' => MapController::LNG_NOT_AUTHENTICATE],
            'zoom' => MapController::ZOOM_NOT_AUTHENTICATE,
            'maxZoom' => MapController::ZOOM_MAX_NOT_AUTHENTICATE,
        ];

        /** @var User $user */
        if ($user = $this->getUser()) {
            $userLocation = $user->getLocation();

            $params = [
                'location' => ['lat' => $userLocation->getLatitude(), 'lng' => $userLocation->getLongitude()],
                'zoom' => MapController::ZOOM_AUTHENTICATE,
                'maxZoom' => MapController::ZOOM_MAX_AUTHENTICATE,
            ];
        }

        return $this->render('home/map.html.twig', $params);
    }

    /**
     * @Route("/participate", name="participate")
     */
    public function participate(): Response
    {
        $form = $this->createForm(LocationFormType::class);

        return $this->render('home/participate.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/faq", name="faq")
     */
    public function faq(): Response
    {
        return $this->render('home/faq.html.twig');
    }

    /**
     * @Route("/cgu", name="cgu")
     */
    public function cgu(): Response
    {
        return $this->render('home/cgu.html.twig');
    }

    /**
     * @Route("/cookie_policy", name="cookie")
     */
    public function cookiePolicy(): Response
    {
        return $this->render('home/cookie_policy.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, MailerService $mailer): Response
    {
        $form = $this->createForm(ContactFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mailer->sendContactFormEmail($form->get('email')->getData(), $form->get('body')->getData());
        }

        return $this->render('home/contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/events", name="events")
     */
    public function events(EventRepository $eventRepo): Response
    {
        return $this->render('home/events.html.twig', [
            'events' => $eventRepo->findBy(['status' => EventStatusEnum::STATUS_OPEN]),
        ]);
    }

    /**
     * @Route("/doc/{filename}", name="download_file", requirements={"filename" : ".+\.pdf"})
     */
    public function downloadFile(string $filename): Response
    {
        return $this->file('documents/'.$filename);
    }
}
