<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Accommodation;
use App\Entity\AccommodationDemand;
use App\Enum\AccommodationStatusEnum;
use App\Enum\AccommodationTypeEnum;
use App\Enum\AccommodationVoterEnum;
use App\Enum\UserRoleEnum;
use App\Form\AccommodationDemandFormType;
use App\Repository\AccommodationRepository;
use App\Tabulator\Response\AccommodationListResponse;
use App\Tabulator\TabulatorModel;
use App\Tabulator\TabulatorResponseFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccommodationController extends BaseController
{
    /**
     * @Route("/admin/accommodations", name="accommodation_list")
     */
    public function list(TranslatorInterface $translator): Response
    {
        $types = [];
        $status = [];

        foreach (AccommodationTypeEnum::getReadableValues() as $key => $value) {
            $types[$key] = $translator->trans($value);
        }

        foreach (AccommodationStatusEnum::getReadableValues() as $key => $value) {
            $status[$key] = $translator->trans($value);
        }

        return $this->render('accommodation/list.html.twig', [
            'types' => $types,
            'status' => $status,
            'booleanFilter' => [1 => $translator->trans('yes'), 0 => $translator->trans('no')],
        ]);
    }

    /**
     * @Route("/admin/accommodations/data", name="accommodation_list_data", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function listData(
        AccommodationRepository $accommodationRepo,
        TabulatorResponseFactory $responseFactory,
        TabulatorModel $tabulatorParams
    ): JsonResponse {
        $association = $this->getUser()->getAssociation();

        return new JsonResponse([
            'data' => $responseFactory->getResponse(
                AccommodationListResponse::class,
                $accommodationRepo->getListAccommodations($tabulatorParams, $association)
            ),
            'last_page' => ceil(
                $accommodationRepo->getNbListAccommodations($tabulatorParams, $association) / $tabulatorParams->getMaxResults()
            ),
        ]);
    }

    /**
     * @Route("/accommodations/{id}/demands/new", name="accommodation_demand_new", requirements={"id" : "\d+"})
     * @IsGranted(UserRoleEnum::ROLE_YOUNG)
     * @IsGranted(AccommodationVoterEnum::CREATE_DEMAND, subject="accommodation")
     */
    public function newDemand(Request $request, TranslatorInterface $translator, Accommodation $accommodation): Response
    {
        $senior = $accommodation->getSenior();

        $accommodationDemand = new AccommodationDemand();
        $accommodationDemand->setYoung($this->getUser());
        $accommodation->addAccommodationDemand($accommodationDemand);

        $form = $this->createForm(AccommodationDemandFormType::class, $accommodationDemand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodationDemand);
            $em->flush();

            $this->addFlash('success', $translator->trans('accommodation_demand.create.flash'));

            return $this->redirectToRoute('user_show', ['id' => $senior->getId()]);
        }

        return $this->render('account/profile_other.html.twig', [
            'form' => $form->createView(),
            'user' => $senior,
        ]);
    }

    /**
     * @Route(
     *     "/accommodations/{id}/open",
     *     name="accommodation_status_open",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AccommodationVoterEnum::APPROVE, subject="accommodation")
     */
    public function openAccommodation(
        Request $request,
        TranslatorInterface $translator,
        Accommodation $accommodation
    ): Response {
        return $this->setAccommodationStatus(
            $request,
            $translator,
            $accommodation,
            AccommodationStatusEnum::STATUS_OPEN,
            $this->isGranted(UserRoleEnum::ROLE_SENIOR)
                ? AccommodationStatusEnum::STATUS_AWAITING_APPROVAL
                : null,
        );
    }

    /**
     * @Route(
     *     "/admin/accommodations/{id}/close",
     *     name="accommodation_status_closed",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(AccommodationVoterEnum::CLOSE, subject="accommodation")
     */
    public function closeAccommodation(
        Request $request,
        TranslatorInterface $translator,
        Accommodation $accommodation
    ): Response {
        return $this->setAccommodationStatus(
            $request,
            $translator,
            $accommodation,
            AccommodationStatusEnum::STATUS_CLOSED,
            null,
            'btn-danger'
        );
    }

    private function setAccommodationStatus(
        Request $request,
        TranslatorInterface $translator,
        Accommodation $accommodation,
        string $status,
        string $newStatus = null,
        string $submitClass = null
    ): Response {
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $accommodation->setStatus($newStatus ?? $status);

            $om = $this->getDoctrine()->getManager();
            $om->persist($accommodation);
            $om->flush();

            $this->addFlash('success', $translator->trans('accommodation.'.$status.'.flash'));

            return $this->redirectToRoute('user_show', ['id' => $accommodation->getSenior()->getId()]);
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $accommodation->getId(),
            'path' => 'accommodation_status_'.$status,
            'approvalText' => $translator->trans("popup.accommodation.status_{$status}.content"),
            'submitClass' => $submitClass,
        ]);
    }
}
