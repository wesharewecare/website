<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Accommodation;
use App\Entity\User;
use App\Enum\AccommodationVoterEnum;
use App\Enum\ProfileGenderEnum;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Form\AccommodationFormType;
use App\Form\Model\EditUser;
use App\Form\UserFormType;
use App\Repository\AccommodationDemandRepository;
use App\Repository\AssociationRepository;
use App\Repository\UserRepository;
use App\Service\GrantedService;
use App\Service\ImageService;
use App\Service\LocationHelper;
use App\Tabulator\Response\UserListResponse;
use App\Tabulator\TabulatorModel;
use App\Tabulator\TabulatorResponseFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tbbc\MoneyBundle\Pair\PairManagerInterface;

class UserController extends BaseController
{
    /**
     * @Route("/admin/users", name="user_list")
     */
    public function list(TranslatorInterface $translator, AssociationRepository $associationRepo): Response
    {
        $roles = [
            UserRoleEnum::ROLE_YOUNG => $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_YOUNG)),
            UserRoleEnum::ROLE_SENIOR => $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_SENIOR)),
            UserRoleEnum::ROLE_ASSOCIATION => $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_ASSOCIATION)),
        ];

        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $roles[UserRoleEnum::ROLE_ADMIN] = $translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_ADMIN));
        }

        $associations = $associationRepo->getAssociationsLinkedToUsers();
        array_walk(
            $associations,
            function (&$association): void {
                $association = $association->getName();
            }
        );

        $genders = ProfileGenderEnum::getReadableValues();
        array_walk(
            $genders,
            function (&$gender) use ($translator): void {
                $gender = $translator->trans($gender);
            }
        );

        return $this->render('user/list.html.twig', [
            'roles' => $roles,
            'booleanFilter' => [1 => $translator->trans('yes'), 0 => $translator->trans('no')],
            'associations' => $associations,
            'genders' => $genders,
        ]);
    }

    /**
     * @Route("/admin/users/data", name="user_list_data", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function listData(
        Request $request,
        UserRepository $userRepo,
        TabulatorResponseFactory $responseFactory,
        TabulatorModel $tabulatorParams
    ): JsonResponse {
        $association = $this->getUser()->getAssociation();

        if ($request->query->has('event')) {
            $tabulatorParams->addSearchParam('event', '=', (string) $request->query->get('event'));
        }

        return new JsonResponse([
            'data' => $responseFactory->getResponse(
                UserListResponse::class,
                $userRepo->getListUsers($tabulatorParams, $association)
            ),
            'last_page' => ceil(
                $userRepo->getNbListUsers($tabulatorParams, $association) / $tabulatorParams->getMaxResults()
            ),
        ]);
    }

    /**
     * @Route(
     *     "/admin/users/new",
     *     name="user_new"
     * )
     */
    public function newUser(
        Request $request,
        TranslatorInterface $translator,
        UserPasswordHasherInterface $passwordHasher,
        LocationHelper $locationHelper
    ): Response {
        $currentUser = $this->getUser();

        $this->denyAccessUnlessGranted(UserVoterEnum::CREATE_SENIOR_ACCOUNT, $currentUser);

        $newUser = new User();
        $newUser->getProfile()->setLocaleLanguage($request->getLocale());
        $newUser->setRoles([UserRoleEnum::ROLE_SENIOR]);
        $newUser->setIsVerified(true);
        $createUser = new EditUser($newUser);

        if ($association = $currentUser->getAssociation()) {
            $createUser->getUser()->setAssociation($association);
        }

        if (!$this->isGranted(UserRoleEnum::ROLE_SUPER_ADMIN)) {
            $locationHelper->copyLocationInformation($currentUser->getLocation(), $createUser->getLocation());
        }

        $form = $this->createForm(
            UserFormType::class,
            $createUser,
            [
                'mode' => UserProfileTypeEnum::PROFILE_USER,
                'password_field' => true,
                'is_external_edit' => true,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newUser = $createUser->getUser();

            if ($newUser->getPlainPassword()) {
                $newUser->setPassword(
                    $passwordHasher->hashPassword(
                        $newUser,
                        $newUser->getPlainPassword()
                    )
                );
            }

            $om = $this->getDoctrine()->getManager();
            $om->persist($newUser);
            $om->flush();

            $this->addFlash('success', $translator->trans('profile.create.flash'));

            return $this->redirectToRoute('user_show', ['id' => $newUser->getId()]);
        }

        return $this->render('user/new.html.twig', [
            'form' => $form->createView(),
            'user' => $newUser,
        ]);
    }

    /**
     * @Route(
     *     "/users/{id}",
     *     name="user_show",
     *     requirements={"id" : "\d+"},
     *     options={ "expose" : true }
     * )
     * @IsGranted(UserVoterEnum::SHOW, subject="user")
     */
    public function showUser(
        AccommodationDemandRepository $accommodationDemandRepo,
        PairManagerInterface $pairManager,
        User $user
    ): Response {
        $currentUser = $this->getUser();
        if ($currentUser === $user) {
            return $this->redirectToRoute('profile');
        }

        $params = ['user' => $user];
        $showHomesharing = $currentUser->getProfile()->isShowHomesharing();

        if ($this->isGranted(UserRoleEnum::ROLE_YOUNG)) {
            if ($showHomesharing && $accommodation = $user->getAccommodation()) {
                if ($accommodationDemand = $accommodationDemandRepo->getExistingDemand($currentUser, $accommodation)) {
                    $params['accommodationDemands'] = [$accommodationDemand];
                }

                $preferredCurrency = $currentUser->getProfile()->getPreferredCurrency();
                if ($preferredCurrency && $preferredCurrency !== $accommodation->getCurrencyCode()) {
                    $params['accommodationPriceConversion'] = $pairManager->convert($accommodation->getPrice(), $preferredCurrency);
                }
            }
        } elseif ($this->isGranted(UserRoleEnum::ROLE_SENIOR)) {
            $accommodation = $currentUser->getAccommodation();
            if ($showHomesharing
                && $accommodation
                && $accommodationDemand = $accommodationDemandRepo->getExistingDemand($user, $accommodation)
            ) {
                $params['accommodationDemands'] = [$accommodationDemand];
            }
        } else {
            $params['accommodationDemands'] = $accommodationDemandRepo->getViewable(
                $user,
                $currentUser->getAssociation()
            );
        }

        return $this->render('account/profile_other.html.twig', $params);
    }

    /**
     * @Route(
     *     "/admin/users/{id}/edit",
     *     name="user_edit",
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(UserVoterEnum::EDIT, subject="user")
     */
    public function editUser(
        Request $request,
        GrantedService $grantedService,
        TranslatorInterface $translator,
        User $user
    ): Response {
        if ($this->getUser() === $user) {
            return $this->redirectToRoute('profile');
        }

        $editUser = new EditUser($user);
        $form = $this->createForm(
            UserFormType::class,
            $editUser,
            [
                'mode' => $grantedService->getProfileMode($user),
                'is_external_edit' => true,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($editUser->getUser());
            $om->flush();

            $this->addFlash('success', $translator->trans('profile.edit.flash'));

            return $this->redirectToRoute('user_show', ['id' => $user->getId()]);
        }

        return $this->render('account/profile_other.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/admin/users/{id}/accommodation", name="user_accommodation_edit", requirements={"id" : "\d+"})
     */
    public function editAccommodation(Request $request, TranslatorInterface $translator, User $user): Response
    {
        if ($accommodation = $user->getAccommodation()) {
            $this->denyAccessUnlessGranted(AccommodationVoterEnum::EDIT, $accommodation);
        } else {
            $this->denyAccessUnlessGranted(UserVoterEnum::CREATE_ACCOMMODATION, $user);
            $accommodation = new Accommodation();
            $accommodation->setSenior($user);
        }

        $form = $this->createForm(AccommodationFormType::class, $accommodation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodation);
            $em->flush();

            $this->addFlash('success', $translator->trans('accommodation.edit.flash'));

            return $this->redirectToRoute('user_show', ['id' => $user->getId()]);
        }

        return $this->render('account/profile_other.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route(
     *     "/admin/users/{id}/approve",
     *     name="user_approval",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(UserVoterEnum::APPROVE, subject="userToApprove")
     */
    public function approveUser(Request $request, TranslatorInterface $translator, User $userToApprove): Response
    {
        $association = $this->getUser()->getAssociation();
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userToApprove->getProfile()->setIsApproved(true);

            if ($association) {
                $userToApprove->setAssociation($association);
            }

            $om = $this->getDoctrine()->getManager();
            $om->persist($userToApprove);
            $om->flush();

            $this->addFlash('success', $translator->trans('user.approved.flash', ['%email%' => $userToApprove->getFullName()]));

            return $this->redirectToRoute('user_show', ['id' => $userToApprove->getId()]);
        }

        $approvalText = $translator->trans('popup.user.approve.content', ['%email%' => $userToApprove->getFullName()]);

        /*if ($association) {
            $approvalText .= "\n\n".$translator->trans('popup.user.approve.content.association_warning');
        }*/

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $userToApprove->getId(),
            'path' => 'user_approval',
            'approvalText' => $approvalText,
        ]);
    }

    /**
     * @Route(
     *     "/users/{id}/delete",
     *     name="user_delete",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(UserVoterEnum::DELETE, subject="userToDelete")
     */
    public function deleteUser(Request $request, TranslatorInterface $translator, User $userToDelete): Response
    {
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($userToDelete);
            $om->flush();

            $this->addFlash('success', $translator->trans('user.deleted.flash'));

            return $this->redirectToRoute('user_list');
        }

        $approvalText = $this->getUser() === $userToDelete
            ? $translator->trans('popup.user.self_delete.content')
            : $translator->trans('popup.user.delete.content', ['%email%' => $userToDelete->getFullName()])
        ;

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $userToDelete->getId(),
            'path' => 'user_delete',
            'approvalText' => $approvalText,
            'submitText' => $translator->trans('actions.delete'),
            'submitClass' => 'btn-danger',
        ]);
    }

    /**
     * @Route(
     *     "/admin/users/{id}/claim_management",
     *     name="user_claim_management",
     *     options={"expose" : true},
     *     requirements={"id" : "\d+"}
     * )
     * @IsGranted(UserVoterEnum::CLAIM_MANAGEMENT, subject="userToClaim")
     */
    public function claimUserManagement(Request $request, TranslatorInterface $translator, User $userToClaim): Response
    {
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userToClaim->setAssociation($this->getUser()->getAssociation());

            $om = $this->getDoctrine()->getManager();
            $om->persist($userToClaim);
            $om->flush();

            $this->addFlash('success', $translator->trans('user.claim_management.flash', ['%email%' => $userToClaim->getFullName()]));

            return $this->redirectToRoute('user_show', ['id' => $userToClaim->getId()]);
        }

        return $this->render('default/_popup_content_approval.html.twig', [
            'form' => $form->createView(),
            'id' => $userToClaim->getId(),
            'path' => 'user_claim_management',
            'approvalText' => $translator->trans('popup.user.claim_management.content', ['%email%' => $userToClaim->getFullName()]),
        ]);
    }

    /**
     * @Route("/user-suggest", name="user_suggest", options={ "expose" : true })
     */
    public function userSuggest(UserRepository $userRepo, ImageService $imageService, Request $request): JsonResponse
    {
        $user = $this->getUser();
        $query = $request->query->get('search', '');
        $users = $userRepo->getUsersSearchAutocomplete(
            (string) $query,
            $user,
            8
        );
        $data = [];

        foreach ($users as $user) {
            $data[] = [
                'email' => $user->getEmail(),
                'fullname' => $user->getFullName(),
                'image' => $imageService->getAvatar($user),
            ];
        }

        return new JsonResponse($data, 200, [
            'Cache-Control' => 'no-cache',
        ]);
    }
}
