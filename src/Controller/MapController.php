<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\MapMarker;
use App\Entity\Accommodation;
use App\Entity\Association;
use App\Entity\Event;
use App\Entity\Location;
use App\Entity\User;
use App\Enum\AccommodationStatusEnum;
use App\Enum\EventStatusEnum;
use App\Enum\UserRoleEnum;
use App\Repository\AccommodationRepository;
use App\Repository\AssociationRepository;
use App\Repository\EventRepository;
use App\Service\GrantedService;
use App\Service\ImageService;
use App\Service\LocaleManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/api")
 */
class MapController extends BaseController
{
    public const ZOOM_AUTHENTICATE = 9;
    public const ZOOM_NOT_AUTHENTICATE = 5;
    public const ZOOM_MAX_AUTHENTICATE = 16;
    public const ZOOM_MAX_NOT_AUTHENTICATE = 14;
    public const LAT_NOT_AUTHENTICATE = 46.948002;
    public const LNG_NOT_AUTHENTICATE = 7.448130;
    public const RADIUS_KMS = 200;

    private $localeManager;
    private $imageService;
    private $translator;
    private $grantedService;

    public function __construct(
        LocaleManager $localeManager,
        ImageService $imageService,
        TranslatorInterface $translator,
        GrantedService $grantedService
    ) {
        $this->localeManager = $localeManager;
        $this->imageService = $imageService;
        $this->translator = $translator;
        $this->grantedService = $grantedService;
    }

    /**
     * @Route(
     *     "/accommodations",
     *     methods={"GET"},
     *     name="map_api_get_accommodations",
     *     options={ "expose" : true }
     * )
     */
    public function getAccommodations(
        Request $request,
        AccommodationRepository $accommodationRepo,
        SerializerInterface $serializer
    ): JsonResponse {
        $user = $this->getUser();
        $accommodations = [];
        $statusToDisplay = [AccommodationStatusEnum::STATUS_OPEN, AccommodationStatusEnum::STATUS_OCCUPIED];

        if ($user) {
            $role = $this->grantedService->getRole($user);
            if (UserRoleEnum::ROLE_SENIOR !== $role) {
                $accommodations = $accommodationRepo->getByDistance(
                    $this->getLocation($request, $user),
                    $this->getRadiusKm($request),
                    !\in_array($role, UserRoleEnum::$managerAndAdmin, true)
                        ? $statusToDisplay
                        : [],
                    UserRoleEnum::ROLE_ASSOCIATION === $role
                        ? $user->getAssociation()
                        : null,
                    UserRoleEnum::ROLE_ADMIN === $role
                        ? $user->getLocation()->getCountryCode()
                        : null,
                );
            }
        } else {
            $accommodations = $accommodationRepo->getByStatus($statusToDisplay);
        }

        $data = [];
        foreach ($accommodations as $accommodation) {
            if ($mapMaker = $this->getMapMarkerDTO($accommodation, $user)) {
                $data[] = $mapMaker;
            }
        }

        return new JsonResponse($serializer->serialize($data, 'json'), Response::HTTP_OK, [], true);
    }

    /**
     * @Route(
     *     "/events",
     *     methods={"GET"},
     *     name="map_api_get_events",
     *     options={ "expose" : true }
     * )
     */
    public function getEvents(
        Request $request,
        EventRepository $eventRepo,
        SerializerInterface $serializer
    ): JsonResponse {
        $user = $this->getUser();
        $statusToDisplay = [EventStatusEnum::STATUS_OPEN];

        if ($user) {
            $role = $this->grantedService->getRole($user);

            $events = $eventRepo->getByDistance(
                $this->getLocation($request, $user),
                $this->getRadiusKm($request),
                !\in_array($role, UserRoleEnum::$managerAndAdmin, true)
                    ? $statusToDisplay
                    : [],
                UserRoleEnum::ROLE_ASSOCIATION === $role
                    ? $user->getAssociation()
                    : null,
                UserRoleEnum::ROLE_ADMIN === $role
                    ? $user->getLocation()->getCountryCode()
                    : null,
            );
        } else {
            $events = $eventRepo->getByStatus($statusToDisplay);
        }

        $data = [];
        foreach ($events as $event) {
            if ($mapMaker = $this->getMapMarkerDTO($event, $user)) {
                $data[] = $mapMaker;
            }
        }

        return new JsonResponse($serializer->serialize($data, 'json'), Response::HTTP_OK, [], true);
    }

    /**
     * @Route(
     *     "/associations",
     *     methods={"GET"},
     *     name="map_api_get_associations",
     *     options={ "expose" : true }
     * )
     */
    public function getAssociations(
        Request $request,
        AssociationRepository $associationRepo,
        SerializerInterface $serializer
    ): JsonResponse {
        $user = $this->getUser();

        if ($user) {
            $associations = $associationRepo->getByDistance(
                $this->getLocation($request, $user),
                $this->getRadiusKm($request),
                UserRoleEnum::ROLE_ADMIN === $this->grantedService->getRole($user)
                    ? $user->getLocation()->getCountryCode()
                    : null,
            );
        } else {
            $associations = $associationRepo->findBy(['activated' => true]);
        }

        $data = [];
        foreach ($associations as $association) {
            if ($mapMaker = $this->getMapMarkerDTO($association, $user)) {
                $data[] = $mapMaker;
            }
        }

        return new JsonResponse($serializer->serialize($data, 'json'), Response::HTTP_OK, [], true);
    }

    /**
     * @Route(
     *     "/associations-from-location",
     *     methods={"GET"},
     *     name="map_api_get_associations_from_location",
     *     options={ "expose" : true }
     * )
     */
    public function getAssociationsInArea(
        Request $request,
        AssociationRepository $associationRepo,
        SerializerInterface $serializer
    ): JsonResponse {
        $associations = $associationRepo->getAssociationsReachingLocation($this->getLocation($request));

        $data = [];
        /** @var Association $association */
        foreach ($associations as $association) {
            $data[] = [
                'id' => $association->getId(),
                'name' => $association->getName(),
                'platformUse' => $association->getPlatformUse(),
                'logo' => $this->imageService->getAssociationLogo($association),
                'city' => $association->getLocation()->getCity().' ('.$association->getLocation()->getZipCode().')',
                'website' => $association->getWebsite(),
            ];
        }

        return new JsonResponse($serializer->serialize($data, 'json'), Response::HTTP_OK, [], true);
    }

    private function getMapMarkerDTO(object $object, UserInterface $user = null): ?MapMarker
    {
        if ($object instanceof Accommodation) {
            $dateFormatter = \IntlDateFormatter::create(
                $this->localeManager->getCurrentLocale(),
                \IntlDateFormatter::MEDIUM,
                \IntlDateFormatter::NONE
            );

            $senior = $object->getSenior();
            $location = $senior->getLocation();

            return new MapMarker(
                $senior->getId(),
                $senior->getFirstname(),
                $user ? $this->imageService->getAvatar($senior) : $this->imageService->getDefaultAvatar(),
                $user ? $this->imageService->getAvatar($senior) : $this->imageService->getDefaultAvatar(),
                $location->getCity().' ('.$location->getZipCode().')',
                $location->getLatitude(),
                $location->getLongitude(),
                MapMarker::TYPE_ACCOMMODATION,
                $senior->getProfile()->getDescription(),
                $object->getStartDate()
                    ? '<i class="fas fa-calendar-alt"></i> '.$this->translator->trans('accommodation.start_date', ['%date%' => $dateFormatter->format($object->getStartDate())])
                    : null,
                '<div class="status '.(AccommodationStatusEnum::STATUS_OPEN === $object->getStatus() ? 'accepted' : 'declined').'">'.$this->translator->trans(AccommodationStatusEnum::getReadableValue($object->getStatus())).'</div>'
            );
        }

        if ($object instanceof Association) {
            $location = $object->getLocation();

            return new MapMarker(
                $object->getId(),
                $object->getName(),
                $this->imageService->getAssociationLogo($object),
                $this->imageService->getDefaultAssociationLogo(),
                $location->getCity().' ('.$location->getZipCode().')',
                $location->getLatitude(),
                $location->getLongitude(),
                MapMarker::TYPE_ASSOCIATION,
                '<a href="'.$object->getWebsite().'" target="_blank" class="link">'.$object->getWebsite().'</a>'
            );
        }

        if ($object instanceof Event) {
            $dateFormatter = \IntlDateFormatter::create(
                $this->localeManager->getCurrentLocale(),
                \IntlDateFormatter::MEDIUM,
                \IntlDateFormatter::SHORT
            );

            $location = $object->getLocation();

            return new MapMarker(
                $object->getId(),
                $object->getName(),
                $this->imageService->getEventImage($object),
                $this->imageService->getEventImage($object),
                $location->getCity().' ('.$location->getZipCode().')',
                $location->getLatitude(),
                $location->getLongitude(),
                MapMarker::TYPE_EVENT,
                $object->getDescription(),
                '<i class="fas fa-calendar-alt"></i> '.$dateFormatter->format($object->getDate()),
                $user && $object->getUsers()->contains($user)
                    ? '<div class="status accepted">'.$this->translator->trans('event.participation.tag').'</div>'
                    : null,
            );
        }

        return null;
    }

    private function getLocation(Request $request, User $user = null): Location
    {
        if ($request->query->has('lat') && $request->query->has('lng')) {
            $location = new Location();
            $location->setLatitude((float) ($request->query->get('lat')));
            $location->setLongitude((float) ($request->query->get('lng')));

            return $location;
        }

        return $user->getLocation();
    }

    private function getRadiusKm(Request $request): int
    {
        $defaultZoom = self::ZOOM_AUTHENTICATE;
        $radius = self::RADIUS_KMS;

        if ($request->query->has('zoom')) {
            $zoom = (int) ($request->query->get('zoom'));

            if ($zoom < $defaultZoom) {
                for ($i = $zoom; $i < $defaultZoom; ++$i) {
                    $radius = $radius + self::RADIUS_KMS;
                }
            }
        }

        return $radius;
    }
}
