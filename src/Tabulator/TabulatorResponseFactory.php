<?php

declare(strict_types=1);

namespace App\Tabulator;

use App\Service\GrantedService;
use App\Service\ImageService;
use App\Service\LocaleManager;
use Doctrine\ORM\EntityManagerInterface;
use Misd\PhoneNumberBundle\Templating\Helper\PhoneNumberHelper;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tbbc\MoneyBundle\Formatter\MoneyFormatter;
use Twig\Environment;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class TabulatorResponseFactory
{
    private $em;
    private $translator;
    private $security;
    private $router;
    private $templating;
    private $uploaderHelper;
    private $formFactory;
    private $grantedService;
    private $imageService;
    private $localeManager;
    private $phoneNumberHelper;
    private $moneyFormatter;
    private $defaultLocale;

    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        Security $security,
        UrlGeneratorInterface $router,
        Environment $templating,
        UploaderHelper $uploaderHelper,
        FormFactoryInterface $formFactory,
        GrantedService $grantedService,
        ImageService $imageService,
        LocaleManager $localeManager,
        PhoneNumberHelper $phoneNumberHelper,
        MoneyFormatter $moneyFormatter,
        string $defaultLocale
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->security = $security;
        $this->router = $router;
        $this->templating = $templating;
        $this->uploaderHelper = $uploaderHelper;
        $this->formFactory = $formFactory;
        $this->grantedService = $grantedService;
        $this->imageService = $imageService;
        $this->localeManager = $localeManager;
        $this->phoneNumberHelper = $phoneNumberHelper;
        $this->moneyFormatter = $moneyFormatter;
        $this->defaultLocale = $defaultLocale;
    }

    public function create(string $type): TabulatorResponseInterface
    {
        return new $type(
            $this->em,
            $this->translator,
            $this->security,
            $this->router,
            $this->templating,
            $this->uploaderHelper,
            $this->formFactory,
            $this->grantedService,
            $this->imageService,
            $this->localeManager,
            $this->phoneNumberHelper,
            $this->moneyFormatter,
            $this->defaultLocale
        );
    }

    public function createTabulatorResponse(string $type, $entity, array $options = []): array
    {
        if (class_exists($type)) {
            $interfaces = class_implements($type);

            if (isset($interfaces[TabulatorResponseInterface::class])) {
                $response = $this->create($type);
                $response->configure($entity, $options);

                return $response->getArrayResponse();
            }
        }

        return [];
    }

    public function getResponse(string $type, array $entities, array $options = []): array
    {
        $response = [];

        foreach ($entities as $entity) {
            $response[] = $this->createTabulatorResponse($type, $entity, $options);
        }

        return $response;
    }
}
