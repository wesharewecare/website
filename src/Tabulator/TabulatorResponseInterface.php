<?php

declare(strict_types=1);

namespace App\Tabulator;

interface TabulatorResponseInterface
{
    public function configure($entity, array $options): void;

    public function getArrayResponse(): array;
}
