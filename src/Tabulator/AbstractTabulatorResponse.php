<?php

declare(strict_types=1);

namespace App\Tabulator;

use App\Service\GrantedService;
use App\Service\ImageService;
use App\Service\LocaleManager;
use Doctrine\ORM\EntityManagerInterface;
use Misd\PhoneNumberBundle\Templating\Helper\PhoneNumberHelper;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tbbc\MoneyBundle\Formatter\MoneyFormatter;
use Twig\Environment;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

abstract class AbstractTabulatorResponse implements TabulatorResponseInterface
{
    protected $em;
    protected $translator;
    protected $security;
    protected $router;
    protected $templating;
    protected $uploaderHelper;
    protected $values;
    protected $formFactory;
    protected $grantedService;
    protected $imageService;
    protected $phoneNumberHelper;
    protected $moneyFormatter;
    protected $defaultLocale;
    protected $dateFormatter;
    protected $datetimeFormatter;

    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        Security $security,
        UrlGeneratorInterface $router,
        Environment $templating,
        UploaderHelper $uploaderHelper,
        FormFactoryInterface $formFactory,
        GrantedService $grantedService,
        ImageService $imageService,
        LocaleManager $localeManager,
        PhoneNumberHelper $phoneNumberHelper,
        MoneyFormatter $moneyFormatter,
        string $defaultLocale
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->security = $security;
        $this->router = $router;
        $this->templating = $templating;
        $this->uploaderHelper = $uploaderHelper;
        $this->formFactory = $formFactory;
        $this->grantedService = $grantedService;
        $this->imageService = $imageService;
        $this->values = [];
        $this->phoneNumberHelper = $phoneNumberHelper;
        $this->moneyFormatter = $moneyFormatter;
        $this->defaultLocale = $defaultLocale;
        $this->dateFormatter = \IntlDateFormatter::create(
            $localeManager->getCurrentLocale(),
            \IntlDateFormatter::MEDIUM,
            \IntlDateFormatter::NONE
        );
        $this->datetimeFormatter = \IntlDateFormatter::create(
            $localeManager->getCurrentLocale(),
            \IntlDateFormatter::MEDIUM,
            \IntlDateFormatter::SHORT
        );
    }

    public function getArrayResponse(): array
    {
        return $this->values;
    }
}
