<?php

declare(strict_types=1);

namespace App\Tabulator\Response;

use App\Entity\Accommodation;
use App\Entity\AccommodationDemand;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationStatusEnum;
use App\Enum\AccommodationTypeEnum;
use App\Enum\AccommodationVoterEnum;
use App\Enum\CohabitationVoterEnum;
use App\Tabulator\AbstractTabulatorResponse;

class AccommodationListResponse extends AbstractTabulatorResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Accommodation) {
            $accommodationDemandRepo = $this->em->getRepository(AccommodationDemand::class);

            $accommodation = $entity;
            $location = $accommodation->getSenior()->getLocation();

            /** @var AccommodationDemand $acceptedDemand */
            $acceptedDemand = $accommodationDemandRepo->findOneBy(['accommodation' => $accommodation, 'status' => AccommodationDemandStatusEnum::STATUS_ACCEPTED]);

            $this->values['id'] = $accommodation->getId();
            $this->values['name'] = $accommodation->getName();
            $this->values['type'] = $this->translator->trans(AccommodationTypeEnum::getReadableValue($accommodation->getType()));
            $this->values['status'] = $this->translator->trans(AccommodationStatusEnum::getReadableValue($accommodation->getStatus()));
            $this->values['houseSize'] = $accommodation->getHouseSize();
            $this->values['roomSize'] = $accommodation->getRoomSize();
            $this->values['city'] = $location->getCity() ?: '';
            $this->values['country'] = $location->getCountry() ?: '';
            $this->values['startDate'] = $accommodation->getStartDate() ? $this->dateFormatter->format($accommodation->getStartDate()) : '';
            $this->values['endDate'] = $accommodation->getEndDate() ? $this->dateFormatter->format($accommodation->getEndDate()) : '';
            $this->values['hasPet'] = $accommodation->hasPet() ? $this->translator->trans('yes') : $this->translator->trans('no');
            $this->values['hasWifi'] = $accommodation->hasWifi() ? $this->translator->trans('yes') : $this->translator->trans('no');
            $this->values['acceptSmokers'] = $accommodation->acceptSmokers() ? $this->translator->trans('yes') : $this->translator->trans('no');
            $this->values['price'] = $this->moneyFormatter->localizedFormatMoney($accommodation->getPrice());
            $this->values['nbDemands'] = \count($accommodationDemandRepo->findBy(['accommodation' => $accommodation, 'status' => AccommodationDemandStatusEnum::STATUS_PENDING]));
            $this->values['createdAt'] = $this->dateFormatter->format($accommodation->getCreatedAt());

            $actions = ['showButtons' => []];

            if ($this->security->isGranted(AccommodationVoterEnum::SHOW, $accommodation)) {
                array_push(
                    $actions['showButtons'],
                    [
                        'path' => $this->router->generate(
                            'user_show',
                            [
                                'id' => $accommodation->getSenior()->getId(),
                            ]
                        ),
                    ]
                );
            }

            if ($acceptedDemand
                && ($cohabitation = $acceptedDemand->getCohabitation())
                && $this->security->isGranted(CohabitationVoterEnum::SHOW, $cohabitation)
            ) {
                array_push(
                    $actions['showButtons'],
                    [
                        'path' => $this->router->generate(
                            'cohabitation_show',
                            [
                                'id' => $cohabitation->getId(),
                            ],
                        ),
                        'class' => 'btn-sm btn-outline-secondary',
                        'icon' => 'fa-file-contract',
                        'text' => $this->translator->trans('cohabitation.show.button'),
                    ]
                );
            }

            $this->values['actions'] = $this->templating->render('default/_list_actions.html.twig', $actions);
        }
    }
}
