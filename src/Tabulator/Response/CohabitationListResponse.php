<?php

declare(strict_types=1);

namespace App\Tabulator\Response;

use App\Entity\Cohabitation;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\CohabitationVoterEnum;
use App\Enum\CohabitationWorkflowStepStatusEnum;
use App\Tabulator\AbstractTabulatorResponse;

class CohabitationListResponse extends AbstractTabulatorResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Cohabitation) {
            $cohabitation = $entity;
            $accommodationDemand = $cohabitation->getAccommodationDemand();

            $this->values['id'] = $cohabitation->getId();
            $this->values['young'] = $accommodationDemand->getYoung()->getFullName();
            $this->values['senior'] = $accommodationDemand->getSenior()->getFullName();
            $this->values['status'] = $this->translator->trans(AccommodationDemandStatusEnum::getReadableValue($accommodationDemand->getStatus()));
            $this->values['price'] = $this->moneyFormatter->localizedFormatMoney($accommodationDemand->getAccommodation()->getPrice());
            $this->values['currentStep'] = $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue($cohabitation->getCurrentStep()->getStatus()));
            $this->values['startDate'] = $cohabitation->getStartDate() ? $this->dateFormatter->format($cohabitation->getStartDate()) : '';
            $this->values['endDate'] = $cohabitation->getEndDate() ? $this->dateFormatter->format($cohabitation->getEndDate()) : '';

            $actions = ['showButtons' => []];
            if ($this->security->isGranted(CohabitationVoterEnum::SHOW, $cohabitation)) {
                array_push(
                    $actions['showButtons'],
                    [
                        'path' => $this->router->generate(
                            'cohabitation_show',
                            [
                                'id' => $cohabitation->getId(),
                            ]
                        ),
                    ]
                );
            }

            $this->values['actions'] = $this->templating->render('default/_list_actions.html.twig', $actions);
        }
    }
}
