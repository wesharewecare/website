<?php

declare(strict_types=1);

namespace App\Tabulator\Response;

use App\Entity\Event;
use App\Enum\EventStatusEnum;
use App\Enum\UserRoleEnum;
use App\Tabulator\AbstractTabulatorResponse;

class EventListResponse extends AbstractTabulatorResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Event) {
            $event = $entity;
            $location = $event->getLocation();

            $this->values['id'] = $event->getId();
            $this->values['name'] = $event->getName();
            $this->values['date'] = $event->getDate() ? $this->datetimeFormatter->format($event->getDate()) : '';
            $this->values['status'] = $this->translator->trans(EventStatusEnum::getReadableValue($event->getStatus()));
            $this->values['nbParticipants'] = $event->getUsers()->count();
            $this->values['city'] = $location->getCity() ?: '';

            if ($this->security->isGranted(UserRoleEnum::ROLE_ADMIN)) {
                if ($this->security->isGranted(UserRoleEnum::ROLE_SUPER_ADMIN)) {
                    $this->values['country'] = $location->getCountry() ?: '';
                }

                $this->values['association'] = $event->getAssociation()->getName();
            }

            $actions = [
                'showButtons' => [
                    [
                        'path' => $this->router->generate(
                            'event_show',
                            [
                                'id' => $event->getId(),
                            ]
                        ),
                    ],
                ],
                'approvalButtons' => [],
            ];

            $this->values['actions'] = $this->templating->render('default/_list_actions.html.twig', $actions);
        }
    }
}
