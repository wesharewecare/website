<?php

declare(strict_types=1);

namespace App\Tabulator\Response;

use App\Entity\Association;
use App\Enum\AssociationRadiusUnitEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\AssociationVoterEnum;
use App\Tabulator\AbstractTabulatorResponse;

class AssociationListResponse extends AbstractTabulatorResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Association) {
            $association = $entity;
            $location = $association->getLocation();

            $this->values['id'] = $association->getId();
            $this->values['name'] = $association->getName();
            $this->values['website'] = $association->getWebsite();
            $this->values['city'] = $location->getCity() ?: '';
            $this->values['country'] = $location->getCountry() ?: '';
            $this->values['phoneNumber'] = $association->getPhoneNumber() ? $this->phoneNumberHelper->format($association->getPhoneNumber()) : '';
            $this->values['type'] = $this->translator->trans(AssociationTypeEnum::getReadableValue($association->getType()));
            $this->values['createdAt'] = $this->dateFormatter->format($association->getCreatedAt());
            $this->values['radiusRange'] = $association->getRadiusRange().' '.$this->translator->trans(AssociationRadiusUnitEnum::getReadableValue($association->getRadiusUnit()));
            $this->values['activated'] = $association->isActivated() ? $this->translator->trans('yes') : $this->translator->trans('no');

            $actions = [
                'showButtons' => [
                    [
                        'path' => $this->router->generate(
                            'association_show',
                            [
                                'id' => $association->getId(),
                            ]
                        ),
                    ],
                ],
                'approvalButtons' => [],
            ];

            if ($this->security->isGranted(AssociationVoterEnum::DELETE, $association)) {
                array_push(
                    $actions['approvalButtons'],
                    [
                        'route' => 'association_delete',
                        'icon' => 'fa-trash-alt',
                        'class' => 'btn-sm btn-outline-danger',
                        'text' => $this->translator->trans('actions.delete'),
                    ]
                );
            }

            $this->values['actions'] = $this->templating->render('default/_list_actions.html.twig', $actions);
        }
    }
}
