<?php

declare(strict_types=1);

namespace App\Tabulator\Response;

use App\Entity\Cohabitation;
use App\Entity\User;
use App\Enum\CohabitationVoterEnum;
use App\Enum\ProfileGenderEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Tabulator\AbstractTabulatorResponse;

class UserListResponse extends AbstractTabulatorResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof User) {
            $user = $entity;
            $profile = $user->getProfile();
            $location = $user->getLocation();
            $isManager = $this->grantedService->isManager($user);

            $this->values['id'] = $user->getId();
            $this->values['firstname'] = $user->getFirstname();
            $this->values['lastname'] = $user->getLastname();
            $this->values['email'] = $user->getEmail() ?: '';
            $this->values['phoneNumber'] = $profile->getPhoneNumber() ? $this->phoneNumberHelper->format($profile->getPhoneNumber()) : '';
            $this->values['city'] = $location->getCity() ?: '';
            $this->values['age'] = $user->getAge() ?: '';
            $this->values['gender'] = $profile->getGender() ? $this->translator->trans(ProfileGenderEnum::getReadableValue($profile->getGender())) : '';
            $this->values['isApproved'] = $profile->isApproved() ? $this->translator->trans('yes') : $this->translator->trans('no');
            $this->values['roles'] = $this->translator->trans(UserRoleEnum::getReadableValue($this->grantedService->getRole($user)));
            $this->values['createdAt'] = $this->dateFormatter->format($user->getCreatedAt());
            $this->values['association'] = $user->getAssociation() ? $user->getAssociation()->getName() : '';
            $this->values['showHomesharing'] = !$isManager
                ? $profile->isShowHomesharing()
                    ? $this->translator->trans('yes')
                    : $this->translator->trans('no')
                : ''
            ;
            $this->values['nbEvents'] = $user->getEvents()->count();

            $actions = [
                'showButtons' => [],
                'approvalButtons' => [],
            ];

            if ($this->security->isGranted(UserVoterEnum::SHOW, $user)) {
                array_push($actions['showButtons'], [
                    'path' => $this->router->generate(
                        'user_show',
                        [
                            'id' => $user->getId(),
                        ]
                    ),
                ]);
            }

            if ($this->security->isGranted(UserVoterEnum::SEND_MESSAGE, $user)) {
                array_push($actions['showButtons'], [
                    'path' => $this->router->generate(
                        'messages',
                        [
                            '_fragment' => 'new-thread',
                            'recipient' => $user->getId(),
                        ]
                    ),
                    'icon' => 'fa-envelope',
                    'class' => 'btn-sm btn-outline-info',
                    'text' => $this->translator->trans('actions.message'),
                ]);
            }

            if ($this->security->isGranted(UserVoterEnum::APPROVE, $user)) {
                array_push($actions['approvalButtons'], [
                    'route' => 'user_approval',
                    'class' => 'btn-sm btn-outline-success',
                ]);
            }

            if ($this->security->isGranted(UserVoterEnum::CLAIM_MANAGEMENT, $user)) {
                array_push($actions['approvalButtons'], [
                    'route' => 'user_claim_management',
                    'icon' => 'fa-user-plus',
                    'class' => 'btn-sm btn-outline-success',
                    'text' => $this->translator->trans('actions.manage'),
                ]);
            }

            if ($this->security->isGranted(UserVoterEnum::DELETE, $user)) {
                array_push(
                    $actions['approvalButtons'],
                    [
                        'route' => 'user_delete',
                        'icon' => 'fa-trash-alt',
                        'class' => 'btn-sm btn-outline-danger',
                        'text' => $this->translator->trans('actions.delete'),
                    ]
                );
            }

            if (!$isManager) {
                $cohabitationRepo = $this->em->getRepository(Cohabitation::class);
                $cohabitation = $cohabitationRepo->getCurrentCohabitationByUser($user);

                if ($this->security->isGranted(CohabitationVoterEnum::SHOW, $cohabitation)) {
                    array_push(
                        $actions['showButtons'],
                        [
                            'path' => $this->router->generate(
                                'cohabitation_show',
                                [
                                    'id' => $cohabitation->getId(),
                                ],
                            ),
                            'class' => 'btn-sm btn-outline-secondary',
                            'icon' => 'fa-file-contract',
                            'text' => $this->translator->trans('cohabitation.show.button'),
                        ]
                    );
                }
            }

            $this->values['actions'] = $this->templating->render('default/_list_actions.html.twig', $actions);
        }
    }
}
