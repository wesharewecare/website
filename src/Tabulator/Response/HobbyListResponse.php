<?php

declare(strict_types=1);

namespace App\Tabulator\Response;

use App\Entity\Hobby;
use App\Tabulator\AbstractTabulatorResponse;

class HobbyListResponse extends AbstractTabulatorResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Hobby) {
            $hobby = $entity;

            $hobby->setTranslatableLocale($this->defaultLocale);
            $this->em->refresh($hobby);

            $this->values['id'] = $hobby->getId();
            $this->values['name'] = $hobby->getName();

            $actions = [
                'showButtons' => [
                    [
                        'path' => $this->router->generate(
                            'hobby_edit',
                            [
                                'id' => $hobby->getId(),
                            ]
                        ),
                        'icon' => 'fa-pen',
                        'text' => $this->translator->trans('actions.edit'),
                    ],
                ],
                'approvalButtons' => [
                    [
                        'route' => 'hobby_delete',
                        'icon' => 'fa-trash-alt',
                        'class' => 'btn-sm btn-outline-danger',
                        'text' => $this->translator->trans('actions.delete'),
                    ],
                ],
            ];

            $this->values['actions'] = $this->templating->render('default/_list_actions.html.twig', $actions);
        }
    }
}
