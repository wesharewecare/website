<?php

declare(strict_types=1);

namespace App\Tabulator;

use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

class TabulatorModel
{
    private $security;
    private $grantedService;
    private $parameters;
    private $sort;
    private $search;
    private $page;
    private $maxResults;

    public function __construct(RequestStack $requestStack, Security $security, GrantedService $grantedService)
    {
        $this->security = $security;
        $this->grantedService = $grantedService;

        if ($request = $requestStack->getCurrentRequest()) {
            $this->parameters = $request->query->all();
            $this->setSort();
            $this->setSearch();
            $this->setPage();
            $this->setMaxResults();
        }
    }

    public function getSort(): array
    {
        return $this->sort;
    }

    public function getSearch(): array
    {
        return $this->search;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getMaxResults(): ?int
    {
        return $this->maxResults;
    }

    public function addSearchParam(string $field, string $type, string $value): void
    {
        $this->search[$field] = ['value' => $value, 'type' => $type];
    }

    private function setSort(): void
    {
        $this->sort = [];

        if (\array_key_exists('sorters', $this->parameters)) {
            foreach ($this->parameters['sorters'] as $column) {
                $this->sort[$column['field']] = $column['dir'];
            }
        }
    }

    private function setSearch(): void
    {
        $this->search = [];

        if (\array_key_exists('filters', $this->parameters)) {
            foreach ($this->parameters['filters'] as $column) {
                if (\array_key_exists('value', $column)) {
                    $columnName = $column['field'];
                    $searchValue = $column['value'];
                    $searchType = $column['type'];

                    $this->search[$columnName] = ['value' => $searchValue, 'type' => $searchType];
                }
            }
        }

        if (($user = $this->security->getUser())
            && UserRoleEnum::ROLE_ADMIN === $this->grantedService->getRole($user)
        ) {
            $this->search['countryCode'] = [
                'value' => $user->getLocation()->getCountryCode(),
                'type' => '=',
            ];
        }
    }

    private function setPage(): void
    {
        $this->page = \array_key_exists('page', $this->parameters)
            ? (int) $this->parameters['page']
            : $this->page = 0;
    }

    private function setMaxResults(): void
    {
        $this->maxResults = \array_key_exists('size', $this->parameters)
            ? (int) $this->parameters['size']
            : $this->maxResults = 0
        ;
    }
}
