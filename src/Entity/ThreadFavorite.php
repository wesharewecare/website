<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ThreadFavoriteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ThreadFavoriteRepository::class)
 * @UniqueEntity(fields={"user", "thread"})
 */
class ThreadFavorite
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="threadFavorites")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $thread;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getThread(): string
    {
        return $this->thread;
    }

    public function setThread(string $thread): void
    {
        $this->thread = $thread;
    }
}
