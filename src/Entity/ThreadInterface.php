<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface ThreadInterface
{
    public function getId(): int;

    public function getSubject(): string;

    public function setSubject(string $subject): void;

    public function addMessage(Message $message): void;

    public function getMessages(): Collection;

    public function addMetadata(ThreadMetadata $meta): void;

    public function getMetadata(): Collection;

    public function getUsers(): Collection;

    public function addUser(User $user): void;

    public function removeUser(User $user): void;

    public function isParticipant(User $participant): bool;

    public function getLastMessageAt(): ?\DateTime;

    public function setLastMessageAt(?\DateTime $lastMessageAt): void;

    public function getOtherUser(User $user): ?User;
}
