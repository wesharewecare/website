<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use EmailChecker\Constraints as EmailCheckerAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 *
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, \Serializable, PasswordAuthenticatedUserInterface
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true, nullable=true)
     * @Assert\Email(mode=Email::VALIDATION_MODE_HTML5)
     * @EmailCheckerAssert\NotThrowawayEmail
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $lastname;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @var string|null
     */
    private $plainPassword;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @var Profile
     *
     * @ORM\OneToOne(targetEntity="Profile", inversedBy="user", cascade={"persist", "remove"})
     */
    private $profile;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="sender", cascade={"persist", "remove"}))
     */
    private $messages;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreadMetadata", mappedBy="participant", cascade={"persist", "remove"}))
     */
    private $threadMetadatas;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreadFavorite", mappedBy="user", cascade={"persist", "remove"}))
     */
    private $threadFavorites;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Thread", mappedBy="users", cascade={"persist", "remove"})
     * @ORM\OrderBy({"lastMessageAt" : "DESC"})
     */
    private $threads;

    /**
     * @var Accommodation|null
     *
     * @ORM\OneToOne(targetEntity="Accommodation", mappedBy="senior", cascade={"persist", "remove"})
     */
    private $accommodation;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AccommodationDemand", mappedBy="young", orphanRemoval=true)
     */
    private $accommodationDemands;

    /**
     * @var Location
     *
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     */
    private $location;

    /**
     * @var Association|null
     *
     * @ORM\ManyToOne(targetEntity="Association", inversedBy="users")
     */
    private $association;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Event", mappedBy="users", cascade={"persist", "remove"})
     * @ORM\OrderBy({"date" : "ASC"})
     */
    private $events;

    /**
     * @var UserNotification|null
     *
     * @ORM\OneToOne(targetEntity="UserNotification", cascade={"persist", "remove"})
     */
    private $userNotification;

    public function __construct()
    {
        $this->setProfile(new Profile());
        $this->messages = new ArrayCollection();
        $this->threads = new ArrayCollection();
        $this->threadMetadatas = new ArrayCollection();
        $this->threadFavorites = new ArrayCollection();
        $this->accommodationDemands = new ArrayCollection();
        $this->location = new Location();
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): void
    {
        $this->isVerified = $isVerified;
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function setProfile(Profile $profile): void
    {
        $this->profile = $profile;
        $profile->setUser($this);
    }

    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function getThreadMetadatas(): Collection
    {
        return $this->threadMetadatas;
    }

    public function getThreadFavorites(): Collection
    {
        return $this->threadFavorites;
    }

    public function getThreads(): Collection
    {
        return $this->threads;
    }

    public function getAccommodation(): ?Accommodation
    {
        return $this->accommodation;
    }

    public function setAccommodation(?Accommodation $accommodation): void
    {
        $this->accommodation = $accommodation;
        $accommodation->setSenior($this);
    }

    public function getAccommodationDemands(): Collection
    {
        return $this->accommodationDemands;
    }

    public function getLocation(): Location
    {
        return $this->location;
    }

    public function setLocation(Location $location): void
    {
        $this->location = $location;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): void
    {
        $this->association = $association;
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function getUserNotification(): ?UserNotification
    {
        return $this->userNotification;
    }

    public function setUserNotification(UserNotification $userNotification): void
    {
        $this->userNotification = $userNotification;
    }

    public function getFullName(): string
    {
        return $this->getFirstname().' '.$this->getLastname();
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials(): void
    {
    }

    public function getAge(): ?int
    {
        $age = ($this->birthday instanceof \DateTimeInterface)
            ? date_diff(new \DateTime('now'), $this->birthday)->y
            : null;

        return (\is_int($age) && $age > 1) ? $age : null;
    }

    public function serialize(): string
    {
        return serialize([$this->id, $this->email, $this->password]);
    }

    public function unserialize($data): void
    {
        [
            $this->id,
            $this->email,
            $this->password
            ] = unserialize($data);
    }

    public function addMessage(Message $message): void
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
            $message->setSender($this);
        }
    }

    public function removeMessage(Message $message): void
    {
        $this->messages->removeElement($message);
    }

    public function addThreadMetadata(ThreadMetadata $threadMetadata): void
    {
        if (!$this->threadMetadatas->contains($threadMetadata)) {
            $this->threadMetadatas->add($threadMetadata);
            $threadMetadata->setParticipant($this);
        }
    }

    public function removeThreadMetadata(ThreadMetadata $threadMetadata): void
    {
        $this->threadMetadatas->removeElement($threadMetadata);
    }

    public function addThreadFavorite(ThreadFavorite $threadFavorite): void
    {
        if (!$this->threadFavorites->contains($threadFavorite)) {
            $this->threadFavorites->add($threadFavorite);
            $threadFavorite->setUser($this);
        }
    }

    public function removeThreadFavorite(ThreadFavorite $threadFavorite): void
    {
        $this->threadFavorites->removeElement($threadFavorite);
    }

    public function addThread(Thread $thread): void
    {
        if (!$this->threads->contains($thread)) {
            $this->threads->add($thread);
            $thread->addUser($this);
        }
    }

    public function removeThread(Thread $thread): void
    {
        $this->threads->removeElement($thread);
        $thread->removeUser($this);
    }

    public function addAccommodationDemand(AccommodationDemand $accommodationDemand): void
    {
        if (!$this->accommodationDemands->contains($accommodationDemand)) {
            $this->accommodationDemands->add($accommodationDemand);
            $accommodationDemand->setYoung($this);
        }
    }

    public function removeAccommodationDemand(AccommodationDemand $accommodationDemand): void
    {
        $this->accommodationDemands->removeElement($accommodationDemand);
    }

    public function addEvent(Event $event): void
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
            $event->addUser($this);
        }
    }

    public function removeEvent(Event $event): void
    {
        $this->events->removeElement($event);
        $event->removeUser($this);
    }
}
