<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class UserNotification
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[]"})
     */
    private $notifications = [];

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbNotifSent = 0;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $notifSentAt;

    public function __construct(array $notifications = [])
    {
        $this->addMissingNotifications($notifications);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNotifications(): array
    {
        return $this->notifications;
    }

    public function setNotifications(array $notifications): void
    {
        $this->notifications = $notifications;
    }

    public function getNbNotifSent(): int
    {
        return $this->nbNotifSent;
    }

    public function setNbNotifSent(int $nbNotifSent): void
    {
        $this->nbNotifSent = $nbNotifSent;
    }

    public function getNotifSentAt(): ?\DateTime
    {
        return $this->notifSentAt;
    }

    public function setNotifSentAt(?\DateTime $notifSentAt): void
    {
        $this->notifSentAt = $notifSentAt;
    }

    public function addMissingNotifications(array $notifications): void
    {
        foreach ($notifications as $notification) {
            if (!\array_key_exists($notification, $this->notifications)) {
                $this->notifications[$notification] = false;
            }
        }
    }

    public function resetNotifSentAt(): void
    {
        $this->setNotifSentAt(new \DateTime());
    }

    public function increaseNbNotifSent(): void
    {
        $this->setNbNotifSent($this->getNbNotifSent() + 1);
        $this->resetNotifSentAt();
    }

    public function resetNotifSent(): void
    {
        $this->setNbNotifSent(0);
        $this->setNotifSentAt(null);
    }
}
