<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\AccommodationDemandStatusEnum;
use App\Repository\AccommodationDemandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity(repositoryClass=AccommodationDemandRepository::class)
 */
class AccommodationDemand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="accommodationDemands")
     * @ORM\JoinColumn(nullable=false)
     */
    private $young;

    /**
     * @var Accommodation
     *
     * @ORM\ManyToOne(targetEntity="Accommodation", inversedBy="accommodationDemands")
     * @ORM\JoinColumn(nullable=false)
     */
    private $accommodation;

    /**
     * @var string
     *
     * @ORM\Column(type="AccommodationDemandStatusType")
     * @DoctrineAssert\Enum(entity="App\Enum\AccommodationDemandStatusEnum")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var ThreadAccommodationDemand|null
     *
     * @ORM\OneToOne(targetEntity="ThreadAccommodationDemand", inversedBy="accommodationDemand", cascade={"persist", "remove"})
     */
    private $thread;

    /**
     * @var Cohabitation|null
     *
     * @ORM\OneToOne(targetEntity="Cohabitation", inversedBy="accommodationDemand", cascade={"persist", "remove"})
     */
    private $cohabitation;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Association")
     */
    private $associations;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $refusingExplanation;

    public function __construct()
    {
        $this->associations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYoung(): User
    {
        return $this->young;
    }

    public function setYoung(User $young): void
    {
        $this->young = $young;
    }

    public function getAccommodation(): Accommodation
    {
        return $this->accommodation;
    }

    public function setAccommodation(Accommodation $accommodation): void
    {
        $this->accommodation = $accommodation;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getThread(): ?ThreadAccommodationDemand
    {
        return $this->thread;
    }

    public function setThread(?ThreadAccommodationDemand $thread): void
    {
        $this->thread = $thread;

        if ($thread) {
            $thread->setAccommodationDemand($this);
        }
    }

    public function getCohabitation(): ?Cohabitation
    {
        return $this->cohabitation;
    }

    public function setCohabitation(?Cohabitation $cohabitation): void
    {
        $this->cohabitation = $cohabitation;
    }

    public function getAssociations(): Collection
    {
        return $this->associations;
    }

    public function getRefusingExplanation(): ?string
    {
        return $this->refusingExplanation;
    }

    public function setRefusingExplanation(?string $refusingExplanation): void
    {
        $this->refusingExplanation = $refusingExplanation;
    }

    public function getClassStatus(): string
    {
        switch ($this->status) {
            case AccommodationDemandStatusEnum::STATUS_PENDING:
            case AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL:
                return 'pending';

            case AccommodationDemandStatusEnum::STATUS_ACCEPTED:
            case AccommodationDemandStatusEnum::STATUS_COMPLETED:
                return 'accepted';
        }

        return 'declined';
    }

    public function getSenior(): User
    {
        return $this->accommodation->getSenior();
    }

    public function addAssociation(Association $association): void
    {
        if (!$this->associations->contains($association)) {
            $this->associations->add($association);
        }
    }

    public function removeAssociation(Association $association): void
    {
        $this->associations->removeElement($association);
    }
}
