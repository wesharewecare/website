<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 */
class CohabitationWorkflowStep
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="CohabitationWorkflowStepStatusType")
     * @DoctrineAssert\Enum(entity="App\Enum\CohabitationWorkflowStepStatusEnum")
     */
    private $status;

    /**
     * @var Cohabitation
     *
     * @ORM\ManyToOne(targetEntity="Cohabitation", inversedBy="cohabitationWorkflowSteps")
     */
    private $cohabitation;

    public function getId(): int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getCohabitation(): Cohabitation
    {
        return $this->cohabitation;
    }

    public function setCohabitation(Cohabitation $cohabitation): void
    {
        $this->cohabitation = $cohabitation;
    }
}
