<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 * @Vich\Uploadable
 */
class Document
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $locale;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="toolbox", fileNameProperty="documentName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/gif",
     *         "image/bmp",
     *         "application/pdf"
     *     }
     * )
     */
    private $documentFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $documentName;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="toolbox", fileNameProperty="thumbnailName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/bmp",
     *     }
     * )
     */
    private $thumbnailFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $thumbnailName;

    /**
     * @var Association|null
     *
     * @ORM\ManyToOne(targetEntity="Association", inversedBy="documents")
     */
    private $association;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[]"})
     */
    private $recipients;

    public function __construct()
    {
        $this->recipients = [];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    public function getDocumentFile(): ?File
    {
        return $this->documentFile;
    }

    public function setDocumentFile(?File $documentFile): void
    {
        $this->documentFile = $documentFile;

        if (null !== $documentFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getDocumentName(): ?string
    {
        return $this->documentName;
    }

    public function setDocumentName(?string $documentName): void
    {
        $this->documentName = $documentName;
    }

    public function getThumbnailFile(): ?File
    {
        return $this->thumbnailFile;
    }

    public function setThumbnailFile(?File $thumbnailFile): void
    {
        $this->thumbnailFile = $thumbnailFile;

        if (null !== $thumbnailFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getThumbnailName(): ?string
    {
        return $this->thumbnailName;
    }

    public function setThumbnailName(?string $thumbnailName): void
    {
        $this->thumbnailName = $thumbnailName;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): void
    {
        $this->association = $association;
    }

    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function addRecipient(string $recipient): void
    {
        if (false === array_search($recipient, $this->recipients)) {
            array_push($this->recipients, $recipient);
        }
    }

    public function removeRecipient(string $recipient): void
    {
        $key = array_search($recipient, $this->recipients);
        if (false !== $key) {
            unset($this->recipients[$key]);
        }
    }
}
