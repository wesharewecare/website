<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Location
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $zipCode;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $district;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $street;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $country;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $countryCode;

    /**
     * @var float|null
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float|null
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(?string $district): void
    {
        $this->district = $district;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    public function setCountryCode(?string $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getReadableAddress(): string
    {
        $address = '';

        foreach ([$this->getCountry(), $this->getZipCode(), $this->getCity(), $this->getStreet()] as $field) {
            if (!empty($field)) {
                if (!empty($address)) {
                    $address .= ', ';
                }

                $address .= $field;
            }
        }

        return $address;
    }
}
