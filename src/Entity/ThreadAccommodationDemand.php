<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ThreadAccommodationDemandRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ThreadAccommodationDemandRepository::class)
 */
class ThreadAccommodationDemand extends Thread implements ThreadInterface
{
    /**
     * @var AccommodationDemand
     *
     * @ORM\OneToOne(targetEntity="AccommodationDemand", mappedBy="thread")
     */
    private $accommodationDemand;

    public function getAccommodationDemand(): AccommodationDemand
    {
        return $this->accommodationDemand;
    }

    public function setAccommodationDemand(AccommodationDemand $accommodationDemand): void
    {
        $this->accommodationDemand = $accommodationDemand;
    }
}
