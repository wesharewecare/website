<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ThreadMetadataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ThreadMetadataRepository::class)
 */
class ThreadMetadata
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var Thread
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Thread", inversedBy="metadata")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $thread;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="threadMetadatas")
     */
    protected $participant;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastMessageReadAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function getThread(): Thread
    {
        return $this->thread;
    }

    public function setThread(Thread $thread): void
    {
        $this->thread = $thread;
    }

    public function getParticipant(): User
    {
        return $this->participant;
    }

    public function setParticipant(User $participant): void
    {
        $this->participant = $participant;
    }

    public function getLastMessageReadAt(): ?\DateTime
    {
        return $this->lastMessageReadAt;
    }

    public function setLastMessageReadAt(?\DateTime $lastMessageReadAt): void
    {
        $this->lastMessageReadAt = $lastMessageReadAt;
    }
}
