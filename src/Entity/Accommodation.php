<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\AccommodationStatusEnum;
use App\Repository\AccommodationRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Money\Currency;
use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AccommodationRepository::class)
 */
class Accommodation
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="accommodation", cascade={"persist", "remove"})
     */
    private $senior;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $roomDescription;

    /**
     * @var string
     *
     * @ORM\Column(type="AccommodationTypeType")
     * @DoctrineAssert\Enum(entity="App\Enum\AccommodationTypeEnum")
     */
    private $type;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var int|null
     *
     * @Assert\Range(min=0)
     *
     * @ORM\Column(type="integer", options={"unsigned" : true}, nullable=true)
     */
    private $priceAmount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $currencyCode = 'EUR';

    /**
     * @var int
     *
     * @Assert\Range(min=1)
     *
     * @ORM\Column(type="integer", options={"unsigned" : true})
     */
    private $roomSize;

    /**
     * @var int
     *
     * @Assert\Range(min=1)
     *
     * @ORM\Column(type="integer", options={"unsigned" : true})
     */
    private $houseSize;

    /**
     * @var int
     *
     * @Assert\Range(min=0)
     *
     * @ORM\Column(type="integer", options={"unsigned" : true})
     */
    private $sharedHouseRooms = 0;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $hasPet = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $hasWifi = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $acceptSmokers = false;

    /**
     * @var string
     *
     * @ORM\Column(type="AccommodationStatusType")
     * @DoctrineAssert\Enum(entity="App\Enum\AccommodationStatusEnum")
     */
    private $status = AccommodationStatusEnum::STATUS_AWAITING_APPROVAL;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AccommodationImage", mappedBy="accommodation", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $images;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AccommodationDemand", mappedBy="accommodation", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $accommodationDemands;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[]"})
     */
    private $ancillaryRooms = [];

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->accommodationDemands = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSenior(): User
    {
        return $this->senior;
    }

    public function setSenior(User $senior): void
    {
        $this->senior = $senior;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getRoomDescription(): string
    {
        return $this->roomDescription;
    }

    public function setRoomDescription(string $roomDescription): void
    {
        $this->roomDescription = $roomDescription;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function getPriceAmount(): ?int
    {
        return $this->priceAmount;
    }

    public function setPriceAmount(?int $priceAmount): void
    {
        $this->priceAmount = $priceAmount;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    public function getRoomSize(): int
    {
        return $this->roomSize;
    }

    public function setRoomSize(int $roomSize): void
    {
        $this->roomSize = $roomSize;
    }

    public function getHouseSize(): int
    {
        return $this->houseSize;
    }

    public function setHouseSize(int $houseSize): void
    {
        $this->houseSize = $houseSize;
    }

    public function getSharedHouseRooms(): int
    {
        return $this->sharedHouseRooms;
    }

    public function setSharedHouseRooms(int $sharedHouseRooms): void
    {
        $this->sharedHouseRooms = $sharedHouseRooms;
    }

    public function hasPet(): bool
    {
        return $this->hasPet;
    }

    public function setHasPet(bool $hasPet): void
    {
        $this->hasPet = $hasPet;
    }

    public function hasWifi(): bool
    {
        return $this->hasWifi;
    }

    public function setHasWifi(bool $hasWifi): void
    {
        $this->hasWifi = $hasWifi;
    }

    public function acceptSmokers(): bool
    {
        return $this->acceptSmokers;
    }

    public function setAcceptSmokers(bool $acceptSmokers): void
    {
        $this->acceptSmokers = $acceptSmokers;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getAccommodationDemands(): Collection
    {
        return $this->accommodationDemands;
    }

    public function getAncillaryRooms(): array
    {
        return $this->ancillaryRooms;
    }

    public function getPrice(): ?Money
    {
        if (!$this->currencyCode) {
            return null;
        }

        if (!$this->priceAmount) {
            return new Money(0, new Currency($this->currencyCode));
        }

        return new Money($this->priceAmount, new Currency($this->currencyCode));
    }

    public function getCurrency(): Currency
    {
        return new Currency($this->currencyCode);
    }

    public function setCurrency(?Currency $currency): void
    {
        $this->currencyCode = $currency->getCode();
    }

    public function getClassStatus(): string
    {
        switch ($this->status) {
            case AccommodationStatusEnum::STATUS_OPEN:
                return 'open';

            case AccommodationStatusEnum::STATUS_OCCUPIED:
                return 'accepted';

            case AccommodationStatusEnum::STATUS_CLOSED:
                return 'declined';

            case AccommodationStatusEnum::STATUS_AWAITING_APPROVAL:
                return 'pending';
        }

        return '';
    }

    public function addAccommodationDemand(AccommodationDemand $accommodationDemand): void
    {
        if (!$this->accommodationDemands->contains($accommodationDemand)) {
            $this->accommodationDemands->add($accommodationDemand);
            $accommodationDemand->setAccommodation($this);
        }
    }

    public function removeAccommodationDemand(AccommodationDemand $accommodationDemand): void
    {
        $this->accommodationDemands->removeElement($accommodationDemand);
    }

    public function addImage(AccommodationImage $accommodationImage): void
    {
        if (!$this->images->contains($accommodationImage)) {
            $this->images->add($accommodationImage);
            $accommodationImage->setAccommodation($this);
        }
    }

    public function removeImage(AccommodationImage $accommodationImage): void
    {
        $this->images->removeElement($accommodationImage);
        $accommodationImage->setAccommodation(null);
    }

    public function addAncillaryRoom(string $ancillaryRoom): void
    {
        if (false === array_search($ancillaryRoom, $this->ancillaryRooms, true)) {
            $this->ancillaryRooms[] = $ancillaryRoom;
        }
    }

    public function removeAncillaryRoom(string $ancillaryRoom): void
    {
        $key = array_search($ancillaryRoom, $this->ancillaryRooms, true);
        if (false !== $key) {
            unset($this->ancillaryRooms[$key]);
        }
    }
}
