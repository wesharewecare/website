<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\EventStatusEnum;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use EmailChecker\Constraints as EmailCheckerAssert;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use libphonenumber\PhoneNumber;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @Vich\Uploadable
 */
class Event
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="event", fileNameProperty="eventImageName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/gif",
     *         "image/bmp",
     *     }
     * )
     */
    private $eventImageFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $eventImageName;

    /**
     * @var Location
     *
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     */
    private $location;

    /**
     * @var Association|null
     *
     * @ORM\ManyToOne(targetEntity="Association", inversedBy="events")
     */
    private $association;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="events", cascade={"persist"})
     */
    private $users;

    /**
     * @var string
     *
     * @ORM\Column(type="EventStatusType")
     * @DoctrineAssert\Enum(entity="App\Enum\EventStatusEnum")
     */
    private $status = EventStatusEnum::STATUS_DRAFT;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isFree = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isHandicapAccessible = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isOpenAir = false;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $languages = [];

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     * @Assert\Email(mode=Email::VALIDATION_MODE_HTML5)
     * @EmailCheckerAssert\NotThrowawayEmail
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $lastname;

    /**
     * @var PhoneNumber|null
     *
     * @ORM\Column(type="phone_number", nullable=true)
     */
    private $phoneNumber;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $reminderNotificationSent = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $newParticipantNotificationToSend = false;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->location = new Location();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): void
    {
        $this->date = $date;
    }

    public function getEventImageFile(): ?File
    {
        return $this->eventImageFile;
    }

    public function setEventImageFile(?File $eventImageFile): void
    {
        $this->eventImageFile = $eventImageFile;

        if (null !== $eventImageFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getEventImageName(): ?string
    {
        return $this->eventImageName;
    }

    public function setEventImageName(?string $eventImageName): void
    {
        $this->eventImageName = $eventImageName;
    }

    public function getLocation(): Location
    {
        return $this->location;
    }

    public function setLocation(Location $location): void
    {
        $this->location = $location;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): void
    {
        $this->association = $association;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function isFree(): bool
    {
        return $this->isFree;
    }

    public function setIsFree(bool $isFree): void
    {
        $this->isFree = $isFree;
    }

    public function isHandicapAccessible(): bool
    {
        return $this->isHandicapAccessible;
    }

    public function setIsHandicapAccessible(bool $isHandicapAccessible): void
    {
        $this->isHandicapAccessible = $isHandicapAccessible;
    }

    public function isOpenAir(): bool
    {
        return $this->isOpenAir;
    }

    public function setIsOpenAir(bool $isOpenAir): void
    {
        $this->isOpenAir = $isOpenAir;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function setLanguages(array $languages): void
    {
        $this->languages = $languages;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getPhoneNumber(): ?PhoneNumber
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?PhoneNumber $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function isReminderNotificationSent(): bool
    {
        return $this->reminderNotificationSent;
    }

    public function setReminderNotificationSent(bool $reminderNotificationSent): void
    {
        $this->reminderNotificationSent = $reminderNotificationSent;
    }

    public function isNewParticipantNotificationToSend(): bool
    {
        return $this->newParticipantNotificationToSend;
    }

    public function setNewParticipantNotificationToSend(bool $newParticipantNotificationToSend): void
    {
        $this->newParticipantNotificationToSend = $newParticipantNotificationToSend;
    }

    public function isParticipating(User $user): bool
    {
        return $this->getUsers()->contains($user);
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function clearUsers(): void
    {
        $this->users = new ArrayCollection();
    }
}
