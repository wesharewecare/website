<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use libphonenumber\PhoneNumber;
use Money\Currency;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 *
 * @Vich\Uploadable
 */
class Profile
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $motivation;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $university;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $studies;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Hobby")
     */
    private $hobbies;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="profile_picture", fileNameProperty="profilePictureName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/gif",
     *         "image/bmp",
     *     }
     * )
     */
    private $profilePictureFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $profilePictureName;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="profile_passport", fileNameProperty="passportName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "application/pdf"
     *     }
     * )
     */
    private $passportFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $passportName;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="mobility_letter", fileNameProperty="mobilityLetterName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "application/pdf"
     *     }
     * )
     */
    private $mobilityLetterFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $mobilityLetterName;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isApproved = false;

    /**
     * @var PhoneNumber|null
     *
     * @ORM\Column(type="phone_number", nullable=true)
     */
    private $phoneNumber;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", mappedBy="profile")
     */
    private $user;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $languages = [];

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $preferredCurrency;

    /**
     * @var string|null
     *
     * @ORM\Column(type="ProfileGenderType", nullable=true)
     * @DoctrineAssert\Enum(entity="App\Enum\ProfileGenderEnum")
     */
    private $gender;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $showHomesharing = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $international = false;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $localeLanguage;

    public function __construct()
    {
        $this->hobbies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getMotivation(): ?string
    {
        return $this->motivation;
    }

    public function setMotivation(?string $motivation): void
    {
        $this->motivation = $motivation;
    }

    public function getUniversity(): ?string
    {
        return $this->university;
    }

    public function setUniversity(?string $university): void
    {
        $this->university = $university;
    }

    public function getStudies(): ?string
    {
        return $this->studies;
    }

    public function setStudies(?string $studies): void
    {
        $this->studies = $studies;
    }

    public function getHobbies(): Collection
    {
        return $this->hobbies;
    }

    public function setHobbies(Collection $hobbies): void
    {
        $this->hobbies = $hobbies;
    }

    public function getProfilePictureFile(): ?File
    {
        return $this->profilePictureFile;
    }

    public function setProfilePictureFile(?File $picture): void
    {
        $this->profilePictureFile = $picture;

        if (null !== $picture) {
            $this->updatedAt = new DateTime();
        }
    }

    public function getProfilePictureName(): ?string
    {
        return $this->profilePictureName;
    }

    public function setProfilePictureName(?string $pictureName): void
    {
        $this->profilePictureName = $pictureName;
    }

    public function getPassportFile(): ?File
    {
        return $this->passportFile;
    }

    public function setPassportFile(?File $passportFile): void
    {
        $this->passportFile = $passportFile;

        if (null !== $passportFile) {
            $this->updatedAt = new DateTime();
        }
    }

    public function getPassportName(): ?string
    {
        return $this->passportName;
    }

    public function setPassportName(?string $passportName): void
    {
        $this->passportName = $passportName;
    }

    public function getMobilityLetterFile(): ?File
    {
        return $this->mobilityLetterFile;
    }

    public function setMobilityLetterFile(?File $mobilityLetterFile): void
    {
        $this->mobilityLetterFile = $mobilityLetterFile;

        if (null !== $mobilityLetterFile) {
            $this->updatedAt = new DateTime();
        }
    }

    public function getMobilityLetterName(): ?string
    {
        return $this->mobilityLetterName;
    }

    public function setMobilityLetterName(?string $mobilityLetterName): void
    {
        $this->mobilityLetterName = $mobilityLetterName;
    }

    public function isApproved(): bool
    {
        return $this->isApproved;
    }

    public function setIsApproved(bool $isApproved): void
    {
        $this->isApproved = $isApproved;
    }

    public function getPhoneNumber(): ?PhoneNumber
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?PhoneNumber $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function setLanguages(array $languages): void
    {
        $this->languages = $languages;
    }

    public function getPreferredCurrency(): ?string
    {
        return $this->preferredCurrency;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): void
    {
        $this->gender = $gender;
    }

    public function isShowHomesharing(): bool
    {
        return $this->showHomesharing;
    }

    public function setShowHomesharing(bool $showHomesharing): void
    {
        $this->showHomesharing = $showHomesharing;
    }

    public function isInternational(): bool
    {
        return $this->international;
    }

    public function setInternational(bool $international): void
    {
        $this->international = $international;
    }

    public function getLocaleLanguage(): string
    {
        return $this->localeLanguage;
    }

    public function setLocaleLanguage(string $localeLanguage): void
    {
        $this->localeLanguage = $localeLanguage;
    }

    public function addHobby(Hobby $hobby): void
    {
        $this->hobbies->add($hobby);
    }

    public function removeHobby(Hobby $hobby): void
    {
        $this->hobbies->removeElement($hobby);
    }

    public function isCompleted(): bool
    {
        return !empty($this->getProfilePictureName())
               && !empty($this->getPassportName())
        ;
    }

    public function getCurrency(): ?Currency
    {
        if (!$this->preferredCurrency) {
            return null;
        }

        return new Currency($this->preferredCurrency);
    }

    public function setCurrency(?Currency $currency): void
    {
        if ($currency) {
            $this->preferredCurrency = $currency->getCode();
        }
    }
}
