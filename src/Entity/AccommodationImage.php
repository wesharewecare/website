<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class AccommodationImage
{
    use TimestampableEntity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Accommodation", inversedBy="images", cascade={"persist"})
     */
    protected $accommodation;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="accommodation_image", fileNameProperty="imageName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/gif",
     *         "image/bmp",
     *     }
     * )
     */
    private $imageFile;

    /**
     * @var string|null
     *
     * @ORM\Column
     */
    private $imageName;

    public function getAccommodation()
    {
        return $this->accommodation;
    }

    public function setAccommodation(?Accommodation $accommodation): void
    {
        $this->accommodation = $accommodation;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }
}
