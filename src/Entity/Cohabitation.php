<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CohabitationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Money\Currency;
use Money\Money;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=CohabitationRepository::class)
 *
 * @Vich\Uploadable
 */
class Cohabitation
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var AccommodationDemand
     *
     * @ORM\OneToOne(targetEntity="AccommodationDemand", mappedBy="cohabitation")
     */
    private $accommodationDemand;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(
     *     mapping="cohabitation_agreement",
     *     fileNameProperty="agreementName",
     *     mimeType="agreementMimeType"
     * )
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes="application/pdf"
     * )
     */
    private $agreementFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $agreementName;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(
     *     mapping="cohabitation_agreement",
     *     fileNameProperty="agreementSignedName",
     *     mimeType="agreementMimeType"
     * )
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes="application/pdf"
     * )
     */
    private $agreementSignedFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $agreementSignedName;

    /**
     * @var string
     */
    private $agreementMimeType = 'application/pdf';

    /**
     * @var CohabitationWorkflowStep
     *
     * @ORM\OneToOne(targetEntity="CohabitationWorkflowStep", cascade={"persist", "remove"})
     */
    private $currentStep;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="CohabitationWorkflowStep", mappedBy="cohabitation", cascade={"persist", "remove"})
     */
    private $cohabitationWorkflowSteps;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $closingExplanation;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $insuranceChecked = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $depositChecked = false;

    /**
     * @var int|null
     *
     * @Assert\Range(min=0)
     *
     * @ORM\Column(type="integer", options={"unsigned" : true}, nullable=true)
     */
    private $priceAmount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $currencyCode = 'EUR';

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[{}]"})
     */
    private $agreementParams = [[]];

    public function __construct()
    {
        $this->cohabitationWorkflowSteps = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccommodationDemand(): AccommodationDemand
    {
        return $this->accommodationDemand;
    }

    public function setAccommodationDemand(AccommodationDemand $accommodationDemand): void
    {
        $this->accommodationDemand = $accommodationDemand;
        $accommodationDemand->setCohabitation($this);
    }

    public function getAgreementFile(): ?File
    {
        return $this->agreementFile;
    }

    public function setAgreementFile(?File $agreementFile): void
    {
        $this->agreementFile = $agreementFile;

        if (null !== $agreementFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getAgreementName(): ?string
    {
        return $this->agreementName;
    }

    public function setAgreementName(?string $agreementName): void
    {
        $this->agreementName = $agreementName;
    }

    public function getAgreementSignedFile(): ?File
    {
        return $this->agreementSignedFile;
    }

    public function setAgreementSignedFile(?File $agreementSignedFile): void
    {
        $this->agreementSignedFile = $agreementSignedFile;

        if (null !== $agreementSignedFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getAgreementSignedName(): ?string
    {
        return $this->agreementSignedName;
    }

    public function setAgreementSignedName(?string $agreementSignedName): void
    {
        $this->agreementSignedName = $agreementSignedName;
    }

    public function getAgreementMimeType(): string
    {
        return $this->agreementMimeType;
    }

    public function setAgreementMimeType(?string $agreementMimeType): void
    {
    }

    public function getCurrentStep(): CohabitationWorkflowStep
    {
        return $this->currentStep;
    }

    public function setCurrentStep(CohabitationWorkflowStep $currentStep): void
    {
        $this->currentStep = $currentStep;
    }

    public function getCohabitationWorkflowSteps(): ArrayCollection
    {
        return $this->cohabitationWorkflowSteps;
    }

    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function getClosingExplanation(): ?string
    {
        return $this->closingExplanation;
    }

    public function setClosingExplanation(?string $closingExplanation): void
    {
        $this->closingExplanation = $closingExplanation;
    }

    public function isInsuranceChecked(): bool
    {
        return $this->insuranceChecked;
    }

    public function setInsuranceChecked(bool $insuranceChecked): void
    {
        $this->insuranceChecked = $insuranceChecked;
    }

    public function isDepositChecked(): bool
    {
        return $this->depositChecked;
    }

    public function setDepositChecked(bool $depositChecked): void
    {
        $this->depositChecked = $depositChecked;
    }

    public function getPriceAmount(): ?int
    {
        return $this->priceAmount;
    }

    public function setPriceAmount(?int $priceAmount): void
    {
        $this->priceAmount = $priceAmount;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    public function getAgreementParams(): array
    {
        return $this->agreementParams;
    }

    public function setAgreementParams(array $agreementParams): void
    {
        $this->agreementParams = $agreementParams;
    }

    public function getAccommodation(): Accommodation
    {
        return $this->accommodationDemand->getAccommodation();
    }

    public function addCohabitationWorkflowStep(CohabitationWorkflowStep $cohabitationWorkflowStep): void
    {
        if (!$this->cohabitationWorkflowSteps->contains($cohabitationWorkflowStep)) {
            $this->cohabitationWorkflowSteps->add($cohabitationWorkflowStep);
            $cohabitationWorkflowStep->setCohabitation($this);
            $this->setCurrentStep($cohabitationWorkflowStep);
        }
    }

    public function removeCohabitationWorkflowStep(CohabitationWorkflowStep $cohabitationWorkflowStep): void
    {
        $this->cohabitationWorkflowSteps->removeElement($cohabitationWorkflowStep);
    }

    public function getStepsDone(): array
    {
        $stepsDone = [];

        /** @var CohabitationWorkflowStep $workflowStep */
        foreach ($this->cohabitationWorkflowSteps as $workflowStep) {
            $stepsDone[$workflowStep->getStatus()] = [
                'date' => $workflowStep->getUpdatedAt(),
            ];
        }

        return $stepsDone;
    }

    public function getCurrency(): Currency
    {
        return new Currency($this->currencyCode);
    }

    public function setCurrency(?Currency $currency): void
    {
        $this->currencyCode = $currency->getCode();
    }

    public function getPrice(): ?Money
    {
        if (!$this->currencyCode) {
            return null;
        }

        if (!$this->priceAmount) {
            return new Money(0, new Currency($this->currencyCode));
        }

        return new Money($this->priceAmount, new Currency($this->currencyCode));
    }

    public function getAgreementParamsValues(): array
    {
        $params = reset($this->agreementParams);

        foreach ($params as $param => $value) {
            if (str_contains(strtolower($param), 'date') && \array_key_exists('date', $value)) {
                $params[$param] = new \DateTime($value['date'], new \DateTimeZone($value['timezone']));
            }
        }

        return $params;
    }
}
