<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\AssociationAgreementVersionEnum;
use App\Enum\AssociationRadiusUnitEnum;
use App\Enum\AssociationTypeEnum;
use App\Repository\AssociationRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use EmailChecker\Constraints as EmailCheckerAssert;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use libphonenumber\PhoneNumber;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=AssociationRepository::class)
 *
 * @Vich\Uploadable
 */
class Association
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="association_logo", fileNameProperty="logoName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/gif",
     *         "image/bmp",
     *     }
     * )
     */
    private $logoFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $logoName;

    /**
     * @var PhoneNumber|null
     *
     * @ORM\Column(type="phone_number", nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="AssociationTypeType")
     * @DoctrineAssert\Enum(entity="App\Enum\AssociationTypeEnum")
     */
    private $type = AssociationTypeEnum::TYPE_BOTH;

    /**
     * @return int
     * @ORM\Column(type="integer", options={"default" : 50, "unsigned" : true}))
     */
    private $radiusRange = 50;

    /**
     * @return string
     *
     * @ORM\Column(type="AssociationRadiusUnitType")
     * @DoctrineAssert\Enum(entity="App\Enum\AssociationRadiusUnitEnum")
     */
    private $radiusUnit = AssociationRadiusUnitEnum::UNIT_KMS;

    /**
     * @return int
     * @ORM\Column(type="integer", options={"default" : 50, "unsigned" : true}))
     */
    private $radiusRangeInKms = 50;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="association", orphanRemoval=true)
     */
    private $users;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $activated = true;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true}))
     */
    private $platformUse = 0;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $checkInsurance = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $checkDeposit = true;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Event", mappedBy="association", cascade={"persist", "remove"})
     * @ORM\OrderBy({"date" : "ASC"})
     */
    private $events;

    /**
     * @var string
     *
     * @ORM\Column(type="AssociationAgreementVersionType")
     * @DoctrineAssert\Enum(entity="App\Enum\AssociationAgreementVersionEnum")
     */
    private $agreementVersion = AssociationAgreementVersionEnum::VERSION_DEFAULT;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Assert\Email(mode=Email::VALIDATION_MODE_HTML5)
     * @EmailCheckerAssert\NotThrowawayEmail
     */
    private $email;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="association", cascade={"persist", "remove"})
     */
    private $documents;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(Location $location): void
    {
        $this->location = $location;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    public function setLogoFile(?File $logoFile): void
    {
        $this->logoFile = $logoFile;

        if (null !== $logoFile) {
            $this->updatedAt = new DateTime();
        }
    }

    public function getLogoName(): ?string
    {
        return $this->logoName;
    }

    public function setLogoName(?string $logoName): void
    {
        $this->logoName = $logoName;
    }

    public function getPhoneNumber(): ?PhoneNumber
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?PhoneNumber $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getRadiusRange(): int
    {
        return $this->radiusRange;
    }

    public function setRadiusRange(int $radiusRange): void
    {
        $this->radiusRange = $radiusRange;
    }

    public function getRadiusUnit(): string
    {
        return $this->radiusUnit;
    }

    public function setRadiusUnit(string $radiusUnit): void
    {
        $this->radiusUnit = $radiusUnit;
    }

    public function getRadiusRangeInKms(): int
    {
        return $this->radiusRangeInKms;
    }

    public function setRadiusRangeInKms(int $radiusRangeInKms): void
    {
        $this->radiusRangeInKms = $radiusRangeInKms;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function isActivated(): bool
    {
        return $this->activated;
    }

    public function setActivated(bool $activated): void
    {
        $this->activated = $activated;
    }

    public function getPlatformUse(): int
    {
        return $this->platformUse;
    }

    public function setPlatformUse(int $platformUse): void
    {
        $this->platformUse = $platformUse;
    }

    public function isCheckInsurance(): bool
    {
        return $this->checkInsurance;
    }

    public function setCheckInsurance(bool $checkInsurance): void
    {
        $this->checkInsurance = $checkInsurance;
    }

    public function isCheckDeposit(): bool
    {
        return $this->checkDeposit;
    }

    public function setCheckDeposit(bool $checkDeposit): void
    {
        $this->checkDeposit = $checkDeposit;
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function getAgreementVersion(): string
    {
        return $this->agreementVersion;
    }

    public function setAgreementVersion(string $agreementVersion): void
    {
        $this->agreementVersion = $agreementVersion;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function addEvent(Event $event): void
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
        }
    }

    public function removeEvent(Event $event): void
    {
        $this->events->removeElement($event);
    }

    public function addDocument(Document $document): void
    {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
            $document->setAssociation($this);
        }
    }

    public function removeDocument(Document $document): void
    {
        $this->documents->removeElement($document);
    }
}
