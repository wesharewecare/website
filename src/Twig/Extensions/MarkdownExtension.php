<?php

declare(strict_types=1);

namespace App\Twig\Extensions;

class MarkdownExtension extends \Parsedown
{
    public function blockTable($Line, array $Block = null)
    {
        if (!isset($Block) || isset($Block['type']) || isset($Block['interrupted'])) {
            return;
        }

        if (false !== strpos($Block['element']['text'], '|') && '' === rtrim($Line['text'], ' -:|')) {
            $alignments = [];

            $divider = $Line['text'];

            $divider = trim($divider);
            $divider = trim($divider, '|');

            $dividerCells = explode('|', $divider);

            foreach ($dividerCells as $dividerCell) {
                $dividerCell = trim($dividerCell);

                if ('' === $dividerCell) {
                    continue;
                }

                $alignment = null;

                if (':' === $dividerCell[0]) {
                    $alignment = 'left';
                }

                if (':' === substr($dividerCell, -1)) {
                    $alignment = 'left' === $alignment ? 'center' : 'right';
                }

                $alignments[] = $alignment;
            }

            $HeaderElements = [];

            $header = $Block['element']['text'];

            $header = trim($header);
            $header = trim($header, '|');

            $headerCells = explode('|', $header);

            foreach ($headerCells as $index => $headerCell) {
                $headerCell = trim($headerCell);

                $HeaderElement = [
                    'name' => 'th',
                    'text' => $headerCell,
                    'handler' => 'line',
                ];

                if (isset($alignments[$index])) {
                    $alignment = $alignments[$index];

                    $HeaderElement['attributes'] = [
                        'style' => 'text-align: '.$alignment.';',
                    ];
                }

                $HeaderElements[] = $HeaderElement;
            }

            $Block = [
                'alignments' => $alignments,
                'identified' => true,
                'element' => [
                    'name' => 'table',
                    'handler' => 'elements',
                    'attributes' => [
                        'class' => 'table',
                    ],
                ],
            ];

            $Block['element']['text'][] = [
                'name' => 'thead',
                'handler' => 'elements',
            ];

            $Block['element']['text'][] = [
                'name' => 'tbody',
                'handler' => 'elements',
                'text' => [],
            ];

            $Block['element']['text'][0]['text'][] = [
                'name' => 'tr',
                'handler' => 'elements',
                'text' => $HeaderElements,
            ];

            return $Block;
        }
    }

    protected function elements(array $Elements): string
    {
        $markup = '';

        foreach ($Elements as $Element) {
            $markup .= $this->element($Element);
        }

        return $markup;
    }
}
