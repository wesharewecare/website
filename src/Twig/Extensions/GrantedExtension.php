<?php

declare(strict_types=1);

namespace App\Twig\Extensions;

use App\Entity\User;
use App\Service\GrantedService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class GrantedExtension extends AbstractExtension
{
    private $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('role', [$this, 'getRole']),
            new TwigFilter('readable_role', [$this, 'getReadableRole']),
            new TwigFilter('is_manager', [$this, 'isManager']),
        ];
    }

    public function getRole(User $user): string
    {
        return $this->grantedService->getRole($user);
    }

    public function getReadableRole(User $user): string
    {
        return $this->grantedService->getReadableRole($user);
    }

    public function isManager(User $user): bool
    {
        return $this->grantedService->isManager($user);
    }
}
