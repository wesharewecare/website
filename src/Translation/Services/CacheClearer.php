<?php

declare(strict_types=1);

namespace App\Translation\Services;

use Bazinga\Bundle\JsTranslationBundle\Dumper\TranslationDumper;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CacheClearer
{
    private $kernelCacheDir;
    private $translator;
    private $filesystem;
    private $translationDumper;

    public function __construct(
        ParameterBagInterface $parameterBag,
        TranslatorInterface $translator,
        Filesystem $filesystem,
        TranslationDumper $translationDumper
    ) {
        $this->kernelCacheDir = $parameterBag->get('kernel.cache_dir');
        $this->translator = $translator;
        $this->filesystem = $filesystem;
        $this->translationDumper = $translationDumper;
    }

    public function clearAndWarmUp(?string $locale = null): void
    {
        $translationDir = sprintf('%s/translations', $this->kernelCacheDir);
        $jsTranslationDir = sprintf('%s/bazinga-js-translation', $this->kernelCacheDir);
        $this->translationDumper->dump('public/js/');

        $finder = new Finder();

        // Make sure the directory exists
        $this->filesystem->mkdir($translationDir);
        $this->filesystem->mkdir($jsTranslationDir);

        // Remove the translations for this locale
        $files = $finder
            ->files()
            ->name($locale ? '*.'.$locale.'.*' : '*')
            ->in([$translationDir, $jsTranslationDir])
        ;
        foreach ($files as $file) {
            $this->filesystem->remove($file);
        }

        // Build them again
        if ($this->translator instanceof WarmableInterface) {
            $this->translator->warmUp($translationDir);
            $this->translator->warmUp($jsTranslationDir);
        }
    }
}
