<?php

declare(strict_types=1);

namespace App\Translation\Services;

use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Translation\MessageCatalogueInterface;
use Translation\Common\Model\Message;
use Translation\Common\Model\MessageInterface;
use Translation\Common\Storage;
use Translation\Common\TransferableStorage;

class TranslationEntityStorage implements Storage, TransferableStorage
{
    private $em;
    private $translationEntityDoctrineHelper;

    public function __construct(
        EntityManagerInterface $em,
        TranslationEntityDoctrineHelper $translationEntityDoctrineHelper
    ) {
        $this->em = $em;
        $this->translationEntityDoctrineHelper = $translationEntityDoctrineHelper;
    }

    public function get(string $locale, string $domain, string $key): ?MessageInterface
    {
        if (\in_array($domain, $this->translationEntityDoctrineHelper->getTranslatableDomains(), true)) {
            return $this->translationEntityDoctrineHelper->getMessageFromKey($key, $domain, $locale);
        }

        return null;
    }

    public function create(MessageInterface $message): void
    {
        $this->update($message);
    }

    public function update(MessageInterface $message): void
    {
        if (\in_array($message->getDomain(), $this->translationEntityDoctrineHelper->getTranslatableDomains(), true)) {
            $this->translationEntityDoctrineHelper->translate(
                $message->getKey(),
                $message->getLocale(),
                $message->getTranslation()
            );

            $this->em->flush();
        }
    }

    public function delete(string $locale, string $domain, string $key): void
    {
        $this->update(new Message($key, $domain, $locale));
    }

    public function export(MessageCatalogueInterface $catalogue, array $options = []): void
    {
        $locale = $catalogue->getLocale();
        $classes = $this->translationEntityDoctrineHelper->getTranslatableClasses();

        foreach ($classes as $class) {
            $domain = $this->translationEntityDoctrineHelper->getDomainFromClass($class);

            if (!\array_key_exists('domains', $options) || \in_array($domain, $options['domains'], true)) {
                $objects = $this->em->getRepository($class)->findAll();

                /** @var Translatable $object */
                foreach ($objects as $object) {
                    $translations = $this->translationEntityDoctrineHelper->getAllTranslations($object);
                    if (\array_key_exists($locale, $translations)) {
                        foreach ($translations[$locale] as $property => $value) {
                            $catalogue->set($this->translationEntityDoctrineHelper->getKey($class, $object, $property), $value, $domain);
                        }
                    }
                }
            }
        }
    }

    public function import(MessageCatalogueInterface $catalogue, array $options = []): void
    {
        $locale = $catalogue->getLocale();
        $translatableDomains = $this->translationEntityDoctrineHelper->getTranslatableDomains();

        foreach ($translatableDomains as $translatableDomain) {
            foreach ($catalogue->all($translatableDomain) as $key => $translation) {
                $this->translationEntityDoctrineHelper->translate($key, $locale, $translation);
            }
        }

        $this->em->flush();
    }
}
