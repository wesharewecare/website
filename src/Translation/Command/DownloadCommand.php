<?php

declare(strict_types=1);

namespace App\Translation\Command;

use App\Translation\Services\CacheClearer;
use App\Translation\Services\TranslationEntityStorage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Translation\Bundle\Catalogue\CatalogueWriter;
use Translation\Bundle\Command\BundleTrait;
use Translation\Bundle\Command\StorageTrait;
use Translation\Bundle\Service\ConfigurationManager;
use Translation\Bundle\Service\StorageManager;

class DownloadCommand extends Command
{
    use BundleTrait;
    use StorageTrait;

    protected static $defaultName = 'translation:download';

    private $configurationManager;
    private $cacheCleaner;
    private $catalogueWriter;
    private $translationEntityHelper;

    public function __construct(
        StorageManager $storageManager,
        ConfigurationManager $configurationManager,
        CacheClearer $cacheCleaner,
        CatalogueWriter $catalogueWriter,
        TranslationEntityStorage $translationEntityHelper
    ) {
        $this->storageManager = $storageManager;
        $this->configurationManager = $configurationManager;
        $this->cacheCleaner = $cacheCleaner;
        $this->catalogueWriter = $catalogueWriter;
        $this->translationEntityHelper = $translationEntityHelper;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Replace local messages with messages from remote')
            ->setHelp(
                <<<'EOT'
The <info>%command.name%</info> will erase all your local translations and replace them with translations downloaded from the remote.
EOT
            )
            ->addArgument('configuration', InputArgument::OPTIONAL, 'The configuration to use', 'default')
            ->addOption('bundle', 'b', InputOption::VALUE_REQUIRED, 'The bundle you want update translations from.')
            ->addOption('export-config', 'exconf', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'Options to send to the StorageInterface::export() function. Ie, when downloading. Example: --export-config foo:bar', [])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $configName = $input->getArgument('configuration');
        $config = $this->configurationManager->getConfiguration($configName);
        $storage = $this->getStorage($configName);

        $this->configureBundleDirs($input, $config);

        $translationsDirectory = $config->getOutputDir();
        $md5BeforeDownload = $this->hashDirectory($translationsDirectory);

        $exportOptions = $this->cleanParameters($input->getOption('export-config'));
        $catalogues = $storage->download($exportOptions);
        $this->catalogueWriter->writeCatalogues($config, $catalogues);

        $translationsCount = 0;
        foreach ($catalogues as $locale => $catalogue) {
            $this->translationEntityHelper->import($catalogue);
            foreach ($catalogue->all() as $domain => $messages) {
                $translationsCount += \count($messages);
            }
        }

        $io->text("<info>{$translationsCount}</info> translations have been downloaded.");

        $md5AfterDownload = $this->hashDirectory($translationsDirectory);

        if ($md5BeforeDownload !== $md5AfterDownload) {
            $io->success('Translations updated successfully!');
            $this->cacheCleaner->clearAndWarmUp();
        } else {
            $io->success('All translations were up to date.');
        }

        return 0;
    }

    private function hashDirectory(string $directory): ?string
    {
        if (!is_dir($directory)) {
            return null;
        }

        $finder = new Finder();
        $finder->files()->in($directory)->notName('/~$/')->sortByName();

        $hash = hash_init('md5');
        foreach ($finder as $file) {
            hash_update_file($hash, $file->getRealPath());
        }

        return hash_final($hash);
    }

    private function cleanParameters(array $raw): array
    {
        $config = [];

        foreach ($raw as $string) {
            [$key, $value] = explode(':', $string, 2);
            $config[$key][] = $value;
        }

        return $config;
    }
}
