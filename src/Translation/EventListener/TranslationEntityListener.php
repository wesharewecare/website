<?php

declare(strict_types=1);

namespace App\Translation\EventListener;

use App\Translation\Services\TranslationEntityDoctrineHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Gedmo\Translatable\Translatable;
use Translation\Bundle\Service\StorageManager;
use Translation\Common\Model\Message;

class TranslationEntityListener
{
    private $defaultLocale;
    private $storage;
    private $translationEntityDoctrineHelper;

    public function __construct(
        TranslationEntityDoctrineHelper $translationEntityDoctrineHelper,
        StorageManager $storageManager,
        string $defaultLocale,
        string $translationStorageName
    ) {
        $this->storage = $storageManager->getStorage($translationStorageName);
        $this->defaultLocale = $defaultLocale;
        $this->translationEntityDoctrineHelper = $translationEntityDoctrineHelper;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $object = $args->getObject();

        if ($this->isTranslatableObject($object)) {
            $messages = $this->translationEntityDoctrineHelper->getMessagesFromObject($object);

            foreach ($messages as $message) {
                $this->storage->create($message);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $object = $args->getObject();

        if ($this->isTranslatableObject($object)) {
            $object->setTranslatableLocale($this->defaultLocale);
            $args->getObjectManager()->refresh($object);
            $messages = $this->translationEntityDoctrineHelper->getMessagesFromObject($object);

            foreach ($messages as $message) {
                $this->storage->update($message);
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $object = $args->getObject();

        if ($this->isTranslatableObject($object)) {
            $messages = $this->translationEntityDoctrineHelper->getMessagesFromObject($object);

            /** @var Message $message */
            foreach ($messages as $message) {
                $this->storage->delete($message->getLocale(), $message->getDomain(), $message->getKey());
            }
        }
    }

    private function isTranslatableObject(object $object): bool
    {
        return \in_array(Translatable::class, class_implements($object), true);
    }
}
