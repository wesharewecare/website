<?php

declare(strict_types=1);

namespace App\Notifier;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class NotificationFactory
{
    private $translator;
    private $urlGenerator;

    public function __construct(TranslatorInterface $translator, UrlGeneratorInterface $urlGenerator)
    {
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
    }

    public function getNotification(string $type, $entity, array $options = []): ?Notification
    {
        if (class_exists($type)) {
            $interfaces = class_implements($type);

            if (isset($interfaces[NotificationInterface::class])) {
                $notification = $this->create($type, $entity, $options);

                return (new Notification(
                    $notification->getSubject(),
                    $notification->getContext(),
                    $notification->getChannels()
                ))->content($notification->getContent());
            }
        }

        return null;
    }

    private function create(string $type, $entity, array $options): AbstractNotification
    {
        return new $type(
            $this->translator,
            $this->urlGenerator,
            $entity,
            $options
        );
    }
}
