<?php

declare(strict_types=1);

namespace App\Notifier;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractNotification implements NotificationInterface
{
    protected $translator;
    protected $urlGenerator;
    protected $entity;
    protected $options;

    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator,
        $entity,
        array $options
    ) {
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
        $this->entity = $entity;
        $this->options = $options;
    }

    protected function trans(string $key, array $parameters = [], string $domain = null): string
    {
        $locale = \array_key_exists('_locale', $this->options)
            ? $this->options['_locale']
            : null
        ;

        return $this->translator->trans($key, $parameters, $domain, $locale);
    }
}
