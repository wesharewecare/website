<?php

declare(strict_types=1);

namespace App\Notifier\Event;

class EventNearbyNotification extends AbstractEventNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.event.nearby.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.event.nearby.content', ['%name%' => $this->entity->getName()]);
    }
}
