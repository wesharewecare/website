<?php

declare(strict_types=1);

namespace App\Notifier\Event;

class EventParticipantNotification extends AbstractEventNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.event.participant.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.event.participant.content', ['%name%' => $this->entity->getName()]);
    }
}
