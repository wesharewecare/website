<?php

declare(strict_types=1);

namespace App\Notifier\Event;

class EventUpdatedNotification extends AbstractEventNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.event.updated.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.event.updated.content', ['%name%' => $this->entity->getName()]);
    }
}
