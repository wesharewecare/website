<?php

declare(strict_types=1);

namespace App\Notifier\Event;

class EventReminderNotification extends AbstractEventNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.event.reminder.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.event.reminder.content', ['%name%' => $this->entity->getName()]);
    }
}
