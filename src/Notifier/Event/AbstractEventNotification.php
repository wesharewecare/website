<?php

declare(strict_types=1);

namespace App\Notifier\Event;

use App\Entity\Event;
use App\Notifier\AbstractNotification;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractEventNotification extends AbstractNotification
{
    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator,
        Event $entity,
        array $options
    ) {
        parent::__construct($translator, $urlGenerator, $entity, $options);
    }

    public function getContext(): array
    {
        return [
            'action_text' => $this->trans('actions.show'),
            'action_url' => $this->urlGenerator->generate('event_show', ['id' => $this->entity->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
        ];
    }

    public function getChannels(): array
    {
        return [];
    }
}
