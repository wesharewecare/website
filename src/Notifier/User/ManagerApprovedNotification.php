<?php

declare(strict_types=1);

namespace App\Notifier\User;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ManagerApprovedNotification extends AbstractUserNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.manager_approved.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.manager_approved.content');
    }

    public function getContext(): array
    {
        $context = parent::getContext();
        $context['action_text'] = $this->trans('login.button');
        $context['action_url'] = $this->urlGenerator->generate('login', [], UrlGeneratorInterface::ABSOLUTE_URL);

        return $context;
    }
}
