<?php

declare(strict_types=1);

namespace App\Notifier\User;

use App\Entity\User;
use App\Notifier\AbstractNotification;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractUserNotification extends AbstractNotification
{
    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator,
        User $entity,
        array $options
    ) {
        parent::__construct($translator, $urlGenerator, $entity, $options);
    }

    public function getContext(): array
    {
        return [
            'action_text' => $this->trans('actions.show'),
        ];
    }

    public function getChannels(): array
    {
        return [];
    }
}
