<?php

declare(strict_types=1);

namespace App\Notifier\User;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NewUserNotification extends AbstractUserNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.registration.new_user.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.registration.new_user.content', ['%name%' => $this->entity->getFullname()]);
    }

    public function getContext(): array
    {
        $context = parent::getContext();
        $context['action_url'] = $this->urlGenerator->generate('user_show', ['id' => $this->entity->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return $context;
    }
}
