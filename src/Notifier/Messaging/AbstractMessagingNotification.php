<?php

declare(strict_types=1);

namespace App\Notifier\Messaging;

use App\Entity\Thread;
use App\Notifier\AbstractNotification;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractMessagingNotification extends AbstractNotification
{
    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator,
        ?Thread $entity,
        array $options
    ) {
        parent::__construct($translator, $urlGenerator, $entity, $options);
    }

    public function getContext(): array
    {
        return [
            'action_text' => $this->trans('actions.show'),
            'action_url' => $this->urlGenerator->generate('messages', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ];
    }

    public function getChannels(): array
    {
        return [];
    }
}
