<?php

declare(strict_types=1);

namespace App\Notifier\Messaging;

class MessagingNewNotification extends AbstractMessagingNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.messaging.new.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.messaging.new.content');
    }
}
