<?php

declare(strict_types=1);

namespace App\Notifier\Accommodation;

class AccommodationNewNotification extends AbstractAccommodationNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.accommodation.new.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.accommodation.new.content');
    }
}
