<?php

declare(strict_types=1);

namespace App\Notifier\Accommodation;

class AccommodationPublishedNotification extends AbstractAccommodationNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.accommodation.published.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.accommodation.published.content');
    }
}
