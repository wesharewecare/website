<?php

declare(strict_types=1);

namespace App\Notifier\Association;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AssociationActivatedNotification extends AbstractAssociationNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.association_activated.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.association_activated.content', ['%name%' => $this->entity->getName()]);
    }

    public function getContext(): array
    {
        $context = parent::getContext();
        $context['action_text'] = $this->trans('signup.button');
        $context['action_url'] = $this->urlGenerator->generate('register_manager', [], UrlGeneratorInterface::ABSOLUTE_URL);

        return $context;
    }
}
