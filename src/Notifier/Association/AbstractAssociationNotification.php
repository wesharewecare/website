<?php

declare(strict_types=1);

namespace App\Notifier\Association;

use App\Entity\Association;
use App\Notifier\AbstractNotification;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractAssociationNotification extends AbstractNotification
{
    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator,
        Association $entity,
        array $options
    ) {
        parent::__construct($translator, $urlGenerator, $entity, $options);
    }

    public function getContext(): array
    {
        return [];
    }

    public function getChannels(): array
    {
        return [];
    }
}
