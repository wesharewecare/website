<?php

declare(strict_types=1);

namespace App\Notifier;

interface NotificationInterface
{
    public function getSubject(): string;

    public function getContent(): string;

    public function getContext(): array;

    public function getChannels(): array;
}
