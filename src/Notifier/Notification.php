<?php

declare(strict_types=1);

namespace App\Notifier;

use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification as BaseNotification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;

class Notification extends BaseNotification implements EmailNotificationInterface
{
    private $context;

    public function __construct(string $subject = '', array $context = [], array $channels = [])
    {
        parent::__construct($subject, $channels);
        $this->context = $context;
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $email = NotificationEmail::asPublicEmail()
            ->to($recipient->getEmail())
            ->subject($this->getSubject())
        ;

        if (\array_key_exists('markdown', $this->context) && $this->context['markdown']) {
            $email->markdown($this->getContent());
        } else {
            $email->content($this->getContent(), \array_key_exists('raw', $this->context) ? $this->context['raw'] : false);
        }

        if (\array_key_exists('importance', $this->context)) {
            $email->importance($this->context['importance']);
        }

        if (\array_key_exists('action_url', $this->context) && \array_key_exists('action_text', $this->context)) {
            $email->action($this->context['action_text'], $this->context['action_url']);
        }

        return new EmailMessage($email);
    }
}
