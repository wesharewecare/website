<?php

declare(strict_types=1);

namespace App\Notifier\AccommodationDemand;

class AccommodationDemandNewNotification extends AbstractAccommodationDemandNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.accommodation_demand.new.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.accommodation_demand.new.content', ['%name%' => $this->entity->getAccommodation()->getName()]);
    }
}
