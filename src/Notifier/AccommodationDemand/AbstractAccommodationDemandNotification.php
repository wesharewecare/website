<?php

declare(strict_types=1);

namespace App\Notifier\AccommodationDemand;

use App\Entity\AccommodationDemand;
use App\Notifier\AbstractNotification;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractAccommodationDemandNotification extends AbstractNotification
{
    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator,
        AccommodationDemand $entity,
        array $options
    ) {
        parent::__construct($translator, $urlGenerator, $entity, $options);
    }

    public function getContext(): array
    {
        return [
            'action_text' => $this->trans('actions.show'),
            'action_url' => $this->urlGenerator->generate('user_show', ['id' => $this->entity->getSenior()->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
        ];
    }

    public function getChannels(): array
    {
        return [];
    }
}
