<?php

declare(strict_types=1);

namespace App\Notifier\AccommodationDemand;

class AccommodationDemandUpdatedNotification extends AbstractAccommodationDemandNotification
{
    public function getSubject(): string
    {
        return $this->trans('mail.accommodation_demand.updated.subject');
    }

    public function getContent(): string
    {
        return $this->trans('mail.accommodation_demand.updated.content', ['%name%' => $this->entity->getAccommodation()->getName()]);
    }
}
