<?php

declare(strict_types=1);

namespace App\Command\Event;

use App\Entity\Event;
use App\Enum\NotificationTypeEnum;
use App\Notifier\Event\EventReminderNotification;
use App\Repository\EventRepository;
use App\Service\NotifierHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EventReminderCommand extends Command
{
    private const DAYS_BEFORE = 2;

    private $entityManager;
    private $eventRepo;
    private $notifierHelper;

    public function __construct(
        EntityManagerInterface $entityManager,
        EventRepository $eventRepo,
        NotifierHelper $notifierHelper
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->eventRepo = $eventRepo;
        $this->notifierHelper = $notifierHelper;
    }

    protected function configure(): void
    {
        $this
            ->setName('event:reminder')
            ->setDescription('Notify participants X days before event')
            ->addOption(
                'days',
                'd',
                InputOption::VALUE_OPTIONAL,
                'Number of days before event',
                self::DAYS_BEFORE
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $daysBefore = (int) $input->getOption('days');
        $events = $this->eventRepo->getUpcoming($daysBefore);

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($events));
        }

        /** @var Event $event */
        foreach ($events as $event) {
            $this->notifierHelper->sendNotifications(
                EventReminderNotification::class,
                $event->getUsers()->toArray(),
                NotificationTypeEnum::TYPE_EVENT_REMINDER,
                $event
            );

            $event->setReminderNotificationSent(true);

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->progressFinish();
        }

        $this->entityManager->flush();

        return 0;
    }
}
