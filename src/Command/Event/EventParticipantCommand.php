<?php

declare(strict_types=1);

namespace App\Command\Event;

use App\Entity\Event;
use App\Enum\NotificationTypeEnum;
use App\Notifier\Event\EventParticipantNotification;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use App\Service\NotifierHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EventParticipantCommand extends Command
{
    private $entityManager;
    private $notifierHelper;
    private $eventRepo;
    private $userRepo;

    public function __construct(
        EntityManagerInterface $entityManager,
        NotifierHelper $notifierHelper,
        EventRepository $eventRepo,
        UserRepository $userRepo
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->eventRepo = $eventRepo;
        $this->notifierHelper = $notifierHelper;
        $this->userRepo = $userRepo;
    }

    protected function configure(): void
    {
        $this
            ->setName('event:participant')
            ->setDescription('Notify managers of new participants to events')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $events = $this->eventRepo->getWithNewParticipants();

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($events));
        }

        /** @var Event $event */
        foreach ($events as $event) {
            $this->notifierHelper->sendNotifications(
                EventParticipantNotification::class,
                $this->userRepo->getAssociationManager($event->getAssociation()),
                NotificationTypeEnum::TYPE_EVENT_PARTICIPANT,
                $event
            );

            $event->setNewParticipantNotificationToSend(false);

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->progressFinish();
        }

        $this->entityManager->flush();

        return 0;
    }
}
