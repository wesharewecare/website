<?php

declare(strict_types=1);

namespace App\Command\Event;

use App\Entity\Event;
use App\Enum\EventStatusEnum;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EventPastCommand extends Command
{
    private $entityManager;
    private $eventRepo;

    public function __construct(EntityManagerInterface $entityManager, EventRepository $eventRepo)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->eventRepo = $eventRepo;
    }

    protected function configure(): void
    {
        $this
            ->setName('event:is_past')
            ->setDescription('Close event if date reached')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $events = $this->eventRepo->getWithDateReached();

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($events));
        }

        /** @var Event $event */
        foreach ($events as $event) {
            if (EventStatusEnum::STATUS_CANCELED === $event->getStatus()) {
                $event->clearUsers();
            }

            $event->setStatus(EventStatusEnum::STATUS_CLOSED);

            $this->entityManager->persist($event);

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->progressFinish();
        }

        $this->entityManager->flush();

        return 0;
    }
}
