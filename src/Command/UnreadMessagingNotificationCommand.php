<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Enum\ThreadChannelTypeEnum;
use App\Notifier\Messaging\MessagingNewNotification;
use App\Repository\ThreadRepository;
use App\Repository\UserRepository;
use App\Service\MessagingHelper;
use App\Service\NotifierHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UnreadMessagingNotificationCommand extends Command
{
    private const HOURS_INTERVAL = 2;
    private const MAX_NOTIF = 3;

    private $em;
    private $userRepo;
    private $notifierHelper;
    private $threadRepo;
    private $threadChannelType;
    private $notificationField;
    private $messagingHelper;

    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepo,
        NotifierHelper $notifierHelper,
        ThreadRepository $threadRepo,
        MessagingHelper $messagingHelper,
        ?string $name = null
    ) {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->notifierHelper = $notifierHelper;
        $this->userRepo = $userRepo;
        $this->threadRepo = $threadRepo;
        $this->messagingHelper = $messagingHelper;

        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('messaging:unread_messages')
            ->setDescription('Sent email notifications if unread messages')
            ->addArgument('threadChannelType', InputArgument::REQUIRED, 'Which threadChannelType?')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->threadChannelType = $input->getArgument('threadChannelType');

        if (!\in_array($this->threadChannelType, ThreadChannelTypeEnum::getChoices())) {
            throw new \Exception('threadChannelType value not allowed.');
        }

        $this->notificationField = '$.messaging_'.$this->threadChannelType;

        $batchSize = 1000;
        $minimumId = 0;

        do {
            [$batchCount, $minimumId] = $this->runOneBatch($io, $batchSize, $minimumId);
        } while ($batchCount === $batchSize);

        return 0;
    }

    private function runOneBatch(SymfonyStyle $io, int $batchSize, int $minimumId): array
    {
        $batch = $this->userRepo->createQueryBuilder('user')
            ->distinct()
            ->innerJoin('user.userNotification', 'notification')
            ->andWhere('user.id > :minId')
            ->andWhere('JSON_VALUE(notification.notifications, :notificationType) = 1')
            ->andWhere("(notification.notifSentAt is null or
                        CURRENT_TIMESTAMP() >= DATE_ADD(notification.notifSentAt, CASE WHEN notification.nbNotifSent > 1 THEN 2 ELSE 1 END,'DAY'))")
            ->andWhere('notification.nbNotifSent < :maxNotif')
            ->setMaxResults($batchSize)
            ->orderBy('user.id')
            ->setParameter('minId', $minimumId)
            ->setParameter('notificationType', $this->notificationField)
            ->setParameter('maxNotif', self::MAX_NOTIF)
            ->getQuery()->getResult()
        ;

        $batchCount = \count($batch);

        if ($batchCount > 0) {
            $minimumId = $batch[$batchCount - 1]->getId();

            /** @var User $user */
            foreach ($batch as $user) {
                $threads = $this->messagingHelper->getSubscribedThread(
                    $user,
                    $this->threadChannelType
                );

                if (\count($threads) > 0) {
                    if ($this->threadRepo->hasUnread($user, $threads, self::HOURS_INTERVAL)) {
                        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
                            $io->comment("\t<comment>".$user->getEmail().'</comment>');
                        }

                        $this->notifierHelper->sendNotification(
                            MessagingNewNotification::class,
                            $user->getEmail(),
                            null,
                            ['_locale' => $user->getProfile()->getLocaleLanguage()]
                        );

                        $user->getUserNotification()->increaseNbNotifSent();
                        $this->em->persist($user);
                    }
                }

                $user = null;
                unset($user);
                $threads = null;
                unset($threads);
            }

            $batch = null;
            unset($batch);

            $this->em->flush();
            $this->em->clear();
            gc_collect_cycles();
        }

        return [$batchCount, $minimumId];
    }
}
