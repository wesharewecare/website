<?php

declare(strict_types=1);

namespace App\Command;

use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationStatusEnum;
use App\Repository\CohabitationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CheckCohabitationCompletedCommand extends Command
{
    private $entityManager;
    private $cohabitationRepo;

    public function __construct(EntityManagerInterface $entityManager, CohabitationRepository $cohabitationRepo)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->cohabitationRepo = $cohabitationRepo;
    }

    protected function configure(): void
    {
        $this
            ->setName('cohabitation:is_completed')
            ->setDescription('Close cohabitation if end date reached')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $cohabitations = $this->cohabitationRepo->getWithEndDateReached();

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($cohabitations));
        }

        foreach ($cohabitations as $cohabitation) {
            $accommodationDemand = $cohabitation->getAccommodationDemand();
            $accommodation = $accommodationDemand->getAccommodation();
            $accommodationDemand->setStatus(AccommodationDemandStatusEnum::STATUS_CLOSED);
            $accommodation->setStatus(AccommodationStatusEnum::STATUS_CLOSED);

            $this->entityManager->persist($cohabitation);

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->progressFinish();
        }

        $this->entityManager->flush();

        return 0;
    }
}
