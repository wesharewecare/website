<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\Author;
use App\DTO\ThreadGroup;
use App\DTO\Topic;
use App\Entity\Message;
use App\Entity\ThreadAccommodationDemand;
use App\Entity\ThreadInterface;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\ThreadChannelTypeEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Repository\AssociationRepository;
use App\Repository\ThreadRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Service\FilterService;
use Misd\Linkify\Linkify;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class MessagingProvider
{
    private $em;
    private $userRepo;
    private $threadRepo;
    private $associationRepo;
    private $messagingHelper;
    private $security;
    private $translator;
    private $uploaderHelper;
    private $linkify;
    private $grantedService;
    private $imageService;
    private $assetsManager;
    private $router;
    private $imagine;
    private $filesystem;
    private $projectDir;
    private $twig;

    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepo,
        ThreadRepository $threadRepo,
        AssociationRepository $associationRepo,
        MessagingHelper $messagingHelper,
        Security $security,
        TranslatorInterface $translator,
        UploaderHelper $uploaderHelper,
        GrantedService $grantedService,
        ImageService $imageService,
        Packages $assetsManager,
        UrlGeneratorInterface $router,
        FilterService $imagine,
        Filesystem $filesystem,
        KernelInterface $kernel,
        Environment $twig
    ) {
        $this->em = $em;
        $this->userRepo = $userRepo;
        $this->threadRepo = $threadRepo;
        $this->associationRepo = $associationRepo;
        $this->messagingHelper = $messagingHelper;
        $this->security = $security;
        $this->translator = $translator;
        $this->uploaderHelper = $uploaderHelper;
        $this->grantedService = $grantedService;
        $this->imageService = $imageService;
        $this->assetsManager = $assetsManager;
        $this->router = $router;
        $this->imagine = $imagine;
        $this->filesystem = $filesystem;
        $this->twig = $twig;
        $this->projectDir = $kernel->getProjectDir();
        $this->linkify = new Linkify();
    }

    public function getAllowedThreads(User $user): array
    {
        $threads = [];

        foreach ($this->messagingHelper->getSubscribedThread($user, ThreadChannelTypeEnum::INDIVIDUAL_CHANNEL) as $thread) {
            $threads[] = $this->getThreadGroupData($thread, $user);
        }

        foreach ($this->messagingHelper->getSubscribedThread($user, ThreadChannelTypeEnum::GROUP_CHANNEL) as $thread) {
            $threads[] = $this->getThreadGroupData($thread, $user);
        }

        usort($threads, function ($a, $b) {
            if ($a->getLastMessageAt() && $b->getLastMessageAt()
                && $a->getLastMessageAt() === $b->getLastMessageAt()
            ) {
                return 0;
            }

            if ($a->isFavorite() && !$b->isFavorite()) {
                return -1;
            }

            if ($b->isFavorite() && !$a->isFavorite()) {
                return 1;
            }

            return ($a->getLastMessageAt() < $b->getLastMessageAt()) ? 1 : -1;
        });

        return $threads;
    }

    public function getMessageDTO(Message $message): \App\DTO\Message
    {
        $sender = $message->getSender();
        $thread = $message->getThread();

        $threadGroupId = (string) $thread->getId();

        $author = new Author(
            (string) $sender->getId(),
            $sender->getFirstname(),
            $sender->getLastname(),
            $this->imageService->getAvatar($sender)
        );

        return new \App\DTO\Message(
            (string) $message->getId(),
            $threadGroupId,
            $author,
            $message->getCreatedAt(),
            $this->linkify->process($message->getBody(), ['attr' => ['target' => '_blank']])
        );
    }

    public function getThreadIRI(ThreadInterface $thread): string
    {
        return '/threads/'.$thread->getId();
    }

    public function getUserIRI(User $user): string
    {
        return '/users/'.$user->getId();
    }

    public function getAllTopicsIRI(User $user): array
    {
        $topics = [$this->getUserIRI($user)];

        foreach ($this->messagingHelper->getSubscribedThread($user) as $thread) {
            $topics[] = $this->getThreadIRI($thread);
        }

        return $topics;
    }

    public function getThreadsByThreadGroupId(string $id): array
    {
        return $this->threadRepo->findBy(['id' => $id]);
    }

    public function getThreadGroupData(ThreadInterface $thread, User $user): ThreadGroup
    {
        $icon = '';
        $avatar = '';
        $channelTypeTooltip = '';
        $showFooter = true;
        $replaceFooterForm = null;

        $isManager = $this->grantedService->isGranted($user, UserRoleEnum::ROLE_ASSOCIATION);

        if ($thread instanceof ThreadAccommodationDemand) {
            $demand = $thread->getAccommodationDemand();
            $senior = $demand->getSenior();
            $young = $demand->getYoung();
            $profileUrl = null;

            if ($demand->getCohabitation()) {
                $profileUrl = $this->router->generate('cohabitation_show', ['id' => $demand->getCohabitation()->getId()]);
            }

            if (AccommodationDemandStatusEnum::STATUS_REFUSED === $demand->getStatus()) {
                $replaceFooterForm = $this->twig->render('accommodation_demand/_message_refused_explanation.html.twig', [
                    'accommodationDemand' => $demand,
                ]);
            }

            if ($isManager) {
                $icon = 'house-user';
                $name = $young->getFirstname().' & '.$senior->getFirstname();

                if (!$profileUrl) {
                    $profileUrl = $this->router->generate('user_show', ['id' => $senior->getId()]);
                }
            } else {
                if ($user === $senior) {
                    $otherParticipant = $young;

                    if (!$profileUrl) {
                        $profileUrl = $this->router->generate('user_show', ['id' => $young->getId()]);
                    }
                } else {
                    $otherParticipant = $senior;

                    if (!$profileUrl) {
                        $profileUrl = $this->router->generate('user_show', ['id' => $senior->getId()]);
                    }
                }

                $avatar = $this->imageService->getAvatar($otherParticipant);
                $name = $otherParticipant->getFullName();
            }

            foreach ($thread->getUsers() as $participant) {
                if ($participant !== $user) {
                    $channelTypeTooltip .= $participant->getFullname()."\n";
                }
            }

            $channelType = ThreadChannelTypeEnum::GROUP_CHANNEL;
        } else {
            $otherParticipant = $thread->getOtherUser($user);
            $avatar = $this->imageService->getAvatar($otherParticipant);
            $name = $otherParticipant->getFullName();
            $channelType = ThreadChannelTypeEnum::INDIVIDUAL_CHANNEL;
            $profileUrl = $this->security->isGranted(UserVoterEnum::SHOW, $otherParticipant)
                ? $this->router->generate('user_show', ['id' => $otherParticipant->getId()])
                : null
            ;
        }

        return new ThreadGroup(
            (string) $thread->getId(),
            $name,
            $channelType,
            $channelTypeTooltip,
            $icon,
            $avatar,
            $thread->getLastMessageAt(),
            [new Topic($this->getThreadIRI($thread), $thread->getId())],
            $this->messagingHelper->isRead($thread, $user),
            $this->messagingHelper->isFavorite((string) $thread->getId(), $user),
            $profileUrl,
            $showFooter,
            $replaceFooterForm
        );
    }
}
