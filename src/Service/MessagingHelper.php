<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\AccommodationDemand;
use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\ThreadAccommodationDemand;
use App\Entity\ThreadFavorite;
use App\Entity\ThreadInterface;
use App\Entity\ThreadMetadata;
use App\Entity\User;
use App\Enum\ThreadChannelTypeEnum;
use App\Repository\ThreadAccommodationDemandRepository;
use App\Repository\ThreadFavoriteRepository;
use App\Repository\ThreadMetadataRepository;
use App\Repository\ThreadRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use HtmlSanitizer\Sanitizer;

class MessagingHelper
{
    public const DIRECT_MESSAGE = 'DEFAULT';
    public const CHANNEL_ACCOMMODATION_DEMAND = 'ACCOMMODATION_DEMAND';

    protected $em;
    protected $threadRepo;
    protected $threadMetadataRepo;
    protected $threadFavoriteRepo;
    protected $threadAccommodationDemandRepo;
    protected $userRepo;
    protected $grantedService;

    public function __construct(
        EntityManagerInterface $em,
        ThreadRepository $threadRepo,
        ThreadMetadataRepository $threadMetadataRepo,
        ThreadFavoriteRepository $threadFavoriteRepo,
        ThreadAccommodationDemandRepository $threadAccommodationDemandRepo,
        UserRepository $userRepo,
        GrantedService $grantedService
    ) {
        $this->em = $em;
        $this->threadRepo = $threadRepo;
        $this->threadMetadataRepo = $threadMetadataRepo;
        $this->threadFavoriteRepo = $threadFavoriteRepo;
        $this->threadAccommodationDemandRepo = $threadAccommodationDemandRepo;
        $this->userRepo = $userRepo;
        $this->grantedService = $grantedService;
    }

    public function createMessage(Thread $thread, string $body, User $sender): Message
    {
        $sanitizer = Sanitizer::create(['extensions' => ['basic']]);

        $message = new Message();
        $message->setBody($sanitizer->sanitize($body));
        $message->setSender($sender);
        $thread->addMessage($message);

        $this->em->persist($message);

        $this->markAsRead($thread, $sender);

        return $message;
    }

    public function markAsRead(Thread $thread, User $user): void
    {
        if ($lastMessage = $thread->getMessages()->last()) {
            /** @var ThreadMetadata $meta */
            $meta = $this->threadMetadataRepo->findOneBy(['thread' => $thread, 'participant' => $user]);

            if (!$meta) {
                $meta = new ThreadMetadata();
                $meta->setThread($thread);
                $meta->setParticipant($user);
            }

            $meta->setLastMessageReadAt($lastMessage->getCreatedAt());

            /*if (!$this->hasUnread($user)) {
                if ($user && $userNotif = $user->getUserNotification()) {
                    $userNotif->resetNewMessage();
                }
            }*/

            $this->em->persist($meta);
        }
    }

    public function isRead(ThreadInterface $thread, User $user): bool
    {
        if ($thread->getMessages()->isEmpty()) {
            return true;
        }

        /** @var ThreadMetadata $meta */
        if ($meta = $this->threadMetadataRepo->findOneBy(['thread' => $thread, 'participant' => $user])) {
            return $meta->getLastMessageReadAt() === $thread->getLastMessageAt();
        }

        return false;
    }

    public function hasUnread(User $user): bool
    {
        return $this->threadRepo->hasUnread($user, $this->getSubscribedThread($user));
    }

    public function isFavorite(string $thread, User $user): bool
    {
        return null !== $this->threadFavoriteRepo->findOneBy(['user' => $user, 'thread' => $thread]);
    }

    public function toggleFavorite(string $thread, User $user): void
    {
        $threadFavorite = $this->threadFavoriteRepo->findOneBy(['user' => $user, 'thread' => $thread]);

        if ($threadFavorite) {
            $this->em->remove($threadFavorite);
        } else {
            $threadFavorite = new ThreadFavorite();
            $threadFavorite->setUser($user);
            $threadFavorite->setThread($thread);
            $this->em->persist($threadFavorite);
        }

        $this->em->flush();
    }

    public function getThreadBySubjectAndParticipants(string $subject, array $participants): Thread
    {
        $thread = $this->threadRepo->getBySubjectAndUsers($subject, $participants);

        if (!$thread) {
            $thread = $this->createThread($subject, $participants);
        }

        return $thread;
    }

    public function getThreadAccommodationDemand(AccommodationDemand $demand): ThreadInterface
    {
        if ($thread = $demand->getThread()) {
            return $thread;
        }

        $participants = [$demand->getSenior(), $demand->getYoung()];

        foreach ($demand->getAssociations() as $association) {
            $participants = array_merge(
                $participants,
                $this->userRepo->getAssociationManager($association)
            );
        }

        $thread = $this->createThread(
            self::CHANNEL_ACCOMMODATION_DEMAND.'_'.$demand->getId(),
            $participants,
            ThreadAccommodationDemand::class
        );
        $demand->setThread($thread);

        return $thread;
    }

    public function getSubscribedThread(User $user, string $threadChannelType = null): array
    {
        $threads = [];

        if (!$threadChannelType || ThreadChannelTypeEnum::INDIVIDUAL_CHANNEL === $threadChannelType) {
            $threads = array_merge(
                $this->threadRepo->getByUserAndSubject($user, self::DIRECT_MESSAGE),
            );
        }

        if (!$threadChannelType || ThreadChannelTypeEnum::GROUP_CHANNEL === $threadChannelType) {
            $threads = array_merge(
                $threads,
                $this->threadAccommodationDemandRepo->getByUser($user),
            );
        }

        return $threads;
    }

    private function createThread(
        string $subject,
        array $participants,
        string $threadType = Thread::class
    ): ThreadInterface {
        $thread = new $threadType();
        $thread->setSubject($subject);

        /** @var User $participant */
        foreach ($participants as $participant) {
            if ($participant->getEmail()) {
                $thread->addUser($participant);
            }
        }

        $this->em->persist($thread);

        return $thread;
    }
}
