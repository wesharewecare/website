<?php

declare(strict_types=1);

namespace App\Service;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Symfony\Component\HttpFoundation\Cookie;

class JWTFactory
{
    private $jwtKey;
    private $host;

    public function __construct(string $jwtKey, string $baseHost)
    {
        $this->jwtKey = $jwtKey;
        $this->host = $baseHost;
    }

    public function getCookie(array $topics): Cookie
    {
        return Cookie::create('mercureAuthorization')
            ->withValue($this->getToken($topics))
            ->withPath('/.well-known/mercure')
            ->withDomain($this->host)
            ->withSecure(true)
            ->withHttpOnly(true)
            ->withRaw(false)
            ->withSameSite(Cookie::SAMESITE_LAX)
        ;
    }

    private function getToken(array $topics): string
    {
        $key = Key\InMemory::plainText($this->jwtKey);
        $configuration = Configuration::forSymmetricSigner(new Sha256(), $key);

        return $configuration->builder()
            ->withClaim('mercure', ['subscribe' => $topics])
            ->getToken($configuration->signer(), $configuration->signingKey())
            ->toString()
        ;
    }
}
