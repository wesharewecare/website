<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Association;
use App\Entity\Cohabitation;
use App\Entity\CohabitationWorkflowStep;
use App\Entity\User;
use App\Enum\AssociationAgreementVersionEnum;
use App\Enum\CohabitationAvailableServicesEnum;
use App\Enum\CohabitationWorkflowStepStatusEnum;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Vich\UploaderBundle\Handler\UploadHandler;

class CohabitationWorkflowStepManager
{
    private $translator;
    private $twig;
    private $router;
    private $pdfGenerator;
    private $uploadHandler;

    public function __construct(
        TranslatorInterface $translator,
        Environment $twig,
        RouterInterface $router,
        Pdf $pdfGenerator,
        UploadHandler $uploadHandler
    ) {
        $this->translator = $translator;
        $this->twig = $twig;
        $this->router = $router;
        $this->pdfGenerator = $pdfGenerator;
        $this->uploadHandler = $uploadHandler;
    }

    public function getWorkflow(): array
    {
        return [
            CohabitationWorkflowStepStatusEnum::STATUS_COHABITATION_INIT => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_COHABITATION_INIT)),
                'next' => CohabitationWorkflowStepStatusEnum::STATUS_PRE_VISIT_DONE,
                'previous' => null,
            ],
            CohabitationWorkflowStepStatusEnum::STATUS_PRE_VISIT_DONE => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_PRE_VISIT_DONE)),
                'next' => CohabitationWorkflowStepStatusEnum::STATUS_INTERVIEW_DONE,
                'previous' => CohabitationWorkflowStepStatusEnum::STATUS_COHABITATION_INIT,
            ],
            CohabitationWorkflowStepStatusEnum::STATUS_INTERVIEW_DONE => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_INTERVIEW_DONE)),
                'next' => CohabitationWorkflowStepStatusEnum::STATUS_JOINT_MEETING_DONE,
                'previous' => CohabitationWorkflowStepStatusEnum::STATUS_PRE_VISIT_DONE,
            ],
            CohabitationWorkflowStepStatusEnum::STATUS_JOINT_MEETING_DONE => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_JOINT_MEETING_DONE)),
                'next' => CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT,
                'previous' => CohabitationWorkflowStepStatusEnum::STATUS_INTERVIEW_DONE,
            ],
            CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT)),
                'next' => CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED,
                'previous' => CohabitationWorkflowStepStatusEnum::STATUS_JOINT_MEETING_DONE,
            ],
            CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED)),
                'next' => CohabitationWorkflowStepStatusEnum::STATUS_APPOINTMENT_DONE,
                'previous' => CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT,
            ],
            CohabitationWorkflowStepStatusEnum::STATUS_APPOINTMENT_DONE => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_APPOINTMENT_DONE)),
                'next' => CohabitationWorkflowStepStatusEnum::STATUS_DEBRIEFING_DONE,
                'previous' => CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED,
            ],
            CohabitationWorkflowStepStatusEnum::STATUS_DEBRIEFING_DONE => [
                'name' => $this->translator->trans(CohabitationWorkflowStepStatusEnum::getReadableValue(CohabitationWorkflowStepStatusEnum::STATUS_DEBRIEFING_DONE)),
                'next' => null,
                'previous' => CohabitationWorkflowStepStatusEnum::STATUS_APPOINTMENT_DONE,
            ],
        ];
    }

    public function reachNextStep(
        Cohabitation $cohabitation,
        Association $association = null
    ): ?CohabitationWorkflowStep {
        $currentStep = $cohabitation->getCurrentStep();
        $nextStep = $this->getWorkflow()[$currentStep->getStatus()]['next'];

        if (!$nextStep || !$this->isStepReachable($cohabitation, $nextStep, $association)) {
            return null;
        }

        return $this->applyStep($cohabitation, $nextStep);
    }

    public function reachPreviousStep(Cohabitation $cohabitation): ?CohabitationWorkflowStep
    {
        $currentStep = $cohabitation->getCurrentStep();
        $previousStep = $this->getWorkflow()[$currentStep->getStatus()]['previous'];

        if (!$previousStep) {
            return null;
        }

        return $this->applyStep($cohabitation, $previousStep);
    }

    public function applyStep(Cohabitation $cohabitation, string $step): CohabitationWorkflowStep
    {
        $cohabitationWorkflowStep = new CohabitationWorkflowStep();
        $cohabitationWorkflowStep->setStatus($step);
        $cohabitation->addCohabitationWorkflowStep($cohabitationWorkflowStep);

        return $cohabitationWorkflowStep;
    }

    public function getStepsInformation(Cohabitation $cohabitation): array
    {
        $infos = [];

        $currentStep = $cohabitation->getCurrentStep();
        $workflow = $this->getWorkflow();

        do {
            $workflowStep = key($workflow);
            $isCurrentStep = $workflowStep === $workflow[$currentStep->getStatus()]['next'];

            if ($isCurrentStep) {
                switch ($workflowStep) {
                    case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT:
                        $infos[CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT] = $this->twig->render('cohabitation/_show_preview_link.html.twig', [
                            'path' => $this->router->generate('agreement_show', ['id' => $cohabitation->getId(), 'force_refresh' => 1]),
                            'text' => $this->translator->trans('cohabitation.workflow.see_agreement_preview.button'),
                        ]);

                        break;
                }
            } else {
                switch ($workflowStep) {
                    case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT:
                        $infos[CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT] = $this->twig->render('cohabitation/_show_agreement_button.html.twig', [
                            'path' => $this->router->generate('agreement_show', ['id' => $cohabitation->getId()]),
                            'text' => $this->translator->trans('cohabitation.workflow.see_agreement.button'),
                        ]);

                        break;

                    case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED:
                        $infos[CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED] = $this->twig->render('cohabitation/_show_agreement_button.html.twig', [
                            'path' => $this->router->generate('agreement_signed_show', ['id' => $cohabitation->getId()]),
                            'text' => $this->translator->trans('cohabitation.workflow.see_agreement_signed.button'),
                        ]);

                        break;
                }
            }

            next($workflow);
        } while (!$isCurrentStep);

        return $infos;
    }

    public function generateAgreement(Cohabitation $cohabitation, User $manager): void
    {
        $accommodationDemand = $cohabitation->getAccommodationDemand();
        $senior = $accommodationDemand->getSenior();
        $young = $accommodationDemand->getYoung();
        $association = $senior->getAssociation();
        $accommodation = $accommodationDemand->getAccommodation();

        $tmpName = '/tmp/agreement_'.$cohabitation->getId().'.pdf';

        file_put_contents(
            $tmpName,
            $this->pdfGenerator->getOutputFromHtml(
                $this->twig->render(
                    'cohabitation/agreement.html.twig',
                    [
                        'manager' => $manager,
                        'senior' => $senior,
                        'young' => $young,
                        'cohabitation' => $cohabitation,
                        'association' => $association,
                        'accommodation' => $accommodation,
                    ]
                ),
            )
        );

        $cohabitation->setAgreementFile(
            new UploadedFile(
                $tmpName,
                $tmpName,
                $cohabitation->getAgreementMimeType(),
                null,
                true
            )
        );
        $this->uploadHandler->upload($cohabitation, 'agreementFile');
    }

    public function getAgreementFieldsByVersion(string $agreementVersion): array
    {
        $fields = [];

        switch ($agreementVersion) {
            case AssociationAgreementVersionEnum::VERSION_DEFAULT:
                $fields = [
                    'cohabitation' => [
                        'chargesTypes' => ['required' => true],
                    ],
                ];

                break;

            case AssociationAgreementVersionEnum::VERSION_COHABILIS:
                $fields = [
                    'cohabitation' => [
                        'houseRooms' => ['required' => true],
                        'sharedHouseSize' => ['required' => true],
                        'chargesTypes' => ['required' => false],
                        'chargesAmount' => ['required' => true],
                        'depositAmount' => ['required' => true],
                    ],
                    'accommodation' => [
                        'type' => ['required' => true],
                        'houseSize' => ['required' => true],
                        'sharedHouseRooms' => ['required' => true],
                        'ancillaryRooms' => ['required' => false],
                    ],
                ];

                break;

            case AssociationAgreementVersionEnum::VERSION_1TOIT2AGES:
                $fields = [
                    'cohabitation' => [
                        'nbKeys' => ['required' => true],
                        'packageType' => ['required' => true],
                        'sharedRoomsDescription' => ['required' => false],
                        'youngTasks' => ['required' => false],
                        'chargesTypes' => ['required' => false],
                        'accountNumber' => ['required' => true],
                        'depositDate' => ['required' => true],
                    ],
                    'accommodation' => [
                        'type' => ['required' => true],
                    ],
                ];

                break;

            case AssociationAgreementVersionEnum::VERSION_SOLIDARIOS:
                $services = [];
                foreach (CohabitationAvailableServicesEnum::$solidariosServices as $service) {
                    $services[(string) CohabitationAvailableServicesEnum::getReadableValue($service)] = $service;
                }

                $fields = [
                    'cohabitation' => [
                        'youngTasks' => ['required' => true],
                        'availableServices' => [
                            'required' => false,
                            'choices' => $services,
                        ],
                        'passportSenior' => ['required' => true],
                        'passportYoung' => ['required' => true],
                        'contactSenior' => ['required' => true],
                        'contactYoung' => ['required' => true],
                        'seniorAcceptToPromote' => ['required' => false],
                        'youngAcceptToPromote' => ['required' => false],
                    ],
                ];

                break;
        }

        return $fields;
    }

    private function isStepReachable(Cohabitation $cohabitation, string $step, Association $association = null): bool
    {
        if ($association) {
            switch ($step) {
                case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT:
                    $propertyAccessor = PropertyAccess::createPropertyAccessor();
                    $fields = $this->getAgreementFieldsByVersion($association->getAgreementVersion());
                    $accommodation = $cohabitation->getAccommodation();
                    $agreementParams = $cohabitation->getAgreementParamsValues();

                    if (\array_key_exists('cohabitation', $fields)) {
                        foreach ($fields['cohabitation'] as $field => $content) {
                            if ($content['required'] && empty($agreementParams[$field])) {
                                return false;
                            }
                        }
                    }

                    if (\array_key_exists('accommodation', $fields)) {
                        foreach ($fields['accommodation'] as $field => $content) {
                            if ($content['required'] && empty($propertyAccessor->getValue($accommodation, $field))) {
                                return false;
                            }
                        }
                    }

                    return true;

                case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED:
                    return $cohabitation->getAgreementSignedName()
                        && (!$association->isCheckInsurance() || $cohabitation->isInsuranceChecked())
                        && (!$association->isCheckDeposit() || $cohabitation->isDepositChecked());
            }
        }

        return true;
    }
}
