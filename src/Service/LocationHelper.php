<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Location;

class LocationHelper
{
    public function copyLocationInformation(Location $origin, Location $destination, bool $fullCopy = false): void
    {
        $destination->setCountry($origin->getCountry());
        $destination->setCountryCode($origin->getCountryCode());
        $destination->setLatitude($origin->getLatitude());
        $destination->setLongitude($origin->getLongitude());

        if ($fullCopy) {
            $destination->setCity($origin->getCity());
            $destination->setDistrict($origin->getDistrict());
            $destination->setStreet($origin->getStreet());
            $destination->setZipCode($origin->getZipCode());
        }
    }
}
