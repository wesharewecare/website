<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Association;
use App\Entity\Event;
use App\Entity\User;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ImageService
{
    public const DEFAULT_AVATAR = 'build/images/no_photo.png';
    public const DEFAULT_ASSOCIATION_LOGO = 'build/images/no_logo.svg';
    public const DEFAULT_EVENT_IMAGE = 'build/images/no_event.svg';

    private $uploaderHelper;
    private $filesystem;
    private $assetsManager;
    private $projectDir;

    public function __construct(
        UploaderHelper $uploaderHelper,
        Filesystem $filesystem,
        Packages $assetsManager,
        KernelInterface $kernel
    ) {
        $this->uploaderHelper = $uploaderHelper;
        $this->filesystem = $filesystem;
        $this->assetsManager = $assetsManager;
        $this->projectDir = $kernel->getProjectDir();
    }

    public function getAvatar(User $user): ?string
    {
        return $this->getImage(
            $user->getProfile(),
            'profilePictureFile',
            self::DEFAULT_AVATAR
        );
    }

    public function getDefaultAvatar(): string
    {
        return $this->assetsManager->getUrl(self::DEFAULT_AVATAR);
    }

    public function getAssociationLogo(Association $association): ?string
    {
        return $this->getImage(
            $association,
            'logoFile',
            self::DEFAULT_ASSOCIATION_LOGO
        );
    }

    public function getDefaultAssociationLogo(): string
    {
        return $this->assetsManager->getUrl(self::DEFAULT_ASSOCIATION_LOGO);
    }

    public function getEventImage(Event $event): ?string
    {
        return $this->getImage(
            $event,
            'eventImageFile',
            self::DEFAULT_EVENT_IMAGE
        );
    }

    public function getDefaultEventImage(): string
    {
        return $this->assetsManager->getUrl(self::DEFAULT_EVENT_IMAGE);
    }

    private function getImage(object $entity, string $fieldName, string $defaultFile = null): ?string
    {
        $url = $this->uploaderHelper->asset($entity, $fieldName);

        if (!empty($url) && $this->filesystem->exists($this->projectDir.'/public'.$url)) {
            return $this->assetsManager->getUrl($url);
        }

        if ($defaultFile) {
            return $this->assetsManager->getUrl($defaultFile);
        }

        return null;
    }
}
