<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class GrantedService
{
    private $accessDecisionManager;

    public function __construct(AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->accessDecisionManager = $accessDecisionManager;
    }

    public function isGranted(User $user, string $role, $object = null): bool
    {
        $token = new UsernamePasswordToken($user, 'none', $user->getRoles());

        return $this->accessDecisionManager->decide($token, [$role], $object);
    }

    public function getRole(User $user): string
    {
        if ($this->isGranted($user, UserRoleEnum::ROLE_SUPER_ADMIN)) {
            $role = UserRoleEnum::ROLE_SUPER_ADMIN;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_ADMIN)) {
            $role = UserRoleEnum::ROLE_ADMIN;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_ASSOCIATION)) {
            $role = UserRoleEnum::ROLE_ASSOCIATION;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_YOUNG)) {
            $role = UserRoleEnum::ROLE_YOUNG;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_SENIOR)) {
            $role = UserRoleEnum::ROLE_SENIOR;
        } else {
            $role = UserRoleEnum::ROLE_USER;
        }

        return $role;
    }

    public function getProfileMode(User $user): string
    {
        if ($this->isGranted($user, UserRoleEnum::ROLE_ADMIN)) {
            $mode = UserProfileTypeEnum::PROFILE_ADMIN;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_ASSOCIATION)) {
            $mode = UserProfileTypeEnum::PROFILE_ASSOCIATION;
        } else {
            $mode = UserProfileTypeEnum::PROFILE_USER;
        }

        return $mode;
    }

    public function isManager(User $user): bool
    {
        return \in_array($this->getRole($user), UserRoleEnum::$managerAndAdmin, true);
    }

    public function getReadableRole(User $user): string
    {
        return UserRoleEnum::getReadableValue($this->getRole($user));
    }
}
