<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Notifier\NotificationFactory;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

class NotifierHelper
{
    private $notificationFactory;
    private $notifier;

    public function __construct(NotificationFactory $notificationFactory, NotifierInterface $notifier)
    {
        $this->notificationFactory = $notificationFactory;
        $this->notifier = $notifier;
    }

    public function sendNotifications(
        string $notificationClass,
        array $users,
        string $notificationType = null,
        $entity = null,
        array $options = []
    ): void {
        /** @var User $user */
        foreach ($users as $user) {
            if ($email = $user->getEmail()) {
                $notificationsPreferences = $user->getUserNotification()->getNotifications();

                if (!$notificationType
                    || (
                        \array_key_exists($notificationType, $notificationsPreferences)
                        && $notificationsPreferences[$notificationType]
                    )
                ) {
                    $options['_locale'] = $user->getProfile()->getLocaleLanguage();
                    $this->sendNotification($notificationClass, $email, $entity, $options);
                }
            }
        }
    }

    public function sendNotification(
        string $notificationClass,
        string $email,
        $entity = null,
        array $options = []
    ): void {
        $notification = $this->notificationFactory->getNotification(
            $notificationClass,
            $entity,
            $options
        );

        $this->notifier->send($notification, new Recipient($email));
    }
}
