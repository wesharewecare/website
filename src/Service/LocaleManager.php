<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Intl\Locales;

class LocaleManager
{
    private $defaultLocale;
    private $locales;
    private $currentLocale;
    private $request;
    private $requestStack;

    public function __construct(RequestStack $requestStack, string $defaultLocale, string $locales)
    {
        $this->requestStack = $requestStack;
        $this->defaultLocale = $defaultLocale;
        $this->locales = explode('|', $locales);
    }

    public function getDefaultLocale(): string
    {
        return $this->defaultLocale;
    }

    public function getCurrentLocale(): string
    {
        if (null === $this->request) {
            $this->request = $this->requestStack->getCurrentRequest();
        }

        if (!empty($this->currentLocale)) {
            $locale = $this->currentLocale;
        } elseif (null !== $this->request) {
            $locale = $this->request->getLocale();
        } else {
            $locale = $this->defaultLocale;
        }

        return $locale;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (HttpKernelInterface::MAIN_REQUEST === $event->getRequestType()) {
            $this->request = $event->getRequest();
            $this->currentLocale = $this->request->getLocale();
        }
    }

    public function getLocalesAllowed(): array
    {
        return $this->locales;
    }

    public function getLocalesTranslated(): array
    {
        $locales = [];

        foreach ($this->locales as $locale) {
            $locales[$locale] = Locales::getName($locale);
        }

        return $locales;
    }
}
