<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Intl\Languages;

class LanguageManager
{
    public const EUROPEAN_LANGUAGE_CODES = ['af', 'sq', 'an', 'hy', 'as', 'ae', 'be', 'bn', 'bs', 'br', 'bg', 'ca', 'kw', 'co', 'hr', 'cs', 'da', 'dv', 'nl', 'en', 'fo', 'fr', 'gd', 'gl', 'de', 'el', 'gu', 'hi', 'is', 'ga', 'it', 'ks', 'ku', 'la', 'lv', 'li', 'lt', 'lb', 'mk', 'gv', 'mr', 'ne', 'no', 'nb', 'nn', 'oc', 'or', 'os', 'pi', 'ps', 'fa', 'pl', 'pt', 'pa', 'ro', 'rm', 'ru', 'sa', 'sc', 'sr', 'sd', 'si', 'sk', 'sl', 'es', 'sv', 'tg', 'uk', 'ur', 'wa', 'cy', 'fy', 'yi'];

    public function getEuropeanLanguages(): array
    {
        $languages = [];

        foreach (self::EUROPEAN_LANGUAGE_CODES as $code) {
            $languages[$code] = Languages::getName($code);
        }

        return $languages;
    }
}
