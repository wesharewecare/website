<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Cohabitation;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use Vich\UploaderBundle\Storage\FileSystemStorage;

class MailerService
{
    private $translator;
    private $userRepo;
    private $mailer;
    private $fileSystemStorage;
    private $mailerTo;

    public function __construct(
        TranslatorInterface $translator,
        UserRepository $userRepo,
        MailerInterface $mailer,
        FileSystemStorage $fileSystemStorage,
        string $mailerTo
    ) {
        $this->translator = $translator;
        $this->userRepo = $userRepo;
        $this->mailer = $mailer;
        $this->fileSystemStorage = $fileSystemStorage;
        $this->mailerTo = $mailerTo;
    }

    public function getEmailConfirmation(string $emailRecipient): TemplatedEmail
    {
        return (new TemplatedEmail())
            ->to($emailRecipient)
            ->subject($this->translator->trans('mail.account.email_confirmation.subject'))
            ->htmlTemplate('email/registration_confirmation.html.twig')
        ;
    }

    public function sendPasswordResetEmail(string $emailRecipient, ResetPasswordToken $resetToken): void
    {
        $this->mailer->send(
            (new TemplatedEmail())
                ->to($emailRecipient)
                ->subject($this->translator->trans('mail.account.reset_password.subject'))
                ->htmlTemplate('email/reset_password.html.twig')
                ->context([
                    'resetToken' => $resetToken,
                ])
        );
    }

    public function sendContactFormEmail(string $sender, string $body): void
    {
        $this->mailer->send(
            (new TemplatedEmail())
                ->from(new Address($sender))
                ->to($this->mailerTo)
                ->subject($this->translator->trans('mail.contact_form.subject'))
                ->htmlTemplate('email/contact.html.twig')
                ->context([
                    'body' => $body,
                ])
        );
    }

    public function sendCohabitationAgreement(Cohabitation $cohabitation): void
    {
        $demand = $cohabitation->getAccommodationDemand();
        $senior = $demand->getSenior();
        $young = $demand->getYoung();

        $recipients = [$senior, $young];

        foreach ($demand->getAssociations() as $association) {
            $recipients = array_merge(
                $recipients,
                $this->userRepo->getAssociationManager($association)
            );
        }

        foreach ($recipients as $recipient) {
            if ($recipient->getEmail()) {
                $this->mailer->send(
                    (new TemplatedEmail())
                        ->to($recipient->getEmail())
                        ->subject($this->translator->trans('mail.cohabitation.agreement.subject'))
                        ->htmlTemplate('email/agreement.html.twig')
                        ->attachFromPath($this->fileSystemStorage->resolvePath($cohabitation, 'agreementFile'))
                        ->context([
                            'senior' => $senior,
                            'young' => $young,
                            'cohabitation' => $cohabitation,
                        ])
                );
            }
        }
    }
}
