<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Event;
use App\Entity\User;
use App\Enum\EventStatusEnum;
use App\Enum\NotificationTypeEnum;
use App\Notifier\Event\EventNearbyNotification;
use App\Notifier\Event\EventUpdatedNotification;
use App\Service\NotifierHelper;
use Doctrine\ORM\Event\OnFlushEventArgs;

class EventListener
{
    private $notifierHelper;

    public function __construct(NotifierHelper $notifierHelper)
    {
        $this->notifierHelper = $notifierHelper;
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();
        $userRepo = $em->getRepository(User::class);

        foreach ($entities as $entity) {
            if ($entity instanceof Event) {
                $changeSets = $uow->getEntityChangeSet($entity);

                $keysToRemove = ['updatedAt', 'reminderNotificationSent', 'newParticipantNotificationToSend'];
                foreach ($keysToRemove as $key) {
                    unset($changeSets[$key]);
                }

                if (!empty($changeSets)) {
                    $this->notifierHelper->sendNotifications(
                        EventUpdatedNotification::class,
                        $entity->getUsers()->toArray(),
                        NotificationTypeEnum::TYPE_EVENT_UPDATED,
                        $entity
                    );

                    foreach ($changeSets as $changeColumn => $changeSet) {
                        if ('status' === $changeColumn && EventStatusEnum::STATUS_OPEN === $entity->getStatus()) {
                            $this->notifierHelper->sendNotifications(
                                EventNearbyNotification::class,
                                $userRepo->getByDistance($entity->getLocation()),
                                NotificationTypeEnum::TYPE_EVENT_NEARBY,
                                $entity
                            );
                        }
                    }
                }
            }
        }
    }
}
