<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;

trait UnitOfWorkTrait
{
    public function addUpdateToUOW(EntityManagerInterface $em, $object): void
    {
        $uow = $em->getUnitOfWork();
        $metadata = $em->getClassMetadata(\get_class($object));

        $em->persist($object);

        if (!$uow->isScheduledForUpdate($object)) {
            $uow->computeChangeSet($metadata, $object);
        }

        if ($em->contains($object)) {
            $uow->recomputeSingleEntityChangeSet($metadata, $object);
        }
    }
}
