<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Association;
use App\Entity\User;
use App\Enum\AssociationRadiusUnitEnum;
use App\Notifier\Association\AssociationActivatedNotification;
use App\Service\NotifierHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

class AssociationListener
{
    use UnitOfWorkTrait;

    private $notifierHelper;

    public function __construct(NotifierHelper $notifierHelper)
    {
        $this->notifierHelper = $notifierHelper;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $association = $args->getObject();

        if (!$association instanceof Association) {
            return;
        }

        $this->setRadiusRangeInKms($association);
        $this->setExternalWebsiteUrl($association);
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();

        foreach ($entities as $entity) {
            if ($entity instanceof Association) {
                $changeSets = $uow->getEntityChangeSet($entity);
                foreach ($changeSets as $changeColumn => $changeSet) {
                    if ('activated' === $changeColumn && $entity->isActivated()) {
                        if ($email = $entity->getEmail()) {
                            $this->notifierHelper->sendNotification(
                                AssociationActivatedNotification::class,
                                $email,
                                $entity
                            );
                        }
                    }

                    if (\in_array($changeColumn, ['radiusRange', 'radiusUnit'], true)) {
                        $this->setRadiusRangeInKms($entity);
                    }

                    if ('website' === $changeColumn) {
                        $this->setExternalWebsiteUrl($entity);
                    }
                }
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $association = $args->getObject();

        if (!$association instanceof Association) {
            return;
        }

        $em = $args->getEntityManager();
        $userRepo = $em->getRepository(User::class);

        $managers = $userRepo->getAssociationManager($association);

        /** @var User $manager */
        foreach ($managers as $manager) {
            $manager->getProfile()->setIsApproved(false);
            $em->persist($manager);
        }
    }

    private function setRadiusRangeInKms(Association $association): void
    {
        switch ($association->getRadiusUnit()) {
            case AssociationRadiusUnitEnum::UNIT_MILES:
                $multiplier = 1.60934;

                break;

            default:
                $multiplier = 1;
        }

        $association->setRadiusRangeInKms((int) ($association->getRadiusRange() * $multiplier));
    }

    private function setExternalWebsiteUrl(Association $association): void
    {
        if ($association->getWebsite() && null === parse_url($association->getWebsite(), \PHP_URL_SCHEME)) {
            $association->setWebsite('http://'.$association->getWebsite());
        }
    }
}
