<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\AccommodationDemand;
use App\Entity\Cohabitation;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationStatusEnum;
use App\Enum\CohabitationWorkflowStepStatusEnum;
use App\Enum\NotificationTypeEnum;
use App\Notifier\AccommodationDemand\AccommodationDemandNewNotification;
use App\Notifier\AccommodationDemand\AccommodationDemandUpdatedNotification;
use App\Service\CohabitationWorkflowStepManager;
use App\Service\MessagingHelper;
use App\Service\NotifierHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

class AccommodationDemandListener
{
    use UnitOfWorkTrait;

    private $messagingHelper;
    private $cohabitationWorkflowStepManager;
    private $notifierHelper;

    public function __construct(
        MessagingHelper $messagingHelper,
        CohabitationWorkflowStepManager $cohabitationWorkflowStepManager,
        NotifierHelper $notifierHelper
    ) {
        $this->messagingHelper = $messagingHelper;
        $this->cohabitationWorkflowStepManager = $cohabitationWorkflowStepManager;
        $this->notifierHelper = $notifierHelper;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $demand = $args->getObject();

        if (!$demand instanceof AccommodationDemand) {
            return;
        }

        $senior = $demand->getSenior();
        $seniorProfile = $senior->getProfile();
        $young = $demand->getYoung();
        $youngProfile = $young->getProfile();

        if ($seniorProfile->isApproved() && $youngProfile->isApproved()) {
            $demand->setStatus(AccommodationDemandStatusEnum::STATUS_PENDING);
        } else {
            $demand->setStatus(AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL);
        }

        if ($association = $senior->getAssociation()) {
            $demand->addAssociation($association);
        }

        if ($association = $young->getAssociation()) {
            $demand->addAssociation($association);
        }
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $demand = $args->getObject();

        if (!$demand instanceof AccommodationDemand) {
            return;
        }

        $em = $args->getObjectManager();
        $userRepo = $em->getRepository(User::class);

        if (AccommodationDemandStatusEnum::STATUS_PENDING === $demand->getStatus()) {
            $em->persist($this->messagingHelper->getThreadAccommodationDemand($demand));
            $em->flush();
        }

        $recipients = [$demand->getSenior()];

        foreach ($demand->getAssociations() as $association) {
            $recipients = array_merge($recipients, $userRepo->getAssociationManager($association));
        }

        $this->notifierHelper->sendNotifications(
            AccommodationDemandNewNotification::class,
            $recipients,
            NotificationTypeEnum::TYPE_DEMAND_NEW,
            $demand
        );
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();
        $accommodationDemandRepo = $em->getRepository(AccommodationDemand::class);

        foreach ($entities as $entity) {
            if ($entity instanceof AccommodationDemand) {
                $changeSets = $uow->getEntityChangeSet($entity);
                foreach ($changeSets as $changeColumn => $changeSet) {
                    $young = $entity->getYoung();
                    $senior = $entity->getSenior();

                    if ('status' === $changeColumn) {
                        switch ($entity->getStatus()) {
                            case AccommodationDemandStatusEnum::STATUS_ACCEPTED:
                                $demands = $accommodationDemandRepo->findBy(['young' => $young]);

                                /** @var AccommodationDemand $demand */
                                foreach ($demands as $demand) {
                                    if ($demand !== $entity) {
                                        $demand->setStatus(AccommodationDemandStatusEnum::STATUS_ABANDONED);
                                        $this->addUpdateToUOW($em, $demand);
                                    }
                                }

                                $accommodation = $entity->getAccommodation();
                                $accommodation->setStatus(AccommodationStatusEnum::STATUS_OCCUPIED);
                                $this->addUpdateToUOW($em, $accommodation);

                                /** @var AccommodationDemand $demand */
                                foreach ($accommodation->getAccommodationDemands() as $demand) {
                                    if ($demand !== $entity) {
                                        $demand->setStatus(AccommodationDemandStatusEnum::STATUS_REFUSED);
                                        $this->addUpdateToUOW($em, $demand);
                                    }
                                }

                                $this->createCohabitation($em, $entity);
                            break;

                            case AccommodationDemandStatusEnum::STATUS_PENDING:
                                $this->addUpdateToUOW(
                                    $args->getEntityManager(),
                                    $this->messagingHelper->getThreadAccommodationDemand($entity)
                                );

                            break;
                        }

                        $this->notifierHelper->sendNotifications(
                            AccommodationDemandUpdatedNotification::class,
                            [$young, $senior],
                            NotificationTypeEnum::TYPE_DEMAND_UPDATED,
                            $entity
                        );
                    }
                }
            }
        }
    }

    private function createCohabitation(EntityManagerInterface $em, AccommodationDemand $accommodationDemand): void
    {
        $accommodation = $accommodationDemand->getAccommodation();

        $cohabitation = new Cohabitation();
        $cohabitation->setAccommodationDemand($accommodationDemand);
        $cohabitation->setCurrencyCode($accommodation->getCurrencyCode());
        $cohabitation->setPriceAmount($accommodation->getPriceAmount());
        $cohabitationWorkflowStep = $this->cohabitationWorkflowStepManager->applyStep($cohabitation, CohabitationWorkflowStepStatusEnum::STATUS_COHABITATION_INIT);
        $this->addUpdateToUOW($em, $cohabitation);
        $this->addUpdateToUOW($em, $cohabitationWorkflowStep);
    }
}
