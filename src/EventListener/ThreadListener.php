<?php

declare(strict_types=1);

namespace App\EventListener;

use App\DTO\RefreshTopics;
use App\Entity\Thread;
use App\Service\MessagingProvider;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\SerializerInterface;

class ThreadListener
{
    private $publisher;
    private $serializer;
    private $messagingProvider;

    public function __construct(
        PublisherInterface $publisher,
        SerializerInterface $serializer,
        MessagingProvider $messagingProvider
    ) {
        $this->publisher = $publisher;
        $this->serializer = $serializer;
        $this->messagingProvider = $messagingProvider;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        if ($entity instanceof Thread) {
            $users = $entity->getUsers()->toArray();

            foreach ($users as $user) {
                $this->publisher->__invoke(new Update(
                    $this->messagingProvider->getUserIRI($user),
                    $this->serializer->serialize(
                        new RefreshTopics(),
                        'json'
                    )
                ));
            }
        }
    }
}
