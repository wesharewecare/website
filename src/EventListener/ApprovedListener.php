<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\User;
use App\Enum\UserVoterEnum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class ApprovedListener implements EventSubscriberInterface
{
    private $router;
    private $tokenStorage;
    private $security;

    public function __construct(RouterInterface $router, TokenStorageInterface $tokenStorage, Security $security)
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest']],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $route = $event->getRequest()->attributes->get('_route');

        if (!$this->tokenStorage->getToken()
            || !$this->tokenStorage->getToken()->getUser() instanceof User
            || '_wdt' === $route
            || 0 === strpos($route, '_profiler')
            || 0 === strpos($route, 'php_translation_')
            || 'bazinga_jstranslation_js' === $route
            || 'account_delete' === $route
        ) {
            return;
        }

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if ('manager_not-approved' !== $route
            && !$this->security->isGranted(UserVoterEnum::ACCESS_ACCOUNT, $user)
        ) {
            $event->setResponse(new RedirectResponse($this->router->generate('manager_not-approved')));
        }
    }
}
