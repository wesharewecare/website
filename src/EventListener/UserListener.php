<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\AccommodationDemand;
use App\Entity\Association;
use App\Entity\User;
use App\Entity\UserNotification;
use App\Enum\NotificationTypeEnum;
use App\Enum\UserRoleEnum;
use App\Notifier\User\NewUserNotification;
use App\Service\GrantedService;
use App\Service\NotifierHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

class UserListener
{
    use UnitOfWorkTrait;

    private $grantedService;
    private $notificationTypeEnum;
    private $notifierHelper;

    public function __construct(
        GrantedService $grantedService,
        NotificationTypeEnum $notificationTypeEnum,
        NotifierHelper $notifierHelper
    ) {
        $this->grantedService = $grantedService;
        $this->notificationTypeEnum = $notificationTypeEnum;
        $this->notifierHelper = $notifierHelper;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $user = $args->getObject();

        if (!$user instanceof User) {
            return;
        }

        $notificationTypes = $this->notificationTypeEnum->getNotificationsByRole($this->grantedService->getRole($user));
        $userNotification = new UserNotification($notificationTypes);
        $user->setUserNotification($userNotification);
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();
        $associationRepo = $em->getRepository(Association::class);
        $userRepo = $em->getRepository(User::class);

        foreach ($entities as $entity) {
            if ($entity instanceof User) {
                $changeSets = $uow->getEntityChangeSet($entity);
                foreach ($changeSets as $changeColumn => $changeSet) {
                    $user = $entity;

                    if ('association' === $changeColumn) {
                        $oldAssociation = $changeSet[0];
                        $newAssociation = $changeSet[1];

                        $accommodationDemands = [];

                        switch ($this->grantedService->getRole($user)) {
                            case UserRoleEnum::ROLE_YOUNG:
                                $accommodationDemands = $entity->getAccommodationDemands();

                                break;

                            case UserRoleEnum::ROLE_SENIOR:
                                $accommodationDemands = $entity->getAccommodation()
                                    ? $entity->getAccommodation()->getAccommodationDemands()
                                    : [];

                                break;
                        }

                        /** @var AccommodationDemand $accommodationDemand */
                        foreach ($accommodationDemands as $accommodationDemand) {
                            if (!$oldAssociation || !$accommodationDemand->getAssociations()->contains($oldAssociation)) {
                                $accommodationDemand->addAssociation($newAssociation);
                                $this->addUpdateToUOW($em, $accommodationDemand);
                            }
                        }
                    }

                    if ('isVerified' === $changeColumn) {
                        if (!$this->grantedService->isManager($user)) {
                            $associations = $associationRepo->getAssociationsReachingUser($user);
                            $recipients = [];

                            foreach ($associations as $association) {
                                $recipients = array_merge($recipients, $userRepo->getAssociationManager($association));
                            }

                            $this->notifierHelper->sendNotifications(
                                NewUserNotification::class,
                                $recipients,
                                NotificationTypeEnum::TYPE_USER_NEW,
                                $user
                            );
                        }
                    }
                }
            }
        }
    }
}
