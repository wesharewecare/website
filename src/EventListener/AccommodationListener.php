<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Accommodation;
use App\Entity\User;
use App\Enum\AccommodationStatusEnum;
use App\Enum\NotificationTypeEnum;
use App\Notifier\Accommodation\AccommodationPublishedNotification;
use App\Service\NotifierHelper;
use Doctrine\ORM\Event\OnFlushEventArgs;

class AccommodationListener
{
    private $notifierHelper;

    public function __construct(NotifierHelper $notifierHelper)
    {
        $this->notifierHelper = $notifierHelper;
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();
        $userRepo = $em->getRepository(User::class);

        foreach ($entities as $entity) {
            if ($entity instanceof Accommodation) {
                $changeSets = $uow->getEntityChangeSet($entity);
                foreach ($changeSets as $changeColumn => $changeSet) {
                    if ('status' === $changeColumn
                        && AccommodationStatusEnum::STATUS_OPEN === $entity->getStatus()
                        && $association = $entity->getSenior()->getAssociation()
                    ) {
                        $this->notifierHelper->sendNotifications(
                            AccommodationPublishedNotification::class,
                            $userRepo->getYoungLookingForAccommodation($association),
                            NotificationTypeEnum::TYPE_ACCOMMODATION_PUBLISHED,
                            $entity
                        );
                    }
                }
            }
        }
    }
}
