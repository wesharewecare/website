<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\AccommodationDemand;
use App\Entity\Profile;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationStatusEnum;
use App\Enum\UserRoleEnum;
use App\Notifier\User\ManagerApprovedNotification;
use App\Service\GrantedService;
use App\Service\NotifierHelper;
use Doctrine\ORM\Event\OnFlushEventArgs;

class ProfileListener
{
    use UnitOfWorkTrait;

    private $grantedService;
    private $notifierHelper;

    public function __construct(GrantedService $grantedService, NotifierHelper $notifierHelper)
    {
        $this->grantedService = $grantedService;
        $this->notifierHelper = $notifierHelper;
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();
        $accommodationDemandRepo = $em->getRepository(AccommodationDemand::class);

        foreach ($entities as $entity) {
            if ($entity instanceof Profile) {
                $changeSets = $uow->getEntityChangeSet($entity);
                foreach ($changeSets as $changeColumn => $changeSet) {
                    $user = $entity->getUser();

                    if ('isApproved' === $changeColumn && $entity->isApproved()) {
                        if ($this->grantedService->isManager($user)) {
                            $this->notifierHelper->sendNotification(
                                ManagerApprovedNotification::class,
                                $user->getEmail(),
                                $user,
                                [
                                    '_locale' => $user->getProfile()->getLocaleLanguage(),
                                ]
                            );
                        } else {
                            $role = $this->grantedService->getRole($user);
                            $demands = $accommodationDemandRepo->getDemandsAwaitingApproval($user);

                            /** @var AccommodationDemand $demand */
                            foreach ($demands as $demand) {
                                if ((UserRoleEnum::ROLE_SENIOR === $role && $demand->getYoung()->getProfile()->isApproved())
                                    || (UserRoleEnum::ROLE_YOUNG === $role && $demand->getSenior()->getProfile()->isApproved())
                                ) {
                                    $demand->setStatus(AccommodationDemandStatusEnum::STATUS_PENDING);
                                    $this->addUpdateToUOW($em, $demand);
                                }
                            }
                        }
                    }

                    if ('showHomesharing' === $changeColumn && !$entity->isShowHomesharing()) {
                        switch ($this->grantedService->getRole($user)) {
                            case UserRoleEnum::ROLE_YOUNG:
                                $demands = $accommodationDemandRepo->findBy(['young' => $entity->getUser()]);

                                /** @var AccommodationDemand $demand */
                                foreach ($demands as $demand) {
                                    $demand->setStatus(AccommodationDemandStatusEnum::STATUS_ABANDONED);
                                    $this->addUpdateToUOW($em, $demand);
                                }

                                break;

                            case UserRoleEnum::ROLE_SENIOR:
                                if ($accommodation = $entity->getUser()->getAccommodation()) {
                                    $demands = $accommodation->getAccommodationDemands();

                                    /** @var AccommodationDemand $demand */
                                    foreach ($demands as $demand) {
                                        $demand->setStatus(AccommodationDemandStatusEnum::STATUS_ABANDONED);
                                        $this->addUpdateToUOW($em, $demand);
                                    }

                                    $accommodation->setStatus(AccommodationStatusEnum::STATUS_CLOSED);
                                    $this->addUpdateToUOW($em, $accommodation);
                                }

                                break;
                        }
                    }
                }
            }
        }
    }
}
