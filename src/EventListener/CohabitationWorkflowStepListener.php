<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\CohabitationWorkflowStep;
use App\Enum\CohabitationWorkflowStepStatusEnum;
use App\Service\CohabitationWorkflowStepManager;
use App\Service\MailerService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

class CohabitationWorkflowStepListener
{
    use UnitOfWorkTrait;

    private $mailerService;
    private $cohabitationWorkflowStepManager;
    private $security;

    public function __construct(
        MailerService $mailerService,
        CohabitationWorkflowStepManager $cohabitationWorkflowStepManager,
        Security $security
    ) {
        $this->mailerService = $mailerService;
        $this->cohabitationWorkflowStepManager = $cohabitationWorkflowStepManager;
        $this->security = $security;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $workflowStep = $args->getObject();

        if (!$workflowStep instanceof CohabitationWorkflowStep) {
            return;
        }

        $cohabitation = $workflowStep->getCohabitation();

        switch ($workflowStep->getStatus()) {
            case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT:
                $this->cohabitationWorkflowStepManager->generateAgreement($cohabitation, $this->security->getUser());
                $this->mailerService->sendCohabitationAgreement($cohabitation);

            break;
        }
    }
}
