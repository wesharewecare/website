<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Repository\AccommodationDemandRepository;
use App\Repository\UserRepository;
use App\Service\GrantedService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    private $userRepo;
    private $grantedService;
    private $accommodationDemandRepo;

    public function __construct(
        UserRepository $userRepo,
        GrantedService $grantedService,
        AccommodationDemandRepository $accommodationDemandRepo
    ) {
        $this->userRepo = $userRepo;
        $this->grantedService = $grantedService;
        $this->accommodationDemandRepo = $accommodationDemandRepo;
    }

    protected function supports($attribute, $subject): bool
    {
        if (!\in_array($attribute, UserVoterEnum::getValues(), true)) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case UserVoterEnum::ACCESS_ACCOUNT:
                return $this->canAccessAccount($subject);

            case UserVoterEnum::SHOW:
                return $this->canShow($token, $subject);

            case UserVoterEnum::EDIT:
                return $this->canEdit($token, $subject);

            case UserVoterEnum::EDIT_NOTIFICATIONS:
                return $this->canEditNotifications($token, $subject);

            case UserVoterEnum::DELETE:
                return $this->canDelete($token, $subject);

            case UserVoterEnum::CREATE_ACCOMMODATION:
                return $this->canCreateAccommodation($token, $subject);

            case UserVoterEnum::APPROVE:
                return $this->canApprove($token, $subject);

            case UserVoterEnum::CREATE_SENIOR_ACCOUNT:
                return $this->canCreateSeniorAccount($token);

            case UserVoterEnum::SEND_MESSAGE:
                return $this->canSendMessage($token, $subject);

            case UserVoterEnum::CLAIM_MANAGEMENT:
                return $this->canClaimManagement($token, $subject);

            case UserVoterEnum::CREATE_EVENT:
                return $this->canCreateEvent($subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canAccessAccount(User $user): bool
    {
        return !$this->grantedService->isGranted($user, UserRoleEnum::ROLE_ASSOCIATION)
            || $user->getProfile()->isApproved()
        ;
    }

    private function canShow(TokenInterface $token, User $user): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->adminCanManage($token, $user, false)
            || $this->associationCanManage($token, $user, false)
            || $user === $currentUser
            || (
                $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_YOUNG)
                && $this->grantedService->isGranted($user, UserRoleEnum::ROLE_SENIOR)
            )
            || (
                $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_SENIOR)
                && ($accommodation = $currentUser->getAccommodation())
                && $this->accommodationDemandRepo->getExistingDemand($user, $accommodation)
            );
    }

    private function canEdit(TokenInterface $token, User $user): bool
    {
        return $this->adminCanManage($token, $user)
            || $this->associationCanManage($token, $user)
            || $user === $token->getUser()
        ;
    }

    private function canEditNotifications(TokenInterface $token, User $user): bool
    {
        return $user === $token->getUser();
    }

    private function canDelete(TokenInterface $token, User $user): bool
    {
        return $this->canEdit($token, $user);
    }

    private function canCreateAccommodation(TokenInterface $token, User $user): bool
    {
        return $this->grantedService->isGranted($user, UserRoleEnum::ROLE_SENIOR)
            && $user->getProfile()->isCompleted()
            && $this->canEdit($token, $user)
        ;
    }

    private function canApprove(TokenInterface $token, User $user): bool
    {
        $profile = $user->getProfile();

        return !$profile->isApproved()
            && (
                ($profile->isCompleted() && $this->associationCanManage($token, $user))
                || (
                    $this->grantedService->isGranted($user, UserRoleEnum::ROLE_ASSOCIATION)
                    && $this->adminCanManage($token, $user)
                )
            )
        ;
    }

    private function canCreateSeniorAccount(TokenInterface $token): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            || (
                $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ASSOCIATION)
                && AssociationTypeEnum::TYPE_BOTH === $currentUser->getAssociation()->getType()
            );
    }

    private function canSendMessage(TokenInterface $token, User $user): bool
    {
        return $user !== $token->getUser()
            && $user->getEmail()
            && $this->canAccessAccount($user)
            && (
                $this->adminCanManage($token, $user, false)
                || $this->associationCanManage($token, $user)
            );
    }

    private function canClaimManagement(TokenInterface $token, User $user): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $associationManager = $currentUser->getAssociation();
        $association = $user->getAssociation();

        if (!$this->associationCanManage($token, $user, false)
            || !$associationManager || $associationManager === $association
        ) {
            return false;
        }

        if (!$association) {
            return true;
        }

        switch ($this->grantedService->getRole($user)) {
            case UserRoleEnum::ROLE_YOUNG:
                if (!$this->accommodationDemandRepo->getStatus(
                    $user,
                    [
                        AccommodationDemandStatusEnum::STATUS_ACCEPTED,
                        AccommodationDemandStatusEnum::STATUS_PENDING,
                        AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL,
                    ]
                )) {
                    return true;
                }

                break;

            case UserRoleEnum::ROLE_SENIOR:
                $accommodation = $user->getAccommodation();

                if (!$accommodation
                    || empty($this->accommodationDemandRepo->getByAccommodationAndStatus(
                        $accommodation,
                        [
                            AccommodationDemandStatusEnum::STATUS_ACCEPTED,
                            AccommodationDemandStatusEnum::STATUS_PENDING,
                            AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL,
                        ]
                    ))
                ) {
                    return true;
                }

                break;
        }

        return false;
    }

    private function canCreateEvent(User $user): bool
    {
        return UserRoleEnum::ROLE_ASSOCIATION === $this->grantedService->getRole($user);
    }

    private function adminCanManage(TokenInterface $token, User $user, bool $notSuperAdmin = true): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        if ($this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_SUPER_ADMIN)) {
            return true;
        }

        if ($notSuperAdmin && $this->grantedService->isGranted($user, UserRoleEnum::ROLE_SUPER_ADMIN)) {
            return false;
        }

        if ($this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            && $currentUser->getLocation()->getCountryCode() === $user->getLocation()->getCountryCode()) {
            return true;
        }

        return false;
    }

    private function associationCanManage(TokenInterface $token, User $user, bool $strict = true): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        if (UserRoleEnum::ROLE_ASSOCIATION === $this->grantedService->getRole($currentUser)) {
            $association = $currentUser->getAssociation();

            return (!$strict
                    || AssociationTypeEnum::TYPE_BOTH === $association->getType()
                    || UserRoleEnum::ROLE_YOUNG === $this->grantedService->getRole($user)
                ) && (
                    $user->getAssociation() === $association
                    || (
                        !$strict && $this->userRepo->isViewableByAssociation($association, $user)
                    )
                )
            ;
        }

        return false;
    }
}
