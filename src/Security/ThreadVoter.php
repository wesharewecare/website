<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Thread;
use App\Entity\ThreadAccommodationDemand;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\ThreadVoterEnum;
use App\Repository\UserRepository;
use App\Service\MessagingHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ThreadVoter extends Voter
{
    private $decisionManager;
    private $messagingHelper;
    private $userRepo;

    public function __construct(
        AccessDecisionManagerInterface $decisionManager,
        MessagingHelper $messagingHelper,
        UserRepository $userRepo
    ) {
        $this->decisionManager = $decisionManager;
        $this->messagingHelper = $messagingHelper;
        $this->userRepo = $userRepo;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, ThreadVoterEnum::getValues(), true)) {
            return false;
        }

        if (!$subject instanceof Thread) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case ThreadVoterEnum::SEE:
                return $this->canSee($token, $subject);

            case ThreadVoterEnum::REPLY:
                return $this->canReply($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canSee(TokenInterface $token, Thread $thread): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        return \in_array($thread, $this->messagingHelper->getSubscribedThread($user), true);
    }

    private function canReply(TokenInterface $token, Thread $thread): bool
    {
        return $this->canSee($token, $thread)
            && (
                !$thread instanceof ThreadAccommodationDemand
                || AccommodationDemandStatusEnum::STATUS_REFUSED !== $thread->getAccommodationDemand()->getStatus()
            )
        ;
    }
}
