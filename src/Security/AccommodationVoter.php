<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Accommodation;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationStatusEnum;
use App\Enum\AccommodationVoterEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Repository\AccommodationDemandRepository;
use App\Service\GrantedService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class AccommodationVoter extends Voter
{
    private $grantedService;
    private $accommodationDemandRepo;
    private $security;

    public function __construct(
        GrantedService $grantedService,
        AccommodationDemandRepository $accommodationDemandRepo,
        Security $security
    ) {
        $this->grantedService = $grantedService;
        $this->accommodationDemandRepo = $accommodationDemandRepo;
        $this->security = $security;
    }

    protected function supports($attribute, $subject): bool
    {
        if (!\in_array($attribute, AccommodationVoterEnum::getValues(), true)) {
            return false;
        }

        if (!$subject instanceof Accommodation) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case AccommodationVoterEnum::SHOW:
                return $this->canShow($token, $subject);

            case AccommodationVoterEnum::EDIT:
                return $this->canEdit($token, $subject);

            case AccommodationVoterEnum::CREATE_DEMAND:
                return $this->canCreateDemand($token, $subject);

            case AccommodationVoterEnum::APPROVE:
                return $this->canApprove($token, $subject);

            case AccommodationVoterEnum::CLOSE:
                return $this->canClose($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canShow(TokenInterface $token, Accommodation $accommodation): bool
    {
        return $this->security->isGranted(UserVoterEnum::SHOW, $accommodation->getSenior());
    }

    private function canEdit(TokenInterface $token, Accommodation $accommodation): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->adminCanManage($token, $accommodation)
            || $this->associationCanManage($token, $accommodation)
            || $accommodation->getSenior() === $currentUser
        ;
    }

    private function canCreateDemand(TokenInterface $token, Accommodation $accommodation): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $profile = $currentUser->getProfile();

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_YOUNG)
            && !$this->accommodationDemandRepo->getStatus(
                $currentUser,
                [
                    AccommodationDemandStatusEnum::STATUS_ACCEPTED,
                ]
            )
            && !$this->accommodationDemandRepo->getExistingDemand($currentUser, $accommodation)
            && AccommodationStatusEnum::STATUS_OPEN === $accommodation->getStatus()
            && $profile->isCompleted() && $profile->isShowHomesharing()
        ;
    }

    private function canApprove(TokenInterface $token, Accommodation $accommodation): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $accommodation->getSenior()->getProfile()->isApproved()
            && (
                $this->associationCanManage($token, $accommodation)
                || $accommodation->getSenior() === $currentUser
            )
            && (
                AccommodationStatusEnum::STATUS_CLOSED === $accommodation->getStatus()
                || (
                    AccommodationStatusEnum::STATUS_AWAITING_APPROVAL === $accommodation->getStatus()
                    && $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ASSOCIATION)
                )
            )
        ;
    }

    private function canClose(TokenInterface $token, Accommodation $accommodation): bool
    {
        return AccommodationStatusEnum::STATUS_OPEN === $accommodation->getStatus()
            && $this->canEdit($token, $accommodation)
        ;
    }

    private function adminCanManage(TokenInterface $token, Accommodation $accommodation): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        if ($this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_SUPER_ADMIN)) {
            return true;
        }

        if ($this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            && $currentUser->getLocation()->getCountryCode() === $accommodation->getSenior()->getLocation()->getCountryCode()) {
            return true;
        }

        return false;
    }

    private function associationCanManage(TokenInterface $token, Accommodation $accommodation): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        if (UserRoleEnum::ROLE_ASSOCIATION === $this->grantedService->getRole($currentUser)) {
            $association = $currentUser->getAssociation();

            return $accommodation->getSenior()->getAssociation() === $association;
        }

        return false;
    }
}
