<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Event;
use App\Entity\User;
use App\Enum\EventStatusEnum;
use App\Enum\EventVoterEnum;
use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EventVoter extends Voter
{
    private $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    protected function supports($attribute, $subject): bool
    {
        if (!\in_array($attribute, EventVoterEnum::getValues(), true)) {
            return false;
        }

        if (!$subject instanceof Event) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case EventVoterEnum::PARTICIPATE:
                return $this->canParticipate($token, $subject);

            case EventVoterEnum::EDIT:
                return $this->canEdit($token, $subject);

            case EventVoterEnum::DELETE:
                return $this->canDelete($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canParticipate(TokenInterface $token, Event $event): bool
    {
        /** @var User $user */
        $user = $token->getUser() instanceof User ? $token->getUser() : null;

        return $user && !$this->grantedService->isGranted($user, UserRoleEnum::ROLE_ASSOCIATION)
            && EventStatusEnum::STATUS_OPEN === $event->getStatus()
        ;
    }

    private function canEdit(TokenInterface $token, Event $event): bool
    {
        return $this->isCreatedByAssociation($token, $event)
            && EventStatusEnum::STATUS_CLOSED !== $event->getStatus()
        ;
    }

    private function canDelete(TokenInterface $token, Event $event): bool
    {
        return $this->isCreatedByAssociation($token, $event);
    }

    private function isCreatedByAssociation(TokenInterface $token, Event $event): bool
    {
        /** @var User $user */
        $user = $token->getUser() instanceof User ? $token->getUser() : null;

        return $user
            && UserRoleEnum::ROLE_ASSOCIATION === $this->grantedService->getRole($user)
            && $event->getAssociation() === $user->getAssociation()
        ;
    }
}
