<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\AccommodationDemand;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AccommodationDemandVoterEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AccommodationDemandVoter extends Voter
{
    private $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    protected function supports($attribute, $subject): bool
    {
        if (!\in_array($attribute, AccommodationDemandVoterEnum::getValues(), true)) {
            return false;
        }

        if (!$subject instanceof AccommodationDemand) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case AccommodationDemandVoterEnum::EDIT:
                return $this->canEdit($token, $subject);

            case AccommodationDemandVoterEnum::RESPOND:
                return $this->canRespond($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canEdit(TokenInterface $token, AccommodationDemand $accommodationDemand): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $currentUser === $accommodationDemand->getYoung()
            && \in_array(
                $accommodationDemand->getStatus(),
                [
                    AccommodationDemandStatusEnum::STATUS_AWAITING_USERS_APPROVAL,
                    AccommodationDemandStatusEnum::STATUS_PENDING,
                ],
                true
            )
        ;
    }

    private function canRespond(TokenInterface $token, AccommodationDemand $accommodationDemand): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $young = $accommodationDemand->getYoung();
        $senior = $accommodationDemand->getSenior();

        return AccommodationDemandStatusEnum::STATUS_PENDING === $accommodationDemand->getStatus()
            && !$accommodationDemand->getAssociations()->isEmpty()
            && $senior->getProfile()->isApproved()
            && $young->getProfile()->isApproved()
            && (
                $this->associationCanManage($token, $accommodationDemand)
                || $currentUser === $senior
            )
        ;
    }

    private function associationCanManage(TokenInterface $token, AccommodationDemand $accommodationDemand): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $senior = $accommodationDemand->getSenior();

        if (UserRoleEnum::ROLE_ASSOCIATION === $this->grantedService->getRole($currentUser)) {
            $association = $currentUser->getAssociation();

            return AssociationTypeEnum::TYPE_BOTH === $association->getType()
                && $senior->getAssociation() === $association
                && $accommodationDemand->getAssociations()->contains($association)
            ;
        }

        return false;
    }
}
