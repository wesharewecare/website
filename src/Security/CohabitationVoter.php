<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Cohabitation;
use App\Entity\User;
use App\Enum\AccommodationDemandStatusEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\CohabitationVoterEnum;
use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CohabitationVoter extends Voter
{
    private $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    protected function supports($attribute, $subject): bool
    {
        if (!\in_array($attribute, CohabitationVoterEnum::getValues(), true)) {
            return false;
        }

        if (!$subject instanceof Cohabitation) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case CohabitationVoterEnum::SHOW:
                return $this->canShow($token, $subject);

            case CohabitationVoterEnum::EDIT:
                return $this->canEdit($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canShow(TokenInterface $token, Cohabitation $cohabitation): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $accommodationDemand = $cohabitation->getAccommodationDemand();
        $senior = $accommodationDemand->getSenior();
        $young = $accommodationDemand->getYoung();

        return $currentUser === $young || $currentUser === $senior
            || $this->adminCanManage($token, $cohabitation)
            || $this->associationCanManage($token, $cohabitation)
        ;
    }

    private function canEdit(TokenInterface $token, Cohabitation $cohabitation): bool
    {
        $accommodationDemand = $cohabitation->getAccommodationDemand();

        return AccommodationDemandStatusEnum::STATUS_ACCEPTED === $accommodationDemand->getStatus()
            && $this->associationCanManage($token, $cohabitation, true)
        ;
    }

    private function adminCanManage(TokenInterface $token, Cohabitation $cohabitation): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $accommodationDemand = $cohabitation->getAccommodationDemand();
        $senior = $accommodationDemand->getSenior();

        if ($this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_SUPER_ADMIN)) {
            return true;
        }

        if ($this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            && $currentUser->getLocation()->getCountryCode() === $senior->getLocation()->getCountryCode()) {
            return true;
        }

        return false;
    }

    private function associationCanManage(
        TokenInterface $token,
        Cohabitation $cohabitation,
        bool $seniorAssociationOnly = false
    ): bool {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $accommodationDemand = $cohabitation->getAccommodationDemand();
        $senior = $accommodationDemand->getSenior();
        $young = $accommodationDemand->getYoung();

        if (UserRoleEnum::ROLE_ASSOCIATION === $this->grantedService->getRole($currentUser)) {
            $association = $currentUser->getAssociation();

            return $accommodationDemand->getAssociations()->contains($association)
                && (
                    AssociationTypeEnum::TYPE_BOTH === $association->getType()
                    && $senior->getAssociation() === $association
                )
                || (!$seniorAssociationOnly && $young->getAssociation() === $association);
        }

        return false;
    }
}
