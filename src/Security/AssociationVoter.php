<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Association;
use App\Entity\User;
use App\Enum\AssociationVoterEnum;
use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AssociationVoter extends Voter
{
    private $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    protected function supports($attribute, $subject): bool
    {
        if (!\in_array($attribute, AssociationVoterEnum::getValues(), true)) {
            return false;
        }

        if (!$subject instanceof Association) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case AssociationVoterEnum::SHOW:
                return $this->canShow();

            case AssociationVoterEnum::EDIT:
                return $this->canEdit($token, $subject);

            case AssociationVoterEnum::DELETE:
                return $this->canDelete($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canShow(): bool
    {
        return true;
    }

    private function canEdit(TokenInterface $token, Association $association): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->adminCanManage($currentUser, $association)
            || $this->associationCanManage($currentUser, $association)
        ;
    }

    private function canDelete(TokenInterface $token, Association $association): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->adminCanManage($currentUser, $association);
    }

    private function adminCanManage(User $user, Association $association): bool
    {
        if ($this->grantedService->isGranted($user, UserRoleEnum::ROLE_SUPER_ADMIN)) {
            return true;
        }

        if ($this->grantedService->isGranted($user, UserRoleEnum::ROLE_ADMIN)
            && $user->getLocation()->getCountryCode() === $association->getLocation()->getCountryCode()) {
            return true;
        }

        return false;
    }

    private function associationCanManage(User $user, Association $association): bool
    {
        if (UserRoleEnum::ROLE_ASSOCIATION === $this->grantedService->getRole($user)) {
            return $user->getAssociation() === $association;
        }

        return false;
    }
}
