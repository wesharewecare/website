<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Document;
use App\Entity\User;
use App\Enum\DocumentVoterEnum;
use App\Service\GrantedService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DocumentVoter extends Voter
{
    private $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, DocumentVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof Document) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case DocumentVoterEnum::EDIT:
                return $this->canEdit($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canEdit(TokenInterface $token, Document $document): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        return $this->grantedService->isManager($user)
            && $user->getAssociation() === $document->getAssociation()
        ;
    }
}
