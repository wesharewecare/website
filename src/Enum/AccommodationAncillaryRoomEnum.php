<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AccommodationAncillaryRoomEnum extends AbstractEnumType
{
    public const ROOM_CELLAR = 'cellar';
    public const ROOM_GARAGE = 'garage';
    public const ROOM_ATTIC = 'attic';

    protected static $choices = [
        self::ROOM_CELLAR => 'accommodation.ancillary_room.'.self::ROOM_CELLAR,
        self::ROOM_GARAGE => 'accommodation.ancillary_room.'.self::ROOM_GARAGE,
        self::ROOM_ATTIC => 'accommodation.ancillary_room.'.self::ROOM_ATTIC,
    ];
}
