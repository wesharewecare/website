<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class DocumentRecipientEnum extends AbstractEnumType
{
    public const RECIPIENT_YOUNG = 'young';
    public const RECIPIENT_SENIOR = 'senior';
    public const RECIPIENT_ASSOCIATION = 'association';

    public static $associationChoices = [
        self::RECIPIENT_YOUNG => 'toolbox.document.recipient.'.self::RECIPIENT_YOUNG,
        self::RECIPIENT_SENIOR => 'toolbox.document.recipient.'.self::RECIPIENT_SENIOR,
    ];

    protected static $choices = [
        self::RECIPIENT_YOUNG => 'toolbox.document.recipient.'.self::RECIPIENT_YOUNG,
        self::RECIPIENT_SENIOR => 'toolbox.document.recipient.'.self::RECIPIENT_SENIOR,
        self::RECIPIENT_ASSOCIATION => 'toolbox.document.recipient.'.self::RECIPIENT_ASSOCIATION,
    ];
}
