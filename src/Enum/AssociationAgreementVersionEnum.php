<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AssociationAgreementVersionEnum extends AbstractEnumType
{
    public const VERSION_DEFAULT = 'default_version';
    public const VERSION_COHABILIS = 'cohabilis_version';
    public const VERSION_1TOIT2AGES = '1toit2ages_version';
    public const VERSION_SOLIDARIOS = 'solidarios_version';

    protected static $choices = [
        self::VERSION_DEFAULT => 'association.agreement.version.'.self::VERSION_DEFAULT,
        self::VERSION_COHABILIS => 'association.agreement.version.'.self::VERSION_COHABILIS,
        self::VERSION_1TOIT2AGES => 'association.agreement.version.'.self::VERSION_1TOIT2AGES,
        self::VERSION_SOLIDARIOS => 'association.agreement.version.'.self::VERSION_SOLIDARIOS,
    ];
}
