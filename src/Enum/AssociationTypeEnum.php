<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AssociationTypeEnum extends AbstractEnumType
{
    public const TYPE_BOTH = 'both';
    public const TYPE_YOUNG_ONLY = 'young_only';

    protected static $choices = [
        self::TYPE_BOTH => 'association.type.'.self::TYPE_BOTH,
        self::TYPE_YOUNG_ONLY => 'association.type.'.self::TYPE_YOUNG_ONLY,
    ];
}
