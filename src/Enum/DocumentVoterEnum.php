<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class DocumentVoterEnum extends AbstractEnumType
{
    public const EDIT = 'edit';

    protected static $choices = [
        self::EDIT => self::EDIT,
    ];
}
