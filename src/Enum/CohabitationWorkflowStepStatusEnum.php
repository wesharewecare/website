<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class CohabitationWorkflowStepStatusEnum extends AbstractEnumType
{
    public const STATUS_COHABITATION_INIT = 'cohabitation_init';
    public const STATUS_PRE_VISIT_DONE = 'pre_visit_done';
    public const STATUS_INTERVIEW_DONE = 'interview_done';
    public const STATUS_JOINT_MEETING_DONE = 'joint_meeting_done';
    public const STATUS_AGREEMENT_SENT = 'agreement_sent';
    public const STATUS_AGREEMENT_SIGNED = 'agreement_signed';
    public const STATUS_APPOINTMENT_DONE = 'appointment_done';
    public const STATUS_DEBRIEFING_DONE = 'debriefing_done';

    protected static $choices = [
        self::STATUS_COHABITATION_INIT => 'cohabitation_workflow_step.status.cohabitation_init',
        self::STATUS_PRE_VISIT_DONE => 'cohabitation_workflow_step.status.pre_visit_done',
        self::STATUS_INTERVIEW_DONE => 'cohabitation_workflow_step.status.interview_done',
        self::STATUS_JOINT_MEETING_DONE => 'cohabitation_workflow_step.status.joint_meeting_done',
        self::STATUS_AGREEMENT_SENT => 'cohabitation_workflow_step.status.agreement_sent',
        self::STATUS_AGREEMENT_SIGNED => 'cohabitation_workflow_step.status.agreement_signed',
        self::STATUS_APPOINTMENT_DONE => 'cohabitation_workflow_step.status.appointment_done',
        self::STATUS_DEBRIEFING_DONE => 'cohabitation_workflow_step.status.debriefing_done',
    ];
}
