<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ThreadChannelTypeEnum extends AbstractEnumType
{
    public const INDIVIDUAL_CHANNEL = 'individual';
    public const GROUP_CHANNEL = 'group';

    protected static $choices = [
        self::INDIVIDUAL_CHANNEL => self::INDIVIDUAL_CHANNEL,
        self::GROUP_CHANNEL => self::GROUP_CHANNEL,
    ];
}
