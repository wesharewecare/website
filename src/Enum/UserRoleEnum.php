<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserRoleEnum extends AbstractEnumType
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_SENIOR = 'ROLE_SENIOR';
    public const ROLE_YOUNG = 'ROLE_YOUNG';
    public const ROLE_ASSOCIATION = 'ROLE_ASSOCIATION';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public static $users = [self::ROLE_YOUNG, self::ROLE_SENIOR];
    public static $managerAndAdmin = [self::ROLE_ASSOCIATION, self::ROLE_ADMIN, self::ROLE_SUPER_ADMIN];
    public static $admins = [self::ROLE_ADMIN, self::ROLE_SUPER_ADMIN];

    protected static $choices = [
        self::ROLE_USER => 'user.roles.user',
        self::ROLE_SENIOR => 'user.roles.senior',
        self::ROLE_YOUNG => 'user.roles.young',
        self::ROLE_ASSOCIATION => 'user.roles.association',
        self::ROLE_ADMIN => 'user.roles.admin',
        self::ROLE_SUPER_ADMIN => 'user.roles.super_admin',
    ];
}
