<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AccommodationVoterEnum extends AbstractEnumType
{
    public const SHOW = 'show';
    public const EDIT = 'edit';
    public const CREATE_DEMAND = 'create_demand';
    public const APPROVE = 'approve';
    public const CLOSE = 'close';

    protected static $choices = [
        self::SHOW => self::SHOW,
        self::EDIT => self::EDIT,
        self::CREATE_DEMAND => self::CREATE_DEMAND,
        self::APPROVE => self::APPROVE,
        self::CLOSE => self::CLOSE,
    ];
}
