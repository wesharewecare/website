<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class EventVoterEnum extends AbstractEnumType
{
    public const PARTICIPATE = 'participate';
    public const EDIT = 'edit';
    public const DELETE = 'delete';

    protected static $choices = [
        self::PARTICIPATE => self::PARTICIPATE,
        self::EDIT => self::EDIT,
        self::DELETE => self::DELETE,
    ];
}
