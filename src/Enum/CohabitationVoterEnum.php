<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class CohabitationVoterEnum extends AbstractEnumType
{
    public const SHOW = 'show';
    public const EDIT = 'edit';

    protected static $choices = [
        self::SHOW => self::SHOW,
        self::EDIT => self::EDIT,
    ];
}
