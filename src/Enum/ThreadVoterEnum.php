<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ThreadVoterEnum extends AbstractEnumType
{
    public const SEE = 'see';
    public const REPLY = 'reply';

    protected static $choices = [
        self::SEE => self::SEE,
        self::REPLY => self::REPLY,
    ];
}
