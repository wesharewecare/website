<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AccommodationTypeEnum extends AbstractEnumType
{
    public const TYPE_HOUSE = 'house';
    public const TYPE_FLAT = 'flat';

    protected static $choices = [
        self::TYPE_HOUSE => 'accommodation.type.'.self::TYPE_HOUSE,
        self::TYPE_FLAT => 'accommodation.type.'.self::TYPE_FLAT,
    ];
}
