<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class EventStatusEnum extends AbstractEnumType
{
    public const STATUS_OPEN = 'open';
    public const STATUS_DRAFT = 'draft';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_CLOSED = 'closed';

    protected static $choices = [
        self::STATUS_OPEN => 'event.status.'.self::STATUS_OPEN,
        self::STATUS_CLOSED => 'event.status.'.self::STATUS_CLOSED,
        self::STATUS_CANCELED => 'event.status.'.self::STATUS_CANCELED,
        self::STATUS_DRAFT => 'event.status.'.self::STATUS_DRAFT,
    ];
}
