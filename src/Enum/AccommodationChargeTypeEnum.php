<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AccommodationChargeTypeEnum extends AbstractEnumType
{
    public const CHARGE_ELECTRICITY = 'electricity';
    public const CHARGE_WATER = 'water';
    public const CHARGE_GAS = 'gas';
    public const CHARGE_INTERNET = 'internet';

    protected static $choices = [
        self::CHARGE_ELECTRICITY => 'accommodation.charge_type.'.self::CHARGE_ELECTRICITY,
        self::CHARGE_WATER => 'accommodation.charge_type.'.self::CHARGE_WATER,
        self::CHARGE_GAS => 'accommodation.charge_type.'.self::CHARGE_GAS,
        self::CHARGE_INTERNET => 'accommodation.charge_type.'.self::CHARGE_INTERNET,
    ];
}
