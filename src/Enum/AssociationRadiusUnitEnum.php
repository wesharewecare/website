<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AssociationRadiusUnitEnum extends AbstractEnumType
{
    public const UNIT_KMS = 'kms';
    public const UNIT_MILES = 'miles';

    protected static $choices = [
        self::UNIT_KMS => 'association.radius_unit.'.self::UNIT_KMS,
        self::UNIT_MILES => 'association.radius_unit.'.self::UNIT_MILES,
    ];
}
