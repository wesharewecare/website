<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class NotificationTypeEnum extends AbstractEnumType
{
    public const TYPE_MESSAGING_GROUP = 'messaging_group';
    public const TYPE_MESSAGING_INDIVIDUAL = 'messaging_individual';
    public const TYPE_EVENT_NEARBY = 'event_nearby';
    public const TYPE_EVENT_UPDATED = 'event_updated';
    public const TYPE_EVENT_REMINDER = 'event_reminder';
    public const TYPE_EVENT_PARTICIPANT = 'event_participant';
    public const TYPE_ACCOMMODATION_NEW = 'accommodation_new';
    public const TYPE_ACCOMMODATION_PUBLISHED = 'accommodation_published';
    public const TYPE_DEMAND_NEW = 'demand_new';
    public const TYPE_DEMAND_UPDATED = 'demand_updated';
    public const TYPE_USER_NEW = 'user_new';

    public static $notificationSenior = [
        self::TYPE_MESSAGING_GROUP,
        self::TYPE_MESSAGING_INDIVIDUAL,
        self::TYPE_EVENT_NEARBY,
        self::TYPE_EVENT_UPDATED,
        self::TYPE_EVENT_REMINDER,
        self::TYPE_DEMAND_NEW,
        self::TYPE_DEMAND_UPDATED,
    ];

    public static $notificationYoung = [
        self::TYPE_MESSAGING_GROUP,
        self::TYPE_MESSAGING_INDIVIDUAL,
        self::TYPE_EVENT_NEARBY,
        self::TYPE_EVENT_UPDATED,
        self::TYPE_EVENT_REMINDER,
        self::TYPE_ACCOMMODATION_PUBLISHED,
        self::TYPE_DEMAND_UPDATED,
    ];

    public static $notificationAssociation = [
        self::TYPE_MESSAGING_GROUP,
        self::TYPE_MESSAGING_INDIVIDUAL,
        self::TYPE_EVENT_PARTICIPANT,
        self::TYPE_ACCOMMODATION_NEW,
        self::TYPE_DEMAND_NEW,
        self::TYPE_USER_NEW,
    ];

    public static $notificationAdmin = [
        self::TYPE_MESSAGING_GROUP,
        self::TYPE_MESSAGING_INDIVIDUAL,
        self::TYPE_EVENT_UPDATED,
        self::TYPE_EVENT_REMINDER,
    ];

    protected static $choices = [
        self::TYPE_MESSAGING_GROUP => 'user.notification.type.'.self::TYPE_MESSAGING_GROUP,
        self::TYPE_MESSAGING_INDIVIDUAL => 'user.notification.type.'.self::TYPE_MESSAGING_INDIVIDUAL,
        self::TYPE_EVENT_NEARBY => 'user.notification.type.'.self::TYPE_EVENT_NEARBY,
        self::TYPE_EVENT_UPDATED => 'user.notification.type.'.self::TYPE_EVENT_UPDATED,
        self::TYPE_EVENT_REMINDER => 'user.notification.type.'.self::TYPE_EVENT_REMINDER,
        self::TYPE_EVENT_PARTICIPANT => 'user.notification.type.'.self::TYPE_EVENT_PARTICIPANT,
        self::TYPE_ACCOMMODATION_NEW => 'user.notification.type.'.self::TYPE_ACCOMMODATION_NEW,
        self::TYPE_ACCOMMODATION_PUBLISHED => 'user.notification.type.'.self::TYPE_ACCOMMODATION_PUBLISHED,
        self::TYPE_DEMAND_NEW => 'user.notification.type.'.self::TYPE_DEMAND_NEW,
        self::TYPE_DEMAND_UPDATED => 'user.notification.type.'.self::TYPE_DEMAND_UPDATED,
        self::TYPE_USER_NEW => 'user.notification.type.'.self::TYPE_USER_NEW,
    ];

    public function getNotificationsByRole(string $role): ?array
    {
        switch ($role) {
            case UserRoleEnum::ROLE_SUPER_ADMIN:
            case UserRoleEnum::ROLE_ADMIN:
                return NotificationTypeEnum::$notificationAdmin;
            case UserRoleEnum::ROLE_ASSOCIATION:
                return NotificationTypeEnum::$notificationAssociation;
            case UserRoleEnum::ROLE_SENIOR:
                return NotificationTypeEnum::$notificationSenior;
            case UserRoleEnum::ROLE_YOUNG:
                return NotificationTypeEnum::$notificationYoung;
            default:
                return null;
        }
    }
}
