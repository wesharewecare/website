<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ProfileGenderEnum extends AbstractEnumType
{
    public const GENDER_MALE = 'male';
    public const GENDER_FEMALE = 'female';
    public const GENDER_OTHER = 'other';

    protected static $choices = [
        self::GENDER_MALE => 'profile.gender.male',
        self::GENDER_FEMALE => 'profile.gender.female',
        self::GENDER_OTHER => 'profile.gender.other',
    ];
}
