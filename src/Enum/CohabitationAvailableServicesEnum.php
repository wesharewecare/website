<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class CohabitationAvailableServicesEnum extends AbstractEnumType
{
    public const SERVICE_SINGLE_ROOM = 'single_room';
    public const SERVICE_KITCHEN = 'kitchen';
    public const SERVICE_WASHING_MACHINE = 'washing_machine';
    public const SERVICE_HOT_WATER = 'hot_water';
    public const SERVICE_HOUSE_KEYS = 'house_keys';
    public const SERVICE_REFRIGERATOR = 'refrigerator';
    public const SERVICE_LIVING_ROOM = 'living_room';
    public const SERVICE_HEATING = 'heating';

    public static $solidariosServices = [
        self::SERVICE_SINGLE_ROOM,
        self::SERVICE_KITCHEN,
        self::SERVICE_WASHING_MACHINE,
        self::SERVICE_HOT_WATER,
        self::SERVICE_HOUSE_KEYS,
        self::SERVICE_REFRIGERATOR,
        self::SERVICE_LIVING_ROOM,
        self::SERVICE_HEATING,
    ];

    protected static $choices = [
        self::SERVICE_SINGLE_ROOM => 'cohabitation.available_services.'.self::SERVICE_SINGLE_ROOM,
        self::SERVICE_KITCHEN => 'cohabitation.available_services.'.self::SERVICE_KITCHEN,
        self::SERVICE_WASHING_MACHINE => 'cohabitation.available_services.'.self::SERVICE_WASHING_MACHINE,
        self::SERVICE_HOT_WATER => 'cohabitation.available_services.'.self::SERVICE_HOT_WATER,
        self::SERVICE_HOUSE_KEYS => 'cohabitation.available_services.'.self::SERVICE_HOUSE_KEYS,
        self::SERVICE_REFRIGERATOR => 'cohabitation.available_services.'.self::SERVICE_REFRIGERATOR,
        self::SERVICE_LIVING_ROOM => 'cohabitation.available_services.'.self::SERVICE_LIVING_ROOM,
        self::SERVICE_HEATING => 'cohabitation.available_services.'.self::SERVICE_HEATING,
    ];
}
