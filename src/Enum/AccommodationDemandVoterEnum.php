<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AccommodationDemandVoterEnum extends AbstractEnumType
{
    public const EDIT = 'edit';
    public const RESPOND = 'respond';

    protected static $choices = [
        self::EDIT => self::EDIT,
        self::RESPOND => self::RESPOND,
    ];
}
