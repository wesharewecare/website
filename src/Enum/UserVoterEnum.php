<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserVoterEnum extends AbstractEnumType
{
    public const ACCESS_ACCOUNT = 'access_account';
    public const SHOW = 'show';
    public const EDIT = 'edit';
    public const EDIT_NOTIFICATIONS = 'edit_notification';
    public const DELETE = 'delete';
    public const CREATE_ACCOMMODATION = 'create_accommodation';
    public const APPROVE = 'approve';
    public const CREATE_SENIOR_ACCOUNT = 'create_senior_account';
    public const SEND_MESSAGE = 'send_message';
    public const CLAIM_MANAGEMENT = 'claim_management';
    public const CREATE_EVENT = 'create_event';

    protected static $choices = [
        self::ACCESS_ACCOUNT => self::ACCESS_ACCOUNT,
        self::SHOW => self::SHOW,
        self::EDIT => self::EDIT,
        self::EDIT_NOTIFICATIONS => self::EDIT_NOTIFICATIONS,
        self::DELETE => self::DELETE,
        self::CREATE_ACCOMMODATION => self::CREATE_ACCOMMODATION,
        self::APPROVE => self::APPROVE,
        self::CREATE_SENIOR_ACCOUNT => self::CREATE_SENIOR_ACCOUNT,
        self::SEND_MESSAGE => self::SEND_MESSAGE,
        self::CLAIM_MANAGEMENT => self::CLAIM_MANAGEMENT,
        self::CREATE_EVENT => self::CREATE_EVENT,
    ];
}
