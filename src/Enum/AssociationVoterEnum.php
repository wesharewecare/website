<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AssociationVoterEnum extends AbstractEnumType
{
    public const SHOW = 'show';
    public const EDIT = 'edit';
    public const DELETE = 'delete';

    protected static $choices = [
        self::SHOW => self::SHOW,
        self::EDIT => self::EDIT,
        self::DELETE => self::DELETE,
    ];
}
