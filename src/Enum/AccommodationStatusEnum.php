<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AccommodationStatusEnum extends AbstractEnumType
{
    public const STATUS_OPEN = 'open';
    public const STATUS_CLOSED = 'closed';
    public const STATUS_OCCUPIED = 'occupied';
    public const STATUS_AWAITING_APPROVAL = 'awaiting_approval';

    protected static $choices = [
        self::STATUS_OPEN => 'accommodation.status.'.self::STATUS_OPEN,
        self::STATUS_CLOSED => 'accommodation.status.'.self::STATUS_CLOSED,
        self::STATUS_OCCUPIED => 'accommodation.status.'.self::STATUS_OCCUPIED,
        self::STATUS_AWAITING_APPROVAL => 'accommodation.status.'.self::STATUS_AWAITING_APPROVAL,
    ];
}
