<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AccommodationDemandStatusEnum extends AbstractEnumType
{
    public const STATUS_AWAITING_USERS_APPROVAL = 'awaiting_users_approval';
    public const STATUS_PENDING = 'pending';
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_REFUSED = 'refused';
    public const STATUS_ABANDONED = 'abandoned';
    public const STATUS_COMPLETED = 'completed';
    public const STATUS_CLOSED = 'closed';

    protected static $choices = [
        self::STATUS_AWAITING_USERS_APPROVAL => 'accommodation_demand.status.'.self::STATUS_AWAITING_USERS_APPROVAL,
        self::STATUS_PENDING => 'accommodation_demand.status.'.self::STATUS_PENDING,
        self::STATUS_ACCEPTED => 'accommodation_demand.status.'.self::STATUS_ACCEPTED,
        self::STATUS_REFUSED => 'accommodation_demand.status.'.self::STATUS_REFUSED,
        self::STATUS_COMPLETED => 'accommodation_demand.status.'.self::STATUS_COMPLETED,
        self::STATUS_CLOSED => 'accommodation_demand.status.'.self::STATUS_CLOSED,
        self::STATUS_ABANDONED => 'accommodation_demand.status.'.self::STATUS_ABANDONED,
    ];
}
