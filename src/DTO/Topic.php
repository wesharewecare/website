<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Topic
{
    private $id;
    private $threadId;

    public function __construct(string $id, int $threadId)
    {
        $this->id = $id;
        $this->threadId = $threadId;
    }

    /**
     * @SerializedName("@id")
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getThreadId(): int
    {
        return $this->threadId;
    }
}
