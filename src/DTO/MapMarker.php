<?php

declare(strict_types=1);

namespace App\DTO;

class MapMarker
{
    public const TYPE_ACCOMMODATION = 'accommodation';
    public const TYPE_ASSOCIATION = 'association';
    public const TYPE_EVENT = 'event';

    private $id;
    private $name;
    private $image;
    private $icon;
    private $address;
    private $longitude;
    private $latitude;
    private $type;
    private $description;
    private $date;
    private $status;

    public function __construct(
        int $id,
        string $name,
        string $image,
        string $icon,
        string $address,
        float $latitude,
        float $longitude,
        string $type,
        string $description = null,
        string $date = null,
        string $status = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->image = $image;
        $this->icon = $icon;
        $this->address = $address;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->type = $type;
        $this->description = $description;
        $this->date = $date;
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }
}
