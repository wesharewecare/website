<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Message
{
    private const TYPE = 'MESSAGE';

    private $id;
    private $threadGroupId;
    private $author;
    private $datetime;
    private $body;

    public function __construct(string $id, string $threadGroupId, Author $author, \DateTime $datetime, string $body)
    {
        $this->id = $id;
        $this->threadGroupId = $threadGroupId;
        $this->author = $author;
        $this->datetime = $datetime;
        $this->body = $body;
    }

    /**
     * @SerializedName("@id")
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @SerializedName("thread")
     */
    public function getThreadGroupId(): string
    {
        return $this->threadGroupId;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function getDatetime(): \DateTime
    {
        return $this->datetime;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @SerializedName("@var")
     */
    public function getType(): string
    {
        return self::TYPE;
    }
}
