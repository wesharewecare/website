<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Author
{
    private $id;
    private $firstname;
    private $lastname;
    private $avatar;

    public function __construct(string $id, string $firstname, string $lastname, string $avatar)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->avatar = $avatar;
    }

    /**
     * @SerializedName("@id")
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }
}
