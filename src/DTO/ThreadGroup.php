<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class ThreadGroup
{
    private $id;
    private $name;
    private $channelType;
    private $channelTypeTooltip;
    private $icon;
    private $avatar;
    private $lastMessageAt;
    private $topics;
    private $isRead;
    private $showFooter;
    private $isFavorite;
    private $profileUrl;
    private $replaceFooterForm;

    public function __construct(
        string $id,
        string $name,
        string $channelType,
        string $channelTypeTooltip,
        string $icon,
        string $avatar,
        \DateTime $lastMessageAt = null,
        array $topics = [],
        bool $isRead = true,
        bool $isFavorite = false,
        string $profileUrl = null,
        bool $showFooter = true,
        string $replaceFooterForm = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->channelType = $channelType;
        $this->channelTypeTooltip = $channelTypeTooltip;
        $this->avatar = $avatar;
        $this->icon = $icon;
        $this->lastMessageAt = $lastMessageAt;
        $this->topics = $topics;
        $this->isRead = $isRead;
        $this->isFavorite = $isFavorite;
        $this->showFooter = $showFooter;
        $this->profileUrl = $profileUrl;
        $this->replaceFooterForm = $replaceFooterForm;
    }

    /**
     * @SerializedName("@id")
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getChannelType(): string
    {
        return $this->channelType;
    }

    public function getChannelTypeTooltip(): string
    {
        return $this->channelTypeTooltip;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }

    public function getLastMessageAt(): ?\DateTime
    {
        return $this->lastMessageAt;
    }

    public function getTopics(): array
    {
        return $this->topics;
    }

    public function isRead(): bool
    {
        return $this->isRead;
    }

    public function isShowFooter(): bool
    {
        return $this->showFooter;
    }

    public function isFavorite(): bool
    {
        return $this->isFavorite;
    }

    public function getProfileUrl(): ?string
    {
        return $this->profileUrl;
    }

    public function getReplaceFooterForm(): ?string
    {
        return $this->replaceFooterForm;
    }
}
