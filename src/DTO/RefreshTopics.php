<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class RefreshTopics
{
    private const TYPE = 'REFRESH_TOPICS';

    /**
     * @SerializedName("@var")
     */
    public function getType(): string
    {
        return self::TYPE;
    }
}
