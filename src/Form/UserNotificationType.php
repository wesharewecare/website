<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\UserNotification;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserNotificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('notifications', CollectionType::class, [
            'entry_type' => CheckboxType::class,
            'entry_options' => [
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'label_format' => 'user.notification.type.%name%',
                'required' => false,
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserNotification::class,
        ]);
    }
}
