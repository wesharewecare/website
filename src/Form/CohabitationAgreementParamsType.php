<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Accommodation;
use App\Entity\Association;
use App\Entity\Cohabitation;
use App\Enum\AccommodationAncillaryRoomEnum;
use App\Enum\AccommodationChargeTypeEnum;
use App\Enum\AccommodationTypeEnum;
use App\Service\CohabitationWorkflowStepManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CohabitationAgreementParamsType extends AbstractType
{
    private $workflowStepManager;

    public function __construct(CohabitationWorkflowStepManager $workflowStepManager)
    {
        $this->workflowStepManager = $workflowStepManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Association $association */
        $association = $options['association'];

        /** @var Cohabitation $cohabitation */
        $cohabitation = $options['cohabitation'];

        $accommodation = $cohabitation->getAccommodation();

        $agreementVersion = $association->getAgreementVersion();
        $fields = $this->workflowStepManager->getAgreementFieldsByVersion($agreementVersion);
        $agreementParams = $cohabitation->getAgreementParamsValues();
        $currentYear = date('Y');

        if (\array_key_exists('cohabitation', $fields)) {
            foreach ($fields['cohabitation'] as $field => $params) {
                switch ($field) {
                    case 'houseRooms':
                        $builder->add('houseRooms', IntegerType::class, [
                            'required' => false,
                            'label' => 'accommodation.rooms',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'sharedHouseSize':
                        $builder->add('sharedHouseSize', IntegerType::class, [
                            'required' => false,
                            'label' => 'accommodation.shared_size',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'chargesTypes':
                        $builder->add('chargesTypes', ChoiceType::class, [
                            'required' => false,
                            'choices' => AccommodationChargeTypeEnum::getChoices(),
                            'multiple' => true,
                            'attr' => [
                                'data-controller' => 'choices',
                            ],
                            'label' => 'accommodation.charges_type',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'chargesAmount':
                        $builder->add('chargesAmount', IntegerType::class, [
                            'required' => false,
                            'attr' => [
                                'min' => 0,
                            ],
                            'label' => 'accommodation.charges',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'depositAmount':
                        $builder->add('depositAmount', IntegerType::class, [
                            'required' => false,
                            'attr' => [
                                'min' => 0,
                            ],
                            'label' => 'accommodation.deposit',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'nbKeys':
                        $builder->add('nbKeys', IntegerType::class, [
                            'required' => false,
                            'attr' => [
                                'min' => 0,
                            ],
                            'label' => 'accommodation.nb_keys',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'sharedRoomsDescription':
                        $builder->add('sharedRoomsDescription', TextareaType::class, [
                            'required' => false,
                            'label' => 'accommodation.shared_rooms_description',
                            'attr' => [
                                'data-controller' => 'textarea-autogrow',
                            ],
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'packageType':
                        $builder->add('packageType', ChoiceType::class, [
                            'required' => false,
                            'choices' => [
                                'cohabitation.package_type.classic' => 'classic',
                                'cohabitation.package_type.services' => 'services',
                            ],
                            'label' => 'cohabitation.package_type',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'youngTasks':
                        $builder->add('youngTasks', TextareaType::class, [
                            'required' => false,
                            'label' => 'cohabitation.youngTasks',
                            'attr' => [
                                'data-controller' => 'textarea-autogrow',
                            ],
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'accountNumber':
                        $builder->add('accountNumber', TextType::class, [
                            'required' => false,
                            'label' => 'cohabitation.accountNumber',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'depositDate':
                        $builder->add('depositDate', DateType::class, [
                            'required' => false,
                            'label' => 'cohabitation.deposit_date',
                            'years' => range($currentYear, $currentYear + 1),
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                            'data' => \array_key_exists('depositDate', $agreementParams)
                                ? $agreementParams['depositDate']
                                : null,
                        ]);

                        break;

                    case 'passportSenior':
                        $builder->add('passportSenior', TextType::class, [
                            'required' => false,
                            'label' => 'cohabitation.passport_senior',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'passportYoung':
                        $builder->add('passportYoung', TextType::class, [
                            'required' => false,
                            'label' => 'cohabitation.passport_young',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'contactSenior':
                        $builder->add('contactSenior', TextareaType::class, [
                            'required' => false,
                            'label' => 'cohabitation.contact_senior',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'contactYoung':
                        $builder->add('contactYoung', TextareaType::class, [
                            'required' => false,
                            'label' => 'cohabitation.contact_young',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'seniorAcceptToPromote':
                        $builder->add('seniorAcceptToPromote', CheckboxType::class, [
                            'required' => false,
                            'label' => 'cohabitation.senior_accept_to_promote',
                            'label_attr' => [
                                'size' => 'md',
                                'class' => 'switch-custom'.($params['required'] ? ' required' : ''),
                            ],
                        ]);

                        break;

                    case 'youngAcceptToPromote':
                        $builder->add('youngAcceptToPromote', CheckboxType::class, [
                            'required' => false,
                            'label' => 'cohabitation.young_accept_to_promote',
                            'label_attr' => [
                                'size' => 'md',
                                'class' => 'switch-custom'.($params['required'] ? ' required' : ''),
                            ],
                        ]);

                        break;

                    case 'availableServices':
                        $builder->add('availableServices', ChoiceType::class, [
                            'required' => false,
                            'label' => 'cohabitation.available_services',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                            'multiple' => true,
                            'attr' => [
                                'data-controller' => 'choices',
                            ],
                            'choices' => $params['choices'],
                        ]);
                }
            }
        }

        if (\array_key_exists('accommodation', $fields)) {
            $accommodationForm = $builder->create('accommodation', FormType::class, [
                'data_class' => Accommodation::class,
                'data' => $accommodation,
                'label' => false,
            ]);

            foreach ($fields['accommodation'] as $field => $params) {
                switch ($field) {
                    case 'type':
                        $accommodationForm->add('type', ChoiceType::class, [
                            'required' => false,
                            'choices' => AccommodationTypeEnum::getChoices(),
                            'label' => 'accommodation.type',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'houseSize':
                        $accommodationForm->add('houseSize', IntegerType::class, [
                            'required' => false,
                            'attr' => [
                                'min' => 1,
                            ],
                            'label' => 'accommodation.size',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'sharedHouseRooms':
                        $accommodationForm->add('sharedHouseRooms', IntegerType::class, [
                            'required' => false,
                            'label' => 'accommodation.shared_rooms',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;

                    case 'ancillaryRooms':
                        $accommodationForm->add('ancillaryRooms', ChoiceType::class, [
                            'required' => false,
                            'choices' => AccommodationAncillaryRoomEnum::getChoices(),
                            'multiple' => true,
                            'attr' => [
                                'data-controller' => 'choices',
                            ],
                            'label' => 'accommodation.ancillary_rooms',
                            'label_attr' => [
                                'class' => $params['required'] ? 'required' : '',
                            ],
                        ]);

                        break;
                }
            }

            $builder->add($accommodationForm);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'association' => null,
            'cohabitation' => null,
        ]);
    }
}
