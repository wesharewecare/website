<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Accommodation;
use App\Entity\AccommodationImage;
use App\Entity\Association;
use App\Enum\AccommodationAncillaryRoomEnum;
use App\Enum\AccommodationTypeEnum;
use App\Enum\AssociationAgreementVersionEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tbbc\MoneyBundle\Form\Type\CurrencyType;

class AccommodationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Accommodation $accommodation */
        $accommodation = $builder->getData();

        /** @var Association $association */
        $association = $accommodation->getSenior()->getAssociation();

        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'accommodation.name',
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'accommodation.description',
                'attr' => [
                    'data-controller' => 'textarea-autogrow',
                ],
            ])
            ->add('roomDescription', TextareaType::class, [
                'required' => true,
                'label' => 'accommodation.description',
                'attr' => [
                    'data-controller' => 'textarea-autogrow',
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => AccommodationTypeEnum::getChoices(),
                'label' => 'accommodation.type',
            ])
            ->add('startDate', DateType::class, [
                'required' => false,
                'label' => 'accommodation.start_date',
                'label_translation_parameters' => ['%date%' => ''],
            ])
            ->add('endDate', DateType::class, [
                'required' => false,
                'label' => 'accommodation.end_date',
                'label_translation_parameters' => ['%date%' => ''],
            ])
            ->add('roomSize', IntegerType::class, [
                'required' => true,
                'attr' => [
                    'min' => 1,
                ],
                'label' => 'accommodation.size',
            ])
            ->add('houseSize', IntegerType::class, [
                'required' => true,
                'attr' => [
                    'min' => 1,
                ],
                'label' => 'accommodation.size',
            ])
            ->add('sharedHouseRooms', IntegerType::class, [
                'required' => false,
                'label' => 'accommodation.shared_rooms',
            ])
            ->add('hasPet', CheckboxType::class, [
                'required' => false,
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'label' => 'accommodation.has_pet',
            ])
            ->add('hasWifi', CheckboxType::class, [
                'required' => false,
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'label' => 'accommodation.has_wifi',
            ])
            ->add('acceptSmokers', CheckboxType::class, [
                'required' => false,
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'label' => 'accommodation.accept_smokers',
            ])
            ->add('images', CollectionType::class, [
                'entry_type' => AccommodationImageFormType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => function (AccommodationImage $image) {
                    return empty($image->getImageFile()) && empty($image->getImageName());
                },
                'prototype' => true,
                'prototype_name' => '__image__name__',
                'by_reference' => false,
                'entry_options' => [
                    'label' => false,
                ],
                'attr' => [
                    'data-controller' => 'symfony-collection',
                ],
                'label' => 'accommodation.images',
            ])
            ->add('ancillaryRooms', ChoiceType::class, [
                'choices' => AccommodationAncillaryRoomEnum::getChoices(),
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'data-controller' => 'choices',
                ],
                'label' => 'accommodation.ancillary_rooms',
            ])
        ;

        if ($association
            && !\in_array(
                $association->getAgreementVersion(),
                [AssociationAgreementVersionEnum::VERSION_SOLIDARIOS],
                true
            )
        ) {
            $builder
                ->add('priceAmount', IntegerType::class, [
                    'required' => true,
                    'label' => 'accommodation.price',
                    'attr' => [
                        'min' => 0,
                    ],
                    'help' => 'accommodation.price.help',
                ])
                ->add('currency', CurrencyType::class, [
                    'required' => true,
                    'label' => 'accommodation.currency',
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Accommodation::class,
        ]);
    }
}
