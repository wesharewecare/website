<?php

declare(strict_types=1);

namespace App\Form\Model;

use App\Entity\Location;
use App\Entity\Profile;
use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class CreateUser
{
    /**
     * @Assert\Valid
     */
    public $user;

    /**
     * @Assert\Valid
     */
    public $profile;

    /**
     * @Assert\Valid
     */
    public $location;

    public function __construct()
    {
        $this->user = new User();
        $this->profile = $this->user->getProfile();
        $this->location = $this->user->getLocation();
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function getLocation(): Location
    {
        return $this->location;
    }
}
