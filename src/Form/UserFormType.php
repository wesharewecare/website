<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Form\Model\EditUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var EditUser $editUser */
        $editUser = $builder->getData();

        $userForm = $builder->create('user', FormType::class, [
            'data_class' => User::class,
        ]);

        $userForm
            ->add('email', EmailType::class, [
                'required' => !$options['is_external_edit'],
                'label' => 'user.email',
                'attr' => [
                    'data-homesharing-dynamic-form-target' => 'email',
                ],
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'user.firstname',
                'attr' => [
                    'data-homesharing-dynamic-form-target' => 'firstname',
                ],
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'user.lastname',
                'attr' => [
                    'data-homesharing-dynamic-form-target' => 'lastname',
                ],
            ])
        ;

        $builder
            ->add($userForm)
            ->add('profile', ProfileFormType::class, [
                'mode' => $options['mode'],
                'label' => false,
                'data' => $editUser->getProfile(),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EditUser::class,
            'mode' => UserProfileTypeEnum::PROFILE_USER,
            'is_external_edit' => false,
            'password_field' => false,
        ]);
    }
}
