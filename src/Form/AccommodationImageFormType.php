<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\AccommodationImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AccommodationImageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('imageFile', VichImageType::class, [
            'required' => false,
            'allow_delete' => false,
            'label' => false,
            'imagine_pattern' => 'accommodation_image',
            'download_uri' => false,
            'attr' => [
                'data-controller' => 'file-input',
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AccommodationImage::class,
        ]);
    }
}
