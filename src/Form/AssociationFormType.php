<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Association;
use App\Enum\AssociationAgreementVersionEnum;
use App\Enum\AssociationRadiusUnitEnum;
use App\Enum\AssociationTypeEnum;
use App\Enum\UserRoleEnum;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class AssociationFormType extends AbstractType
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Association $association */
        $association = $builder->getData();

        $builder
            ->add('name', TextType::class, [
                'label' => 'association.name',
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'association.description',
                'attr' => [
                    'data-controller' => 'textarea-autogrow',
                ],
            ])
            ->add('logoFile', FileType::class, [
                'required' => false,
                'attr' => [
                    'data-controller' => 'file-input',
                ],
                'label' => 'association.logo',
            ])
            ->add('website', TextType::class, [
                'required' => false,
                'label' => 'association.website',
            ])
            ->add('phoneNumber', PhoneNumberType::class, [
                'widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE,
                'required' => false,
                'label' => 'association.phone_number',
            ])
            ->add('radiusRange', IntegerType::class, [
                'attr' => [
                    'min' => 1,
                ],
                'label' => 'association.range_radius',
            ])
            ->add('radiusUnit', ChoiceType::class, [
                'choices' => AssociationRadiusUnitEnum::getChoices(),
                'label' => false,
            ])
            ->add(
                'checkDeposit',
                CheckboxType::class,
                [
                    'label_attr' => [
                        'class' => 'switch-custom',
                        'size' => 'md',
                    ],
                    'label' => 'association.check_deposit',
                    'required' => false,
                ]
            )
            ->add(
                'checkInsurance',
                CheckboxType::class,
                [
                    'label_attr' => [
                        'class' => 'switch-custom',
                        'size' => 'md',
                    ],
                    'label' => 'association.check_insurance',
                    'required' => false,
                ]
            )
            ->add('agreementVersion', ChoiceType::class, [
                'choices' => AssociationAgreementVersionEnum::getChoices(),
                'label' => 'association.agreement.version',
            ])
        ;

        if ($this->security->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $builder
                ->add('type', ChoiceType::class, [
                    'choices' => AssociationTypeEnum::getChoices(),
                    'label' => 'association.type',
                ])
                ->add('location', LocationFormType::class, [
                    'required' => true,
                    'label' => 'association.location',
                    'data' => $association->getLocation(),
                    'show_map' => true,
                ])
                ->add(
                    'activated',
                    CheckboxType::class,
                    [
                        'label_attr' => [
                            'class' => 'switch-custom',
                            'size' => 'md',
                        ],
                        'label' => 'association.activated',
                        'required' => false,
                    ]
                )
            ;
        } elseif ($options['isSuggestion']) {
            $builder
                ->add('location', LocationFormType::class, [
                    'required' => true,
                    'label' => 'association.location',
                    'data' => $association->getLocation(),
                ])
                ->add('email', EmailType::class, [
                    'required' => true,
                    'label' => 'user.email',
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Association::class,
            'isSuggestion' => false,
        ]);
    }
}
