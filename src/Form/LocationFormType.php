<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Location;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Location $location */
        $location = $builder->getData();

        $builder
            ->add('address', TextType::class, [
                'mapped' => false,
                'required' => true,
                'label' => false,
                'data' => $location ? $location->getReadableAddress() : '',
                'attr' => [
                    'class' => 'address-field',
                    'autocorrect' => 'off',
                    'autocomplete' => 'off',
                    'data-controller' => 'geocoding',
                    'data-show-map' => $options['show_map'],
                ],
            ])
            ->add('zipCode', HiddenType::class, [
                'attr' => ['data-geocode-fill-postalCode' => ''],
            ])
            ->add('district', HiddenType::class, [
                'attr' => ['data-geocode-fill-district' => ''],
            ])
            ->add('street', HiddenType::class, [
                'attr' => ['data-geocode-fill-street' => ''],
            ])
            ->add('country', HiddenType::class, [
                'attr' => ['data-geocode-fill-country' => ''],
            ])
            ->add('countryCode', HiddenType::class, [
                'attr' => [
                    'data-geocode-fill-country_code' => '',
                    'data-form-submission-target' => 'inputNotEmpty',
                    'data-action' => 'change->form-submission#change',
                ],
            ])
            ->add('latitude', HiddenType::class, [
                'attr' => [
                    'data-geocode-fill-latitude' => '',
                    'data-form-submission-target' => 'inputNotEmpty',
                    'data-action' => 'change->form-submission#change',
                ],
            ])
            ->add('longitude', HiddenType::class, [
                'attr' => [
                    'data-geocode-fill-longitude' => '',
                    'data-form-submission-target' => 'inputNotEmpty',
                    'data-action' => 'change->form-submission#change',
                ],
            ])
            ->add('city', HiddenType::class, [
                'attr' => ['data-geocode-fill-city' => ''],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
            'show_map' => false,
        ]);
    }
}
