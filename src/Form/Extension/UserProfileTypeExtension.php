<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\Hobby;
use App\Entity\Profile;
use App\Enum\ProfileGenderEnum;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Form\ProfileFormType;
use App\Service\GrantedService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Tbbc\MoneyBundle\Form\Type\CurrencyType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class UserProfileTypeExtension extends AbstractTypeExtension
{
    private $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProfileFormType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_USER !== $options['mode']) {
            return;
        }

        /** @var Profile $profile */
        $profile = $builder->getData();
        $user = $profile->getUser();
        $role = $this->grantedService->getRole($user);
        $isPersisted = !empty($user->getId());

        $builder
            ->add('hobbies', EntityType::class, [
                'class' => Hobby::class,
                'multiple' => true,
                'choice_label' => 'name',
                'label' => 'user.hobbies',
                'required' => false,
                'attr' => [
                    'data-controller' => 'choices',
                ],
            ])
            ->add('gender', ChoiceType::class, [
                'required' => true,
                'choices' => ProfileGenderEnum::getChoices(),
                'label' => 'user.gender',
            ])
            ->add('showHomesharing', CheckboxType::class, [
                'required' => false,
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'attr' => [
                    'data-action' => 'homesharing-dynamic-form#change',
                    'data-homesharing-dynamic-form-target' => 'showHomesharing',
                ],
                'label' => 'user.homesharing.form',
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($role, $isPersisted): void {
                /** @var Profile $profile */
                $profile = $event->getData();
                $form = $event->getForm();

                $this->manageHomesharingFields($form, $profile->isShowHomesharing(), $profile->isInternational(), $role, $isPersisted);
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($role, $isPersisted): void {
                $data = $event->getData();
                $form = $event->getForm();

                if (!\array_key_exists('showHomesharing', $data)) {
                    $data['showHomesharing'] = false;
                }

                if (!\array_key_exists('international', $data)) {
                    $data['international'] = false;
                }

                $this->manageHomesharingFields($form, (bool) ($data['showHomesharing']), (bool) ($data['international']), $role, $isPersisted);
            })
        ;

        if (UserRoleEnum::ROLE_YOUNG === $role) {
            $builder
                ->add('university', TextType::class, [
                    'required' => false,
                    'label' => 'user.university',
                ])
                ->add('studies', TextType::class, [
                    'required' => false,
                    'label' => 'user.studies',
                ])
                ->add('international', CheckboxType::class, [
                    'required' => false,
                    'label_attr' => [
                        'class' => 'switch-custom',
                        'size' => 'md',
                    ],
                    'attr' => [
                        'data-action' => 'homesharing-dynamic-form#change',
                        'data-homesharing-dynamic-form-target' => 'international',
                    ],
                    'label' => 'user.international.form',
                ])
            ;
        }

        if ($isPersisted) {
            $builder
                ->add('passportFile', VichFileType::class, [
                    'required' => false,
                    'allow_delete' => false,
                    'download_uri' => true,
                    'label' => 'user.passport',
                    'attr' => [
                        'data-controller' => 'file-input',
                    ],
                ])
                ->add('description', TextareaType::class, [
                    'required' => false,
                    'label' => 'user.description',
                ])
            ;
        }
    }

    private function manageHomesharingFields(
        FormInterface $form,
        bool $showHomesharing,
        bool $isInternational,
        string $role,
        bool $isPersisted
    ): void {
        if ($showHomesharing) {
            if ($isPersisted) {
                $form->add('motivation', TextareaType::class, [
                    'required' => true,
                    'label' => 'user.motivation',
                ]);
            }

            if (UserRoleEnum::ROLE_YOUNG === $role) {
                $form->add('currency', CurrencyType::class, [
                    'required' => true,
                    'label' => 'user.preferred_currency',
                ])
                ;

                if ($isPersisted) {
                    if ($isInternational) {
                        $form->add('mobilityLetterFile', VichFileType::class, [
                            'required' => false,
                            'allow_delete' => false,
                            'download_uri' => true,
                            'label' => 'user.mobility_letter',
                            'attr' => [
                                'data-controller' => 'file-input',
                            ],
                        ]);
                    } else {
                        $form->add('mobilityLetterFile', HiddenType::class);
                    }
                }
            }
        } else {
            $form
                ->add('motivation', HiddenType::class)
                ->add('currency', HiddenType::class)
                ->add('mobilityLetterFile', HiddenType::class)
            ;
        }
    }
}
