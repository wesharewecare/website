<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Form\LocationFormType;
use App\Form\RegistrationStep2FormType;
use App\Form\UserFormType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\FormBuilderInterface;

class UserTypeExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes(): iterable
    {
        return [UserFormType::class, RegistrationStep2FormType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_USER !== $options['mode']) {
            return;
        }

        /** @var User $user */
        $user = $builder->getData()->getUser();

        $builder
            ->get('user')
            ->add('birthday', BirthdayType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'fields-group__control',
                ],
                'label' => 'user.birthday',
            ])
            ->add('location', LocationFormType::class, [
                'label' => 'user.city',
                'required' => true,
                'data' => $user->getLocation(),
            ])
        ;
    }
}
