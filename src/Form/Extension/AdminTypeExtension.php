<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Form\LocationFormType;
use App\Form\RegistrationStep2FormType;
use App\Form\UserFormType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Security;

class AdminTypeExtension extends AbstractTypeExtension
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getExtendedTypes(): iterable
    {
        return [UserFormType::class, RegistrationStep2FormType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_ADMIN !== $options['mode']) {
            return;
        }

        /** @var User $user */
        $user = $builder->getData()->getUser();

        if (!$user->getId() || $this->security->isGranted(UserRoleEnum::ROLE_SUPER_ADMIN)) {
            $builder
                ->get('user')
                ->add('location', LocationFormType::class, [
                    'label' => 'user.city',
                    'required' => true,
                    'data' => $user->getLocation(),
                ])
            ;
        }
    }
}
