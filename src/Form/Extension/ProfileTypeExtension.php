<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Enum\UserProfileTypeEnum;
use App\Form\ProfileFormType;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Languages;

class ProfileTypeExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes(): iterable
    {
        return [ProfileFormType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('phoneNumber', PhoneNumberType::class, [
                'widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE,
                'required' => UserProfileTypeEnum::PROFILE_USER === $options['mode'],
                'label' => 'user.phone_number',
            ])
            ->add('languages', ChoiceType::class, [
                'multiple' => true,
                'label' => 'user.languages',
                'attr' => [
                    'data-controller' => 'choices',
                ],
                'required' => false,
                'choices' => array_flip(Languages::getNames()),
            ])
        ;
    }
}
