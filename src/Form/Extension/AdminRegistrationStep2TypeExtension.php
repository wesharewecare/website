<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Form\RegistrationStep2FormType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminRegistrationStep2TypeExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes(): iterable
    {
        return [RegistrationStep2FormType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_ADMIN !== $options['mode']) {
            return;
        }

        $builder
            ->get('user')
            ->add('roles', CollectionType::class, [
                'entry_type' => HiddenType::class,
                'data' => [UserRoleEnum::ROLE_ADMIN],
                'label' => false,
            ])
        ;
    }
}
