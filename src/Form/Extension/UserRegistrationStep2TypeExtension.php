<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Form\RegistrationStep2FormType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserRegistrationStep2TypeExtension extends AbstractTypeExtension
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getExtendedTypes(): iterable
    {
        return [RegistrationStep2FormType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_USER !== $options['mode']) {
            return;
        }

        $builder
            ->get('user')
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    $this->translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_SENIOR)) => UserRoleEnum::ROLE_SENIOR,
                    $this->translator->trans(UserRoleEnum::getReadableValue(UserRoleEnum::ROLE_YOUNG)) => UserRoleEnum::ROLE_YOUNG,
                ],
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'label' => false,
                'attr' => [
                    'class' => 'btn-group btn-group-toggle radio',
                    'data-toggle' => 'buttons',
                ],
                'label_attr' => [
                    'class' => 'btn btn-secondary',
                ],
                'data' => [UserRoleEnum::ROLE_SENIOR],
            ])
            ->get('roles')->addModelTransformer(new CallbackTransformer(
                function ($property) {
                    return (string) $property[0];
                },
                function ($property) {
                    return [$property];
                }
            ))
        ;
    }
}
