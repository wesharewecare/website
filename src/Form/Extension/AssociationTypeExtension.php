<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\Association;
use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Form\RegistrationStep2FormType;
use App\Form\UserFormType;
use App\Service\LocationHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Security;

class AssociationTypeExtension extends AbstractTypeExtension
{
    private $security;
    private $locationHelper;

    public function __construct(Security $security, LocationHelper $locationHelper)
    {
        $this->security = $security;
        $this->locationHelper = $locationHelper;
    }

    public static function getExtendedTypes(): iterable
    {
        return [UserFormType::class, RegistrationStep2FormType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
            return;
        }

        /** @var User $user */
        $user = $builder->getData()->getUser();

        if (!$user->getId() || $this->security->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $builder
                ->get('user')
                ->add('association', EntityType::class, [
                    'choice_label' => 'name',
                    'class' => Association::class,
                    'query_builder' => function (EntityRepository $er) {
                        $qb = $er->createQueryBuilder('association')
                            ->where('association.activated = :activated')
                            ->setParameter('activated', true)
                            ->orderBy('association.name', 'ASC')
                        ;

                        if ($this->security->isGranted(UserRoleEnum::ROLE_ADMIN)
                            && !$this->security->isGranted(UserRoleEnum::ROLE_SUPER_ADMIN)
                        ) {
                            /** @var User $currentUser */
                            $currentUser = $this->security->getUser();

                            $qb
                                ->innerJoin('association.location', 'location')
                                ->where('location.countryCode = :countryCode')
                                ->setParameter('countryCode', $currentUser->getLocation()->getCountryCode())
                            ;
                        }

                        return $qb;
                    },
                ])
                ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event): void {
                    /** @var User $user */
                    if ($user = $event->getData()) {
                        $this->locationHelper->copyLocationInformation(
                            $user->getAssociation()->getLocation(),
                            $user->getLocation()
                        );
                        $event->setData($user);
                    }
                })
            ;
        }
    }
}
