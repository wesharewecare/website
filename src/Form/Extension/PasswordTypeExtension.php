<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Form\ChangePasswordFormType;
use App\Form\RegistrationStep1FormType;
use App\Form\UserFormType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class PasswordTypeExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes(): iterable
    {
        return [
            ChangePasswordFormType::class,
            RegistrationStep1FormType::class,
            UserFormType::class,
        ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (\array_key_exists('password_field', $options) && !$options['password_field']) {
            return;
        }

        $mapped = false;

        if ($builder->has('user')) {
            $builder = $builder->get('user');
            $mapped = true;
        }

        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => !\array_key_exists('is_external_edit', $options) || !$options['is_external_edit'],
                'first_options' => [
                    'label' => 'user.password',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'user.password.blank',
                            'groups' => ['SelfManagementUser'],
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'user.password.length',
                            'groups' => ['SelfManagementUser'],
                        ]),
                    ],
                ],
                'second_options' => [
                    'label' => 'user.password.confirmation',
                ],
                'invalid_message' => 'user.password.not_match',
                'mapped' => $mapped,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('validation_groups', function (Options $options, $previousValue) {
            if ($options->offsetExists('is_external_edit') && $options->offsetGet('is_external_edit')) {
                return $previousValue;
            }

            return ['Default', 'SelfManagementUser'];
        });
    }
}
