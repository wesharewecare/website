<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Document;
use App\Enum\DocumentRecipientEnum;
use App\Enum\UserRoleEnum;
use App\Service\LocaleManager;
use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class DocumentType extends BaseType
{
    private $security;
    private $localeManager;

    public function __construct(Security $security, LocaleManager $localeManager)
    {
        $this->security = $security;
        $this->localeManager = $localeManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Document $document */
        $document = $builder->getData();

        $recipients = $this->security->isGranted(UserRoleEnum::ROLE_ADMIN)
            ? DocumentRecipientEnum::getChoices()
            : array_flip(DocumentRecipientEnum::$associationChoices)
        ;

        $builder
            ->add('name', TextType::class, [
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
                'label' => 'document.name',
            ])
            ->add('locale', ChoiceType::class, [
                'choices' => array_flip($this->localeManager->getLocalesTranslated()),
                'required' => true,
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
                'label' => 'document.locale',
            ])
            ->add('recipients', ChoiceType::class, [
                'choices' => $recipients,
                'multiple' => true,
                'required' => true,
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
                'attr' => [
                    'data-controller' => 'choices',
                ],
                'label' => 'document.recipients',
            ])
        ;

        if (!$document->getDocumentName()) {
            $builder->add('documentFile', FileType::class, [
                'label' => 'document.file',
                'help' => 'file.max_upload_size',
                'attr' => [
                    'data-controller' => 'file-input',
                    'data-action' => 'file-preview#change',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
