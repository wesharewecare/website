<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Event;
use App\Service\LanguageManager;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    private $languageManager;

    public function __construct(LanguageManager $languageManager)
    {
        $this->languageManager = $languageManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Event $event */
        $event = $builder->getData();

        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'event.name',
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'event.description',
                'attr' => [
                    'data-controller' => 'textarea-autogrow',
                ],
                'help' => 'event.description.help',
            ])
            ->add('date', DateTimeType::class, [
                'required' => true,
                'label' => 'event.date',
                'data' => new \DateTime(),
                'minutes' => ['00', '15', '30', '45'],
                'years' => range((int) date('Y'), (int) date('Y') + 5),
            ])
            ->add('eventImageFile', FileType::class, [
                'required' => false,
                'attr' => [
                    'data-controller' => 'file-input',
                ],
                'label' => 'event.image',
            ])
            ->add('location', LocationFormType::class, [
                'required' => true,
                'label' => 'event.location',
                'data' => $event->getLocation(),
                'show_map' => true,
            ])
            ->add('isFree', CheckboxType::class, [
                'required' => false,
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'label' => 'event.is_free',
            ])
            ->add('isHandicapAccessible', CheckboxType::class, [
                'required' => false,
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'label' => 'event.is_handicap_accessible',
            ])
            ->add('isOpenAir', CheckboxType::class, [
                'required' => false,
                'label_attr' => [
                    'class' => 'switch-custom',
                    'size' => 'md',
                ],
                'label' => 'event.is_open_air',
            ])
            ->add('languages', ChoiceType::class, [
                'multiple' => true,
                'label' => 'event.languages',
                'attr' => [
                    'data-controller' => 'choices',
                ],
                'required' => false,
                'choices' => array_flip($this->languageManager->getEuropeanLanguages()),
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'user.email',
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'user.firstname',
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'user.lastname',
            ])
            ->add('phoneNumber', PhoneNumberType::class, [
                'widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE,
                'required' => false,
                'label' => 'user.phone_number',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
