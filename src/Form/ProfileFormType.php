<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Profile;
use App\Enum\UserProfileTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Profile $profile */
        $profile = $builder->getData();

        if ($profile->getId()) {
            $builder
                ->add('profilePictureFile', FileType::class, [
                    'required' => false,
                    'attr' => [
                        'data-controller' => 'file-input',
                    ],
                    'label' => 'user.profile_picture',
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
            'mode' => UserProfileTypeEnum::PROFILE_USER,
        ]);
    }
}
