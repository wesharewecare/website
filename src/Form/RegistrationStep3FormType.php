<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Form\Model\CreateUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationStep3FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var CreateUser $createUser */
        $createUser = $builder->getData();

        $builder
            ->add('user', FormType::class, [
                'data_class' => User::class,
                'label' => false,
            ])
            ->add('profile', ProfileFormType::class, [
                'mode' => $options['mode'],
                'label' => false,
                'data' => $createUser->getProfile(),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CreateUser::class,
            'mode' => UserProfileTypeEnum::PROFILE_USER,
        ]);
    }
}
