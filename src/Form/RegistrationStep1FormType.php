<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Form\Model\CreateUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationStep1FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $userForm = $builder->create('user', FormType::class, [
            'data_class' => User::class,
        ]);

        $userForm
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'user.email',
            ])
        ;

        $builder->add($userForm);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CreateUser::class,
            'mode' => UserProfileTypeEnum::PROFILE_USER,
        ]);
    }
}
