<?php

declare(strict_types=1);

namespace App\Form;

use Craue\FormFlowBundle\Form\FormFlow;

class RegistrationFlow extends FormFlow
{
    public function getFormOptions($step, array $options = []): array
    {
        $options = parent::getFormOptions($step, $options);
        $options['validation_groups'][] = 'Default';

        return $options;
    }

    protected function loadStepsConfig(): array
    {
        return [
            [
                'form_type' => RegistrationStep1FormType::class,
            ],
            [
                'form_type' => RegistrationStep2FormType::class,
            ],
            [
                'form_type' => RegistrationStep3FormType::class,
            ],
        ];
    }
}
