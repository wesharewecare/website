<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Cohabitation;
use App\Enum\CohabitationWorkflowStepStatusEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tbbc\MoneyBundle\Form\Type\CurrencyType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CohabitationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $association = $options['association'];
        $cohabitation = $builder->getData();

        switch ($options['current_step']) {
            case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SENT:
                $currentYear = date('Y');

                $builder
                    ->add('startDate', DateType::class, [
                        'required' => true,
                        'label' => 'cohabitation.start_date',
                        'years' => range($currentYear, $currentYear + 3),
                    ])
                    ->add('endDate', DateType::class, [
                        'required' => true,
                        'label' => 'cohabitation.end_date',
                        'years' => range($currentYear, $currentYear + 10),
                    ])
                    ->add('priceAmount', IntegerType::class, [
                        'required' => true,
                        'label' => 'accommodation.price',
                        'attr' => [
                            'min' => 0,
                        ],
                        'label_attr' => [
                            'class' => 'col-form-label',
                        ],
                    ])
                    ->add('currency', CurrencyType::class, [
                        'required' => true,
                        'label' => 'accommodation.currency',
                    ])
                    ->add('agreementParams', CollectionType::class, [
                        'entry_type' => CohabitationAgreementParamsType::class,
                        'label' => false,
                        'entry_options' => [
                            'association' => $association,
                            'cohabitation' => $cohabitation,
                            'label' => false,
                        ],
                    ])
                ;

                break;

            case CohabitationWorkflowStepStatusEnum::STATUS_AGREEMENT_SIGNED:
                if ($association->isCheckDeposit()) {
                    $builder->add(
                        'depositChecked',
                        CheckboxType::class,
                        [
                            'label_attr' => ['class' => 'required'],
                            'label' => 'cohabitation.deposit_checked',
                            'required' => false,
                        ]
                    );
                }

                if ($association->isCheckInsurance()) {
                    $builder->add(
                        'insuranceChecked',
                        CheckboxType::class,
                        [
                            'label_attr' => ['class' => 'required'],
                            'label' => 'cohabitation.insurance_checked',
                            'required' => false,
                        ]
                    );
                }

                $builder->add('agreementSignedFile', VichFileType::class, [
                    'required' => false,
                    'allow_delete' => false,
                    'download_uri' => true,
                    'label' => 'cohabitation.agreement_signed',
                    'label_attr' => ['class' => 'required'],
                    'attr' => [
                        'data-controller' => 'file-input',
                    ],
                ]);

                break;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cohabitation::class,
            'current_step' => CohabitationWorkflowStepStatusEnum::STATUS_COHABITATION_INIT,
            'association' => null,
        ]);
    }
}
