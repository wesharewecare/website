<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Association;
use App\Entity\User;
use App\Enum\ProfileGenderEnum;
use App\Enum\UserRoleEnum;
use App\Service\LocaleManager;
use App\Service\LocationHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use libphonenumber\PhoneNumber;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    public const USER_REFERENCE = 'user';
    public const NB_USERS = 20;
    public const GENDERS = [ProfileGenderEnum::GENDER_FEMALE, ProfileGenderEnum::GENDER_MALE];
    public const PROFILE_PICTURES = [ProfileGenderEnum::GENDER_FEMALE => 'default-f.svg', ProfileGenderEnum::GENDER_MALE => 'default-h.svg'];

    private $emailsUsed = [];
    private $faker;
    private $passwordHasher;
    private $locationFixtures;
    private $uploadFileFixtures;
    private $locationHelper;
    private $localeManager;

    public function __construct(
        UserPasswordHasherInterface $passwordHasher,
        LocationFixtures $locationFixtures,
        UploadFileFixtures $uploadFileFixtures,
        LocationHelper $locationHelper,
        LocaleManager $localeManager
    ) {
        $this->passwordHasher = $passwordHasher;
        $this->locationFixtures = $locationFixtures;
        $this->uploadFileFixtures = $uploadFileFixtures;
        $this->locationHelper = $locationHelper;
        $this->localeManager = $localeManager;
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        $manager->persist($admin = $this->createUser(
            'admin',
            '',
            ProfileGenderEnum::GENDER_MALE,
            UserRoleEnum::ROLE_SUPER_ADMIN
        ));
        $this->addReference(self::USER_REFERENCE.'_admin', $admin);

        /** @var Association $association */
        $association = $this->getReference(AssociationFixtures::ASSOCIATION_REFERENCE.'_14');

        $manager->persist($assoManager = $this->createUser(
            'asso',
            '',
            ProfileGenderEnum::GENDER_MALE,
            UserRoleEnum::ROLE_ASSOCIATION,
            $association
        ));
        $this->addReference(self::USER_REFERENCE.'_asso', $assoManager);

        $manager->persist($user = $this->createUser(
            'senior',
            '',
            ProfileGenderEnum::GENDER_FEMALE,
            UserRoleEnum::ROLE_SENIOR,
            $association
        ));
        $this->addReference(self::USER_REFERENCE.'_senior', $user);

        $manager->persist($user = $this->createUser(
            'young',
            '',
            ProfileGenderEnum::GENDER_FEMALE,
            UserRoleEnum::ROLE_YOUNG,
            $association
        ));
        $this->addReference(self::USER_REFERENCE.'_young', $user);

        $roles = [UserRoleEnum::ROLE_SENIOR, UserRoleEnum::ROLE_YOUNG];

        for ($i = 0; $i < self::NB_USERS; ++$i) {
            $gender = self::GENDERS[array_rand(self::GENDERS)];

            [$firstName, $lastName] = $this->getFakerName($gender);
            $manager->persist($user = $this->createUser(
                $firstName,
                $lastName,
                $gender,
                $roles[array_rand($roles)],
                $association
            ));

            $this->addReference(self::USER_REFERENCE.'_'.$i, $user);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 3;
    }

    private function getFakerName(string $gender): array
    {
        do {
            $firstName = $this->faker->firstName($gender);
            $lastName = $this->faker->lastName();
            $email = mb_strtolower("{$firstName}.{$lastName}@wesharewecare.eu");
        } while (\in_array($email, $this->emailsUsed, true));

        $this->emailsUsed[] = $email;

        return [$firstName, $lastName];
    }

    private function createUser(
        string $firstName,
        string $lastName,
        string $gender,
        string $role,
        Association $association = null
    ): User {
        $user = new User();

        $email = empty($lastName)
            ? $firstName
            : "{$firstName}.{$lastName}"
        ;

        $user->setFirstName($firstName);
        $user->setLastname($lastName);
        $user->setEmail(mb_strtolower("{$email}@wesharewecare.eu"));
        $user->setRoles([$role]);
        $user->setPassword(
            $this->passwordHasher->hashPassword(
                $user,
                mb_strtolower($firstName)
            )
        );
        $user->setIsVerified(true);

        if ($association) {
            $user->setAssociation($association);
            $this->locationHelper->copyLocationInformation(
                $association->getLocation(),
                $user->getLocation(),
                true
            );
        } else {
            $user->setLocation($this->locationFixtures->getFakeLocation());
        }

        $profile = $user->getProfile();
        $profile->setGender($gender);
        $profile->setPhoneNumber($this->getFakePhoneNumber());
        $profile->setMotivation($this->faker->text());
        $profile->setIsApproved(true);
        $profile->setLocaleLanguage($this->localeManager->getDefaultLocale());

        $this->uploadFileFixtures->fakeUploadFile($profile, 'profilePictureFile', self::PROFILE_PICTURES[$gender]);
        $this->uploadFileFixtures->fakeUploadFile($profile, 'passportFile', 'id.jpeg');

        if (UserRoleEnum::ROLE_YOUNG === $role) {
            $this->uploadFileFixtures->fakeUploadFile($profile, 'mobilityLetterFile', 'acceptance_letter.pdf');
            $profile->setInternational(true);
        }

        return $user;
    }

    private function getFakePhoneNumber(): PhoneNumber
    {
        $phoneNumber = new PhoneNumber();
        $phoneNumber->setCountryCode(33);
        $phoneNumber->setNationalNumber($this->faker->phoneNumber07());

        return $phoneNumber;
    }
}
