<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Location;
use Faker\Factory;

class LocationFixtures
{
    public function getFakeLocation(): Location
    {
        $faker = Factory::create('fr_FR');

        $location = new Location();
        $location->setCity($faker->city());
        $location->setCountry('France');
        $location->setCountryCode('FRA');
        $location->setStreet($faker->streetAddress());
        $location->setZipCode($faker->postcode());
        $location->setLatitude($faker->latitude(48, 49));
        $location->setLongitude($faker->longitude(2, 3));

        return $location;
    }
}
