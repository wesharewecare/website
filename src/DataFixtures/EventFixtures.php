<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Association;
use App\Entity\Event;
use App\Entity\User;
use App\Enum\EventStatusEnum;
use App\Service\LocationHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EventFixtures extends Fixture implements OrderedFixtureInterface
{
    private $locationHelper;
    private $uploadFileFixtures;
    private $faker;

    public function __construct(LocationHelper $locationHelper, UploadFileFixtures $uploadFileFixtures)
    {
        $this->locationHelper = $locationHelper;
        $this->uploadFileFixtures = $uploadFileFixtures;
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        /** @var Association $association */
        $association = $this->getReference(AssociationFixtures::ASSOCIATION_REFERENCE.'_14');

        /** @var User $contact */
        $contact = $this->getReference(UserFixtures::USER_REFERENCE.'_asso');

        foreach ($this->getEventData() as $event) {
            $e = $this->createEvent($event, $association, $contact);
            $manager->persist($e);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 6;
    }

    private function createEvent(array $data, Association $association, User $contact): Event
    {
        $event = new Event();
        $event->setName($data['name']);
        $event->setDescription($data['description']);
        $event->setLanguages($data['languages']);
        $event->setStatus(EventStatusEnum::STATUS_OPEN);
        $event->setFirstname($contact->getFirstname());
        $event->setLastname($contact->getLastname());
        $event->setEmail($contact->getEmail());
        $event->setPhoneNumber($contact->getProfile()->getPhoneNumber());
        $event->setDate($this->faker->dateTimeBetween('now', '+1 year'));
        $event->setAssociation($association);

        $this->locationHelper->copyLocationInformation($association->getLocation(), $event->getLocation(), true);
        $this->uploadFileFixtures->fakeUploadFile($event, 'eventImageFile', $data['image']);

        return $event;
    }

    private function getEventData(): array
    {
        $events = [];

        $events[] = [
            'name' => 'Repas cuisiné ensemble',
            'description' => "Dans la maison de l'intergénérationnel à Boissy, repas cuisiné et partagé",
            'languages' => ['fr', 'es', 'en'],
            'image' => 'cooking.jpeg',
        ];

        $events[] = [
            'name' => 'Diner Intergénérationnel',
            'description' => 'Diner Intergénérationnel, pour manger ensemble des plats de plusieurs pays du monde et en apprendre plus sur nos cultures ! Les couverts seront fournis, et vous pourrez réchauffez vos plats sur place. 3 grands frigos sont également disponibles. Si vous avez besoin de cuisiner sur place, prévenez nous en avance.',
            'languages' => ['fr', 'es', 'en'],
            'image' => 'diner.jpeg',
        ];

        $events[] = [
            'name' => 'Randonnée de 8km dans la Vallée des 3 rivières',
            'description' => "Nous organisons tous les mois des randonnées tous niveaux, et proposons cette fois un parcours facile de 8kms, où jeunes et seniors pourront discuter en plusieurs langues tout en découvrant les paysages de la Vallée des 3 rivières. Nous ferons une pause d'une demi-heure à mi-parcours. Pensez à bien remplir vos gourdes ! La participation est de 5€, elle inclue un goûter et permettra aussi que nous soyons accompagnés par une spécialiste des oiseaux, qui nous présentera les espèces locales.",
            'languages' => ['fr', 'es', 'en'],
            'image' => 'rando.jpeg',
        ];

        $events[] = [
            'name' => 'Concours de pétanque',
            'description' => 'En haut des buttes chaumont, retrouvons-nous ce samedi pour un concours de pétanque à côté du métro Botzaris',
            'languages' => ['fr', 'es', 'en'],
            'image' => 'petanque.jpeg',
        ];

        return $events;
    }
}
