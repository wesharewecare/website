<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Accommodation;
use App\Entity\User;
use App\Enum\AccommodationStatusEnum;
use App\Enum\AccommodationTypeEnum;
use App\Enum\UserRoleEnum;
use App\Service\GrantedService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AccommodationFixtures extends Fixture implements OrderedFixtureInterface
{
    private $grantedService;
    private $faker;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < UserFixtures::NB_USERS; ++$i) {
            /** @var User $user */
            $user = $this->getReference(UserFixtures::USER_REFERENCE.'_'.$i);

            if ($this->grantedService->isGranted($user, UserRoleEnum::ROLE_SENIOR)) {
                $accommodation = new Accommodation();
                $accommodation->setDescription($this->faker->text());
                $accommodation->setRoomDescription($this->faker->text());
                $accommodation->setType((string) AccommodationTypeEnum::getRandomValue());
                $accommodation->setName($this->faker->text());
                $accommodation->setRoomSize(random_int(9, 50));
                $accommodation->setHouseSize(random_int(40, 500));
                $accommodation->setPriceAmount(random_int(150, 400));
                $accommodation->setCurrencyCode('EUR');
                $accommodation->setStatus(AccommodationStatusEnum::STATUS_OPEN);
                $accommodation->setSenior($user);

                $manager->persist($accommodation);
            }
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 4;
    }
}
