<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Service\MessagingHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MessageFixtures extends Fixture implements OrderedFixtureInterface
{
    private $messagingHelper;
    private $userReference;
    private $nbUsers;

    public function __construct(MessagingHelper $messagingHelper)
    {
        $this->messagingHelper = $messagingHelper;
        $this->userReference = UserFixtures::USER_REFERENCE;
        $this->nbUsers = UserFixtures::NB_USERS;
    }

    public function load(ObjectManager $manager): void
    {
        $admin = $this->getReference($this->userReference.'_admin');

        $users = [$this->getReference($this->userReference.'_asso')];
        for ($i = 0; $i < $this->nbUsers; ++$i) {
            $users[] = $this->getReference($this->userReference.'_'.$i);
        }

        foreach ($users as $user) {
            $thread = $this->messagingHelper->getThreadBySubjectAndParticipants(
                MessagingHelper::DIRECT_MESSAGE,
                [
                    $user,
                    $admin,
                ]
            );
            $manager->persist($thread);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 5;
    }
}
