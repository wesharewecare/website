<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Hobby;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class HobbyFixtures extends Fixture implements OrderedFixtureInterface
{
    public const HOBBY_REFERENCE = 'hobby';

    public function load(ObjectManager $manager): void
    {
        $hobbies = ['Sports', 'Music', 'Talking / Meet New People', 'Traveling', 'Cooking / Gastronomy'];

        for ($i = 0; $i < \count($hobbies); ++$i) {
            $hobby = new Hobby();
            $hobby->setName($hobbies[$i]);

            $manager->persist($hobby);
            $this->addReference(self::HOBBY_REFERENCE.'_'.$i, $hobby);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 2;
    }
}
