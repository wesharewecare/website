<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Association;
use App\Entity\Location;
use App\Enum\AssociationTypeEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use libphonenumber\PhoneNumber;

class AssociationFixtures extends Fixture implements OrderedFixtureInterface
{
    public const ASSOCIATION_REFERENCE = 'association';

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getAssociationData() as $k => $association) {
            $asso = $this->createAssociation($association);
            $manager->persist($asso);
            $this->addReference(self::ASSOCIATION_REFERENCE.'_'.$k, $asso);
        }
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 1;
    }

    private function createAssociation(array $association): Association
    {
        $location = $association['location'];

        $loc = new Location();
        $loc->setStreet($location['street']);
        $loc->setCity($location['city']);
        $loc->setCountry($location['country']);
        $loc->setCountryCode($location['countryCode']);
        $loc->setZipCode($location['zipCode']);
        $loc->setLatitude((float) $location['latitude']);
        $loc->setLongitude((float) $location['longitude']);

        $asso = new Association();
        $asso->setName($association['name']);
        $asso->setWebsite($association['website']);
        $asso->setPhoneNumber($association['phone']);
        $asso->setLocation($loc);
        $asso->setType(AssociationTypeEnum::TYPE_BOTH);

        return $asso;
    }

    private function getAssociationData(): array
    {
        $associations = [];

        $associations[] = [
            'name' => 'Domicile Inter-Générations Isérois (DIGI)',
            'website' => 'http://digi38.org',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('476032418'),
            'location' => [
                'street' => '2 Boulevard Maréchal Joffre',
                'city' => 'Grenoble',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '38000',
                'latitude' => 45.182380,
                'longitude' => 5.727640,
            ],
        ];

        $associations[] = [
            'name' => 'Le Pari Solidaire Lyon',
            'website' => 'www.leparisolidairelyon.org',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('688207705'),
            'location' => [
                'street' => '59 Rue Antoine Charial',
                'city' => 'Lyon',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '69003',
                'latitude' => 45.758190,
                'longitude' => 4.867240,
            ],
        ];

        $associations[] = [
            'name' => 'Ensemble2générations Lyon',
            'website' => 'http://www.leparisolidairelyon.org',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('644027376'),
            'location' => [
                'street' => '8 Chemin de la Halte du Méridien',
                'city' => 'Charbonnières-les-Bains',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '69260',
                'latitude' => 45.772460,
                'longitude' => 4.740350,
            ],
        ];

        $associations[] = [
            'name' => 'Tim & Colette',
            'website' => 'http://esdes-intergenerations.net/',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('681148265'),
            'location' => [
                'street' => '10, place des Archives',
                'city' => 'Lyon',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '69286',
                'latitude' => 45.747610,
                'longitude' => 4.825450,
            ],
        ];

        $associations[] = [
            'name' => 'ADMR34',
            'website' => 'https://www.admr34.fr/',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('467207575'),
            'location' => [
                'street' => '78 Allée John Napier',
                'city' => 'Montpellier',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '34000',
                'latitude' => 43.615010,
                'longitude' => 3.909610,
            ],
        ];

        $associations[] = [
            'name' => 'ABRAPA1',
            'website' => 'https://www.abrapa.asso.fr/',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('388213021'),
            'location' => [
                'street' => '1 Rue Jean Monnet',
                'city' => 'Strasbourg',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '67000',
                'latitude' => 48.5729852,
                'longitude' => 7.798144,
            ],
        ];

        $associations[] = [
            'name' => 'Solidarios',
            'website' => 'www.solidarios.org.es',
            'phone' => (new PhoneNumber())
                ->setCountryCode(34)
                ->setNationalNumber('913946363'),
            'location' => [
                'street' => '65 Calle Donoso Cortés',
                'city' => 'Madrid',
                'country' => 'Spain',
                'countryCode' => 'ESP',
                'zipCode' => '28015',
                'latitude' => 40.4165,
                'longitude' => -3.7026,
            ],
        ];

        $associations[] = [
            'name' => 'ASBL 1 Toit 2 Ages',
            'website' => 'http://www.1toit2ages.be/fr/accueil.html',
            'phone' => (new PhoneNumber())
                ->setCountryCode(32)
                ->setNationalNumber('498737319'),
            'location' => [
                'street' => '16 Rue Sneessens',
                'city' => 'Etterbeek',
                'country' => 'Belgium',
                'countryCode' => 'BEL',
                'zipCode' => '1040',
                'latitude' => 50.8322698,
                'longitude' => 4.3884011,
            ],
        ];

        $associations[] = [
            'name' => 'ensemble2générations Lille',
            'website' => 'http://www.ensemble2generations.fr',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('618483485'),
            'location' => [
                'street' => '1 Rue Saint-Chrysole',
                'city' => 'Verlinghem',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '59237',
                'latitude' => '50.682372',
                'longitude' => '2.996041',
            ],
        ];

        $associations[] = [
            'name' => 'Générations et Cultures',
            'website' => 'http://www.generationsetcultures.fr/',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('640295426'),
            'location' => [
                'street' => '61 Rue de la Justice',
                'city' => 'Lille',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '59000',
                'latitude' => 50.6210418,
                'longitude' => 3.0559857,
            ],
        ];

        $associations[] = [
            'name' => 'Presse Purée',
            'website' => 'http://pressepuree64.fr',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('559309030'),
            'location' => [
                'street' => '58 Rue Castetnau',
                'city' => 'Pau',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '64000',
                'latitude' => 43.3003269,
                'longitude' => -0.3662034,
            ],
        ];

        $associations[] = [
            'name' => 'Maillâges',
            'website' => 'https://www.facebook.com/maillages/',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('660431942'),
            'location' => [
                'street' => '11bis Rue Georges Berges',
                'city' => 'Bayonne',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '64100',
                'latitude' => 43.4960154,
                'longitude' => -1.4879363,
            ],
        ];

        $associations[] = [
            'name' => 'Vivre avec',
            'website' => 'www.logement-solidaire.org',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('557956602'),
            'location' => [
                'street' => '279 Cours de la Somme',
                'city' => 'Bordeaux',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '33800',
                'latitude' => 44.819294,
                'longitude' => -0.572443,
            ],
        ];

        $associations[] = [
            'name' => 'ensemble2générations',
            'website' => 'http://ensemble2generations.fr/',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('130248128'),
            'location' => [
                'street' => '3 bis, rue Dumont d’Urville',
                'city' => 'La Garenne Colombes',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '92250',
                'latitude' => 48.9074745,
                'longitude' => 2.2403189,
            ],
        ];

        $associations[] = [
            'name' => 'Le Pari Solidaire',
            'website' => 'https://www.leparisolidaire.fr',
            'phone' => (new PhoneNumber())
                ->setCountryCode(33)
                ->setNationalNumber('142270620'),
            'location' => [
                'street' => '6 Rue Duchefdelaville',
                'city' => 'Paris',
                'country' => 'France',
                'countryCode' => 'FRA',
                'zipCode' => '75013',
                'latitude' => 48.8323537,
                'longitude' => 2.3705561,
            ],
        ];

        return $associations;
    }
}
