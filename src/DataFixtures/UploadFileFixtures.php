<?php

declare(strict_types=1);

namespace App\DataFixtures;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\PropertyAccess\PropertyAccess;

class UploadFileFixtures
{
    public function fakeUploadFile(object $entity, string $fieldName, string $fileName): void
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        if ($propertyAccessor->isWritable($entity, $fieldName)) {
            $fs = new Filesystem();
            $targetPath = sys_get_temp_dir().'/'.$fileName;
            $fs->copy(__DIR__.'/images/'.$fileName, $targetPath, true);

            $propertyAccessor->setValue(
                $entity,
                $fieldName,
                new UploadedFile($targetPath, $fileName, null, null, true)
            );
        }
    }
}
