<?php

namespace Deployer;

use Symfony\Component\Dotenv\Dotenv;

require 'vendor/autoload.php';
require 'recipe/symfony4.php';
require 'recipe/slack.php';
require 'recipe/yarn.php';
require 'recipe/cachetool.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env.local');

set('application', 'wesharewecare');
set('repository', 'git@gitlab.com:wesharewecare/website.git');

set('slack_webhook', getenv('SLACK_WEBHOOK'));
set('slack_text', 'deploying `{{branch}}` to *{{target}}*');

set('git_tty', true);
set('bin/php', '/usr/bin/php7.4');

set('http_user', 'www-data');
set('keep_releases', 3);

set('cachetool', '/var/run/php/php7.4-fpm.sock');

add('shared_dirs', ['public/maintenance', 'public/uploads', 'public/documents', 'public/media/cache']);

set('clear_paths', [
    '/features',
    '/tests',
    '.docker',
    '.editorconfig',
    '.env.test',
    '.eslintrc',
    '.git',
    '.gitignore',
    '.gitlab-ci.yml',
    '.php_cs.dist',
    '.php-cs-fixer.dist.php',
    'behat.yml.dist',
    'config.toml',
    'deploy.php',
    'deploy_config.dist',
    'docker-compose.yml',
    'docker-compose.override.yml.dist',
    'docker-compose.ci.yml',
    'Makefile',
    'phpunit.xml.dist',
    'psalm.xml',
    'README.md',
]);

set('bin/yarn', function () {
    return run('which yarnpkg');
});

set('bin/composer', function () {
    run('cd {{release_path}} && curl -sS https://getcomposer.org/installer | {{bin/php}}');
    $composer = '{{release_path}}/composer.phar';

    return '{{bin/php}} '.$composer;
});

host('prod', 'staging', 'demo')
    ->configFile('deploy_config')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->multiplexing(false)
    ->forwardAgent(true)
;

host('prod')
    ->stage('prod')
    ->set('host', 'prod')
    ->set('symfony_env', 'prod')
    ->set('branch', 'master')
    ->set('deploy_path', '/srv/www/wesharewecare.eu/prod')
;

host('staging')
    ->stage('staging')
    ->set('host', 'staging')
    ->set('symfony_env', 'dev')
    ->set('branch', 'staging')
    ->set('deploy_path', '/srv/www/wesharewecare.eu/staging')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader')
;

host('demo')
    ->stage('demo')
    ->set('host', 'demo')
    ->set('symfony_env', 'prod')
    ->set('branch', 'staging')
    ->set('deploy_path', '/srv/www/wesharewecare.eu/demo')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader')
;

desc('Compile .env files for production');
task('deploy:dump-env', function () {
    run('cd {{release_path}} && {{bin/composer}} dump-env {{symfony_env}}');
});

desc('Build CSS/JS files');
task('deploy:build_local_assets', function () {
    run('{{bin/php}} {{bin/console}} fos:js-routing:dump --format=json --target={{release_path}}/public/js/fos_js_routes.json');
    run('cd {{release_path}} && {{bin/yarn}} run {{symfony_env}}');
});

desc('Import new translations to DB');
task('translations:import', function () {
    run('{{bin/php}} {{bin/console}} translation:download');
    run('{{bin/php}} {{bin/console}} bazinga:js-translation:dump public/js/');
});

desc('Retrieve money ratio');
task('money:ratio-fetch', function () {
    run('{{bin/php}} {{bin/console}} tbbc:money:ratio-fetch');
});

desc('Sync metadata storage for doctrine migration');
task('database:sync-meta', function () {
    run(sprintf('{{bin/php}} {{bin/console}} doctrine:migrations:sync-metadata-storage {{console_options}}'));
});

desc('Drop database');
task('database:drop', function () {
    run('{{bin/php}} {{bin/console}} doctrine:database:drop --force --if-exists');
    run('{{bin/php}} {{bin/console}} doctrine:database:create --if-not-exists');
})->onHosts('demo');

desc('Load fixtures into database');
task('database:load-fixtures', function () {
    run('{{bin/php}} {{bin/console}} doctrine:fixtures:load --append');
})->onHosts('demo');

before('deploy', 'slack:notify');
after('success', 'slack:notify:success');
after('deploy:failed', 'slack:notify:failure');

after('deploy:update_code', 'deploy:clear_paths');
after('deploy:shared', 'yarn:install');
after('deploy:vendors', 'deploy:dump-env');
after('deploy:vendors', 'deploy:build_local_assets');
before('database:sync-meta', 'database:drop');
before('database:migrate', 'database:sync-meta');
before('deploy:symlink', 'database:migrate');
after('database:migrate', 'database:load-fixtures');
after('database:migrate', 'translations:import');
//before('deploy:symlink', 'money:ratio-fetch');
after('deploy:symlink', 'cachetool:clear:opcache');
after('deploy:failed', 'deploy:unlock');
