import {Controller} from 'stimulus';
import Ajax from '../js/utils/Ajax';

export default class extends Controller {
    static targets = ["email", "firstname", "lastname", "motivation", "showHomesharing", "currency", "mobilityLetter", "international"]

    change() {
        const data = new FormData();

        if (this.hasEmailTarget) {
            data.append(this.emailTarget.getAttribute('name'), this.emailTarget.value);
        }

        if (this.hasFirstnameTarget) {
            data.append(this.firstnameTarget.getAttribute('name'), this.firstnameTarget.value);
        }

        if (this.hasLastnameTarget) {
            data.append(this.lastnameTarget.getAttribute('name'), this.lastnameTarget.value);
        }

        if (this.hasInternationalTarget) {
            data.append(this.internationalTarget.getAttribute('name'), this.internationalTarget.checked ? 1 : 0);
        }

        const flow_instance = this.element.querySelector(`input[name='flow_registration_instance']`);
        if (flow_instance) {
            data.append(flow_instance.getAttribute('name'), flow_instance.value);
        }

        const flow_step = this.element.querySelector(`input[name='flow_registration_step']`);
        if (flow_step) {
            data.append(flow_step.getAttribute('name'), flow_step.value);
        }

        data.append(this.showHomesharingTarget.getAttribute('name'), this.showHomesharingTarget.checked ? 1 : 0);

        Ajax.post(this.element.getAttribute('action'), data, 'html')
            .then((response) => {
                const html = this.htmlToElements(response);

                if (this.hasMotivationTarget) {
                    this.motivationTarget.replaceWith(html.querySelector(`div[data-homesharing-dynamic-form-target="motivation"]`));
                }

                if (this.hasCurrencyTarget) {
                    this.currencyTarget.replaceWith(html.querySelector(`div[data-homesharing-dynamic-form-target="currency"]`));
                }

                if (this.hasMobilityLetterTarget) {
                    this.mobilityLetterTarget.replaceWith(html.querySelector(`div[data-homesharing-dynamic-form-target="mobilityLetter"]`));
                }
            });
    }

    htmlToElements(html) {
        let template = document.createElement('template');
        template.innerHTML = html;
        return template.content;
    }
}
