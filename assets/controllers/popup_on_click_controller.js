import {Controller} from 'stimulus';
import Ajax from '../js/utils/Ajax';
import Popup from "../js/utils/Popup";

export default class extends Controller {
    static values = {
        id: Number
    };

    click(event) {
        const {route, referer, title} = event.target.dataset;

        const options = {};

        if (this.hasIdValue) {
            options['id'] = this.idValue;
        }

        if (referer) {
            options['referer'] = referer
        }

        Ajax.request(Routing.generate(route, options), 'html')
            .then((data) => {
                Popup.show(Translator.trans(title ? title : 'popup.approve.title'), data);
            });
    }
}
