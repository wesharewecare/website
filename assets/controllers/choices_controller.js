import { Controller } from 'stimulus';
import Choices from 'choices.js/src/scripts/choices';
import 'choices.js/src/styles/choices.scss';

export default class extends Controller {
    initialize() {
        new Choices(this.element, {
            removeItemButton: true
        });
    }
}
