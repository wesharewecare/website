import { Controller } from 'stimulus';
import Map from '../js/components/map';

export default class extends Controller {
    initialize() {
        const map = Map(this.element.id);
        map.showLocation(JSON.parse(this.element.dataset.location));
    }
}
