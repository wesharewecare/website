import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = ["inputNotEmpty", "submitButton"];

    initialize() {
        this.toggleSubmitButton();
    }

    change() {
        this.toggleSubmitButton();
    }

    toggleSubmitButton() {
        let disabled = false;

        this.inputNotEmptyTargets.forEach(item => {
            const val = item.value;
            if (!val.trim().length) {
                disabled = true;
                return;
            }
        })

        this.submitButtonTarget.disabled = disabled;
    }
}
