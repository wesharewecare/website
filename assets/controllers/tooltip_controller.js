import { Controller } from 'stimulus';
import {Tooltip} from 'bootstrap/dist/js/bootstrap.bundle.min';

export default class extends Controller {
    initialize() {
        new Tooltip(this.element);
    }
}
