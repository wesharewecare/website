import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = ["divPreview"];

    change(event) {
        const fileInput = event.target;
        const divPreviewTarget = this.divPreviewTarget;

        if (fileInput.files && fileInput.files[0]) {
            const reader = new FileReader();
            reader.onload = function (e) {
                divPreviewTarget.classList.remove('empty-file-preview');
                divPreviewTarget.innerHTML = `<embed src="${e.target.result}" class="file-preview"/>`;
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
