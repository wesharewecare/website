import { Controller } from 'stimulus';
import GeoCoding from '../js/utils/Geocoding';

export default class extends Controller {
    initialize() {
        GeoCoding.handle(this.element, this.element.dataset.showMap);
    }
}
