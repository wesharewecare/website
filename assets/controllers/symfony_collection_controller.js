import { Controller } from 'stimulus';
import a2lixLib from '@a2lix/symfony-collection';

export default class extends Controller {
    initialize() {
        a2lixLib.sfCollection.init();
    }
}
