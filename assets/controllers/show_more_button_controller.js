import {Controller} from 'stimulus';

export default class extends Controller {
    static targets = ['content'];

    click(event) {
        event.target.classList.add('d-none');
        event.target.classList.remove('d-block');
        this.contentTarget.classList.remove('d-none');
    }
}
