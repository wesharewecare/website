import 'regenerator-runtime/runtime';
import 'core-js/stable';
import React from 'react';
import {ThreadProvider} from './context/ThreadContext';
import {MercureProvider} from './context/MercureContext';
import {InboxProvider} from './context/InboxContext';
import {UnreadProvider} from './context/UnreadContext';
import {UserProvider} from './context/UserContext';
import Inbox from './components/Inbox/Inbox';
import Thread from './components/Thread/Thread';
import {LayoutProvider} from "./context/LayoutContext";

type Props = {
    user: string;
    newThread: string;
};

const Chat = ({user, newThread}: Props) => (
    <LayoutProvider>
        <UserProvider {...JSON.parse(user)} newThread={...JSON.parse(newThread) || []}>
            <ThreadProvider>
                <MercureProvider>
                    <InboxProvider>
                        <UnreadProvider>
                            <Inbox/>
                            <Thread/>
                        </UnreadProvider>
                    </InboxProvider>
                </MercureProvider>
            </ThreadProvider>
        </UserProvider>
    </LayoutProvider>
);

export default Chat;
