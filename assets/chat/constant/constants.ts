export const MERCURE_URL = process.env.MERCURE_SUBSCRIBE_URL || 'http://localhost/.well-known/mercure';
export const MARK_AS_READ_DELAY = +process.env.MARK_AS_READ_DELAY || 3000;

