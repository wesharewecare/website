import React, {ReactNode, useContext, useEffect} from 'react';

const WIDTH_BREAKPOINT = 650;
const LayoutContext = React.createContext({});

type Context = {
    dimensions: {
        width: number;
        height: number;
    },
    adapted: boolean;
};

type Props = {
    children?: ReactNode;
};

export const LayoutProvider = (props: Props) => {
    const [dimensions, setDimensions] = React.useState({
        height: window.innerHeight,
        width: window.innerWidth
    });
    let t;

    useEffect(() => {

        function handleResize() {
            setDimensions({
                height: window.innerHeight,
                width: window.innerWidth
            });
        }

        window.addEventListener('resize', () => {
            clearTimeout(t);
            t = setTimeout(handleResize, 250);
        });

        handleResize();
    }, []);

    return <LayoutContext.Provider value={{dimensions, adapted: dimensions.width <= WIDTH_BREAKPOINT}}>
        {props.children}
    </LayoutContext.Provider>;
};
export const useLayoutContext = (): Context => useContext(LayoutContext);
