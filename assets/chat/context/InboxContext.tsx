import React, {
    ReactNode, useContext, useEffect, useState
} from 'react';
import useHash, {Hash} from '../hooks/useHash';
import {findById, getJoinedIds} from '../helpers/entity';
import {useThreadContext} from './ThreadContext';
import Thread from '../type/Thread';

const InboxContext = React.createContext({});

type Context = {
    mode: Mode;
    setMode(mode: Mode): void;
    current: Thread;
    setCurrent(thread?: Thread): void;
    isCurrent(thread: Thread): boolean;
};

export const enum Mode {
    NONE = 'none',
    CREATE = 'create',
    VIEW = 'view',
}

type Props = {
    children?: ReactNode;
};

export const InboxProvider = ({children}: Props) => {
    const [mode, setMode] = useState<Mode>(Mode.NONE);
    const [current, setCurrent] = useState<Thread>();
    const {list} = useThreadContext();
    const {hash, setHash} = useHash();

    useEffect(
        () => {
            if (hash === Hash.NEW_THREAD) {
                setMode(Mode.CREATE);

                return;
            }

            setMode(Mode.VIEW);
            setCurrent(findById(list, hash));
        },
        [getJoinedIds(list), hash]
    );

    const value: Context = {
        mode,
        setMode,
        current,
        setCurrent: (thread) => {
            setMode(Mode.VIEW);
            setCurrent(thread);
            if (thread) {
                setHash(thread['@id']);
            } else {
                setHash('');
            }
        },
        isCurrent: (thread) => current && current['@id'] === thread['@id']
    };

    return <InboxContext.Provider value={value}>{children}</InboxContext.Provider>;
};

export const useInboxContext = (): Context => useContext(InboxContext);
