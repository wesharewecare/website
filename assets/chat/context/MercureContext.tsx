import React, {
    ReactNode, useCallback, useContext, useEffect, useState
} from 'react';
import {EventSourcePolyfill} from 'event-source-polyfill';
import {MERCURE_URL} from '../constant/constants';
import {useThreadContext} from './ThreadContext';
import {useUserContext} from './UserContext';
import Thread from '../type/Thread';
import RefreshTopicsMessage from '../type/RefreshTopicsMessage';
import Message, {MessageTypeEnum} from '../type/Message';

const MercureContext = React.createContext({mercure: null});

type Context = {
    mercure?: EventSourcePolyfill;
    subscribeToThread(thread: Thread, subscriber: (message: Message | RefreshTopicsMessage) => void);
};

type Props = {
    children?: ReactNode;
};

export const MercureProvider = ({children}: Props) => {
    const url = new URL(MERCURE_URL);
    const {getTopics} = useThreadContext();
    const {iri} = useUserContext();
    const [mercure, setMercure] = useState();

    // Create Mercure client
    const topics = getTopics();
    useEffect(
        () => {
            url.searchParams.append('topic', `${iri}`);
            if (0 < topics?.length) {
                topics.forEach((topic) => {
                    url.searchParams.append('topic', topic);
                });
                setMercure(new EventSourcePolyfill(url, {withCredentials: true}));
            }
        },
        [iri, topics.join(',')]
    );

    const subscribeToThread = useCallback(
        (thread, subscriber) => () => {
            if (!mercure) {
                return undefined;
            }

            const subscriberDecorator = (event) => {
                const message: Message | RefreshTopicsMessage = JSON.parse(event.data);

                if (MessageTypeEnum.REFRESH_TOPICS === message['@type'] ||
                    (thread && thread['@id'] === (message as Message).thread)
                ) {
                    subscriber.call(this, message);
                }
            };

            mercure.addEventListener('message', subscriberDecorator);

            return () => mercure.removeEventListener('message', subscriberDecorator);
        },
        [mercure]
    );

    const value: Context = {
        mercure,
        subscribeToThread
    };

    return <MercureContext.Provider value={value}>{children}</MercureContext.Provider>;
};

export const useMercureContext = (): Context => useContext(MercureContext);
