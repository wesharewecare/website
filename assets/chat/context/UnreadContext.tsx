import React, {
    ReactNode, useContext, useEffect, useState
} from 'react';
import { useThreadContext } from './ThreadContext';

const UnreadContext = React.createContext({});

type Context = {
    unread: string[];
    setUnread(id: string[]): void;
};

type Props = {
    children?: ReactNode;
};

export const UnreadProvider = ({ children }: Props) => {
    const [unread, setUnread] = useState<string[]>([]);
    const { list } = useThreadContext();

    useEffect(
        () => {
            const u = list.filter((t) => !t.read).map((t) => t['@id']);
            setUnread(u);
        },
        [list]
    );

    const value: Context = {
        unread,
        setUnread
    };

    return <UnreadContext.Provider value={value}>{ children }</UnreadContext.Provider>;
};

export const useUnreadContext = (): Context => useContext(UnreadContext);
