import React, {
    ReactNode, useContext
} from 'react';
import { flattenDeep } from 'lodash';
import Thread from '../type/Thread';
import useQuery from '../hooks/useQuery';
import Routing from '../../js/utils/Routing';

const ThreadContext = React.createContext({});

type Context = {
    loading: boolean;
    list: Thread[];
    getTopics(): string[];
    refresh(): Promise<Thread[]>;
};

type Props = {
    children?: ReactNode;
};

export const ThreadProvider = ({ children }: Props) => {
    const { data, loading, fetch } = useQuery<Thread[]>(
        Routing.generate('messaging_api_get_threads_groups'),
        'GET',
        null,
        [],
        []
    );

    const value: Context = {
        loading,
        list: data,
        getTopics: () => flattenDeep(data.map((t: Thread) => t.topics.map((to) => to['@id']))),
        refresh: fetch
    };

    return <ThreadContext.Provider value={value}>{ children }</ThreadContext.Provider>;
};

export const useThreadContext = (): Context => useContext(ThreadContext);
