import Entity from './Entity';

type Author = Entity & {
    avatar: string;
    firstname: string;
    lastname: string;
}

export default Author;
