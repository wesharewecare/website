import Entity from './Entity';
import Author from './Author';

export const enum MessageTypeEnum {
    REFRESH_TOPICS = 'REFRESH_TOPICS',
    MESSAGE = 'MESSAGE',
}

type Message = Entity & {
    thread: string;
    datetime: number;
    body: string;
    author: Author;
    recipientName: string;
    recipientNameTooltip: string;
    type: MessageTypeEnum.MESSAGE;
}

export default Message;
