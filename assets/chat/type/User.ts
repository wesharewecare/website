type User = {
    email: string;
    fullname: string;
    image: string;
};

export default User;
