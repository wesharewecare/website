import Entity from './Entity';

type Topic = Entity & {
    threadId: string;
};

type Thread = Entity & {
    name: string;
    channelType: string;
    channelTypeTooltip: string;
    icon: string;
    avatar: string;
    lastMessageAt: number;
    topics: Topic[];
    showFooter: boolean;
    replaceFooterForm?: string;
    read: boolean;
    favorite: boolean;
    profileUrl?: string;
};

export default Thread;
