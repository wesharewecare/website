import * as ReactDOM from 'react-dom';
import * as React from 'react';
import Chat from './index';

export function bootstrap(targetNode: HTMLElement): void {
    ReactDOM.render(<Chat user={targetNode.dataset.user} newThread={targetNode.dataset.new_thread}/>, targetNode);
}
