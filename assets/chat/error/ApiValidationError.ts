type Violation = {
    property: string;
    errors: string[];
}

class ApiValidationError extends Error {
    violations = [];

    constructor(message: string, violations: Violation[] = []) {
        super(message);
        this.violations = violations;
    }
}

export default ApiValidationError;
