import { useRef } from 'react';

const useScroll = () => {
    const ref = useRef();

    const scrollToTop = (offset = 0) => {
        ref.current.parentNode.scroll(0, offset);
    };

    const scrollToBottom = (offset = 0) => {
        ref.current.parentNode.scroll(0, ref.current.parentNode.scrollHeight - offset);
    };

    return { ref, scrollToTop, scrollToBottom };
};

export default useScroll;
