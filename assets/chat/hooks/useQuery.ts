import { useState, useEffect } from 'react';
import { noop } from 'lodash';
import fetcher from '../fetcher/fetcher';

type Response<T> = {
    data: T;
    loading: boolean;
    error: Error;
    fetch<T>(): Promise<T>;
};


const useQuery = <T>(url: string, method = 'GET', body = null, defaultValue = null, deps = []): Response<T> => {
    const [data, setData] = useState(defaultValue);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<Error>();

    const fetch = <T>(): Promise<T> => {
        setLoading(true);

        return fetcher(url, method, body)
            .then((res) => {
                setData(res);

                return res;
            })
            .catch((e) => setError(e))
            .finally(() => setLoading(false));
    };

    useEffect(
        () => {
            fetch().catch(noop);
        },
        deps
    );

    return {
        data, loading, error, fetch
    };
};

export default useQuery;
