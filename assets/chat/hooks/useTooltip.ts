import { useEffect, useRef } from 'react';
import $ from 'jquery';

export const enum Placement {
    RIGHT = 'right',
    LEFT = 'left',
    BOTTOM = 'bottom',
    TOP = 'top',
}

const useTooltip = (placement: Placement = Placement.TOP) => {
    const ref = useRef(null);

    useEffect(
        () => {
            if (!ref.current) return undefined;

            $(ref.current).tooltip({ placement });

            return () => $(ref.current).tooltip('dispose');
        },
        [ref?.current?.title]
    );

    return { ref };
};

export default useTooltip;
