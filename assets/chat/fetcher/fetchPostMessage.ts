import Routing from '../../js/utils/Routing';
import fetcher from './fetcher';
import Message from '../type/Message';

export default (id: string, content: string, recipientType?: string): Promise<Message> => {
    const url = Routing.generate('messaging_api_post_thread_messages', { id });
    const body = { content, recipientType };

    return fetcher(url, 'POST', body);
};
