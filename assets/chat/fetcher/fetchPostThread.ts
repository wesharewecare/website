import Routing from '../../js/utils/Routing';
import fetcher from './fetcher';
import Message from '../type/Message';

export default (content: string, recipient: string): Promise<Message> => {
    const url = Routing.generate('messaging_api_post_thread');
    const body = { recipient, content };

    return fetcher(url, 'POST', body);
};
