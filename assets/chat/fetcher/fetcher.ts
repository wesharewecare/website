import { set } from 'lodash';
import Routing from '../../js/utils/Routing';
import ApiAuthenticationError from '../error/ApiAuthenticationError';
import ApiValidationError from '../error/ApiValidationError';

const loginRedirectUrl = Routing.generate('login_modal', { logged_out: 1 });
const loginUrlRegex = new RegExp(`${Routing.generate('login_modal')}$`);

const parse = (response: Response) => {
    if (!response.ok) {
        throw new Error(response.statusText);
    }

    // API should return 403 but actually redirects to login page
    if (response.redirected && loginUrlRegex.test(response.url)) {
        window.location.href = loginRedirectUrl;
    }

    return response.json()
        .then((result) => {
            if (403 === response.status) {
                throw new ApiAuthenticationError('Access denied.');
            }

            if (400 === response.status) {
                throw new ApiValidationError('Validation error.', result.violations);
            }

            return result;
        });
};

export default (url: string, method = 'GET', body = null, abortController?: AbortController) => {
    const options = {
        method,
        headers: { Accept: 'application/json' }
    };

    if (abortController) {
        set(options, 'signal', abortController.signal);
    }

    if (body) {
        set(options, 'body', JSON.stringify(body));
        set(options, 'headers["Content-Type"]', 'application/json');
    }

    return fetch(url, options).then(parse);
};
