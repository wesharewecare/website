import Routing from '../../js/utils/Routing';
import fetcher from './fetcher';
import Message from '../type/Message';

export default (id: string, page: number, abortController?: AbortController): Promise<Message[]> => {
    const url = Routing.generate('messaging_api_get_threads_groups_messages', { id, page });

    return fetcher(url, 'GET', null, abortController);
};
