import React, {useEffect} from 'react';
import Translator from '../../../js/utils/Translator';
import InboxItem from './InboxItem';
import {useThreadContext} from '../../context/ThreadContext';
import Icon from '../Common/Icon';
import {useUserContext} from '../../context/UserContext';
import {Mode, useInboxContext} from '../../context/InboxContext';
import {useMercureContext} from '../../context/MercureContext';
import {useLayoutContext} from "../../context/LayoutContext";

const getNewThreadButton = (isManager: boolean, handleClick:() => void) => {
    if (isManager) {
        return <div>
                    <button type="button" id="new-thread" onClick={handleClick}>
                       <Icon name='edit'/>
                    </button>
               </div>
        ;
    }
};


const Inbox = () => {
    const {list, refresh} = useThreadContext();
    const {setMode, current, mode} = useInboxContext();
    const {iri, isManager, setRecipientClear} = useUserContext();
    const {mercure, subscribeToThread} = useMercureContext();
    const {adapted} = useLayoutContext();

    const handleClick = () => {
        setMode(Mode.CREATE);
        setRecipientClear(true);
    };

    useEffect(
        () => subscribeToThread(null, refresh)(),
        [iri, mercure]
    );

    return (adapted && (current || mode === Mode.CREATE)) ? null :
        (<div className="inbox__left-wrapper text-left" style={{flex: adapted ? '1' : ''}}>
            <div className="inbox__left-head">
                <div>
                    <h4>{Translator.trans('inbox.title')}</h4>
                </div>
                {getNewThreadButton(isManager, handleClick)}
            </div>
            <ul id="threads-list" className="thread-selector">
                {list.map((thread) => <InboxItem key={thread['@id']} thread={thread}/>)}
            </ul>
        </div>);
};

export default Inbox;
