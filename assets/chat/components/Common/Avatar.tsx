import React from 'react';
import Translator from '../../../js/utils/Translator';
import useTooltip, { Placement } from '../../hooks/useTooltip';

type Props = {
    image: string;
    title?: string;
    className?: string;
}

const Avatar = ({ image, title, className }: Props) => {
    const { ref } = useTooltip(Placement.RIGHT);

    return (
        <div className={className || 'photo'}>
            <img src={ image }
                alt={ Translator.trans('user.profile_picture') }
                title={ title }
                ref={ref}
            />
        </div>
    );
};

export default Avatar;
