import React from 'react';

type Props = {
    name: string;
}

const Icon = ({ name }: Props) => <i className={`fas fa-${name}`} aria-hidden="true"></i>;

export default Icon;
