import React, { useEffect } from 'react';
import {Form, useFormikContext} from 'formik';
import ContentField from './ContentField';
import SubmitField from './SubmitField';
import Thread from '../../../type/Thread';

type Props = {
    thread: Thread;
    error: Error;
};

type Values = {
    content: string;
    threadId: string;
};

const CurrentForm = ({ thread, error }: Props) => {
    const {
        isSubmitting, values, setFieldValue, submitForm
    } = useFormikContext<Values>();

    useEffect(
        () => {
            setFieldValue('threadId', thread.topics[0].threadId);
        },
        [thread['@id']]
    );

    return (
        <Form>
            <div className="row col-12">
                <ContentField disabled={isSubmitting} disabledSubmit={isSubmitting || !values.content} onEnter={() => submitForm()}/>
                <SubmitField disabled={isSubmitting || !values.content} error={error}/>
            </div>
        </Form>
    );
};

export default CurrentForm;
