import React from 'react';
import { useField } from 'formik';
import { components } from 'react-select';
import AsyncSelect from 'react-select/async';
import Translator from '../../../../js/utils/Translator';
import fetcher from '../../../fetcher/fetchUserSuggest';
import User from '../../../type/User';
import Avatar from '../../Common/Avatar';

type Props = {
    recipient?: string;
};

const loadOptions = (inputValue: string, callback) => {
    if ('' === inputValue.trim()) {
        return null;
    }

    return fetcher(inputValue)
        .then((response) => callback(response.map((item) => (
            { email: item.email, label: item.fullname, image: item.image }
        ))))
        .catch(() => callback(null));
};

const Option = props => (
    <components.Option {...props}>
        <div className="suggestion row">
            <Avatar image={props.data.image} title={props.data.label} className="small"/>
            <span>{props.data.label}</span>
            <span className="email">{props.data.email}</span>
        </div>
    </components.Option>
);

const NoOptionsMessage = props => (
    <components.NoOptionsMessage {...props} children={Translator.trans('inbox.new_thread.search_user.not_found')}/>
);

const Placeholder = props => (
    <components.Placeholder {...props} className="suggestion__placeholder" />
);

const LoadingMessage = props => (
    <components.LoadingMessage {...props} inputValue={Translator.trans('inbox.new_thread.search_user.loading')} />
);

const RecipientUserField = ({recipient}: Props) => {
    const [field, meta, helper] = useField('recipient');

    if (recipient) {
        return (
            <p className="thread__user pt-2">{recipient}</p>
        );
    }

    return (
        <AsyncSelect
            loadOptions={loadOptions}
            autoFocus
            cacheOptions
            defaultOptions
            onChange={(user?: User) => helper.setValue(user?.email)}
            components={{ Option, NoOptionsMessage, Placeholder, LoadingMessage }}
            placeholder={Translator.trans('inbox.new_thread.search_user')}
        />
    );
};

export default RecipientUserField;
