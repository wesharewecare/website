import React from 'react';
import Icon from '../../Common/Icon';

type Props = {
    disabled: boolean;
    error?: Error;
};

const SubmitField = ({ disabled, error }: Props) => (
    <button
        className={`message-form__submit ${error && 'error'}`}
        type="submit"
        disabled={disabled}
        title={error?.message}
    >
        <Icon name="paper-plane"/>
    </button>
);

export default SubmitField;
