import React, {useEffect} from 'react';
import {Form, useFormikContext} from 'formik';
import ContentField from './ContentField';
import SubmitField from './SubmitField';
import RecipientUserField from './RecipientUserField';
import {useUserContext} from '../../../context/UserContext';
import {useLayoutContext} from "../../../context/LayoutContext";
import {useInboxContext} from "../../../context/InboxContext";

type Values = {
    content: string;
    recipient?: string;
};

const NewForm = () => {
    const {isManager, newThread, recipientClear} = useUserContext();
    const {
        isSubmitting, values, setFieldValue, submitForm
    } = useFormikContext<Values>();

    useEffect(
        () => {
            if (isManager) {
                if (!recipientClear && 'recipient' in newThread) {
                    setRecipient(newThread['recipient']['email']);
                }
            } else if (1 === newThread.length) {
                setRecipient(newThread[0]);
            }
        },
        [newThread.length]
    );

    const setRecipient = (recipient) => {
        setFieldValue('recipient', recipient);
    }

    const {adapted} = useLayoutContext();
    const {setCurrent} = useInboxContext();
    const backToInbox = () => {
        setCurrent(null);
    };

    return (
        <Form>
            <div className="thread__header p-2">
                {adapted && (<div className="back">
                    <i onClick={() => backToInbox()} className="fas fa-arrow-alt-circle-left" aria-hidden="true"/>
                </div>)}
                <RecipientUserField recipient={!recipientClear && 'recipient' in newThread ? newThread['recipient']['fullname'] : null}/>
            </div>

            <div className="thread__container mt-4"></div>

            <div className="thread__form message-form">
                <ContentField disabled={isSubmitting}
                              disabledSubmit={isSubmitting || !values.content || !values.recipient}
                              onEnter={() => submitForm()}/>
                <SubmitField disabled={isSubmitting || !values.content || !values.recipient}/>
            </div>
        </Form>
    );
};

export default NewForm;
