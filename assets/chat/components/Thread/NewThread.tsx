import React from 'react';
import {Formik} from 'formik';
import {useThreadContext} from '../../context/ThreadContext';
import fetcher from '../../fetcher/fetchPostThread';
import NewForm from './Form/NewForm';
import {useInboxContext} from '../../context/InboxContext';
import {findById} from '../../helpers/entity';

type Values = {
    content: string;
    recipient?: string;
};

const NewThread = () => {
    const {refresh} = useThreadContext();
    const {setCurrent} = useInboxContext();

    const handleSubmit = async ({content, recipient}: Values, {resetForm, setErrors}) => {
        try {
            const message = await fetcher(content, recipient);

            resetForm();
            setErrors([]);

            const threads = await refresh();

            setCurrent(findById(threads, message.thread));
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="current-thread text-left">
            <Formik initialValues={{content: '', recipient: null}} onSubmit={handleSubmit}>
                {() => <NewForm/>}
            </Formik>
        </div>
    );
};

export default NewThread;
