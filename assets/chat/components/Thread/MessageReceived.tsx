import React from 'react';
import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import Translator from '../../../js/utils/Translator';
import Message from '../../type/Message';
import useTooltip, { Placement } from '../../hooks/useTooltip';
import Avatar from '../Common/Avatar';
import MessageTags from './MessageTags';

dayjs.extend(localizedFormat);

type Props = {
    message: Message;
}

const setInnerHtml = (msg: string) => ({ __html: msg });

const MessageReceived = ({ message }: Props) => {
    const { ref } = useTooltip(Placement.RIGHT);

    return (
        <div className="message message--incoming">
            <div className="message__img">
                <Avatar image={message.author.avatar} title={`${message.author.firstname} ${message.author.lastname}`}/>
            </div>
            <div className="message__received">
                <div ref={ref} title={dayjs(message.datetime).locale(Translator.locale).format('LTS')}>
                    <p dangerouslySetInnerHTML={setInnerHtml(message.body)} />
                </div>
            </div>
            <MessageTags message={message}/>
        </div>
    );
};

export default MessageReceived;
