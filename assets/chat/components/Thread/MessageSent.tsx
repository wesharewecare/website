import React from 'react';
import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import Translator from '../../../js/utils/Translator';
import Message from '../../type/Message';
import useTooltip, { Placement } from '../../hooks/useTooltip';
import MessageTags from './MessageTags';

dayjs.extend(localizedFormat);

type Props = {
    message: Message;
}

const setInnerHtml = (msg: string) => ({ __html: msg });

const MessageSent = ({ message }: Props) => {
    const { ref } = useTooltip(Placement.LEFT);

    return (
        <div className="message message--outgoing">
            <div className="message__sent">
                <div ref={ref} title={ dayjs(message.datetime).locale(Translator.locale).format('LTS') }>
                    <p dangerouslySetInnerHTML={setInnerHtml(message.body)} />
                </div>
            </div>
            <MessageTags message={message} />
        </div>
    );
};

export default MessageSent;
