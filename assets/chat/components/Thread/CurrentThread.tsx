import React, {useCallback, useState} from 'react';
import {Formik, FormikHelpers} from 'formik';
import Translator from '../../../js/utils/Translator';
import ThreadMessages from './ThreadMessages';
import fetcher from '../../fetcher/fetchPostMessage';
import Thread from '../../type/Thread';
import useTooltip, {Placement} from '../../hooks/useTooltip';
import CurrentForm from './Form/CurrentForm';
import {useLayoutContext} from "../../context/LayoutContext";
import {useInboxContext} from "../../context/InboxContext";

type Props = {
    thread: Thread;
};

type Values = {
    content: string;
    threadId: string;
};

const getThreadName = (thread: Thread) => {
    if (thread.profileUrl) {
        return <p className="thread__user pt-2"
                  onClick={() => window.location.href = thread.profileUrl}>{thread.name}</p>;
    }
    return <p className="thread__user pt-2">{thread.name}</p>;
};

const getThreadForm = (thread: Thread, handleSubmit:() => void, error: Error) => {
    if (thread.replaceFooterForm) {
        return <div dangerouslySetInnerHTML={{__html: thread.replaceFooterForm}}/>;
    }

    return <Formik initialValues={{content: '', threadId: ''}} onSubmit={handleSubmit}>
                {() => <CurrentForm thread={thread} error={error}/>}
           </Formik>
    ;
};

const CurrentThread = ({thread}: Props) => {
    const [error, setError] = useState<Error>();
    const handleSubmit = useCallback(
        ({content, threadId}: Values, {setSubmitting, setFieldValue, setErrors}: FormikHelpers<Values>) => {
            if (content && threadId) {
                fetcher(threadId, content)
                    .then(() => setFieldValue('content', ''))
                    .then(() => setErrors({}))
                    .catch((e) => setError(e))
                    .finally(() => setSubmitting(false));
            }
        },
        [thread['@id']]
    );

    const {ref} = useTooltip(Placement.BOTTOM);
    const {adapted} = useLayoutContext();
    const { setCurrent} = useInboxContext();
    const backToInbox = ()=> {
        setCurrent(null);
    };

    return (
        <div className="current-thread text-left">
            <div className="thread__header" data-cy="thread-header">
                {adapted && (<div className="back">
                    <i onClick={() => backToInbox()} className="fas fa-arrow-alt-circle-left" aria-hidden="true"/>
                </div>)}
                <div className="content">
                    {getThreadName(thread)}
                    <span className="thread__type" title={thread.channelTypeTooltip} ref={ref}>
                        {Translator.trans(`inbox.channel.type.${thread.channelType}`)}
                    </span>
                </div>
            </div>

            {<ThreadMessages thread={thread}/>}

            {thread.showFooter && 0 < thread.topics.length &&
                <div className="thread__form message-form">
                    {getThreadForm(thread, handleSubmit, error)}
                </div>
            }
        </div>
    );
};

export default CurrentThread;
