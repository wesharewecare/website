import React from 'react';
import Message from '../../type/Message';
import { useUserContext } from '../../context/UserContext';
import MessageSent from './MessageSent';
import MessageReceived from './MessageReceived';

type Props = {
    key?: string;
    message: Message;
}

const Message = ({ message }: Props) => {
    const { id } = useUserContext();

    if (id === message.author['@id']) {
        return <MessageSent message={message} />;
    }

    return <MessageReceived message={message} />;
};

export default Message;
