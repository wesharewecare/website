import React from 'react';
import Message from '../../type/Message';
import useTooltip, { Placement } from '../../hooks/useTooltip';

type Props = {
    message: Message;
};

const MessageTags = ({ message }: Props) => {
    if (0 === message.recipientName?.length) {
        return null;
    }

    const { ref } = useTooltip(Placement.TOP);

    return (
        <div className="row tags">
            <p className="col-auto" title={message.recipientNameTooltip} ref={ref}>{message.recipientName}</p>
        </div>
    );
};

export default MessageTags;
