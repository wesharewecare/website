import Tabulator from './utils/Tabulator';
import Translator from './utils/Translator';

document.addEventListener('DOMContentLoaded', () => {
    const tabulator = document.getElementById('tabulator');
    const types = JSON.parse(tabulator.dataset.types);
    const booleans = JSON.parse(tabulator.dataset.boolean);
    const filters = Object.entries(JSON.parse(tabulator.dataset.filters));
    const filtersToApply = [];

    filters.forEach(([filter, value]) => {
        let type;

        switch (filter) {
        default:
            type = 'like';
            break;
        }

        filtersToApply.push({ field: filter, type, value });
    });

    const columns = [
        { field: 'id', visible: false },
        { title: Translator.trans('association.name'), field: 'name', headerFilter: true },
        { title: Translator.trans('association.website'), field: 'website', headerFilter: true },
        { title: Translator.trans('city'), field: 'city', headerFilter: true },
        { title: Translator.trans('country'), field: 'country', headerFilter: true },
        { title: Translator.trans('association.phone_number'), field: 'phoneNumber', headerSort: false },
        {
            title: Translator.trans('association.type'),
            field: 'type',
            headerFilter: 'select',
            headerFilterParams: { values: types },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('association.range_radius'),
            field: 'radiusRange',
            headerFilter: 'number',
            headerFilterPlaceholder: 'at least...',
            headerFilterFunc: '>='
        },
        {
            title: Translator.trans('association.activated'),
            field: 'activated',
            headerFilter: 'select',
            headerFilterParams: { values: booleans },
            headerFilterFunc: '='
        },
        { title: Translator.trans('created_at'), field: 'createdAt' },
        {
            title: Translator.trans('actions'), field: 'actions', formatter: 'html', headerSort: false
        }
    ];

    Tabulator.init(tabulator, {
        autoColumnsDefinitions: columns,
        ajaxURL: Routing.generate('association_list_data'),
        initialFilter: filtersToApply,
        initialHeaderFilter: filtersToApply
    });
});
