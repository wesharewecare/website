import Tabulator from './utils/Tabulator';
import Translator from './utils/Translator';
import '../sass/pages/event.scss';

document.addEventListener('DOMContentLoaded', () => {
    const tabulator = document.getElementById('tabulator');
    const listParticipantButton = document.getElementById('nav-list-tab');
    const copyContactInfoButton = document.getElementById('copy-contact-information');

    if (tabulator && listParticipantButton) {
        const roles = JSON.parse(tabulator.dataset.roles);
        const booleans = JSON.parse(tabulator.dataset.boolean);
        const genders = JSON.parse(tabulator.dataset.genders);
        let isTabulatorInitialized = false;

        const columns = [
            { field: 'id', visible: false },
            { title: Translator.trans('user.firstname'), field: 'firstname', headerFilter: true },
            { title: Translator.trans('user.lastname'), field: 'lastname', headerFilter: true },
            { title: Translator.trans('user.email'), field: 'email', headerFilter: true },
            { title: Translator.trans('user.phone_number'), field: 'phoneNumber', headerSort: false },
            { title: Translator.trans('user.city'), field: 'city', headerFilter: true },
            { title: Translator.trans('user.age'), field: 'age', headerFilter: true },
            {
                title: Translator.trans('user.gender'),
                field: 'gender',
                headerFilter: 'select',
                headerFilterParams: { values: genders },
                headerFilterFunc: '='
            },
            { field: 'isApproved', visible: false },
            {
                title: Translator.trans('user.roles'),
                field: 'roles',
                headerFilter: 'select',
                headerFilterParams: { values: roles },
                headerFilterFunc: 'like'
            },
            {
                title: Translator.trans('user.nb_events'),
                field: 'nbEvents',
                headerSort: false
            },
            {
                title: Translator.trans('user.homesharing'),
                field: 'showHomesharing',
                headerFilter: 'select',
                headerFilterParams: { values: booleans },
                headerFilterFunc: '='
            },
            {
                title: Translator.trans('created_at'),
                field: 'createdAt'
            },
            { field: 'association', visible: false },
            {
                title: Translator.trans('actions'),
                field: 'actions',
                formatter: 'html',
                headerSort: false,
                download: false
            }
        ];

        listParticipantButton.addEventListener('click', () => {
            if (!isTabulatorInitialized) {
                isTabulatorInitialized = true;

                const table = Tabulator.init(tabulator, {
                    autoColumnsDefinitions: columns,
                    ajaxURL: Routing.generate('user_list_data'),
                    ajaxParams: { event: tabulator.dataset.eventId }
                });

                document.getElementById('download-csv').addEventListener('click', () => {
                    table.download('csv', 'participants.csv');
                });
            }
        });
    }

    if (copyContactInfoButton) {
        copyContactInfoButton.addEventListener('click', function () {
            const data = { ...this.dataset };
            Object.keys(data).forEach((key) => {
                document.querySelector(`[id='event_${key}' i]`).value = data[key];
            });
        });
    }
});
