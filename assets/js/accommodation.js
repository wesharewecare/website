import Splide from '@splidejs/splide';
import '@splidejs/splide/dist/css/splide.min.css';
import '../sass/pages/accommodation.scss';

document.addEventListener('DOMContentLoaded', () => {
    if (null !== document.getElementById('primary-slider')) {
        const primarySlider = new Splide('#primary-slider', {
            rewind: true,
            type: 'fade',
            fixedHeight: '240px',
            fixedWidth: '400px',
            pagination: false,
            arrows: false,
            cover: true
        });

        if (null !== document.getElementById('secondary-slider')) {
            const secondarySlider = new Splide('#secondary-slider', {
                rewind: true,
                fixedWidth: 100,
                height: 60,
                gap: 10,
                cover: true,
                isNavigation: true,
                breakpoints: {
                    600: {
                        fixedWidth: 66,
                        height: 40
                    }
                }
            }).mount();

            primarySlider.sync(secondarySlider).mount();
        } else {
            primarySlider.mount();
        }
    }
});
