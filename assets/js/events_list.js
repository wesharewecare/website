import Tabulator from './utils/Tabulator';
import Translator from './utils/Translator';

document.addEventListener('DOMContentLoaded', () => {
    const tabulator = document.getElementById('tabulator');
    const status = JSON.parse(tabulator.dataset.status);
    const associations = JSON.parse(tabulator.dataset.associations);

    const columns = [
        { field: 'id', visible: false },
        {
            title: Translator.trans('event.name'), field: 'name', headerFilter: true, maxWidth: 300
        },
        { title: Translator.trans('event.description'), field: 'description', maxWidth: 300 },
        { title: Translator.trans('event.date'), field: 'date' },
        { title: Translator.trans('city'), field: 'city', headerFilter: true },
        { title: Translator.trans('country'), field: 'country', headerFilter: true },
        {
            title: Translator.trans('event.status'),
            field: 'status',
            headerFilter: 'select',
            headerFilterParams: { values: status },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('event.association'),
            field: 'association',
            headerFilter: 'select',
            headerFilterParams: { values: associations },
            headerFilterFunc: '='
        },
        { title: Translator.trans('event.participants'), field: 'nbParticipants', headerSort: false },
        {
            title: Translator.trans('actions'), field: 'actions', formatter: 'html', headerSort: false
        }
    ];

    Tabulator.init(tabulator, {
        autoColumnsDefinitions: columns,
        ajaxURL: Routing.generate('event_list_data')
    });
});
