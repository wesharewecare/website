import Tabulator from './utils/Tabulator';
import Translator from './utils/Translator';

import '../sass/pages/lists.scss';

document.addEventListener('DOMContentLoaded', () => {
    const tabulator = document.getElementById('tabulator');
    const roles = JSON.parse(tabulator.dataset.roles);
    const associations = JSON.parse(tabulator.dataset.associations);
    const booleans = JSON.parse(tabulator.dataset.boolean);
    const genders = JSON.parse(tabulator.dataset.genders);
    const filters = Object.entries(JSON.parse(tabulator.dataset.filters));
    const filtersToApply = [];

    filters.forEach(([filter, value]) => {
        let type;

        switch (filter) {
        default:
            type = 'like';
            break;
        }

        filtersToApply.push({ field: filter, type, value });
    });

    const columns = [
        { field: 'id', visible: false },
        { title: Translator.trans('user.firstname'), field: 'firstname', headerFilter: true },
        { title: Translator.trans('user.lastname'), field: 'lastname', headerFilter: true },
        { title: Translator.trans('user.email'), field: 'email', headerFilter: true },
        { title: Translator.trans('user.phone_number'), field: 'phoneNumber', headerSort: false },
        { title: Translator.trans('user.city'), field: 'city', headerFilter: true },
        { title: Translator.trans('user.age'), field: 'age', headerFilter: true },
        {
            title: Translator.trans('user.gender'),
            field: 'gender',
            headerFilter: 'select',
            headerFilterParams: { values: genders, multiselect: true },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('user.roles'),
            field: 'roles',
            headerFilter: 'select',
            headerFilterParams: { values: roles, multiselect: true },
            headerFilterFunc: 'like'
        },
        {
            title: Translator.trans('user.association'),
            field: 'association',
            headerFilter: 'select',
            headerFilterParams: { values: associations, multiselect: true },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('user.is_approved'),
            field: 'isApproved',
            headerFilter: 'select',
            headerFilterParams: { values: booleans },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('user.homesharing'),
            field: 'showHomesharing',
            headerFilter: 'select',
            headerFilterParams: { values: booleans },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('user.nb_events'), field: 'nbEvents', headerSort: false, maxWidth: 120
        },
        { title: Translator.trans('created_at'), field: 'createdAt' },
        {
            title: Translator.trans('actions'), field: 'actions', formatter: 'html', headerSort: false, download: false
        }
    ];

    const table = Tabulator.init(tabulator, {
        autoColumnsDefinitions: columns,
        ajaxURL: Routing.generate('user_list_data'),
        initialFilter: filtersToApply,
        initialHeaderFilter: filtersToApply
    });

    document.getElementById('download-csv').addEventListener('click', () => {
        table.download('csv', 'users.csv');
    });
});
