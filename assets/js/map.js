import '../sass/pages/map.scss';
import Map from './components/map';
import Ajax from './utils/Ajax';
import Popup from './utils/Popup';
import Translator from './utils/Translator';

document.addEventListener('DOMContentLoaded', () => {
    const MAP_ID = 'map';
    const map = Map(MAP_ID);
    const { location, zoom, maxzoom } = document.getElementById(MAP_ID).dataset;
    const { isConnected } = document.getElementById('map-container').dataset;
    const typeSelector = document.getElementById('card-type');
    const typeSelectorValues = [...typeSelector.options].map((option) => option.value);
    let type = 'all';
    const params = {};

    const cardOnClick = (data) => {
        let redirectPath;
        let connectedIsRequired = true;

        switch (data.type) {
        case 'accommodation':
            redirectPath = Routing.generate('user_show', { id: data.id });
            break;
        case 'association':
            redirectPath = Routing.generate('association_show', { id: data.id });
            break;
        case 'event':
            redirectPath = Routing.generate('event_show', { id: data.id });
            connectedIsRequired = false;
            break;
        default:
            redirectPath = null;
            break;
        }

        if (redirectPath) {
            if (!connectedIsRequired || isConnected) {
                window.location.href = redirectPath;
            } else {
                Ajax.request(Routing.generate('login_modal', { referer: redirectPath }), 'html')
                    .then((content) => {
                        Popup.show(Translator.trans('map.login.title'), content);
                    });
            }
        }
    };

    const cardFactory = (data, showLinkButton = false) => {
        const card = document.createElement('div');
        const infos = document.createElement('div');
        card.classList.add('text-center');
        infos.classList.add('information');

        card.innerHTML =
            `<div class="photo">
                <img src="${data.image}"/>
            </div>`;
        infos.innerHTML = `<div class="info name">${data.name}</div>`;

        if (null !== data.description) {
            infos.innerHTML += `<div class="info">${data.description}</div>`;
        }

        infos.innerHTML += `<div class="info">${data.address}</div>`;

        if (null !== data.date) {
            infos.innerHTML += `<div class="info">${data.date}</div>`;
        }

        if (null !== data.status) {
            infos.innerHTML += `<div class="info">${data.status}</div>`;
        }

        card.appendChild(infos);

        if (showLinkButton) {
            const button = document.createElement('button');
            button.classList.add('btn');
            button.classList.add('btn-primary');
            button.classList.add('mt-3');
            button.innerHTML = Translator.trans(`map.show_${data.type}.button`);
            button.addEventListener('click', () => cardOnClick(data));

            card.appendChild(button);
        }

        return card;
    };

    const loadData = (dataType) => {
        if (typeSelectorValues.includes(dataType) && ('all' === type || dataType === type)) {
            map.fromURL(
                Routing.generate(`map_api_get_${dataType}s`, params),
                dataType,
                cardFactory,
                cardOnClick
            );
        }
    };

    const loadAll = () => {
        loadData('accommodation');
        loadData('association');
        loadData('event');
    };

    const reloadData = () => {
        map.clearCards();
        map.clearMarkers();
        loadAll();
    };

    const onMapEvent = (center, ev) => {
        params.lat = center.lat;
        params.lng = center.lng;
        params.zoom = ev.target._zoom;

        reloadData();
    };

    const initMap = () => {
        map.init(location, zoom, maxzoom);
        loadAll();

        if (isConnected) {
            map.event('dragend', (center, ev) => onMapEvent(center, ev));
            map.event('zoomend', (center, ev) => onMapEvent(center, ev));
        }
    };

    typeSelector.addEventListener('change', (event) => {
        type = event.target.value;
        reloadData();
    });

    initMap();
});
