import '../sass/pages/participate.scss';
import Ajax from './utils/Ajax';

document.addEventListener('DOMContentLoaded', () => {
    const latForm = document.getElementById('location_form_latitude');
    const lngForm = document.getElementById('location_form_longitude');
    const associations = document.getElementById('association-list');
    const nonUsingResultsOnly = document.getElementById('non-using-results-only');
    const usingResults = document.getElementById('using-results');
    const noResults = document.getElementById('no-results');
    const params = {};

    latForm.onchange = function () {
        params.lat = latForm.value ? latForm.value : '';
        reloadAssociationsList();
    };
    lngForm.onchange = function () {
        params.lng = lngForm.value ? lngForm.value : '';
        reloadAssociationsList();
    };

    const reloadAssociationsList = () => {
        if (params.lat && params.lng) {
            associations.innerHTML = '';
            nonUsingResultsOnly.style.display = 'none';
            usingResults.style.display = 'none';
            noResults.style.display = 'none';

            Ajax.request(Routing.generate('map_api_get_associations_from_location', params), 'json')
                .then((data) => {
                    let atLeastOneUsesPlatform = false;
                    if (1 <= data.length) {
                        // There is at least one result
                        data.forEach((current) => {
                            const item = document.createElement('li');
                            let content =
                                `<div class="row">
                                    <div class="col-2">
                                        <img src="${current.logo}" alt="Logo"/><br>
                                    </div>
                                    <div class="col-10">
                                        <strong title="asso #${current.id}">${current.name}</strong> - ${current.city}`;
                            if (null !== current.website) {
                                content +=
                                    `<br><a href="${current.website}" target="_blank">${current.website}</a>`;
                            }
                            content +=
                                    `</div>
                                </div>`;
                            item.innerHTML = content;
                            associations.appendChild(item);

                            // Check if at least one association uses the platform
                            if (current.platformUse && 0 < current.platformUse) {
                                atLeastOneUsesPlatform = true;
                            }
                        });
                        if (true === atLeastOneUsesPlatform) {
                            // At least one association uses the platform
                            usingResults.style.display = 'block';
                        } else {
                            // No association found uses the platform
                            nonUsingResultsOnly.style.display = 'block';
                        }
                    } else {
                        // No result found
                        noResults.style.display = 'block';
                    }
                });
        }
    };
});
