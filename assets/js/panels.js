import '../sass/pages/panels.scss';
import Cookie from './utils/Cookie';

document.addEventListener('DOMContentLoaded', () => {
    const canCollapse = Array.from(document.querySelectorAll('.can-collapse'));
    const pStates = {};
    const collapsedClass = 'collapsed';
    const limitXCollapse = 1280;

    function getCollapsePanelCookieName(collapsePanel) {
        return `collapse_panel_${collapsePanel.dataset.collapseMenu}`;
    }

    function setPanelState(collapsePanel, index, optState) {
        const state = optState !== undefined ? optState : !pStates[index];
        pStates[index] = state;
        if (state) {
            collapsePanel.classList.add(collapsedClass);
        } else {
            collapsePanel.classList.remove(collapsedClass);
        }

        Cookie.setCookie(getCollapsePanelCookieName(collapsePanel), state);
    }

    function initCollapses() {
        canCollapse.forEach((collapsePanel, index) => {
            const cookie = Cookie.getCookie(getCollapsePanelCookieName(collapsePanel));

            if (cookie) {
                setPanelState(collapsePanel, index, true);
            }
        });
    }

    function handleCollapses() {
        canCollapse.forEach((collapsePanel, index) => {
            pStates[index] = collapsePanel.classList.contains(collapsedClass);
            const trigger = collapsePanel.querySelector(
                '*[data-collapse-trigger]'
            );
            if (trigger) {
                trigger.addEventListener('click', () => {
                    setPanelState(collapsePanel, index);
                });
            }
        });
    }

    function handleResponsiveContainers() {
        let wDb;
        const limitX = 992;
        const container = document.querySelector(
            '*[data-responsive-container]'
        );
        const fixed = Array.from(
            document.querySelectorAll('*[data-responsive-fixed]')
        );

        if (container && fixed.length) {
            let paddingHeight = 0;
            const applyContainerPadding = () => {
                container.style.paddingBottom = `${paddingHeight}px`;
            };
            const computePaddingHeight = () => {
                paddingHeight = fixed.reduce(
                    (h, e) => h + e.getBoundingClientRect().height,
                    0
                );
            };

            const render = () => {
                const wWidth = window.innerWidth;
                if (wWidth < limitX) {
                    computePaddingHeight();
                } else {
                    paddingHeight = 0;
                }

                if (wWidth < limitXCollapse) {
                    canCollapse.forEach((collapsePanel, index) => {
                        setPanelState(collapsePanel, index, true);
                    });
                }
                applyContainerPadding();
            };

            render();
            window.addEventListener('resize', () => {
                clearTimeout(wDb);
                wDb = setTimeout(() => render(), 16);
            });
        }
    }

    initCollapses();
    handleCollapses();
    handleResponsiveContainers();
});
