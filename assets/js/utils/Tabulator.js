import TabulatorConstructor from 'tabulator-tables';
import 'tabulator-tables/dist/css/bootstrap/tabulator_bootstrap4.min.css';
import { assign } from 'lodash';

const Tabulator = () => {
    function init(element, options) {
        return new TabulatorConstructor(element, assign(
            {
                layout: 'fitDataTable',
                autoColumns: true,
                ajaxFiltering: true,
                ajaxSorting: true,
                pagination: 'remote',
                paginationSize: 5,
                paginationSizeSelector: [5, 10, 25, 50, 100, 500],
                minHeight: '110px',
                ajaxLoaderLoading: '<div class="loader"><span></span></div>',
                rowFormatter(row) {
                    const data = row.getData();

                    if ('id' in data) {
                        row.getElement().setAttribute('data-controller', 'popup-on-click');
                        row.getElement().setAttribute('data-popup-on-click-id-value', data.id);
                    }
                }
                /* langs: {
                    default: {
                        "pagination":{
                            "page_size":"Page Size test",
                            "page_title":"Show Page",
                            "first":"First",
                            "first_title":"First Page",
                            "last":"Last",
                            "last_title":"Last Page",
                            "prev":"Prev",
                            "prev_title":"Prev Page",
                            "next":"Next",
                            "next_title":"Next Page",
                            "all":"All",
                        },
                    }
                } */
            },
            options
        ));
    }

    return {
        init: (element, options) => init(element, options)
    };
};

export default Tabulator();
