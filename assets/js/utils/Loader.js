const Loader = () => {
    let loader = document.querySelector('.loader');

    function start(elemToAppend) {
        if (null === loader) {
            const loaderHTML = document.createElement('div');
            loaderHTML.classList.add('loader');
            loaderHTML.innerHTML = '<span></span>';

            if ('undefined' !== typeof elemToAppend) {
                elemToAppend.appendChild(loaderHTML);
            } else {
                document.body.appendChild(loaderHTML);
            }
            loader = document.querySelector('.loader');
        }

        loader.style.display = 'block';
    }

    function stop() {
        loader.style.display = 'none';
    }

    return {
        start: (elemToAppend) => start(elemToAppend),
        stop: () => stop()
    };
};

export default Loader();
