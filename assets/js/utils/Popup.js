const Popup = () => {
    const popup = document.getElementById('popup');
    const titleElement = popup.getElementsByClassName('modal-title')[0];
    const body = popup.getElementsByClassName('modal-body')[0];
    const closeButton = popup.getElementsByClassName('modal-close')[0];
    closeButton.addEventListener('click', () => closeModal());

    window.onclick = function (event) {
        if (event.target === popup) {
            closeModal();
        }
    };

    function show(title, content, showCloseButton = true) {
        if (!showCloseButton) {
            closeButton.style.display = 'none';
        } else {
            closeButton.style.display = 'block';
        }

        titleElement.innerHTML = title;
        body.innerHTML = '';

        if (content instanceof HTMLElement) {
            body.appendChild(content);
        } else {
            body.innerHTML = content;
        }

        body.getElementsByClassName('modal-close').forEach((button) => {
            button.addEventListener('click', () => closeModal());
        });

        openModal();
    }

    function openModal() {
        popup.style.display = 'block';
        popup.className += 'show';
    }

    function closeModal() {
        popup.style.display = 'none';
        popup.className += popup.className.replace('show', '');
    }

    return {
        show: (title, content, showCloseButton) => show(title, content, showCloseButton),
        openModal: () => openModal(),
        closeModal: () => closeModal()
    };
};

export default Popup();
