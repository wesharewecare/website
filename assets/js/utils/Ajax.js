import Flashbag from './Flashbag';
import Loader from './Loader';

const Ajax = () => {
    async function _send(url, method, data, returnType) {
        Loader.start();

        const init = {
            method
        };

        if ('POST' === method) {
            init.body = data;
        }

        return fetch(url, init)
            .then((response) => {
                Loader.stop();

                switch (returnType) {
                case 'json':
                    return response.json();
                case 'html':
                    return response.text();
                default:
                    return response;
                }
            }).catch(() => {
                Loader.stop();
                Flashbag.show('Error. Please try again.', 'error');
            });
    }

    return {
        request: (url, returnType) => _send(url, 'GET', {}, returnType),
        post: (url, data, returnType) => _send(url, 'POST', data, returnType)
    };
};

export default Ajax();
