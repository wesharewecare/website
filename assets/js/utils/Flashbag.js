import Const from './Const';

const Flashbag = () => {
    const $flashbags = $('#flashbags');
    let id = 1;

    function _getTemplate(text, type, button) {
        let icon;
        switch (type) {
        case 'info':
            icon = 'info-circle';
            break;
        case 'error':
            icon = 'times-circle';
            break;
        case 'warning':
            icon = 'exclamation-circle';
            break;
        case 'success':
            icon = 'check-circle';
            break;
        default:
            icon = 'info-circle';
            break;
        }

        button = 'undefined' !== typeof button || null === button ? button : '';

        const flashbagType = $(
            `<div id="flashbag-${id}" class="flashbag ${type}"></div>`
        );

        $(flashbagType).append(
            $(
                `<p><i class="fas fa-${icon}"></i> ${text}</p>
                 <div class="buttons">${button}</div>
                 <a class="dismiss"><i class="fa fa-times"></i></a>`
            )
        );

        if (button) {
            $(flashbagType).addClass('flashbag__button');
        }

        return {
            html: flashbagType,
            id: `#flashbag-${id}`
        };
    }

    function show(text, type, button, timing) {
        const flashbagTemplate = _getTemplate(text, type, button);
        $flashbags.append(flashbagTemplate.html);
        $(flashbagTemplate.id).slideDown(100);

        $('.dismiss').on('click', function () {
            $(this).parent().slideUp(100);
        });

        if ('undefined' === typeof timing) {
            timing = Const.flashbagTimeout;
        }

        setTimeout(() => {
            $(flashbagTemplate.id).slideUp(100);
        }, timing);

        id += 1;

        $(window).trigger('new_element', $(flashbagTemplate.id));

        return $(flashbagTemplate.id);
    }

    return {
        show: (text, type, button, timing) => show(text, type, button, timing)
    };
};

export default Flashbag();
