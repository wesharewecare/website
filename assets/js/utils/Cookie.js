const Cookie = () => {
    function setCookie(name, value, days) {
        let expires = '';
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = `; expires=${date.toUTCString()}`;
        }
        document.cookie = `${name}=${value || ''}${expires}; path=/`;
    }

    function getCookie(name) {
        const nameEQ = `${name}=`;
        const ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i += 1) {
            let c = ca[i];
            while (' ' === c.charAt(0)) c = c.substring(1, c.length);
            if (0 === c.indexOf(nameEQ)) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    }

    function eraseCookie(name) {
        document.cookie = `${name}=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
    }

    return {
        setCookie: (name, value, days) => setCookie(name, value, days),
        getCookie: (name) => getCookie(name),
        eraseCookie: (name) => eraseCookie(name)
    };
};

export default Cookie();
