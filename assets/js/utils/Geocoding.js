import AutoComplete from '@tarekraafat/autocomplete.js';
import Ajax from './Ajax';
import Map from '../components/map';

import '@tarekraafat/autocomplete.js/dist/css/autoComplete.02.css';

const API_KEY = process.env.HERE_API_KEY;
const API_MAX_RESULTS = parseInt(process.env.HERE_AUTOCOMPLETE_MAX_RESULTS, 10);

const API_URL = 'https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?' +
                `apiKey=${API_KEY}&maxResults=${API_MAX_RESULTS}`;
const API_LOOKUP_URL = `https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=${API_KEY}`;

const DATA_ATTRIBUTES = {
    FILL: {
        district: 'data-geocode-fill-district',
        street: 'data-geocode-fill-street',
        country: 'data-geocode-fill-country',
        countryCode: 'data-geocode-fill-country_code',
        latitude: 'data-geocode-fill-latitude',
        longitude: 'data-geocode-fill-longitude',
        postalCode: 'data-geocode-fill-postalCode',
        city: 'data-geocode-fill-city'
    }
};

const HTTP_REQ_DELAY = 700;

const GeoCoding = () => {
    const fillInputs = {};
    let searchInput = null;
    let map = null;

    function loadFillInputs() {
        const fillAttributes = DATA_ATTRIBUTES.FILL;
        Object.keys(fillAttributes).forEach((f) => {
            fillInputs[f] = document.querySelector(`*[${fillAttributes[f]}`);
        });
    }

    function fillResults(location) {
        Object.keys(fillInputs).forEach((key) => {
            const data = location[key] || '';
            const input = fillInputs[key];
            if (input) {
                // Support both input & div
                input.innerText = data;
                input.value = data;
                input.dispatchEvent(new Event('change'));
            }
        });

        updateMap();
    }

    function isInputsFilled() {
        return fillInputs.latitude.value &&
            fillInputs.longitude.value &&
            fillInputs.countryCode.value;
    }

    function loadLocationResult(location) {
        // 1. Load location id for details
        Ajax.request(`${API_LOOKUP_URL}&locationId=${location.locationId}`, 'json')
            .then((data) => {
                const view = data.Response.View;
                if (view && view.length) {
                    const result = view[0].Result;
                    if (result && result.length) {
                        const first = result[0];
                        const details = first.Location;
                        // 2. Load details into location + flatten
                        const { Latitude, Longitude } = (details.DisplayPosition || {});
                        location.latitude = Latitude;
                        location.longitude = Longitude;
                        location = { ...location, ...location.address };
                        delete location.address;
                        // 3. Fix props
                        location.street = [(location.houseNumber || ''), (location.street || '')].join(' ');
                        // 4. Fill results
                        fillResults(location);
                        searchInput.value = location.label;
                    }
                }
            });
    }

    function attachAutoComplete() {
        const autoComplete = new AutoComplete({
            data: {
                src: () => Ajax.request(`${API_URL}&query=${encodeURIComponent(searchInput.value)}`, 'json')
                    .then((data) => data.suggestions),
                keys: ['label'],
                filter: (list) => {
                    list.forEach((e, k) => {
                        if ('county' === e.value.matchLevel) {
                            list.splice(k, 1);
                        }
                    });

                    return list;
                }
            },
            selector: `#${searchInput.id}`,
            debounce: HTTP_REQ_DELAY,
            threshold: 3
        });

        autoComplete.input.addEventListener('close', () => {
            fillResults([]);
        });

        autoComplete.input.addEventListener('keyup', () => {
            if ('' === searchInput.value) {
                fillResults([]);
            }
        });

        autoComplete.input.addEventListener('selection', (event) => {
            loadLocationResult(event.detail.selection.value);
        });
    }

    function initMap() {
        const mapId = 'map-preview';

        const mapContainer = document.createElement('div');
        mapContainer.classList.add('map', 'mt-4');
        const mapDiv = document.createElement('div');
        mapDiv.id = mapId;
        mapDiv.classList.add('interactive', 'interactive-small');
        mapContainer.appendChild(mapDiv);
        searchInput.closest('.form-group').appendChild(mapContainer);

        map = Map(mapId);

        if (isInputsFilled()) {
            updateMap();
        }
    }

    function updateMap() {
        if (map) {
            if (fillInputs.latitude.value && fillInputs.longitude.value) {
                map.showLocation({
                    latitude: fillInputs.latitude.value,
                    longitude: fillInputs.longitude.value
                });
            } else {
                map.cleanAll();
            }
        }
    }

    function bootstrap(element, showMap = false) {
        if (element) {
            searchInput = element;
            loadFillInputs();
            attachAutoComplete();

            if (showMap) {
                initMap();
            }
        }
    }

    return {
        handle: (element, showMap) => bootstrap(element, showMap)
    };
};

export default GeoCoding();
