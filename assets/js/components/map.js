import * as L from 'leaflet';
import 'leaflet.markercluster';
import Ajax from '../utils/Ajax';
import Translator from '../utils/Translator';

import defaultMarkerImg from '../../images/map_marker.svg';

const defaultMapZoom = 18;
const nbItemsToShow = 3;

const Map = (MAP_ID) => {
    const cards = document.getElementById('map-cards');
    let map = null;

    function init(location, zoom, maxZoom) {
        cleanAll();

        const options = { scrollWheelZoom: false };

        if (maxZoom) {
            options.maxZoom = parseData(maxZoom);
        }

        map = L.map(MAP_ID, options);

        if (location) {
            map.setView(parseData(location), parseData(zoom || defaultMapZoom));
        }

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'WeShareWeCare'
        }).addTo(map);
    }

    function markersCluster() {
        const layer = L.markerClusterGroup({
            spiderfyOnMaxZoom: false,
            showCoverageOnHover: true,
            zoomToBoundsOnClick: true,
            removeOutsideVisibleBounds: true,
            disableClusteringAtZoom: 10,
            maxClusterRadius: 30
        });
        map.addLayer(layer);
        return layer;
    }

    function addMarker(lat, long, icon, html, group) {
        const options = {};
        if (icon) {
            options.icon = icon;
        }
        const marker = L.marker([lat, long], options);
        if (group) {
            group.addLayer(marker);
        } else {
            marker.addTo(map);
        }
        if (html) {
            marker.bindPopup(html);
        }
    }

    function generateIcon(iconUrl, type, size = [32, 32]) {
        const options = {
            iconUrl,
            iconSize: size
        };

        if (type) {
            options.className = `marker-type-${type}`;
        }

        return L.icon(options);
    }

    function loadData(data, cardFactory) {
        const group = markersCluster();
        data.forEach((loc) => {
            addMarker(
                loc.latitude,
                loc.longitude,
                generateIcon(loc.icon, loc.type),
                cardFactory(loc, true),
                group
            );
        });
        map.invalidateSize();
    }

    function clearCards() {
        if (cards) {
            const elements = Array.from(cards.children);
            elements.forEach((e) => {
                cards.removeChild(e);
            });
        }
    }

    function clearMarkers() {
        if (map) {
            map.eachLayer((layer) => {
                if ('_markerCluster' in layer) {
                    map.removeLayer(layer);
                }
            });
        }
    }

    function buildCards(data, type, cardFactory, onClick) {
        const container = document.createElement('div');
        if (cards && cardFactory) {
            if (0 < data.length) {
                container.innerHTML = `<h3 class="text-left mt-5">${Translator.trans(`map.card.${type}.title`)}</h3>`;
                const row = document.createElement('div');
                row.classList.add('row');

                let nbItems = 0;
                data.forEach((current) => {
                    const item = document.createElement('div');
                    item.classList.add('item');
                    item.classList.add('col-md-3');

                    if (nbItems >= nbItemsToShow) {
                        item.classList.add('d-none');
                    }

                    item.setAttribute('data-card-type', type);
                    item.appendChild(cardFactory(current));
                    item.addEventListener('click', () => onClick(current));
                    row.appendChild(item);

                    nbItems += 1;
                });

                container.appendChild(row);

                if (nbItems > nbItemsToShow) {
                    const showMoreButton = document.createElement('button');
                    showMoreButton.classList.add('btn');
                    showMoreButton.classList.add('btn-outline-primary');
                    showMoreButton.innerHTML = Translator.trans('map.list.show_more.button');
                    showMoreButton.addEventListener('click', () => {
                        document.querySelectorAll(`.item[data-card-type='${type}']`)
                            .forEach((item) => {
                                item.classList.remove('d-none');
                            });
                        showMoreButton.remove();
                    });

                    const showMoreButtonDiv = document.createElement('div');
                    showMoreButtonDiv.classList.add('mt-3');
                    showMoreButtonDiv.appendChild(showMoreButton);

                    container.appendChild(showMoreButtonDiv);
                }
            }

            cards.appendChild(container);
        }
    }

    function fromURL(url, type, cardFactory, onClick) {
        Ajax.request(url, 'json')
            .then((data) => {
                loadData(data, cardFactory);
                buildCards(data, type, cardFactory, onClick);
            });
    }

    function event(events, callback) {
        map.on(events, (ev) => {
            callback(map.getCenter(), ev);
        });
    }

    function showLocation(options) {
        init({ lat: options.latitude, lng: options.longitude }, options.zoom);
        addMarker(
            options.latitude,
            options.longitude,
            generateIcon(options.iconUrl || defaultMarkerImg, options.type)
        );
    }

    function parseData(data) {
        if (!data) return {};

        if ('object' === typeof data) return data;

        return JSON.parse(data);
    }

    function cleanAll() {
        clearCards();

        if (map) {
            map.remove();
            map = null;
        }
    }

    return {
        init,
        fromURL,
        clearCards,
        clearMarkers,
        cleanAll,
        event,
        showLocation
    };
};

export default Map;
