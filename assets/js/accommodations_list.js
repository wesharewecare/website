import Tabulator from './utils/Tabulator';
import Translator from './utils/Translator';

document.addEventListener('DOMContentLoaded', () => {
    const tabulator = document.getElementById('tabulator');
    const types = JSON.parse(tabulator.dataset.types);
    const status = JSON.parse(tabulator.dataset.status);
    const booleans = JSON.parse(tabulator.dataset.boolean);
    const filters = Object.entries(JSON.parse(tabulator.dataset.filters));
    const filtersToApply = [];

    filters.forEach(([filter, value]) => {
        let type;

        switch (filter) {
        default:
            type = 'like';
            break;
        }

        filtersToApply.push({ field: filter, type, value });
    });

    const columns = [
        { field: 'id', visible: false },
        {
            title: Translator.trans('accommodation.name'), field: 'name', headerFilter: true, maxWidth: 300
        },
        {
            title: Translator.trans('accommodation.type'),
            field: 'type',
            headerFilter: 'select',
            headerFilterParams: { values: types },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('accommodation.status'),
            field: 'status',
            headerFilter: 'select',
            headerFilterParams: { values: status, multiselect: true },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('accommodation.nb_demands'),
            field: 'nbDemands',
            headerSort: false
        },
        {
            title: Translator.trans('accommodation.house_size'),
            field: 'houseSize',
            headerFilter: 'number',
            headerFilterPlaceholder: 'at least...',
            headerFilterFunc: '>='
        },
        {
            title: Translator.trans('accommodation.room_size'),
            field: 'roomSize',
            headerFilter: 'number',
            headerFilterPlaceholder: 'at least...',
            headerFilterFunc: '>='
        },
        {
            title: Translator.trans('accommodation.price'),
            field: 'price',
            headerFilter: 'number',
            headerFilterPlaceholder: 'at least...',
            headerFilterFunc: '>='
        },
        { title: Translator.trans('city'), field: 'city', headerFilter: true },
        { title: Translator.trans('country'), field: 'country', headerFilter: true },
        { title: Translator.trans('accommodation.start_date', { date: '' }), field: 'startDate' },
        { title: Translator.trans('accommodation.end_date', { date: '' }), field: 'endDate' },
        {
            title: Translator.trans('accommodation.has_pet'),
            field: 'hasPet',
            headerFilter: 'select',
            headerFilterParams: { values: booleans },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('accommodation.has_wifi'),
            field: 'hasWifi',
            headerFilter: 'select',
            headerFilterParams: { values: booleans },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('accommodation.accept_smokers'),
            field: 'acceptSmokers',
            headerFilter: 'select',
            headerFilterParams: { values: booleans },
            headerFilterFunc: '='
        },
        { title: Translator.trans('created_at'), field: 'createdAt' },
        {
            title: Translator.trans('actions'), field: 'actions', formatter: 'html', headerSort: false
        }
    ];

    Tabulator.init(tabulator, {
        autoColumnsDefinitions: columns,
        ajaxURL: Routing.generate('accommodation_list_data'),
        initialFilter: filtersToApply,
        initialHeaderFilter: filtersToApply
    });
});
