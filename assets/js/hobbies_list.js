import Tabulator from './utils/Tabulator';

document.addEventListener('DOMContentLoaded', () => {
    const tabulator = document.getElementById('tabulator');

    const columns = [
        { field: 'id', visible: false },
        { title: 'Name', field: 'name', headerFilter: true },
        {
            title: 'Actions', field: 'actions', formatter: 'html', headerSort: false
        }
    ];

    Tabulator.init(tabulator, {
        autoColumnsDefinitions: columns,
        ajaxURL: Routing.generate('hobby_list_data')
    });
});
