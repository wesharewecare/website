import '../sass/pages/messages.scss';
import { bootstrap } from '../chat/bootstrap';

document.addEventListener('DOMContentLoaded', () => {
    bootstrap(document.getElementById('messaging-app'));
});
