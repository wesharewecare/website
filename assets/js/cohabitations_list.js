import Tabulator from './utils/Tabulator';
import Translator from './utils/Translator';

document.addEventListener('DOMContentLoaded', () => {
    const tabulator = document.getElementById('tabulator');
    const steps = JSON.parse(tabulator.dataset.steps);
    const filters = Object.entries(JSON.parse(tabulator.dataset.filters));
    const filtersToApply = [];

    filters.forEach(([filter, value]) => {
        let type;

        switch (filter) {
        default:
            type = 'like';
            break;
        }

        filtersToApply.push({ field: filter, type, value });
    });

    const columns = [
        { field: 'id', visible: false },
        { title: Translator.trans('accommodation_demand.young_name'), field: 'young', headerSort: false },
        { title: Translator.trans('accommodation_demand.senior_name'), field: 'senior', headerSort: false },
        { title: Translator.trans('accommodation_demand.status'), field: 'status' },
        {
            title: Translator.trans('cohabitation.current_step'),
            field: 'currentStep',
            headerFilter: 'select',
            headerFilterParams: { values: steps, multiselect: true },
            headerFilterFunc: '='
        },
        {
            title: Translator.trans('cohabitation.price'),
            field: 'price',
            headerFilter: 'number',
            headerFilterPlaceholder: 'at least...',
            headerFilterFunc: '>='
        },
        { title: Translator.trans('cohabitation.start_date'), field: 'startDate' },
        { title: Translator.trans('cohabitation.end_date'), field: 'endDate' },
        {
            title: Translator.trans('actions'), field: 'actions', formatter: 'html', headerSort: false
        }
    ];

    Tabulator.init(tabulator, {
        autoColumnsDefinitions: columns,
        ajaxURL: Routing.generate('cohabitation_list_data'),
        initialFilter: filtersToApply,
        initialHeaderFilter: filtersToApply
    });
});
