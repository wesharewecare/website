document.addEventListener('DOMContentLoaded', () => {
    const bubbles = document.querySelector('.bubbles');
    const sub = Array.from(
        document.querySelectorAll('.bubbles > .bubble-part')
    );
    const container = document.body;

    function resizeBubbles() {
        const nodes = Array.from(
            container.querySelectorAll(':scope > *:not(.bubbles)')
        ).filter((el) => {
            const style = window.getComputedStyle(el);
            return 'none' !== style.display;
        });
        const tHeight = Math.max(
            nodes.reduce(
                (acc, el) => acc + el.getBoundingClientRect().height,
                0
            ),
            window.innerHeight,
            document.body.scrollHeight
        );
        bubbles.style.height = `${tHeight}px`;

        const divH = tHeight / nodes.length;
        const divH2 = tHeight / sub.length;
        const divPx = Math.max(divH, divH2);

        sub.forEach((s, i) => {
            s.style.top = `${i * divPx}px`;
        });
    }

    if (bubbles) {
        let wDebounce;
        window.addEventListener('resize', () => {
            clearTimeout(wDebounce);
            wDebounce = setTimeout(resizeBubbles, 300);
        });
        resizeBubbles();
    }
});
