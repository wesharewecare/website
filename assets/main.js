import * as Const from './js/utils/Const';
import Routing from './js/utils/Routing';
import Flashbag from './js/utils/Flashbag';
import './bootstrap';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './sass/main.scss';
import './js/bubbles';

global.Const = Const;
global.Routing = Routing;
global.Flashbag = Flashbag;
