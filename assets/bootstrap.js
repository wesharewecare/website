import { startStimulusApp } from '@symfony/stimulus-bridge';
import TextareaAutogrow from "stimulus-textarea-autogrow";

export const app = startStimulusApp(require.context('./controllers', true, /\.(j|t)sx?$/));
app.register("textarea-autogrow", TextareaAutogrow);
