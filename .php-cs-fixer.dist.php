<?php

use PedroTroller\CS\Fixer\Fixers;
use PedroTroller\CS\Fixer\RuleSetFactory;

$config = new PhpCsFixer\Config();
$config
    ->setRiskyAllowed(true)
    ->setCacheFile(__DIR__.'/var/.php-cs-fixer.cache')
    ->setRules(
        RuleSetFactory::create()
            ->symfony()
            ->phpCsFixer()
            ->doctrineAnnotation()
            ->php(7.2, true)
            ->pedrotroller(true)
            ->enable('PedroTroller/line_break_between_method_arguments', ['max-args' => 20])
            ->enable('PedroTroller/line_break_between_statements')
            ->enable('PedroTroller/useless_code_after_return')
            ->enable('PedroTroller/doctrine_migrations')
            ->enable('no_useless_else')
            ->enable('no_useless_return')
            ->enable('doctrine_annotation_braces')
            ->enable('doctrine_annotation_indentation')
            ->enable('doctrine_annotation_spaces')
            ->enable('align_multiline_comment')
            ->enable('ordered_class_elements')
            ->enable('ordered_imports')
            ->enable('no_superfluous_phpdoc_tags')
            ->enable('phpdoc_separation')
            ->enable('phpdoc_single_line_var_spacing')
            ->enable('phpdoc_var_without_name')
            ->enable('native_constant_invocation')
            ->enable('array_syntax', ['syntax' => 'short'])
            ->enable('concat_space', ['spacing' => 'none'])
            ->enable('multiline_whitespace_before_semicolons', ['strategy' => 'new_line_for_chained_calls'])
            ->enable('native_function_invocation', ['include' => ['@compiler_optimized']])
            ->disable('phpdoc_to_comment')
            ->disable('phpdoc_summary')
            ->getRules()
    )
    ->registerCustomFixers(new Fixers())
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in([
                __DIR__.'/src',
                __DIR__.'/migrations',
                __DIR__.'/tests',
                __DIR__.'/config',
            ])
    )
;

return $config;
