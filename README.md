# We Share We Care

This repository is the open source [Symfony](https://symfony.com/) application that made the [wesharewecare.eu](https://wesharewecare.eu) website. 

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
