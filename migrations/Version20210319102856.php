<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210319102856 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE cohabitation (
          id INT AUTO_INCREMENT NOT NULL,
          current_step_id INT DEFAULT NULL,
          agreement_name VARCHAR(255) DEFAULT NULL,
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          UNIQUE INDEX UNIQ_3D288883D9BF9B19 (current_step_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cohabitation_workflow_step (
          id INT AUTO_INCREMENT NOT NULL,
          cohabitation_id INT DEFAULT NULL,
          status ENUM(
            \'cohabitation_init\',
            \'pre_visit_done\', \'interview_done\',
            \'agreement_sent\', \'agreement_signed\',
            \'appointment_done\', \'debriefing_done\'
          ) NOT NULL COMMENT \'(DC2Type:CohabitationWorkflowStepStatusType)\',
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          INDEX IDX_51CE397743DDDA6D (cohabitation_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          cohabitation
        ADD
          CONSTRAINT FK_3D288883D9BF9B19 FOREIGN KEY (current_step_id) REFERENCES cohabitation_workflow_step (id)');
        $this->addSql('ALTER TABLE
          cohabitation_workflow_step
        ADD
          CONSTRAINT FK_51CE397743DDDA6D FOREIGN KEY (cohabitation_id) REFERENCES cohabitation (id)');
        $this->addSql('ALTER TABLE accommodation_demand ADD cohabitation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          accommodation_demand
        ADD
          CONSTRAINT FK_1FE9DD5243DDDA6D FOREIGN KEY (cohabitation_id) REFERENCES cohabitation (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1FE9DD5243DDDA6D ON accommodation_demand (cohabitation_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation_demand DROP FOREIGN KEY FK_1FE9DD5243DDDA6D');
        $this->addSql('ALTER TABLE cohabitation_workflow_step DROP FOREIGN KEY FK_51CE397743DDDA6D');
        $this->addSql('ALTER TABLE cohabitation DROP FOREIGN KEY FK_3D288883D9BF9B19');
        $this->addSql('DROP TABLE cohabitation');
        $this->addSql('DROP TABLE cohabitation_workflow_step');
        $this->addSql('DROP INDEX UNIQ_1FE9DD5243DDDA6D ON accommodation_demand');
        $this->addSql('ALTER TABLE accommodation_demand DROP cohabitation_id');
    }
}
