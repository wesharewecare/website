<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210428130657 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association
        ADD
            radius_range_in_kms INT UNSIGNED DEFAULT 50 NOT NULL,
        ADD
          radius_unit ENUM(\'kms\', \'miles\') NOT NULL COMMENT \'(DC2Type:AssociationRadiusUnitType)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association
            DROP
                radius_range_in_kms,
            DROP
                radius_unit');
    }
}
