<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201124170440 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE profile ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          profile
        ADD
          CONSTRAINT FK_8157AA0F64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8157AA0F64D218E ON profile (location_id)');
        $this->addSql('ALTER TABLE
          location
        CHANGE
          city city VARCHAR(255) DEFAULT NULL,
        CHANGE
          zip_code zip_code VARCHAR(255) DEFAULT NULL,
        CHANGE
          district district VARCHAR(255) DEFAULT NULL,
        CHANGE
          street street VARCHAR(255) DEFAULT NULL,
        CHANGE
          country country VARCHAR(255) DEFAULT NULL,
        CHANGE
          latitude latitude VARCHAR(255) DEFAULT NULL,
        CHANGE
          longitude longitude VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE profile DROP FOREIGN KEY FK_8157AA0F64D218E');
        $this->addSql('DROP INDEX UNIQ_8157AA0F64D218E ON profile');
        $this->addSql('ALTER TABLE profile DROP location_id');
        $this->addSql('ALTER TABLE
          location
        CHANGE
          city city VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          zip_code zip_code VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          district district VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          street street VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          country country VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          latitude latitude VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          longitude longitude VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
