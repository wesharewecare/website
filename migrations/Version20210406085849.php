<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210406085849 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE accommodation_demand_association (
          accommodation_demand_id INT NOT NULL,
          association_id INT NOT NULL,
          INDEX IDX_F3D5906E439DE7F (accommodation_demand_id),
          INDEX IDX_F3D5906EFB9C8A5 (association_id),
          PRIMARY KEY(
            accommodation_demand_id, association_id
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          accommodation_demand_association
        ADD
          CONSTRAINT FK_F3D5906E439DE7F FOREIGN KEY (accommodation_demand_id) REFERENCES accommodation_demand (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          accommodation_demand_association
        ADD
          CONSTRAINT FK_F3D5906EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE accommodation_demand_association');
    }
}
