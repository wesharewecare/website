<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210831090009 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE profile ADD locale_language VARCHAR(255) NOT NULL');
        $this->addSql('UPDATE profile SET locale_language = \'en\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE profile DROP locale_language');
    }
}
