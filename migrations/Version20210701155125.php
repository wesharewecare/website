<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210701155125 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          cohabitation
        ADD
          agreement_params LONGTEXT DEFAULT \'[]\' COMMENT \'(DC2Type:json)\',
        DROP
          shared_house_size,
        DROP
          charge_types,
        DROP
          deposit_amount,
        DROP
          charges_amount,
        DROP
          house_rooms,
        CHANGE
          agreement_params agreement_params LONGTEXT DEFAULT \'[{}]\' COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          cohabitation
        ADD
          shared_house_size INT UNSIGNED NOT NULL,
        ADD
          charge_types LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'[]\' NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\',
        ADD
          deposit_amount INT UNSIGNED NOT NULL,
        ADD
          charges_amount INT UNSIGNED NOT NULL,
        ADD
          house_rooms INT UNSIGNED NOT NULL,
        DROP
          agreement_params,
        CHANGE
          agreement_params agreement_params LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'[]\' COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\'');
    }
}
