<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210419153907 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          accommodation
        DROP
          deposit_amount,
        DROP
          charges_amount,
        DROP
          shared_house_size,
        DROP
          house_rooms,
        DROP
          charge_types');
        $this->addSql('ALTER TABLE
          cohabitation
        ADD
          shared_house_size INT UNSIGNED NOT NULL,
        ADD
          charge_types LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\',
        ADD
          deposit_amount INT UNSIGNED NOT NULL,
        ADD
          charges_amount INT UNSIGNED NOT NULL,
        ADD
          house_rooms INT UNSIGNED NOT NULL,
        ADD
          price_amount INT UNSIGNED NOT NULL,
        ADD
          currency_code VARCHAR(64) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          deposit_amount INT UNSIGNED NOT NULL,
        ADD
          charges_amount INT UNSIGNED NOT NULL,
        ADD
          shared_house_size INT UNSIGNED NOT NULL,
        ADD
          house_rooms INT UNSIGNED NOT NULL,
        ADD
          charge_types LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'[]\' NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE
          cohabitation
        DROP
          shared_house_size,
        DROP
          charge_types,
        DROP
          deposit_amount,
        DROP
          charges_amount,
        DROP
          house_rooms,
        DROP
          price_amount,
        DROP
          currency_code');
    }
}
