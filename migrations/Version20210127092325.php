<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210127092325 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE message (
          id INT AUTO_INCREMENT NOT NULL,
          thread_id INT DEFAULT NULL,
          sender_id INT DEFAULT NULL,
          body LONGTEXT NOT NULL,
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          INDEX IDX_B6BD307FE2904019 (thread_id),
          INDEX IDX_B6BD307FF624B39D (sender_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread (
          id INT AUTO_INCREMENT NOT NULL,
          subject VARCHAR(255) NOT NULL,
          last_message_at DATETIME DEFAULT NULL,
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          type VARCHAR(255) NOT NULL,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread_user (
          thread_id INT NOT NULL,
          user_id INT NOT NULL,
          INDEX IDX_922CAC7E2904019 (thread_id),
          INDEX IDX_922CAC7A76ED395 (user_id),
          PRIMARY KEY(thread_id, user_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread_favorite (
          id INT AUTO_INCREMENT NOT NULL,
          user_id INT DEFAULT NULL,
          thread VARCHAR(255) NOT NULL,
          INDEX IDX_6774CD05A76ED395 (user_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread_metadata (
          id INT AUTO_INCREMENT NOT NULL,
          thread_id INT DEFAULT NULL,
          participant_id INT DEFAULT NULL,
          last_message_read_at DATETIME DEFAULT NULL,
          INDEX IDX_40A577C8E2904019 (thread_id),
          INDEX IDX_40A577C89D1C3019 (participant_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          message
        ADD
          CONSTRAINT FK_B6BD307FE2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          message
        ADD
          CONSTRAINT FK_B6BD307FF624B39D FOREIGN KEY (sender_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_user
        ADD
          CONSTRAINT FK_922CAC7E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_user
        ADD
          CONSTRAINT FK_922CAC7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_favorite
        ADD
          CONSTRAINT FK_6774CD05A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE
          thread_metadata
        ADD
          CONSTRAINT FK_40A577C8E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_metadata
        ADD
          CONSTRAINT FK_40A577C89D1C3019 FOREIGN KEY (participant_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FE2904019');
        $this->addSql('ALTER TABLE thread_user DROP FOREIGN KEY FK_922CAC7E2904019');
        $this->addSql('ALTER TABLE thread_metadata DROP FOREIGN KEY FK_40A577C8E2904019');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE thread');
        $this->addSql('DROP TABLE thread_user');
        $this->addSql('DROP TABLE thread_favorite');
        $this->addSql('DROP TABLE thread_metadata');
    }
}
