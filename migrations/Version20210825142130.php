<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210825142130 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          cohabitation
        CHANGE
          agreement_params agreement_params LONGTEXT DEFAULT \'[{}]\' NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          cohabitation
        CHANGE
          agreement_params agreement_params LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'[{}]\' COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\'');
    }
}
