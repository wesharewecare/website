<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210408121954 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE cohabitation_workflow_step
            CHANGE status status ENUM(
                \'cohabitation_init\',
                \'pre_visit_done\', \'interview_done\', \'joint_meeting_done\',
                \'agreement_sent\', \'agreement_signed\',
                \'appointment_done\', \'debriefing_done\'
            ) NOT NULL COMMENT \'(DC2Type:CohabitationWorkflowStepStatusType)\''
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE cohabitation_workflow_step
            CHANGE status status ENUM(
                \'cohabitation_init\',
                \'pre_visit_done\', \'interview_done\',
                \'agreement_sent\', \'agreement_signed\',
                \'appointment_done\', \'debriefing_done\'
            ) NOT NULL COMMENT \'(DC2Type:CohabitationWorkflowStepStatusType)\''
        );
    }
}
