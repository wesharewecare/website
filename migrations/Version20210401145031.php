<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210401145031 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          profile
        ADD
          gender ENUM(\'male\', \'female\', \'other\') DEFAULT NULL COMMENT \'(DC2Type:ProfileGenderType)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE profile DROP gender');
    }
}
