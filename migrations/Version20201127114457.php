<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201127114457 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE profile ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          profile
        ADD
          CONSTRAINT FK_8157AA0FEFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_8157AA0FEFB9C8A5 ON profile (association_id)');
        $this->addSql('ALTER TABLE user CHANGE birthday birthday DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE profile DROP FOREIGN KEY FK_8157AA0FEFB9C8A5');
        $this->addSql('DROP INDEX IDX_8157AA0FEFB9C8A5 ON profile');
        $this->addSql('ALTER TABLE profile DROP association_id');
        $this->addSql('ALTER TABLE user CHANGE birthday birthday DATE NOT NULL');
    }
}
