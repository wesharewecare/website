<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210412143650 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          deposit_amount INT UNSIGNED NOT NULL,
        ADD
          charges_amount INT UNSIGNED NOT NULL,
        CHANGE
          price_currency currency_code VARCHAR(64) NOT NULL,
        ADD
          shared_house_size INT UNSIGNED NOT NULL,
        ADD
          house_rooms INT UNSIGNED NOT NULL,
        ADD
          shared_house_rooms INT UNSIGNED NOT NULL,
        ADD
          ancillary_rooms LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\',
        ADD
          charge_types LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          accommodation
        DROP
          deposit_amount,
        DROP
          charges_amount,
        CHANGE
          currency price_currency VARCHAR(64) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        DROP
            shared_house_size,
        DROP
            house_rooms,
        DROP
            shared_house_rooms,
        DROP
            ancillary_rooms,
        DROP
            charge_types');
    }
}
