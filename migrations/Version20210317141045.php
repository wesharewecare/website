<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210317141045 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation DROP FOREIGN KEY FK_2D38541264D218E');
        $this->addSql('DROP INDEX UNIQ_2D38541264D218E ON accommodation');
        $this->addSql('ALTER TABLE accommodation DROP location_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          CONSTRAINT FK_2D38541264D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D38541264D218E ON accommodation (location_id)');
    }
}
