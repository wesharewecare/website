<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210406114631 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association
        ADD
          check_insurance TINYINT(1) NOT NULL,
        ADD
          check_deposit TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE
          cohabitation
        ADD
          insurance_checked TINYINT(1) NOT NULL,
        ADD
          deposit_checked TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association DROP check_insurance, DROP check_deposit');
        $this->addSql('ALTER TABLE cohabitation DROP insurance_checked, DROP deposit_checked');
    }
}
