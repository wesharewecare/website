<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210819143117 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE user_notification (
          id INT AUTO_INCREMENT NOT NULL,
          notifications LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\',
          nb_notif_sent INT UNSIGNED DEFAULT 0 NOT NULL,
          notif_sent_at DATETIME DEFAULT NULL,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD user_notification_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          user
        ADD
          CONSTRAINT FK_8D93D649FDC6F10B FOREIGN KEY (user_notification_id) REFERENCES user_notification (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649FDC6F10B ON user (user_notification_id)');
        $this->addSql('INSERT INTO user_notification (id, notifications)
            SELECT id, CASE WHEN roles LIKE \'%ADMIN%\' THEN \'{"messaging_group":false,"messaging_individual":false,"event_updated":false,"event_reminder":false}\' ELSE
                CASE WHEN roles LIKE \'%ASSOCIATION%\' THEN \'{"messaging_group":false,"messaging_individual":false,"event_participant":false,"accommodation_new":false,"demand_new":false,"user_new":false}\' ELSE
                CASE WHEN roles LIKE \'%SENIOR%\' THEN \'{"messaging_group":false,"messaging_individual":false,"event_nearby":false,"event_updated":false,"event_reminder":false,"demand_new":false,"demand_updated":false}\' ELSE
                CASE WHEN roles LIKE \'%YOUNG%\' THEN \'{"messaging_group":false,"messaging_individual":false,"event_nearby":false,"event_updated":false,"event_reminder":false,"accommodation_published":false,"demand_updated":false}\' ELSE \'{}\' END END END END FROM user');
        $this->addSql('UPDATE user SET user_notification_id = id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FDC6F10B');
        $this->addSql('DROP TABLE user_notification');
        $this->addSql('DROP INDEX UNIQ_8D93D649FDC6F10B ON user');
        $this->addSql('ALTER TABLE user DROP user_notification_id');
    }
}
