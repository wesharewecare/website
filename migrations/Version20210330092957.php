<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210330092957 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          price_currency VARCHAR(64) NOT NULL,
        CHANGE
          price price_amount INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE profile ADD preferred_currency VARCHAR(64) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation DROP price_currency, CHANGE price_amount price INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE profile DROP preferred_currency');
    }
}
