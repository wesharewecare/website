<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201214103054 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          accommodation
        CHANGE
          description description LONGTEXT NOT NULL,
        CHANGE
          room_description room_description LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE profile
            ADD phone_number VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\',
            ADD description LONGTEXT DEFAULT NULL
        ');
        $this->addSql('ALTER TABLE association ADD radius_range INT UNSIGNED DEFAULT 50 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          accommodation
        CHANGE
          description description VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          room_description room_description VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE profile
            DROP phone_number,
            DROP description
        ');
        $this->addSql('ALTER TABLE association DROP radius_range');
    }
}
