<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201210130007 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          location
        CHANGE
          latitude latitude DOUBLE PRECISION DEFAULT NULL,
        CHANGE
          longitude longitude DOUBLE PRECISION DEFAULT NULL');

        $this->addSql('ALTER TABLE
          accommodation
        CHANGE
          start_date start_date DATE DEFAULT NULL,
        CHANGE
          end_date end_date DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          location
        CHANGE
          latitude latitude VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          longitude longitude VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');

        $this->addSql('ALTER TABLE
          accommodation
        CHANGE
          start_date start_date VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          end_date end_date VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
