<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210316141328 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation_demand ADD thread_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          accommodation_demand
        ADD
          CONSTRAINT FK_1FE9DD52E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1FE9DD52E2904019 ON accommodation_demand (thread_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation_demand DROP FOREIGN KEY FK_1FE9DD52E2904019');
        $this->addSql('DROP INDEX UNIQ_1FE9DD52E2904019 ON accommodation_demand');
        $this->addSql('ALTER TABLE accommodation_demand DROP thread_id');
    }
}
