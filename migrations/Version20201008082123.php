<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201008082123 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE accommodation (
          id INT AUTO_INCREMENT NOT NULL,
          senior_id INT DEFAULT NULL,
          location_id INT DEFAULT NULL,
          name VARCHAR(255) NOT NULL,
          description VARCHAR(255) NOT NULL,
          room_description VARCHAR(255) NOT NULL,
          type ENUM(\'house\', \'flat\') NOT NULL COMMENT \'(DC2Type:AccommodationTypeType)\',
          start_date VARCHAR(255) NOT NULL,
          end_date VARCHAR(255) NOT NULL,
          price INT UNSIGNED NOT NULL,
          room_size INT UNSIGNED NOT NULL,
          house_size INT UNSIGNED NOT NULL,
          has_pet TINYINT(1) NOT NULL,
          has_wifi TINYINT(1) NOT NULL,
          accept_smokers TINYINT(1) NOT NULL,
          status ENUM(\'open\', \'closed\', \'occupied\', \'awaiting_approval\') NOT NULL COMMENT \'(DC2Type:AccommodationStatusType)\',
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          UNIQUE INDEX UNIQ_2D385412AB8E2 (senior_id),
          UNIQUE INDEX UNIQ_2D38541264D218E (location_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accommodation_image (
          id INT AUTO_INCREMENT NOT NULL,
          accommodation_id INT DEFAULT NULL,
          image_name VARCHAR(255) NOT NULL,
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          INDEX IDX_C98227A58F3692CD (accommodation_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_translations (
          id INT AUTO_INCREMENT NOT NULL,
          locale VARCHAR(8) NOT NULL,
          object_class VARCHAR(191) NOT NULL,
          field VARCHAR(32) NOT NULL,
          foreign_key VARCHAR(64) NOT NULL,
          content LONGTEXT DEFAULT NULL,
          INDEX translations_lookup_idx (
            locale, object_class, foreign_key
          ),
          UNIQUE INDEX lookup_unique_idx (
            locale, object_class, field, foreign_key
          ),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE hobby (
          id INT AUTO_INCREMENT NOT NULL,
          name VARCHAR(255) NOT NULL,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (
          id INT AUTO_INCREMENT NOT NULL,
          city VARCHAR(255) NOT NULL,
          zip_code VARCHAR(255) NOT NULL,
          district VARCHAR(255) NOT NULL,
          street VARCHAR(255) NOT NULL,
          country VARCHAR(255) NOT NULL,
          latitude VARCHAR(255) NOT NULL,
          longitude VARCHAR(255) NOT NULL,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_hobby (
          user_id INT NOT NULL,
          hobby_id INT NOT NULL,
          INDEX IDX_DBA6086FA76ED395 (user_id),
          INDEX IDX_DBA6086F322B2123 (hobby_id),
          PRIMARY KEY(user_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          CONSTRAINT FK_2D385412AB8E2 FOREIGN KEY (senior_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          CONSTRAINT FK_2D38541264D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE
          accommodation_image
        ADD
          CONSTRAINT FK_C98227A58F3692CD FOREIGN KEY (accommodation_id) REFERENCES accommodation (id)');
        $this->addSql('ALTER TABLE
          user_hobby
        ADD
          CONSTRAINT FK_DBA6086FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user_hobby
        ADD
          CONSTRAINT FK_DBA6086F322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user
        ADD
          firstname VARCHAR(255) NOT NULL,
        ADD
          lastname VARCHAR(255) NOT NULL,
        ADD
          birthday VARCHAR(255) NOT NULL,
        ADD
          profile_picture_name VARCHAR(255) DEFAULT NULL,
        ADD
          passport_name VARCHAR(255) DEFAULT NULL,
        ADD
          created_at DATETIME NOT NULL,
        ADD
          updated_at DATETIME NOT NULL,
        ADD
          type VARCHAR(255) NOT NULL,
        ADD
          university VARCHAR(255) DEFAULT NULL,
        ADD
          studies VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation_image DROP FOREIGN KEY FK_C98227A58F3692CD');
        $this->addSql('ALTER TABLE user_hobby DROP FOREIGN KEY FK_DBA6086F322B2123');
        $this->addSql('ALTER TABLE accommodation DROP FOREIGN KEY FK_2D38541264D218E');
        $this->addSql('DROP TABLE accommodation');
        $this->addSql('DROP TABLE accommodation_image');
        $this->addSql('DROP TABLE ext_translations');
        $this->addSql('DROP TABLE hobby');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE user_hobby');
        $this->addSql('ALTER TABLE
          user
        DROP
          firstname,
        DROP
          lastname,
        DROP
          birthday,
        DROP
          profile_picture_name,
        DROP
          passport_name,
        DROP
          created_at,
        DROP
          updated_at,
        DROP
          type,
        DROP
          university,
        DROP
          studies');
    }
}
