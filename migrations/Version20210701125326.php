<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210701125326 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association
        CHANGE
          agreement_version agreement_version ENUM(
            \'default_version\', \'cohabilis_version\',
            \'1toit2ages_version\'
          ) NOT NULL COMMENT \'(DC2Type:AssociationAgreementVersionType)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association
        CHANGE
          agreement_version agreement_version VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
