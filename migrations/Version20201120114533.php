<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201120114533 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE accommodation_demand (
          id INT AUTO_INCREMENT NOT NULL,
          young_id INT NOT NULL,
          accommodation_id INT NOT NULL,
          status ENUM(\'pending\', \'accepted\', \'refused\', \'abandoned\') NOT NULL COMMENT \'(DC2Type:AccommodationDemandStatusType)\',
          INDEX IDX_1FE9DD52E9DFEF18 (young_id),
          INDEX IDX_1FE9DD528F3692CD (accommodation_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profile (
          id INT AUTO_INCREMENT NOT NULL,
          user_id INT DEFAULT NULL,
          university VARCHAR(255) NOT NULL,
          studies VARCHAR(255) NOT NULL,
          profile_picture_name VARCHAR(255) DEFAULT NULL,
          passport_name VARCHAR(255) DEFAULT NULL,
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          UNIQUE INDEX UNIQ_8157AA0FA76ED395 (user_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profile_hobby (
          profile_id INT NOT NULL,
          hobby_id INT NOT NULL,
          INDEX IDX_CEB06CE5CCFA12B8 (profile_id),
          INDEX IDX_CEB06CE5322B2123 (hobby_id),
          PRIMARY KEY(profile_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          accommodation_demand
        ADD
          CONSTRAINT FK_1FE9DD52E9DFEF18 FOREIGN KEY (young_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE
          accommodation_demand
        ADD
          CONSTRAINT FK_1FE9DD528F3692CD FOREIGN KEY (accommodation_id) REFERENCES accommodation (id)');
        $this->addSql('ALTER TABLE
          profile
        ADD
          CONSTRAINT FK_8157AA0FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE
          profile_hobby
        ADD
          CONSTRAINT FK_CEB06CE5CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          profile_hobby
        ADD
          CONSTRAINT FK_CEB06CE5322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE user_hobby');
        $this->addSql('ALTER TABLE accommodation DROP FOREIGN KEY FK_2D385412AB8E2');
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          CONSTRAINT FK_2D385412AB8E2 FOREIGN KEY (senior_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE
          user
        DROP
          profile_picture_name,
        DROP
          passport_name,
        DROP
          type,
        DROP
          university,
        DROP
          studies,
        CHANGE
          birthday birthday DATE NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation DROP FOREIGN KEY FK_2D385412AB8E2');
        $this->addSql('ALTER TABLE accommodation_demand DROP FOREIGN KEY FK_1FE9DD52E9DFEF18');
        $this->addSql('ALTER TABLE profile_hobby DROP FOREIGN KEY FK_CEB06CE5CCFA12B8');
        $this->addSql('CREATE TABLE user_hobby (
          user_id INT NOT NULL,
          hobby_id INT NOT NULL,
          INDEX IDX_DBA6086FA76ED395 (user_id),
          INDEX IDX_DBA6086F322B2123 (hobby_id),
          PRIMARY KEY(user_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('ALTER TABLE
          user_hobby
        ADD
          CONSTRAINT FK_DBA6086F322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user_hobby
        ADD
          CONSTRAINT FK_DBA6086FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE accommodation_demand');
        $this->addSql('DROP TABLE profile');
        $this->addSql('DROP TABLE profile_hobby');
        $this->addSql('ALTER TABLE accommodation DROP FOREIGN KEY FK_2D385412AB8E2');
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          CONSTRAINT FK_2D385412AB8E2 FOREIGN KEY (senior_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE
          user
        ADD
          profile_picture_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
        ADD
          passport_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
        ADD
          type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
        ADD
          university VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
        ADD
          studies VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
        CHANGE
          birthday birthday VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
