<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210531135809 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE event (
          id INT AUTO_INCREMENT NOT NULL,
          association_id INT DEFAULT NULL,
          name VARCHAR(255) NOT NULL,
          description LONGTEXT NOT NULL,
          date DATETIME DEFAULT NULL,
          event_image_name VARCHAR(255) DEFAULT NULL,
          location_id INT DEFAULT NULL,
          status ENUM(\'open\', \'closed\', \'canceled\', \'draft\') NOT NULL COMMENT \'(DC2Type:EventStatusType)\',
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          INDEX IDX_3BAE0AA7EFB9C8A5 (association_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_user (
          event_id INT NOT NULL,
          user_id INT NOT NULL,
          INDEX IDX_92589AE271F7E88B (event_id),
          INDEX IDX_92589AE2A76ED395 (user_id),
          PRIMARY KEY(event_id, user_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          event
        ADD
          CONSTRAINT FK_3BAE0AA7EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('ALTER TABLE
          event
        ADD
          CONSTRAINT FK_3BAE0AA764D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE
          event_user
        ADD
          CONSTRAINT FK_92589AE271F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          event_user
        ADD
          CONSTRAINT FK_92589AE2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3BAE0AA764D218E ON event (location_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA764D218E');
        $this->addSql('DROP INDEX UNIQ_3BAE0AA764D218E ON event');
        $this->addSql('ALTER TABLE event_user DROP FOREIGN KEY FK_92589AE271F7E88B');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE event_user');
    }
}
