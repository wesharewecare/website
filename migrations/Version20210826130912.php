<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210826130912 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          event
        ADD
          new_participant_notification_to_send TINYINT(1) NOT NULL,
        CHANGE
          reminder_sent reminder_notification_sent TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          event
        ADD
          reminder_sent TINYINT(1) NOT NULL,
        DROP
          reminder_notification_sent,
        DROP
          new_participant_notification_to_send');
    }
}
