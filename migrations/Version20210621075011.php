<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210621075011 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          event
        ADD
          email VARCHAR(180) NOT NULL,
        ADD
          firstname VARCHAR(255) NOT NULL,
        ADD
          lastname VARCHAR(255) NOT NULL,
        ADD
          phone_number VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE event DROP email, DROP firstname, DROP lastname, DROP phone_number');
    }
}
