<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201123160615 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE association (
          id INT AUTO_INCREMENT NOT NULL,
          location_id INT NOT NULL,
          name VARCHAR(255) NOT NULL,
          website VARCHAR(255) DEFAULT NULL,
          logo_name VARCHAR(255) DEFAULT NULL,
          phone_number VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\',
          type ENUM(
            \'young_only\', \'both\'
          ) NOT NULL COMMENT \'(DC2Type:AssociationTypeType)\',
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          UNIQUE INDEX UNIQ_FD8521CC64D218E (location_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          association
        ADD
          CONSTRAINT FK_FD8521CC64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE association');
    }
}
