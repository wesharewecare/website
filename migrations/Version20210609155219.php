<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210609155219 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          event
        ADD
          is_free TINYINT(1) NOT NULL,
        ADD
          is_handicap_accessible TINYINT(1) NOT NULL,
        ADD
          is_open_air TINYINT(1) NOT NULL,
        ADD
          languages LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE event DROP is_free, DROP is_handicap_accessible, DROP is_open_air, DROP languages');
    }
}
