<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210412091451 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation DROP FOREIGN KEY FK_2D385412AB8E2');
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          CONSTRAINT FK_2D385412AB8E2 FOREIGN KEY (senior_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE accommodation_demand DROP FOREIGN KEY FK_1FE9DD52E9DFEF18');
        $this->addSql('ALTER TABLE
          accommodation_demand
        ADD
          CONSTRAINT FK_1FE9DD52E9DFEF18 FOREIGN KEY (young_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE profile DROP FOREIGN KEY FK_8157AA0F64D218E');
        $this->addSql('ALTER TABLE profile DROP FOREIGN KEY FK_8157AA0FEFB9C8A5');
        $this->addSql('DROP INDEX IDX_8157AA0FEFB9C8A5 ON profile');
        $this->addSql('DROP INDEX UNIQ_8157AA0F64D218E ON profile');
        $this->addSql('ALTER TABLE profile DROP location_id, DROP association_id');
        $this->addSql('ALTER TABLE user ADD location_id INT DEFAULT NULL, ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          user
        ADD
          CONSTRAINT FK_8D93D64964D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE
          user
        ADD
          CONSTRAINT FK_8D93D649EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64964D218E ON user (location_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649EFB9C8A5 ON user (association_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE accommodation DROP FOREIGN KEY FK_2D385412AB8E2');
        $this->addSql('ALTER TABLE
          accommodation
        ADD
          CONSTRAINT FK_2D385412AB8E2 FOREIGN KEY (senior_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE accommodation_demand DROP FOREIGN KEY FK_1FE9DD52E9DFEF18');
        $this->addSql('ALTER TABLE
          accommodation_demand
        ADD
          CONSTRAINT FK_1FE9DD52E9DFEF18 FOREIGN KEY (young_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE profile ADD location_id INT DEFAULT NULL, ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          profile
        ADD
          CONSTRAINT FK_8157AA0F64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE
          profile
        ADD
          CONSTRAINT FK_8157AA0FEFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_8157AA0FEFB9C8A5 ON profile (association_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8157AA0F64D218E ON profile (location_id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64964D218E');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649EFB9C8A5');
        $this->addSql('DROP INDEX UNIQ_8D93D64964D218E ON user');
        $this->addSql('DROP INDEX IDX_8D93D649EFB9C8A5 ON user');
        $this->addSql('ALTER TABLE user DROP location_id, DROP association_id');
    }
}
