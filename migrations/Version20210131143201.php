<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210131143201 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
            accommodation_demand
        ADD description LONGTEXT NOT NULL,
        CHANGE status status ENUM(\'pending\', \'accepted\', \'refused\', \'abandoned\', \'awaiting_users_approval\', \'completed\', \'closed\') NOT NULL COMMENT \'(DC2Type:AccommodationDemandStatusType)\'
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
            accommodation_demand
        DROP description,
        CHANGE status status ENUM(\'pending\', \'accepted\', \'refused\', \'abandoned\') NOT NULL COMMENT \'(DC2Type:AccommodationDemandStatusType)\'
        ');
    }
}
