<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210409103223 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association
        ADD
          agreement_version ENUM(
            \'default_version\', \'cohabilis_version\',
            \'1toit2age_version\'
          ) NOT NULL COMMENT \'(DC2Type:AssociationAgreementVersionType)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association DROP agreement_version');
    }
}
