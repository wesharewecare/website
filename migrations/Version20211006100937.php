<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211006100937 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE document (
          id INT AUTO_INCREMENT NOT NULL,
          association_id INT DEFAULT NULL,
          name VARCHAR(255) NOT NULL,
          locale VARCHAR(255) NOT NULL,
          document_name VARCHAR(255) DEFAULT NULL,
          thumbnail_name VARCHAR(255) DEFAULT NULL,
          recipients LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\',
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          INDEX IDX_D8698A76EFB9C8A5 (association_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          document
        ADD
          CONSTRAINT FK_D8698A76EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE document');
    }
}
