const Encore = require('@symfony/webpack-encore');
const DotenvFlow = require('dotenv-flow-webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .autoProvidejQuery()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableIntegrityHashes(Encore.isProduction())
    .enableSingleRuntimeChunk()
    .enableStimulusBridge('./assets/controllers.json')
    .splitEntryChunks()
    .enableSassLoader()
    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })
    .autoProvideVariables({
        'bazinga-translator': 'Translator'
    })
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })
    .addPlugin(new CopyWebpackPlugin({
        patterns: [
            { from: './assets/images', to: 'images' }
        ]
    }))
    .addPlugin(new DotenvFlow({ system_vars: true, silent: true }))
    .enableTypeScriptLoader()
    .enableReactPreset()
    .addEntry('main', './assets/main.js')
    .addEntry('index', './assets/js/index.js')
    .addEntry('map', './assets/js/map.js')
    .addEntry('context', './assets/js/context.js')
    .addEntry('solutions', './assets/js/solutions.js')
    .addEntry('information', './assets/js/information.js')
    .addEntry('participate', './assets/js/participate.js')
    .addEntry('contact', './assets/js/contact.js')
    .addEntry('events', './assets/js/events.js')
    .addEntry('event', './assets/js/event.js')
    .addEntry('login', './assets/js/login.js')
    .addEntry('register', './assets/js/register.js')
    .addEntry('profile', './assets/js/profile.js')
    .addEntry('dashboard', './assets/js/dashboard.js')
    .addEntry('panels', './assets/js/panels.js')
    .addEntry('messages', './assets/js/messages.js')
    .addEntry('users_list', './assets/js/users_list')
    .addEntry('accommodation', './assets/js/accommodation')
    .addEntry('associations_list', './assets/js/associations_list')
    .addEntry('accommodations_list', './assets/js/accommodations_list')
    .addEntry('association', './assets/js/association')
    .addEntry('cohabitation', './assets/js/cohabitation')
    .addEntry('agreement', './assets/js/agreement')
    .addEntry('cohabitations_list', './assets/js/cohabitations_list')
    .addEntry('hobbies_list', './assets/js/hobbies_list')
    .addEntry('hobby', './assets/js/hobby')
    .addEntry('events_list', './assets/js/events_list')
    .addEntry('toolbox', './assets/js/toolbox');
module.exports = Encore.getWebpackConfig();
