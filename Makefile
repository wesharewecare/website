DOCKER_COMPOSE?=docker-compose
RUN=$(DOCKER_COMPOSE) run --rm php
DOCKER_EXEC_PARAMS?=
EXEC?=$(DOCKER_COMPOSE) exec $(DOCKER_EXEC_PARAMS) -u dev php
EXEC_AS_ROOT?=$(DOCKER_COMPOSE) exec $(DOCKER_EXEC_PARAMS) php
COMPOSER=$(EXEC) composer
CONSOLE=bin/console
PHPCSFIXER?=$(EXEC) php -d memory_limit=1024M vendor/bin/php-cs-fixer
BEHAT=$(EXEC) php -d memory_limit=1024M vendor/bin/behat
BEHAT_ARGS?=-vvv --stop-on-failure
PHPUNIT=$(EXEC) vendor/bin/simple-phpunit
PHPUNIT_ARGS=-vvv

.DEFAULT_GOAL := help
.PHONY: help start stop reset db db-diff db-diff-dump db-migrate db-rollback db-load webpack-watch clear clean routing-generate
.PHONY: lint lintfix ly lt lj psalm build up perm deps cc tty test-behat phpcs phpcsfix node_modules
.PHONY: translation-up translation-down
.PHONY: wait-for-db security-check rm-docker-dev.lock

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##
## Project setup
##---------------------------------------------------------------------------

start: build up property perm .env.local db web/built rm-docker-dev.lock                               ## build docker containers, vendors, node and BDD

stop:                                                                                                  ## Remove docker containers
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) rm -v --force

reset: stop start                                                                                      ## Stop and Start

restart: stop build up property perm rm-docker-dev.lock                                                ## Start without building node and BDD

clear: perm rm-docker-dev.lock                                                                         ## Remove all the cache, the logs, the sessions and the built assets
	-$(EXEC) rm -rf var/cache/*
	-$(EXEC) rm -rf var/sessions/*
	rm -rf var/logs/*
	rm -rf public/built

clean: clear                                                                                           ## Clear and remove dependencies
	rm -rf vendor node_modules

cc:                                                                                                    ## Clear the cache in dev env
	$(EXEC) $(CONSOLE) cache:clear --no-warmup --no-debug
	$(EXEC) $(CONSOLE) cache:warmup --no-debug

tty:                                                                                                   ## Run app container in interactive mode
	$(RUN) /bin/bash

##
## Database
##---------------------------------------------------------------------------

wait-for-db:
	$(EXEC) php -r "set_time_limit(60);for(;;){if(@fsockopen('wswc_mysql',3306)){break;}echo \"Waiting for MySQL\n\";sleep(1);}"

db: vendor wait-for-db                                                                                 ## Reset the database and load fixtures
	$(EXEC) $(CONSOLE) doctrine:database:drop --force --if-exists
	$(EXEC) $(CONSOLE) doctrine:database:create --if-not-exists
	$(EXEC) $(CONSOLE) --no-interaction doctrine:migrations:migrate
	$(EXEC) $(CONSOLE) doctrine:fixtures:load --append

db-diff: vendor wait-for-db                                                                            ## Generate a migration by comparing your current database to your mapping information
	$(EXEC) $(CONSOLE) doctrine:migration:diff --formatted

db-diff-dump: vendor wait-for-db                                                                       ## Generate a migration by comparing your current database to your mapping information and display it in console
	$(EXEC) $(CONSOLE) doctrine:schema:update --dump-sql

db-migrate: vendor wait-for-db                                                                         ## Migrate database schema to the latest available version
	$(EXEC) $(CONSOLE) doctrine:migration:migrate -n

db-rollback: vendor wait-for-db                                                                        ## Rollback the latest executed migration
	$(EXEC) $(CONSOLE) doctrine:migration:migrate prev -n

db-load: vendor wait-for-db                                                                            ## Reset the database fixtures
	$(EXEC) $(CONSOLE) doctrine:fixtures:load -n

db-validate: vendor wait-for-db                                                                        ## Check the ORM mapping
	$(EXEC) $(CONSOLE) doctrine:schema:validate

##
## Assets
##---------------------------------------------------------------------------

webpack-watch:                                                                                           ## Watch changes in js and css files
	$(EXEC) yarn run watch

webpack-dev: node_modules
	$(EXEC) yarn run dev

webpack-prod: node_modules
	$(EXEC) yarn run prod

routing-generate:                                                                                       ## Generate js routing
	$(EXEC) $(CONSOLE) fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json

bazinga-translation:
	$(EXEC) $(CONSOLE) bazinga:js-translation:dump public/js/
##
## Tests
##---------------------------------------------------------------------------

security-check: vendor                                                                                 ## Check for vulnerable dependencies
	$(EXEC) symfony check:security

lint: ly lt lj phpcs psalm                                                                               ## Run lint on Twig, YAML, PHP and Javascript files

ly:
	$(EXEC) $(CONSOLE) lint:yaml config

lt:
	$(EXEC) $(CONSOLE) lint:twig templates

phpcs: vendor                                                                                          ## Lint PHP code
	$(PHPCSFIXER) fix --diff --dry-run --no-interaction

psalm:                                                                                                 ## Psalm
	$(EXEC) vendor/bin/psalm --show-info=false --no-cache

lintfix: phpcsfix ljfix

phpcsfix: vendor                                                                                       ## Lint and fix PHP code to follow the convention
	$(PHPCSFIXER) fix

lj: node_modules                                                                                       ## Lint the Javascript to follow the convention
	$(EXEC) yarn run lint

ljfix: node_modules                                                                                    ## Lint and try to fix the Javascript to follow the convention
	$(EXEC) yarn run lintfix

test-phpunit:                                                                                          ## Run phpunit tests
	$(PHPUNIT) $(PHPUNIT_ARGS)

test-phpunit-wip:                                                                                      ## Run wip group of phpunit tests
	$(PHPUNIT) $(PHPUNIT_ARGS) --group wip

test-behat:                                                                                            ## Run behat tests
	$(BEHAT) $(BEHAT_ARGS)

test-behat-wip:                                                                                        ## Run wip behat tests
	$(BEHAT) $(BEHAT_ARGS) --tags=wip

##
## Dependencies
##---------------------------------------------------------------------------

deps: vendor web/built                                                                                 ## Install the project PHP and JS dependencies

# Internal rules

build: docker-dev.lock

docker-dev.lock:
	$(DOCKER_COMPOSE) pull --ignore-pull-failures
	$(DOCKER_COMPOSE) build --force-rm --pull
	touch docker-dev.lock

rm-docker-dev.lock:
	rm -f docker-dev.lock

up:
	$(DOCKER_COMPOSE) up -d --remove-orphans

property:
	$(EXEC_AS_ROOT) chown dev:dev .
	$(EXEC_AS_ROOT) chown -R dev:dev public
	$(EXEC_AS_ROOT) chmod -R 777 .docker

perm:
	$(EXEC_AS_ROOT) chmod -R 777 var
	$(EXEC_AS_ROOT) chown -R www-data:root var

# Rules from files

vendor: composer.lock
	$(COMPOSER) install -n --no-scripts

composer.lock: composer.json
	@echo compose.lock is not up to date.

.env.local: vendor cc
	$(EXEC) composer -n run-script post-install-cmd

node_modules:
	$(EXEC) yarn install

web/built: node_modules routing-generate bazinga-translation
	$(EXEC) yarn run dev

##
## Translation
##---------------------------------------------------------------------------

translation-up:                                                                                        ## upload translations to Loco
	$(EXEC) $(CONSOLE) translation:sync app up

translation-down:                                                                                      ## download translations from Loco
	$(EXEC) $(CONSOLE) translation:sync app down


##
## Deploy
##---------------------------------------------------------------------------

deploy-prod:
	$(EXEC) ./vendor/bin/dep deploy prod -n

deploy-staging:
	$(EXEC) ./vendor/bin/dep deploy staging -n

deploy-demo:
	$(EXEC) ./vendor/bin/dep deploy demo -n
